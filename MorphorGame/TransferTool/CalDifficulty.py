import sys
from openpyxl.reader.excel import load_workbook
from numpy import *
import itertools
import re
from itertools import combinations, permutations
from itertools import repeat

def getPinyinArray(str):
	return re.findall('\((.*?)\)', str)


def is_chinese(uchar):
	if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
		return True
	else:
		return False

def format_str(content):
	#content = unicode(content,'utf-8')
	content_str = ''
	for i in content:
		if is_chinese(i):
			content_str = content_str+i
	return content_str


# str = Homophone, Construction
def GetFrequency(str):
	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb[str]
	FreqDic = {}
	nrows = ws.max_row
	for row in range(1, nrows+1):
		if(ws.cell(row=row,column=1).value!=None and ws.cell(row=row,column=1).value!='/'):
			FreqDic[format_str(ws.cell(row=row,column=1).value)] = ws.cell(row=row,column=3).value
	return FreqDic

def GetCorrectRate():
	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordsEvaluation_all.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['all']
	CorrectDic = {}
	nrows = ws.max_row
	for row in range(2308, 2384):
		if(ws.cell(row=row,column=1).value!=None ):
			CorrectDic[format_str(ws.cell(row=row,column=2).value)] = ws.cell(row=row,column=46).value
	return CorrectDic


def GetSimilarity():
	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordsEvaluation_all.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['all']
	SimDict = {}

	for row in range(3, 2307):
		if(ws.cell(row=row,column=1).value!=None):
			SimDict[format_str(ws.cell(row=row,column=2).value)+format_str(ws.cell(row=row,column=3).value)] = ws.cell(row=row,column=46).value
	return SimDict


def HomophoneDiffer():
	FreqDic = GetFrequency('Homophone')
	SimDict = GetSimilarity()

	print ("FreqDic:"+str(len(FreqDic)));
	print ("SimDict:"+str(len(SimDict)));

	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['HomophoneDiffer']

	nrows = ws.max_row

	for row in range(2, nrows+1):
		answer = format_str(ws.cell(row=row,column=3).value)
		choice1 = format_str(ws.cell(row=row,column=4).value)
		choice2 = format_str(ws.cell(row=row,column=5).value)
		choice3 = ws.cell(row=row,column=6).value
		choice4 = ws.cell(row=row,column=7).value	
		#get list of between choice
		posList = []
		posList.append(answer+choice1)
		posList.append(answer+choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=6).value)
			posList.append(answer+choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=7).value)
			posList.append(answer+choice4)

		#get list of answer with choice
		negaList = []
		negaList.append(choice1+choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=6).value)
			negaList.append(choice1+choice3)
			negaList.append(choice2+choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=7).value)
			negaList.append(choice1+choice4)
			negaList.append(choice2+choice4)
			negaList.append(choice3+choice4)

		#get list of frequency
		frelist = []
		frelist.append(answer)
		frelist.append(choice1)
		frelist.append(choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=6).value)
			frelist.append(choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=7).value)
			frelist.append(choice4)


		posValAll =0
		for item in posList:
			posValAll = posValAll+ float(SimDict.setdefault(item, 0))
		posVal = posValAll/len(posList)

		negaValAll = 0
		for item in negaList:
			negaValAll  = negaValAll + float(SimDict.setdefault(item, 0))
		negaVal = negaValAll/len(negaList)

		freValAll = 0
		for item in frelist:
			freValAll  = freValAll + float(FreqDic.setdefault(item, 0))
		freVal = freValAll/len(frelist)

		diffLevel = 10.0-negaVal+posVal-freVal*0.02
		ws.cell(row= row,column = 2).value = diffLevel
		print("row:"+str(row));

	wb.save(dest_filename)	

def HomophoneSame():
	FreqDic = GetFrequency('Homophone')
	SimDict = GetSimilarity()

	print ("FreqDic:"+str(len(FreqDic)));
	print ("SimDict:"+str(len(SimDict)));

	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['HomophoneSame']

	nrows = ws.max_row

	for row in range(2, nrows+1):
		reference = format_str(ws.cell(row=row,column=3).value)
		answer = format_str(ws.cell(row=row,column=4).value)
		choice1 = format_str(ws.cell(row=row,column=5).value)
		choice2 = ws.cell(row=row,column=6).value
		choice3 = ws.cell(row=row,column=7).value
		#get list of between choice

		#get list of answer with choice
		negaList = []
		negaList.append(reference+choice1)
		negaList.append(answer+choice1)
		if choice2!=None:
			choice2 =format_str(ws.cell(row=row,column=6).value)
			negaList.append(reference+choice2)
			negaList.append(answer+choice2)
			negaList.append(choice1+choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=7).value)
			negaList.append(reference+choice3)
			negaList.append(answer+choice3)
			negaList.append(choice1+choice3)
			negaList.append(choice2+choice3)

		#get list of frequency
		frelist = []
		frelist.append(reference)
		frelist.append(answer)
		frelist.append(choice1)
		if choice2!=None:
			choice2 =format_str(ws.cell(row=row,column=6).value)
			frelist.append(choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=7).value)
			frelist.append(choice3)

		posVal = 0
		posVal = posVal+ float(SimDict.setdefault(reference+answer, 0))

		negaValAll = 0
		for item in negaList:
			negaValAll  = negaValAll + float(SimDict.setdefault(item, 0))
		negaVal = negaValAll/len(negaList)

		freValAll = 0
		for item in frelist:
			freValAll  = freValAll + float(FreqDic.setdefault(item, 0))
		freVal = freValAll/len(frelist)

		diffLevel = 10.0-negaVal+posVal-freVal*0.02
		ws.cell(row= row,column = 2).value = diffLevel
		print("row:"+str(row));

	wb.save(dest_filename)	

def HomophoneTell():
	FreqDic = GetFrequency('Homophone')
	SimDict = GetSimilarity()

	print ("FreqDic:"+str(len(FreqDic)));
	print ("SimDict:"+str(len(SimDict)));

	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['HomophoneTell']

	nrows = ws.max_row

	for row in range(2, nrows+1):
		choice1 = format_str(ws.cell(row=row,column=4).value)
		choice2 = format_str(ws.cell(row=row,column=5).value)
		choice3 = ws.cell(row=row,column=6).value
		choice4 = ws.cell(row=row,column=7).value

		frelist = []
		frelist.append(choice1)
		frelist.append(choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=6).value)
			frelist.append(choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=7).value)
			frelist.append(choice4)

		freValAll = 0
		for item in frelist:
			freValAll  = freValAll + float(FreqDic.setdefault(item, 0))
		freVal = freValAll/len(frelist)

		ws.cell(row= row,column = 2).value = freVal*0.02

	wb.save(dest_filename)	


def ConstructionSame():
	FreqDic = GetFrequency('Construction')
	SimDict = GetSimilarity()
	CorDict = GetCorrectRate()

	print ("FreqDic:"+str(len(FreqDic)));
	print ("SimDict:"+str(len(SimDict)));

	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['ConstructionSame']

	nrows = ws.max_row

	for row in range(2, nrows+1):
		choice1 = format_str(ws.cell(row=row,column=3).value)
		choice2 = format_str(ws.cell(row=row,column=4).value)
		choice3 = format_str(ws.cell(row=row,column=5).value)
		choice4 = ws.cell(row=row,column=6).value
		choice5 = ws.cell(row=row,column=7).value

		frelist = []
		frelist.append(choice1)
		frelist.append(choice2)
		frelist.append(choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=6).value)
			frelist.append(choice4)
		if choice5!=None:
			choice5 =format_str(ws.cell(row=row,column=7).value)
			frelist.append(choice5)

		freValAll = 0
		for item in frelist:
			freValAll  = freValAll + float(FreqDic.setdefault(item, 0))
		freVal = freValAll/len(frelist)

		corValAll = 0
		for item in frelist:
			corValAll  = corValAll + float(CorDict.setdefault(item, 0))
		corVal = corValAll/len(frelist)

		ws.cell(row= row,column = 2).value = freVal*0.02+corVal*10

	wb.save(dest_filename)	

def ConstructionTell():
	FreqDic = GetFrequency('Construction')
	SimDict = GetSimilarity()
	CorDict = GetCorrectRate()

	print ("FreqDic:"+str(len(FreqDic)));
	print ("SimDict:"+str(len(SimDict)));

	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['ConstructionTell']

	nrows = ws.max_row

	for row in range(2, nrows+1):
		choice1 = format_str(ws.cell(row=row,column=4).value)
		choice2 = format_str(ws.cell(row=row,column=5).value)
		choice3 = ws.cell(row=row,column=6).value
		choice4 = ws.cell(row=row,column=7).value

		frelist = []
		frelist.append(choice1)
		frelist.append(choice2)
		if choice3!=None:
			choice3 =format_str(ws.cell(row=row,column=6).value)
			frelist.append(choice3)
		if choice4!=None:
			choice4 =format_str(ws.cell(row=row,column=7).value)
			frelist.append(choice4)

		freValAll = 0
		for item in frelist:
			freValAll  = freValAll + float(FreqDic.setdefault(item, 0))
		freVal = freValAll/len(frelist)

		corValAll = 0
		for item in frelist:
			corValAll  = corValAll + float(CorDict.setdefault(item, 0))
		corVal = corValAll/len(frelist)

		ws.cell(row= row,column = 2).value = freVal*0.02+corVal*10

	wb.save(dest_filename)	

if __name__ == '__main__':
	
	HomophoneDiffer();