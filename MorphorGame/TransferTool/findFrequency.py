# -*- coding: UTF-8 -*-
#import urllib2
import sys
from openpyxl.reader.excel import load_workbook
from numpy import *
import itertools
import re
from itertools import combinations, permutations
from itertools import repeat


def is_chinese(uchar):
	if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
		return True
	else:
		return False

def format_str(content):
	#content = unicode(content,'utf-8')
	content_str = ''
	for i in content:
		if is_chinese(i):
			content_str = content_str+i
	return content_str

def findFrequency(str1):
	raw_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	ref_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\freq.xlsx'

	wb_raw = load_workbook(raw_filename,data_only=True)
	wb_ref = load_workbook(ref_filename, data_only=True)
	ws_raw = wb_raw[str1]
	ws_ref = wb_ref['BigramCombine']
	ws_raw_Ind = 1

	refDict = {}

	nrows_raw = ws_raw.max_row
	nrows_ref = ws_ref.max_row
	

	for row in range(2, nrows_ref+1):
		#print (ws_ref.cell(row=row,column=2).value.encode('utf-8'))
		refDict[ws_ref.cell(row=row,column=2).value] = ws_ref.cell(row=row,column=10).value

	#print 'length:'+str(len(refDict))

	for row in range(1, nrows_raw+1):
		val = ws_raw.cell(row=row,column=1).value
		
		if(val!='/' and val!=None):
			#print (val.encode('utf-8'))
			valChinese = format_str(val)
			if valChinese in refDict:
				ws_raw.cell(row= ws_raw_Ind,column = 2).value = refDict[valChinese]
				print(ws_raw_Ind)
			else:
				ws_raw.cell(row= ws_raw_Ind,column = 2).value = '/'
			

		ws_raw_Ind=ws_raw_Ind+1

	wb_raw.save(raw_filename)	

if __name__ == '__main__':
	
	findFrequency('Homophone');

	


