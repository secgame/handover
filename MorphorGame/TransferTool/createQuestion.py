# -*- coding: UTF-8 -*-
import sys
from openpyxl.reader.excel import load_workbook
from numpy import *
import itertools
import re
from itertools import combinations, permutations
from itertools import repeat
import random
from random import choice

def getPinyinArray(str):
	return re.findall('\((.*?)\)', str)


def is_chinese(uchar):
	if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
		return True
	else:
		return False

def format_str(content):
	#content = unicode(content,'utf-8')
	content_str = ''
	for i in content:
		if is_chinese(i):
			content_str = content_str+i
	return content_str

def create_HomophoneQue():
	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['Homophone']
	ws1 = wb['HomophoneDiffer']
	ws1Ind = 1
	ws2 = wb['HomophoneSame']
	ws2Ind = 1
	ws3 = wb['HomophoneTell']
	ws3Ind = 1
	rawDict = {}
	rawList = []

	nrows = ws.max_row

	#獲得所有的鍵值對
	for row in range(1, nrows+1):
		rawList.append(ws.cell(row=row,column=1).value)
		if(ws.cell(row=row,column=2).value!=None):
			rawDict[format_str(ws.cell(row=row,column=1).value)] = ws.cell(row=row,column=2).value

	tmpArray = []
	tmpArray1 = []
	arrayNum = 1;
	tmpStr = ''
	prepare_list = locals()
	for ind in range(0,len(rawList)):
		if rawList[ind] != '/':
			tmpArray.append(rawList[ind])
			if rawList[ind] == None:
				arrayNum = arrayNum+1
		else:
			print ("new group")
			tmpArray1 = list(tmpArray)
			for i in range(0,arrayNum):
				prepare_list['list_'+str(i)]=[]

				while len(tmpArray)>0 and tmpArray[0] != None:					
					list_pop = tmpArray.pop(0)
					prepare_list['list_'+str(i)].append(list_pop)

				if  len(tmpArray)>0 and  tmpArray[0] == None:
					list_pop = tmpArray.pop(0)
			
			for i in range (2,5):
				for j in range(0, arrayNum):
					#difference 3,4,5
					if len(prepare_list['list_'+str(j)]) >= i:
						tmpCombinationList = list(itertools.combinations(prepare_list['list_'+str(j)],i))						
						tmpSingleList = list((set(tmpArray1) - set(prepare_list['list_'+str(j)]))-set([None]))
						random.seed(len(tmpSingleList))
						if len(tmpSingleList)>1:
							tmpSingleList = random.sample(tmpSingleList,2);
						tmpWholeCombination = list(itertools.product(tmpCombinationList,tmpSingleList))
						for k in range (0,len(tmpWholeCombination)):
							ws1.cell(row=ws1Ind, column= 1).value = tmpWholeCombination[k][1]
							for m in range(0,len(list(tmpWholeCombination[k])[0])):
								ws1.cell(row= ws1Ind,column = m+2).value = list(tmpWholeCombination[k])[0][m]
							#tmpvalue = [','.join('%s' % id for id in list(tmpWholeCombination[k])[0]),tmpWholeCombination[k][1]]
							#ws1.cell(row=ws1Ind, column=1).value =  '|'.join('%s' % id for id in tmpvalue)
							ws1Ind = ws1Ind+1
					else:
						continue

					print ("finishe difference -> range:"+str(i)+" group:"+str(j))

					#same 2,3,4
					if len(prepare_list['list_'+str(j)]) >= 2:
						tmpCombinationList =  list(itertools.combinations(prepare_list['list_'+str(j)],2))
						tmpSingleList = list((set(tmpArray1) - set(prepare_list['list_'+str(j)]))-set([None]))					
						tmpCombinationList2 =list(itertools.combinations(tmpSingleList,i-1))
						tmpWholeCombination = list(itertools.product(tmpCombinationList,tmpCombinationList2))
						random.seed(len(tmpWholeCombination))
						tmpWholeCombinationlimited = random.sample(tmpWholeCombination,len(tmpWholeCombination)/5);
						for k in range (0,len(tmpWholeCombinationlimited)):
							tmpvalue2 =list(list(tmpWholeCombinationlimited[k])[0]) #question
							ws2.cell(row=ws2Ind, column=1).value = tmpvalue2[0]	#question
							ws2.cell(row=ws2Ind, column=2).value = tmpvalue2[1]	#question
							tmpvalue1 =list(list(tmpWholeCombinationlimited[k])[1]) #answer						
							for m in range(3, len(tmpvalue1)+3):
								ws2.cell(row = ws2Ind,column = m).value = tmpvalue1[m-3]
							ws2Ind = ws2Ind+1
					else:
						continue 

					print ("finishe same -> range:"+str(i)+" group:"+str(j))

					#tell 2,3,4
					if len(prepare_list['list_'+str(j)]) >= i:
						#answer is correct
						tmpCombinationList = list(itertools.combinations(prepare_list['list_'+str(j)],i))					
						for k in tmpCombinationList:
							ws3.cell(row=ws3Ind, column=1).value =  '1'
							for m in range(2,len(list(k))+2):
								ws3.cell(row=ws3Ind,column=m).value = k[m-2]
							ws3Ind = ws3Ind+1
						#answer is not correct
						
						#tmpSingleList = list((set(tmpArray1) - set(prepare_list['list_'+str(j)]))-set([None]))
						for l in range(1,i):
							tmpCombinationList = list(itertools.combinations(prepare_list['list_'+str(j)],i-l))
							tmpSingleList = list((set(tmpArray1) - set(prepare_list['list_'+str(j)]))-set([None]))
							tmpCombinationList2 =list(itertools.combinations(tmpSingleList,l))
							tmpWholeCombination = list(itertools.product(tmpCombinationList,tmpCombinationList2))
							random.seed(len(tmpWholeCombination))
							tmpWholeCombinationlimited = random.sample(tmpWholeCombination,len(tmpWholeCombination)/5);
							for n in range(0,len(tmpWholeCombinationlimited)):
								ws3.cell(row=ws3Ind, column=1).value =  '0'
								tmpvalue =list(list(tmpWholeCombinationlimited[n])[0])+list(list(tmpWholeCombinationlimited[n])[1])
								for m in range(2,len(tmpvalue)+2):
									ws3.cell(row=ws3Ind,column=m).value = tmpvalue[m-2]
								ws3Ind = ws3Ind+1
					else:
						continue
					print ("finishe tell -> range:"+str(i)+" group:"+str(j))
			print ("fnished");
				
			arrayNum = 1
			tmpArray = []
			tmpArray1 =[]
	wb.save(dest_filename)	

def create_ConstructionQue():
	dest_filename = r'D:\RA\EDUHK\MorphoAdventure\tool\WordForGameV11.xlsx'
	wb = load_workbook(dest_filename)
	ws = wb['Construction']
	ws1 = wb['ConstructionDiffer']
	ws1Ind = 1
	ws2 = wb['ConstructionSame']
	ws2Ind = 1
	ws3 = wb['ConstructionTell']
	ws3Ind = 1
	rawList = []

	nrows = ws.max_row
	print ("max_row:"+str(nrows))
	arrayNum = 0

	#獲得所有的鍵值對
	for row in range(1, nrows+1):
		rawList.append(ws.cell(row=row,column=1).value)
		if ws.cell(row=row,column=1).value ==None:
			arrayNum = arrayNum+1
		

	#分組
	tmpList = []
	tmpStr = ''
	prepare_list = locals()
	tmpList = list(rawList)
	for i in range(0,arrayNum):
		prepare_list['list_'+str(i)]=[]

		while len(rawList)>0 and rawList[0] != None:					
			list_pop = rawList.pop(0)
			prepare_list['list_'+str(i)].append(list_pop)

		if  len(rawList)>0 and rawList[0] == None:
			list_pop = rawList.pop(0)

		print ("begin:"+str(i))
		#find difference
		
		for j in range(2,5):
			print ("begin:difference-"+str(j))
			if len(prepare_list['list_'+str(i)]) > j:
				#print(str(len(prepare_list['list_'+str(i)])))
				tmpCombinationList =list(itertools.combinations(prepare_list['list_'+str(i)],j))
				#print ('tmpCombinationList[list_'+str(i)+'].len:'+str(len(tmpCombinationList)))	
				random.seed(len(tmpCombinationList))
				tmpCombinationList2 = random.sample(tmpCombinationList,100);
				tmpSingleList = list((set(tmpList) - set(prepare_list['list_'+str(i)]))-set([None]))
				random.seed(len(tmpSingleList))
				tmpSingleList2 = random.sample(tmpSingleList,3);
				#print ('tmpSingleList[list_'+str(i)+'].len:'+str(len(tmpSingleList)))	
				tmpWholeCombination = list(itertools.product(tmpCombinationList2,tmpSingleList2))

				#print ('tmpWholeCombination[list_'+str(i)+'].len:'+str(len(tmpWholeCombination)))
				for k in range (0,len(tmpWholeCombination)):
					ws1.cell(row=ws1Ind, column= 1).value = tmpWholeCombination[k][1]
					for m in range(0,len(list(tmpWholeCombination[k])[0])):
						ws1.cell(row= ws1Ind,column = m+2).value = list(tmpWholeCombination[k])[0][m]
					ws1Ind = ws1Ind+1		
						
			else:
				continue
			
			print ("begin:same-"+str(j))
			#find same
			if len(prepare_list['list_'+str(i)]) >= j:
				tmpCombinationList =  list(itertools.combinations(prepare_list['list_'+str(i)],2))
				tmpSingleList = list((set(tmpList) - set(prepare_list['list_'+str(i)]))-set([None]))
				tmpCombinationList2 =list(itertools.combinations(tmpSingleList,j-1))
				tmpWholeCombination = list(itertools.product(tmpCombinationList,tmpCombinationList2))
				random.seed(len(tmpWholeCombination))
				tmpWholeCombinationlimited = random.sample(tmpWholeCombination,300);#100組兩兩組合
				for k in range (0,len(tmpWholeCombinationlimited)):
					tmpvalue2 =list(list(tmpWholeCombinationlimited[k])[0]) #question
					ws2.cell(row=ws2Ind, column=1).value = tmpvalue2[0]	#question
					ws2.cell(row=ws2Ind, column=2).value = tmpvalue2[1]	#question
					tmpvalue1 =list(list(tmpWholeCombinationlimited[k])[1]) #answer						
					for m in range(3, len(tmpvalue1)+3):
						ws2.cell(row = ws2Ind,column = m).value = tmpvalue1[m-3]
					ws2Ind = ws2Ind+1
			else:
				continue 
		

			#tell
			print ("begin:tell-"+str(j))
			if len(prepare_list['list_'+str(i)]) >= j:
				#answer is correct
				tmpCombinationList = list(itertools.combinations(prepare_list['list_'+str(i)],j))
				random.seed(len(tmpCombinationList))
				tmpWholeCombinationlimited = random.sample(tmpCombinationList,len(tmpCombinationList)/30);
				print ("same,len:"+str(len(tmpWholeCombinationlimited)))	
				for k in tmpWholeCombinationlimited:
					ws3.cell(row=ws3Ind, column=1).value =  '1'
					for m in range(2,len(list(k))+2):
						ws3.cell(row=ws3Ind,column=m).value = k[m-2]
					ws3Ind = ws3Ind+1
				
				#answer is not correct		
				#tmpSingleList = list((set(tmpList) - set(prepare_list['list_'+str(j)]))-set([None]))
				for l in range(1,j):
					tmpCombinationList = list(itertools.combinations(prepare_list['list_'+str(i)],j-1))
					tmpSingleList = list((set(tmpList) - set(prepare_list['list_'+str(i)]))-set([None]))				
					tmpCombinationList2 =list(itertools.combinations(tmpSingleList,l))
					tmpWholeCombination = list(itertools.product(tmpCombinationList,tmpCombinationList2))
					random.seed(len(tmpWholeCombination))
					tmpWholeCombinationlimited = random.sample(tmpWholeCombination, 100)
					print ("difference,len:"+str(len(tmpWholeCombinationlimited)))
					for n in range(0,len(tmpWholeCombinationlimited)):
						ws3.cell(row=ws3Ind, column=1).value =  '0'
						tmpvalue =list(list(tmpWholeCombinationlimited[n])[0])+list(list(tmpWholeCombinationlimited[n])[1])
						for m in range(2,len(tmpvalue)+2):
							ws3.cell(row=ws3Ind,column=m).value = tmpvalue[m-2]
						ws3Ind = ws3Ind+1
			else:
				continue		
		print ("end")
	wb.save(dest_filename)	

if __name__ == '__main__':
	
	create_ConstructionQue();

	


