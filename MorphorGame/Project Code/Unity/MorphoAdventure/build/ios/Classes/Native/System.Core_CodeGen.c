﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000006 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000008 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000009 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000000F TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000011 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000013 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000015 System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88 ();
// 0x00000016 System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262 ();
// 0x00000017 System.Int32 System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x00000018 System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF ();
// 0x00000019 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001A TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001C System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000001E System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000001F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000020 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000022 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000023 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000024 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000026 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000027 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002C System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002F System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000030 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000031 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000036 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000037 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003A System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003B System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003C System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003F System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000040 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000041 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000044 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000045 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000046 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000047 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000048 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x00000049 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x0000004A System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x0000004B System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x0000004C TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000004D System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x0000004E System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x0000004F System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000050 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000051 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x00000052 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x00000053 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x00000054 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x00000055 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x00000056 TKey System.Linq.IGrouping`2::get_Key()
// 0x00000057 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000058 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000059 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x0000005A System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005B System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x0000005C System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x0000005D System.Void System.Linq.Lookup`2::Resize()
// 0x0000005E System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x0000005F System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x00000060 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000061 TKey System.Linq.Lookup`2_Grouping::get_Key()
// 0x00000062 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x00000063 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x00000064 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000065 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000066 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x00000067 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x00000068 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000069 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x0000006A System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x0000006B System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x0000006C TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x0000006D System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x0000006E System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x0000006F System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x00000070 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x00000071 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x00000072 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000073 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x00000074 System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x00000075 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x00000076 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x00000077 System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x00000078 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x00000079 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x0000007A System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x0000007B System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000007C System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x0000007D System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007E System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000007F TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000082 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000083 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000084 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000085 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000086 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000087 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000088 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000008A System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000008B System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000008C System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000091 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000093 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000094 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000096 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000097 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000098 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000099 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000009A T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000009B System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000009C System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[156] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Max_m841FC5439B3D8021B3E0D782522FDD40CFA29D88,
	Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262,
	NULL,
	Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[156] = 
{
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	95,
	1085,
	-1,
	1085,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[43] = 
{
	{ 0x02000004, { 57, 4 } },
	{ 0x02000005, { 61, 9 } },
	{ 0x02000006, { 72, 7 } },
	{ 0x02000007, { 81, 10 } },
	{ 0x02000008, { 93, 11 } },
	{ 0x02000009, { 107, 9 } },
	{ 0x0200000A, { 119, 12 } },
	{ 0x0200000B, { 134, 1 } },
	{ 0x0200000C, { 135, 2 } },
	{ 0x0200000D, { 137, 6 } },
	{ 0x0200000E, { 143, 2 } },
	{ 0x0200000F, { 145, 4 } },
	{ 0x02000010, { 149, 3 } },
	{ 0x02000012, { 152, 17 } },
	{ 0x02000013, { 173, 5 } },
	{ 0x02000014, { 178, 1 } },
	{ 0x02000016, { 179, 4 } },
	{ 0x02000017, { 183, 4 } },
	{ 0x02000018, { 187, 21 } },
	{ 0x0200001A, { 208, 2 } },
	{ 0x06000006, { 0, 10 } },
	{ 0x06000007, { 10, 10 } },
	{ 0x06000008, { 20, 5 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 4 } },
	{ 0x0600000B, { 34, 3 } },
	{ 0x0600000C, { 37, 2 } },
	{ 0x0600000D, { 39, 2 } },
	{ 0x0600000E, { 41, 2 } },
	{ 0x0600000F, { 43, 3 } },
	{ 0x06000010, { 46, 1 } },
	{ 0x06000011, { 47, 1 } },
	{ 0x06000012, { 48, 3 } },
	{ 0x06000013, { 51, 2 } },
	{ 0x06000014, { 53, 3 } },
	{ 0x06000017, { 56, 1 } },
	{ 0x06000028, { 70, 2 } },
	{ 0x0600002D, { 79, 2 } },
	{ 0x06000032, { 91, 2 } },
	{ 0x06000038, { 104, 3 } },
	{ 0x0600003D, { 116, 3 } },
	{ 0x06000042, { 131, 3 } },
	{ 0x06000057, { 169, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[210] = 
{
	{ (Il2CppRGCTXDataType)2, 15928 },
	{ (Il2CppRGCTXDataType)3, 10597 },
	{ (Il2CppRGCTXDataType)2, 15929 },
	{ (Il2CppRGCTXDataType)2, 15930 },
	{ (Il2CppRGCTXDataType)3, 10598 },
	{ (Il2CppRGCTXDataType)2, 15931 },
	{ (Il2CppRGCTXDataType)2, 15932 },
	{ (Il2CppRGCTXDataType)3, 10599 },
	{ (Il2CppRGCTXDataType)2, 15933 },
	{ (Il2CppRGCTXDataType)3, 10600 },
	{ (Il2CppRGCTXDataType)2, 15934 },
	{ (Il2CppRGCTXDataType)3, 10601 },
	{ (Il2CppRGCTXDataType)2, 15935 },
	{ (Il2CppRGCTXDataType)2, 15936 },
	{ (Il2CppRGCTXDataType)3, 10602 },
	{ (Il2CppRGCTXDataType)2, 15937 },
	{ (Il2CppRGCTXDataType)2, 15938 },
	{ (Il2CppRGCTXDataType)3, 10603 },
	{ (Il2CppRGCTXDataType)2, 15939 },
	{ (Il2CppRGCTXDataType)3, 10604 },
	{ (Il2CppRGCTXDataType)2, 15940 },
	{ (Il2CppRGCTXDataType)3, 10605 },
	{ (Il2CppRGCTXDataType)3, 10606 },
	{ (Il2CppRGCTXDataType)2, 12442 },
	{ (Il2CppRGCTXDataType)3, 10607 },
	{ (Il2CppRGCTXDataType)2, 15941 },
	{ (Il2CppRGCTXDataType)3, 10608 },
	{ (Il2CppRGCTXDataType)3, 10609 },
	{ (Il2CppRGCTXDataType)2, 12449 },
	{ (Il2CppRGCTXDataType)3, 10610 },
	{ (Il2CppRGCTXDataType)3, 10611 },
	{ (Il2CppRGCTXDataType)2, 15942 },
	{ (Il2CppRGCTXDataType)2, 15943 },
	{ (Il2CppRGCTXDataType)3, 10612 },
	{ (Il2CppRGCTXDataType)2, 15944 },
	{ (Il2CppRGCTXDataType)3, 10613 },
	{ (Il2CppRGCTXDataType)3, 10614 },
	{ (Il2CppRGCTXDataType)2, 12461 },
	{ (Il2CppRGCTXDataType)3, 10615 },
	{ (Il2CppRGCTXDataType)2, 12462 },
	{ (Il2CppRGCTXDataType)3, 10616 },
	{ (Il2CppRGCTXDataType)2, 15945 },
	{ (Il2CppRGCTXDataType)3, 10617 },
	{ (Il2CppRGCTXDataType)2, 12466 },
	{ (Il2CppRGCTXDataType)2, 15946 },
	{ (Il2CppRGCTXDataType)3, 10618 },
	{ (Il2CppRGCTXDataType)2, 15947 },
	{ (Il2CppRGCTXDataType)2, 12471 },
	{ (Il2CppRGCTXDataType)2, 12473 },
	{ (Il2CppRGCTXDataType)2, 15948 },
	{ (Il2CppRGCTXDataType)3, 10619 },
	{ (Il2CppRGCTXDataType)2, 15949 },
	{ (Il2CppRGCTXDataType)2, 12476 },
	{ (Il2CppRGCTXDataType)2, 12478 },
	{ (Il2CppRGCTXDataType)2, 15950 },
	{ (Il2CppRGCTXDataType)3, 10620 },
	{ (Il2CppRGCTXDataType)3, 10621 },
	{ (Il2CppRGCTXDataType)3, 10622 },
	{ (Il2CppRGCTXDataType)3, 10623 },
	{ (Il2CppRGCTXDataType)2, 12487 },
	{ (Il2CppRGCTXDataType)3, 10624 },
	{ (Il2CppRGCTXDataType)3, 10625 },
	{ (Il2CppRGCTXDataType)2, 12499 },
	{ (Il2CppRGCTXDataType)2, 15951 },
	{ (Il2CppRGCTXDataType)3, 10626 },
	{ (Il2CppRGCTXDataType)3, 10627 },
	{ (Il2CppRGCTXDataType)2, 12501 },
	{ (Il2CppRGCTXDataType)2, 15823 },
	{ (Il2CppRGCTXDataType)3, 10628 },
	{ (Il2CppRGCTXDataType)3, 10629 },
	{ (Il2CppRGCTXDataType)2, 15952 },
	{ (Il2CppRGCTXDataType)3, 10630 },
	{ (Il2CppRGCTXDataType)3, 10631 },
	{ (Il2CppRGCTXDataType)2, 12511 },
	{ (Il2CppRGCTXDataType)2, 15953 },
	{ (Il2CppRGCTXDataType)3, 10632 },
	{ (Il2CppRGCTXDataType)3, 10633 },
	{ (Il2CppRGCTXDataType)3, 10191 },
	{ (Il2CppRGCTXDataType)3, 10634 },
	{ (Il2CppRGCTXDataType)2, 15954 },
	{ (Il2CppRGCTXDataType)3, 10635 },
	{ (Il2CppRGCTXDataType)3, 10636 },
	{ (Il2CppRGCTXDataType)2, 12523 },
	{ (Il2CppRGCTXDataType)2, 15955 },
	{ (Il2CppRGCTXDataType)3, 10637 },
	{ (Il2CppRGCTXDataType)3, 10638 },
	{ (Il2CppRGCTXDataType)3, 10639 },
	{ (Il2CppRGCTXDataType)3, 10640 },
	{ (Il2CppRGCTXDataType)3, 10641 },
	{ (Il2CppRGCTXDataType)3, 10197 },
	{ (Il2CppRGCTXDataType)3, 10642 },
	{ (Il2CppRGCTXDataType)2, 15956 },
	{ (Il2CppRGCTXDataType)3, 10643 },
	{ (Il2CppRGCTXDataType)3, 10644 },
	{ (Il2CppRGCTXDataType)2, 12536 },
	{ (Il2CppRGCTXDataType)2, 15957 },
	{ (Il2CppRGCTXDataType)3, 10645 },
	{ (Il2CppRGCTXDataType)3, 10646 },
	{ (Il2CppRGCTXDataType)2, 12538 },
	{ (Il2CppRGCTXDataType)2, 15958 },
	{ (Il2CppRGCTXDataType)3, 10647 },
	{ (Il2CppRGCTXDataType)3, 10648 },
	{ (Il2CppRGCTXDataType)2, 15959 },
	{ (Il2CppRGCTXDataType)3, 10649 },
	{ (Il2CppRGCTXDataType)3, 10650 },
	{ (Il2CppRGCTXDataType)2, 15960 },
	{ (Il2CppRGCTXDataType)3, 10651 },
	{ (Il2CppRGCTXDataType)3, 10652 },
	{ (Il2CppRGCTXDataType)2, 12553 },
	{ (Il2CppRGCTXDataType)2, 15961 },
	{ (Il2CppRGCTXDataType)3, 10653 },
	{ (Il2CppRGCTXDataType)3, 10654 },
	{ (Il2CppRGCTXDataType)3, 10655 },
	{ (Il2CppRGCTXDataType)3, 10208 },
	{ (Il2CppRGCTXDataType)2, 15962 },
	{ (Il2CppRGCTXDataType)3, 10656 },
	{ (Il2CppRGCTXDataType)3, 10657 },
	{ (Il2CppRGCTXDataType)2, 15963 },
	{ (Il2CppRGCTXDataType)3, 10658 },
	{ (Il2CppRGCTXDataType)3, 10659 },
	{ (Il2CppRGCTXDataType)2, 12569 },
	{ (Il2CppRGCTXDataType)2, 15964 },
	{ (Il2CppRGCTXDataType)3, 10660 },
	{ (Il2CppRGCTXDataType)3, 10661 },
	{ (Il2CppRGCTXDataType)3, 10662 },
	{ (Il2CppRGCTXDataType)3, 10663 },
	{ (Il2CppRGCTXDataType)3, 10664 },
	{ (Il2CppRGCTXDataType)3, 10665 },
	{ (Il2CppRGCTXDataType)3, 10214 },
	{ (Il2CppRGCTXDataType)2, 15965 },
	{ (Il2CppRGCTXDataType)3, 10666 },
	{ (Il2CppRGCTXDataType)3, 10667 },
	{ (Il2CppRGCTXDataType)2, 15966 },
	{ (Il2CppRGCTXDataType)3, 10668 },
	{ (Il2CppRGCTXDataType)3, 10669 },
	{ (Il2CppRGCTXDataType)3, 10670 },
	{ (Il2CppRGCTXDataType)3, 10671 },
	{ (Il2CppRGCTXDataType)3, 10672 },
	{ (Il2CppRGCTXDataType)2, 12597 },
	{ (Il2CppRGCTXDataType)3, 10673 },
	{ (Il2CppRGCTXDataType)2, 15967 },
	{ (Il2CppRGCTXDataType)3, 10674 },
	{ (Il2CppRGCTXDataType)3, 10675 },
	{ (Il2CppRGCTXDataType)2, 15968 },
	{ (Il2CppRGCTXDataType)2, 15969 },
	{ (Il2CppRGCTXDataType)2, 15970 },
	{ (Il2CppRGCTXDataType)3, 10676 },
	{ (Il2CppRGCTXDataType)2, 12609 },
	{ (Il2CppRGCTXDataType)3, 10677 },
	{ (Il2CppRGCTXDataType)2, 15971 },
	{ (Il2CppRGCTXDataType)3, 10678 },
	{ (Il2CppRGCTXDataType)2, 15971 },
	{ (Il2CppRGCTXDataType)2, 12630 },
	{ (Il2CppRGCTXDataType)3, 10679 },
	{ (Il2CppRGCTXDataType)3, 10680 },
	{ (Il2CppRGCTXDataType)3, 10681 },
	{ (Il2CppRGCTXDataType)3, 10682 },
	{ (Il2CppRGCTXDataType)2, 15972 },
	{ (Il2CppRGCTXDataType)2, 15973 },
	{ (Il2CppRGCTXDataType)2, 15974 },
	{ (Il2CppRGCTXDataType)3, 10683 },
	{ (Il2CppRGCTXDataType)3, 10684 },
	{ (Il2CppRGCTXDataType)2, 12626 },
	{ (Il2CppRGCTXDataType)2, 12629 },
	{ (Il2CppRGCTXDataType)3, 10685 },
	{ (Il2CppRGCTXDataType)3, 10686 },
	{ (Il2CppRGCTXDataType)2, 12633 },
	{ (Il2CppRGCTXDataType)3, 10687 },
	{ (Il2CppRGCTXDataType)2, 15975 },
	{ (Il2CppRGCTXDataType)2, 12623 },
	{ (Il2CppRGCTXDataType)2, 15976 },
	{ (Il2CppRGCTXDataType)3, 10688 },
	{ (Il2CppRGCTXDataType)3, 10689 },
	{ (Il2CppRGCTXDataType)3, 10690 },
	{ (Il2CppRGCTXDataType)2, 15977 },
	{ (Il2CppRGCTXDataType)3, 10691 },
	{ (Il2CppRGCTXDataType)3, 10692 },
	{ (Il2CppRGCTXDataType)3, 10693 },
	{ (Il2CppRGCTXDataType)2, 12648 },
	{ (Il2CppRGCTXDataType)3, 10694 },
	{ (Il2CppRGCTXDataType)2, 15978 },
	{ (Il2CppRGCTXDataType)3, 10695 },
	{ (Il2CppRGCTXDataType)3, 10696 },
	{ (Il2CppRGCTXDataType)2, 15979 },
	{ (Il2CppRGCTXDataType)2, 12687 },
	{ (Il2CppRGCTXDataType)2, 12685 },
	{ (Il2CppRGCTXDataType)2, 15980 },
	{ (Il2CppRGCTXDataType)3, 10697 },
	{ (Il2CppRGCTXDataType)2, 15981 },
	{ (Il2CppRGCTXDataType)3, 10698 },
	{ (Il2CppRGCTXDataType)3, 10699 },
	{ (Il2CppRGCTXDataType)3, 10700 },
	{ (Il2CppRGCTXDataType)2, 12691 },
	{ (Il2CppRGCTXDataType)3, 10701 },
	{ (Il2CppRGCTXDataType)3, 10702 },
	{ (Il2CppRGCTXDataType)2, 12694 },
	{ (Il2CppRGCTXDataType)3, 10703 },
	{ (Il2CppRGCTXDataType)1, 15982 },
	{ (Il2CppRGCTXDataType)2, 12693 },
	{ (Il2CppRGCTXDataType)3, 10704 },
	{ (Il2CppRGCTXDataType)1, 12693 },
	{ (Il2CppRGCTXDataType)1, 12691 },
	{ (Il2CppRGCTXDataType)2, 15983 },
	{ (Il2CppRGCTXDataType)2, 12693 },
	{ (Il2CppRGCTXDataType)3, 10705 },
	{ (Il2CppRGCTXDataType)3, 10706 },
	{ (Il2CppRGCTXDataType)3, 10707 },
	{ (Il2CppRGCTXDataType)2, 12692 },
	{ (Il2CppRGCTXDataType)3, 10708 },
	{ (Il2CppRGCTXDataType)2, 12705 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	156,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	43,
	s_rgctxIndices,
	210,
	s_rgctxValues,
	NULL,
};
