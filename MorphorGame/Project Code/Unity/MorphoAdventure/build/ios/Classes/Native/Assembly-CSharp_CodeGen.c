﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AlertController::setup(System.String,System.String,UnityEngine.Sprite,System.String,System.String,System.Boolean)
extern void AlertController_setup_mFF5316D39F0AFE3DBF01B5C7EF93A86B182B9EDB ();
// 0x00000002 System.Void AlertController::dismiss_self()
extern void AlertController_dismiss_self_mC8130B326B05CB1DDC040907AAE02D196401A888 ();
// 0x00000003 System.Void AlertController::confirmClicked()
extern void AlertController_confirmClicked_mC1DB32214E30E22EA249A0FC58C3EB425F8A2AC1 ();
// 0x00000004 System.Void AlertController::cancelClicked()
extern void AlertController_cancelClicked_mECC295CDB1084460C3CC1F631F97C12FC98F4CF4 ();
// 0x00000005 System.Void AlertController::.ctor()
extern void AlertController__ctor_mC0A30B81BC06B5848E7F1C4B3ED48604F837BED3 ();
// 0x00000006 System.Void OnBoardTutorialController::highlight_obj(UnityEngine.GameObject,System.String,UnityEngine.GameObject)
extern void OnBoardTutorialController_highlight_obj_mE2C519C293022C49175CBB24FC11CE1A52E23ECB ();
// 0x00000007 System.Void OnBoardTutorialController::reset_toturialContent()
extern void OnBoardTutorialController_reset_toturialContent_m60122051EB9DD895AC21023A1B4528D5D7C5F036 ();
// 0x00000008 System.Void OnBoardTutorialController::.ctor()
extern void OnBoardTutorialController__ctor_mE8EA536872E07D6FC07747653F8282F3F1F94F57 ();
// 0x00000009 System.Void APISetting::.ctor()
extern void APISetting__ctor_m5782FF5B51B10E5F7A0A2EFD78B8716934723F78 ();
// 0x0000000A System.Void APISetting::.cctor()
extern void APISetting__cctor_mAE553E712FA6762FCD32388C6C348E41D4AC182C ();
// 0x0000000B System.Void Achievement_effect::Start()
extern void Achievement_effect_Start_m30F79934EB54F2996DFA82090D2863366D6A7536 ();
// 0x0000000C System.Void Achievement_effect::Update()
extern void Achievement_effect_Update_m693D97691F3F9CDDED95CDAEF09FC90EFFC6F855 ();
// 0x0000000D System.Void Achievement_effect::change_page_effect(System.Boolean)
extern void Achievement_effect_change_page_effect_mA96BCA876A9755E0E9B9751B8CF0CC0CA461AB6F ();
// 0x0000000E System.Void Achievement_effect::call_reloadStickers()
extern void Achievement_effect_call_reloadStickers_mEB0332373044C1DF40E2F3C41F5104BA990344D0 ();
// 0x0000000F System.Void Achievement_effect::.ctor()
extern void Achievement_effect__ctor_m1FBEF5188E15D581A09976DC4C671A96B6D8AD2B ();
// 0x00000010 System.Void AchivementController::Awake()
extern void AchivementController_Awake_mF6B74651CDE3BF441814A7E8A1DE7541C49A443C ();
// 0x00000011 System.Void AchivementController::Start()
extern void AchivementController_Start_mDE70F8245789BE75D79590B8ECA33430E10EEC7A ();
// 0x00000012 System.Void AchivementController::Update()
extern void AchivementController_Update_mB9B8A5DBD5A698732B845B712622315613F66E01 ();
// 0x00000013 System.Void AchivementController::setup_gridView(System.Boolean)
extern void AchivementController_setup_gridView_mA13343802BC685D90F9E238E39EE674D956C48C0 ();
// 0x00000014 System.Void AchivementController::backClicked()
extern void AchivementController_backClicked_m1B1CEABB42FF1911EA6D5339E76490D052CFC728 ();
// 0x00000015 System.Void AchivementController::clean_stickers()
extern void AchivementController_clean_stickers_m52396708D8C21651859628E7ADEB708AED3AF512 ();
// 0x00000016 System.Void AchivementController::show_sticker_small_clicked()
extern void AchivementController_show_sticker_small_clicked_m1317914E5FBB07EB6CB57651E9E5838BF9F921BC ();
// 0x00000017 System.Void AchivementController::load_smallSticker()
extern void AchivementController_load_smallSticker_m065D91461A6EE2C9449D428A59F13D9B9C76FE1B ();
// 0x00000018 System.Void AchivementController::init_sticker(System.Boolean,UnityEngine.Sprite,UnityEngine.Sprite,System.Single,System.Boolean)
extern void AchivementController_init_sticker_m2709427B5FDD7815F2903D90AF557339A7DDCF1A ();
// 0x00000019 System.Void AchivementController::show_sticker_large_clicked()
extern void AchivementController_show_sticker_large_clicked_m7A6DBECA18A808C870A2F840AC67C538D064A726 ();
// 0x0000001A System.Void AchivementController::load_largeSticker()
extern void AchivementController_load_largeSticker_m7DEAB10DDBAFAF209CC372514505BC7B91ECB349 ();
// 0x0000001B System.Void AchivementController::set_changePageButtons()
extern void AchivementController_set_changePageButtons_m883FF3B4FCC2018E439B4F4137EE9A8CA32E3DE1 ();
// 0x0000001C System.Void AchivementController::next_page_clicked()
extern void AchivementController_next_page_clicked_mC363B2F69137424893ADCE4858CF4536D64B8E02 ();
// 0x0000001D System.Void AchivementController::previous_page_clicked()
extern void AchivementController_previous_page_clicked_m0AE7CDF97A6D6D07DCAD4F395569AEE1380B7F50 ();
// 0x0000001E System.Void AchivementController::.ctor()
extern void AchivementController__ctor_m12C63F41598CFBE1BE253002DFD1C1F4F16DC178 ();
// 0x0000001F System.Void CSVReader::Start()
extern void CSVReader_Start_mBA3D657B973751B9D936A4DA4788FF1C75D26B31 ();
// 0x00000020 System.Void CSVReader::DebugOutputGrid(System.String[0...,0...])
extern void CSVReader_DebugOutputGrid_m6A48E539290678F3EC61E05F76ABBA36561B9A3A ();
// 0x00000021 System.String[0...,0...] CSVReader::SplitCsvGrid(System.String)
extern void CSVReader_SplitCsvGrid_m8D6CDD28EF1E2C26B448F5691AE7D5401184ED0A ();
// 0x00000022 System.String[] CSVReader::SplitCsvLine(System.String)
extern void CSVReader_SplitCsvLine_m0A1A26B97C16EFB18357E1B7316C3BA98115A195 ();
// 0x00000023 System.Void CSVReader::.ctor()
extern void CSVReader__ctor_mC259EFA1874891B2736A0559B5614CCB8C1BD9AC ();
// 0x00000024 System.Void CollectionObjectController::setup(UnityEngine.Sprite,UnityEngine.Sprite,System.Single,System.Boolean)
extern void CollectionObjectController_setup_m74F60CF14FA85249055BC458E31DE1438D82DEEB ();
// 0x00000025 System.Void CollectionObjectController::.ctor()
extern void CollectionObjectController__ctor_mA2EBEAFCD6310770D6D37F09ED32C6F9A93427F6 ();
// 0x00000026 System.Void EffectController::Start()
extern void EffectController_Start_m44EF5B5E5700D7D9F3EAD8DDE6A80A2F52E9C8A2 ();
// 0x00000027 System.Void EffectController::Update()
extern void EffectController_Update_mE64CFF76F4DCFEB3391DA7095A0F94D0D802D9B3 ();
// 0x00000028 System.Void EffectController::DestroyGameObject()
extern void EffectController_DestroyGameObject_m1EBACBF4864FB3230997F394ECB9BF2168743AC7 ();
// 0x00000029 System.Void EffectController::.ctor()
extern void EffectController__ctor_m4D367DCBC8B71FDB6EE65A6ED43C44F56B6CF649 ();
// 0x0000002A System.Void GameManager::Awake()
extern void GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F ();
// 0x0000002B System.Void GameManager::Start()
extern void GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E ();
// 0x0000002C System.Void GameManager::Update()
extern void GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A ();
// 0x0000002D System.Void GameManager::set_audios_volume()
extern void GameManager_set_audios_volume_m91E50913CB4855E23B6745CB405E61E2730D7FC5 ();
// 0x0000002E System.Void GameManager::change_scene(System.String)
extern void GameManager_change_scene_mF1B55AAA457C1A91196BC077BBCC913E25779291 ();
// 0x0000002F System.Int64 GameManager::GetTimeStamp(System.Boolean)
extern void GameManager_GetTimeStamp_m86A31AF5E65C2E1A37E0C845F98EB9ABFB1652C0 ();
// 0x00000030 System.DateTime GameManager::UnixTimestampToDateTime(System.Double)
extern void GameManager_UnixTimestampToDateTime_m873D959D049FDAF7B9A7FC6C191AD2EE1F845390 ();
// 0x00000031 System.Void GameManager::do_upload_answer(Models.GameAnswer)
extern void GameManager_do_upload_answer_m1D04DECDCD2CB7E1571B45B81FE0E1DACEA40DC7 ();
// 0x00000032 System.Void GameManager::do_upload_gameTime(Models.GameTime)
extern void GameManager_do_upload_gameTime_m75EAB05DD93538BFB0C424CB0BC8401FEC2E5728 ();
// 0x00000033 System.Void GameManager::do_upload_gameProgress(Models.GameProgress)
extern void GameManager_do_upload_gameProgress_mA10061CA6D0242836679FA1BB5F9CD2068C30737 ();
// 0x00000034 System.Void GameManager::do_update_difficulity(Models.User)
extern void GameManager_do_update_difficulity_m2A42C621C32AAA0D1135727B6341F0A85D1B4FA7 ();
// 0x00000035 System.Int32 GameManager::get_current_week()
extern void GameManager_get_current_week_m9DF0FE2697B7CCA4F74A81B3A975554108EA675F ();
// 0x00000036 System.Int32 GameManager::get_current_week_FromProgress()
extern void GameManager_get_current_week_FromProgress_mD6295276A5E7A45BE348449442A3B7845E4B1BF7 ();
// 0x00000037 System.Void GameManager::play_SE(System.String,System.Single)
extern void GameManager_play_SE_m37D30E734731D1B8F5685C8EEF3434F0B6E01894 ();
// 0x00000038 System.Void GameManager::play_BGM(System.String)
extern void GameManager_play_BGM_m9787BA7006E57AD327CEC9C1CC4656A590963816 ();
// 0x00000039 System.Single[][] GameManager::group_score_by_day()
extern void GameManager_group_score_by_day_m7DF02170EFA5AC235EC7402A74170E41DD7A4119 ();
// 0x0000003A System.Single[][] GameManager::group_score_by_day(System.Int32)
extern void GameManager_group_score_by_day_mAD10CE899BE8BFFCE28A1187087A55FE3E067219 ();
// 0x0000003B System.Int32 GameManager::get_maxDayFromProgress()
extern void GameManager_get_maxDayFromProgress_m59EB0F5F3192F655ABA5DEED080AF67B5FD8658D ();
// 0x0000003C System.Single[] GameManager::get_scoresOfDay(System.Int32)
extern void GameManager_get_scoresOfDay_m8EFED0F1FD12F3839F95D1A1C3E49B9C8068143C ();
// 0x0000003D Models.GameProgress[] GameManager::get_progressOfDay(System.Int32)
extern void GameManager_get_progressOfDay_m87085B411E75D31A09ADA283FD9CC999226FE7B4 ();
// 0x0000003E System.Int32 GameManager::compare_dayDiffToNow(System.Int64)
extern void GameManager_compare_dayDiffToNow_mB3074477EEBC0E56387C5C4AB23439957E40ABA4 ();
// 0x0000003F AlertController GameManager::showAlert(System.String,System.String,UnityEngine.Sprite,System.String,System.String,System.Boolean)
extern void GameManager_showAlert_m94EF799509708BCF3663061A65EA4503F86B30A0 ();
// 0x00000040 System.Void GameManager::show_loading(System.Boolean)
extern void GameManager_show_loading_mB73335C43295F6C02ADD9EC002415E58540E38BE ();
// 0x00000041 System.Void GameManager::play_video(System.String)
extern void GameManager_play_video_mDAF6E08A9A26E80124D6FD939009EFB241FB591B ();
// 0x00000042 System.Void GameManager::DidEndVideoPlay(UnityEngine.Video.VideoPlayer)
extern void GameManager_DidEndVideoPlay_m91DADA8DDA03A52F20C90A80FAE31D1CE8FA2650 ();
// 0x00000043 System.Boolean GameManager::check_networkStatus()
extern void GameManager_check_networkStatus_m4C391B60C4A7D1033943E2704BD97986E3FAB516 ();
// 0x00000044 System.Void GameManager::highlight_obj(UnityEngine.GameObject,System.String,UnityEngine.GameObject)
extern void GameManager_highlight_obj_mC8A9AAAA97A4FC61F2FA3E5C7B5FC39F9F3E5E8E ();
// 0x00000045 System.Void GameManager::.ctor()
extern void GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693 ();
// 0x00000046 System.Void GameSetting::.ctor()
extern void GameSetting__ctor_mE3E9CB53C4483431CC85C718E24B2FA164A48CB2 ();
// 0x00000047 System.Void GameSetting::.cctor()
extern void GameSetting__cctor_mAF6F0BFEEC54791457175C733644D1AC2E6D966F ();
// 0x00000048 System.Void LoginController::Awake()
extern void LoginController_Awake_m13813482EB515C6BF0E1F200EC65169729DCBE9A ();
// 0x00000049 System.Void LoginController::Start()
extern void LoginController_Start_mFB2204778D7EA0F9DDA0EB6877C51AF1407F55CA ();
// 0x0000004A System.Void LoginController::Update()
extern void LoginController_Update_m7DE707345A79196417B91B5F5B0622667E8B2A3C ();
// 0x0000004B System.Void LoginController::loginClicked()
extern void LoginController_loginClicked_m572560CF7A6A7190D111F9F39E4281377CAE106C ();
// 0x0000004C System.Void LoginController::do_login()
extern void LoginController_do_login_m33D291048A633CB7EEAF431DE62707BE942D09CF ();
// 0x0000004D System.Void LoginController::setButtonsInteractable(System.Boolean)
extern void LoginController_setButtonsInteractable_m5F4BFB86A9F201E23808ACF8E7ECCFA32EE05087 ();
// 0x0000004E System.Boolean LoginController::checkInput()
extern void LoginController_checkInput_m042CC0E05608CBC9A26E3A9589DF3086C6D78F61 ();
// 0x0000004F System.Void LoginController::registerClicked()
extern void LoginController_registerClicked_m54F0B278502570ED3372A02E58E0120ACD29C6E5 ();
// 0x00000050 System.Void LoginController::.ctor()
extern void LoginController__ctor_m82192FD3AC4B3431849B96EABB27850484FF3695 ();
// 0x00000051 System.Void LoginController::<do_login>b__12_1(System.Exception)
extern void LoginController_U3Cdo_loginU3Eb__12_1_m62524ACDC6FE964DAB67651ECC8958F4267483AF ();
// 0x00000052 System.Void MainController::Awake()
extern void MainController_Awake_mCB019C420771675CC0A6A3F28973467E6B3CE32B ();
// 0x00000053 System.Void MainController::Start()
extern void MainController_Start_m35DA7A10A3616E68633F7BB8D2C562D05D8CBFA1 ();
// 0x00000054 System.Void MainController::didLoadMiniGameData()
extern void MainController_didLoadMiniGameData_mD73F5CDCFBFEFD79642F6D8F78279F30728F3FF3 ();
// 0x00000055 System.Void MainController::didLoadStoryData()
extern void MainController_didLoadStoryData_m5E9F565BEBE16916A07B7654D34D6940A53C2FCE ();
// 0x00000056 System.Void MainController::check_and_startStory()
extern void MainController_check_and_startStory_m1829B8040FF88D119996091FD87F096C72AB0A46 ();
// 0x00000057 System.Void MainController::checkShowTutorialContent()
extern void MainController_checkShowTutorialContent_mFD6825752BED1FCC690D8F0547C25221C1FD886C ();
// 0x00000058 System.Void MainController::Update()
extern void MainController_Update_m263887AAE4B665CD62FC9C0B80B884E8E7B4D524 ();
// 0x00000059 System.Void MainController::do_nextPage()
extern void MainController_do_nextPage_m0FBEC278FAFB56B4616F3D78039C4F3A8C99BABD ();
// 0x0000005A System.Void MainController::do_previousPage()
extern void MainController_do_previousPage_m743D096A720C7ECCC3D00BA405AE702987DCDD95 ();
// 0x0000005B System.Void MainController::setup_map_UI()
extern void MainController_setup_map_UI_m2545AF67769AA32F0E7652BD68222BC66C422E0E ();
// 0x0000005C System.Void MainController::playTutorialVideo(System.String)
extern void MainController_playTutorialVideo_m250CAB57CFDE5B5AE576BA68F5ED852B65404768 ();
// 0x0000005D System.Void MainController::DidEndTutorialVideo(UnityEngine.Video.VideoPlayer)
extern void MainController_DidEndTutorialVideo_m0C5BD6BD5973BB5EFF81BAA692CEF614012B3BE1 ();
// 0x0000005E System.Void MainController::logout_clicked()
extern void MainController_logout_clicked_mF99F7526CBD7CFF55A9BCF7898C48B2DBC39A342 ();
// 0x0000005F System.Void MainController::do_logout()
extern void MainController_do_logout_m243E5D9BDBD9589B3E5897811942F562E1E119C0 ();
// 0x00000060 System.Void MainController::setup_days_UI()
extern void MainController_setup_days_UI_m36568D55F6EF8F70D483A5C409F9F8594D938C6D ();
// 0x00000061 System.Void MainController::gotoSettingClicked()
extern void MainController_gotoSettingClicked_m45C8B383B2E6FE5D1DB08655C7B7130A167C6C0E ();
// 0x00000062 System.Void MainController::closeSettingClicked()
extern void MainController_closeSettingClicked_mE46C0783E30B9D9397EFB26DDAC666A4B7388451 ();
// 0x00000063 System.Void MainController::gotoAchivementClicked()
extern void MainController_gotoAchivementClicked_mB1B3773D236E2486D19D6D825D7E6EDC34F3E5AF ();
// 0x00000064 System.Void MainController::startGameClicked(System.Int32)
extern void MainController_startGameClicked_mEC96807EBE0B1AE185A1F0FD503AE22484F7F4E6 ();
// 0x00000065 System.Void MainController::daysClicked(System.Int32)
extern void MainController_daysClicked_m57D7C59BDD6D4329C930074B9F4D48E0DEFB98B5 ();
// 0x00000066 System.Boolean MainController::check_playTutorialVideo()
extern void MainController_check_playTutorialVideo_mDC85C8F5B0BB24417B1E7B24C68BE9CF995305D1 ();
// 0x00000067 System.Void MainController::showDaysClicked(System.Int32)
extern void MainController_showDaysClicked_mC68F0D65F7F905CE314534F2A0E0B4851A0AEE94 ();
// 0x00000068 System.Void MainController::hideDaysClicked()
extern void MainController_hideDaysClicked_mD1A9B340023261809E46CFDEA775108D3CF7DABE ();
// 0x00000069 System.Void MainController::readStoryClicked()
extern void MainController_readStoryClicked_mF1CCE42D7BBC75153C888D6C3920AD76E630E9D5 ();
// 0x0000006A System.Void MainController::next_storySlide(System.Boolean)
extern void MainController_next_storySlide_mB0B70BC52C9BC78A7E32131EDBF599FCEF69F08F ();
// 0x0000006B System.Void MainController::previous_storySlide()
extern void MainController_previous_storySlide_m6ACCA48A8927A2E616CE6961CA2B27C59F1F9F22 ();
// 0x0000006C System.Void MainController::checkAndShowAchievement()
extern void MainController_checkAndShowAchievement_m821BFAF3DDBA4736025C10699A8BAAFAAF18A927 ();
// 0x0000006D System.Void MainController::check_new_largeSticker()
extern void MainController_check_new_largeSticker_mCDD5A2895CA13B9857AD195E02CEBA11E9DC1DDD ();
// 0x0000006E UnityEngine.Sprite MainController::get_lastSmallStickerImage()
extern void MainController_get_lastSmallStickerImage_m1788989F13AE1892CFCA965AEEC60CF812CE4DCF ();
// 0x0000006F System.Int32 MainController::get_smallSticker_count()
extern void MainController_get_smallSticker_count_mCA2786A94937326824FDEE7205C2AD426EDA4A5E ();
// 0x00000070 System.Int32 MainController::get_largeSticker_count()
extern void MainController_get_largeSticker_count_mAE286AFFDACB396B2055ECAB7D3263E5DE20DC56 ();
// 0x00000071 System.Void MainController::switch_page(MainController_MainGamePage)
extern void MainController_switch_page_m5BBE7FD5061F6E6A6E4CFE8524B0A19BE63B7D5E ();
// 0x00000072 System.Void MainController::gotoMapClicked()
extern void MainController_gotoMapClicked_mB9C165983221BC06BB3AD9C333598DCDF9B4399A ();
// 0x00000073 System.Void MainController::endStoryToMap()
extern void MainController_endStoryToMap_mAF94C8F0CA846FFD85687C586204BF32DCEC8D74 ();
// 0x00000074 System.Void MainController::endOneDayTraining()
extern void MainController_endOneDayTraining_m60B0487F1263F7C6D3835FCC8E60DD7608133564 ();
// 0x00000075 System.Void MainController::gotoStoryClicked()
extern void MainController_gotoStoryClicked_mF516F24512FB94B9AE6CDA35C93F8A9ACD38A1F0 ();
// 0x00000076 System.Void MainController::upload_storyTime()
extern void MainController_upload_storyTime_m48DFE89FE4AE75EA334435167D7FDD7A6B3ACE1C ();
// 0x00000077 System.Void MainController::gotoMiniGameClicked()
extern void MainController_gotoMiniGameClicked_m336CD764C929FB20450F981EE005D278DA5F480D ();
// 0x00000078 MinigameData MainController::get_currentMinigameData(System.Int32,System.Int32)
extern void MainController_get_currentMinigameData_mFC5CA5C17180A5772B8E5377917A48BAC36CACD6 ();
// 0x00000079 System.Void MainController::show_loading(System.Boolean)
extern void MainController_show_loading_mE511E527F2AF3A642FDAC8E5E4DEADEDAF992394 ();
// 0x0000007A System.Void MainController::do_load_storyData()
extern void MainController_do_load_storyData_mD32540EF6C996512284AECFB7B56A5D3B79AEEF5 ();
// 0x0000007B System.Collections.IEnumerator MainController::load_storyData()
extern void MainController_load_storyData_m98A217ED7CF21E87F1009C7A4A0F921EA08299AC ();
// 0x0000007C System.Void MainController::ProcessStoryData(System.String[0...,0...])
extern void MainController_ProcessStoryData_m3B5E36F0EE497F74EEAB6CF3A5619CEE2CF6D3B7 ();
// 0x0000007D System.Void MainController::do_load_minigameData()
extern void MainController_do_load_minigameData_m12179D423AB77D5111A54A8A9F74505F5D995D3A ();
// 0x0000007E System.Collections.IEnumerator MainController::load_minigameData()
extern void MainController_load_minigameData_m3624EC5B2355FA3F87F83BDFAB74C1E58A680FE2 ();
// 0x0000007F System.Void MainController::ProcessMinigameData(System.String[0...,0...])
extern void MainController_ProcessMinigameData_m53DA52616E02B8A76AB7F11B273471EB6D1E4A89 ();
// 0x00000080 System.Void MainController::.ctor()
extern void MainController__ctor_m74A09948F6E80BEFF77C13D185CCBB07899CE96E ();
// 0x00000081 System.Void MainController::<logout_clicked>b__73_0()
extern void MainController_U3Clogout_clickedU3Eb__73_0_mD3F7D2B3BC103F80C1F9529CAFAD0A115E078816 ();
// 0x00000082 System.Void MainController::<endStoryToMap>b__94_0()
extern void MainController_U3CendStoryToMapU3Eb__94_0_mA004DDE054A6B4287A5829500E84C49FCE64C804 ();
// 0x00000083 System.Void MainController::<endStoryToMap>b__94_1()
extern void MainController_U3CendStoryToMapU3Eb__94_1_mAF8BF36823466391BF52BC5DE3153D9D2ECAF838 ();
// 0x00000084 System.Void MiniGameController::load_minigame(MiniGameController_MiniGameType,MiniGameController_MiniGameSelectionsPosition,MiniGameController_MiniGameQuestionType,MiniGameController_MiniGameMoveDirection,System.Int32,System.Int32)
extern void MiniGameController_load_minigame_m3B155ACC0E9BA2D595FFD6DD60B5C58131E7D34E ();
// 0x00000085 System.Void MiniGameController::.ctor()
extern void MiniGameController__ctor_m0C22708AE971B4F8ADD322934839B632331E5082 ();
// 0x00000086 System.Void MiniGameMaster::setup_graphicUI()
extern void MiniGameMaster_setup_graphicUI_m40C5AB58A3880FCE86D4F771B33FB039D60769D9 ();
// 0x00000087 System.Void MiniGameMaster::checkShowTutorialContent()
extern void MiniGameMaster_checkShowTutorialContent_m688C36CA6F847171CC91232488A9C6867B3EB90A ();
// 0x00000088 System.Void MiniGameMaster::reset_selectionsAnim(UnityEngine.GameObject)
extern void MiniGameMaster_reset_selectionsAnim_m2B85B2AAA4C4683F9D22A8FD16BF766DFFCD0FFC ();
// 0x00000089 System.Void MiniGameMaster::setup_questionTypeUI()
extern void MiniGameMaster_setup_questionTypeUI_mB472643AA6CCFEC882DEE3C4365B77C8FD1D68F9 ();
// 0x0000008A System.Void MiniGameMaster::showNextQuestion()
extern void MiniGameMaster_showNextQuestion_m676343EA248251066E54182F57B74D69A17EA6DC ();
// 0x0000008B System.Void MiniGameMaster::do_showNextQuestion()
extern void MiniGameMaster_do_showNextQuestion_m075A292874587FB0B92358703078FB478FEB6850 ();
// 0x0000008C System.Void MiniGameMaster::check_playingAudio()
extern void MiniGameMaster_check_playingAudio_m02CA46B4E146547F6ABAF3309855AEE86D126771 ();
// 0x0000008D System.Void MiniGameMaster::start_game()
extern void MiniGameMaster_start_game_m0FB68EC2AF2D193471447DB460E4D15D06E1894A ();
// 0x0000008E System.Void MiniGameMaster::do_start_game()
extern void MiniGameMaster_do_start_game_mFA4CEB81017E604E1BCBE24A43C295288DBF6CBE ();
// 0x0000008F System.Void MiniGameMaster::update_gameTime()
extern void MiniGameMaster_update_gameTime_m40F5CFF02A95BBBD5BCF0BAE62888C2DA258290B ();
// 0x00000090 System.Void MiniGameMaster::update_gameProgress(System.Int32)
extern void MiniGameMaster_update_gameProgress_m71710839A543B0020E9A1AC3440444711FEE3721 ();
// 0x00000091 System.Void MiniGameMaster::generateQuestion_and_setAnimation()
extern void MiniGameMaster_generateQuestion_and_setAnimation_m2AFB9E06614240B0570579D4FC3FCC142FF7E622 ();
// 0x00000092 System.Boolean MiniGameMaster::check_countOfSelections()
extern void MiniGameMaster_check_countOfSelections_mE97F542F4C8924669F2776A82D8E121D4A9F9562 ();
// 0x00000093 Question_Different[] MiniGameMaster::get_questionSetByDifficulity_different(System.Int32)
extern void MiniGameMaster_get_questionSetByDifficulity_different_mF361EB7A70374B10BC09386E1F54A0E4375FF09C ();
// 0x00000094 Question_Same[] MiniGameMaster::get_questionSetByDifficulity_same(System.Int32)
extern void MiniGameMaster_get_questionSetByDifficulity_same_mCA9EE3BCC580E9D5C66F1DAEA0EF20C9EFDC4EA3 ();
// 0x00000095 Question_Tell[] MiniGameMaster::get_questionSetByDifficulity_judge(System.Int32)
extern void MiniGameMaster_get_questionSetByDifficulity_judge_mA4495A6290D12DA7572EB68A41051409535686F3 ();
// 0x00000096 System.Void MiniGameMaster::generateQuestion()
extern void MiniGameMaster_generateQuestion_m7415900A23C154DA7D3DE5BE33F6F7501AEB65F0 ();
// 0x00000097 System.Void MiniGameMaster::set_selections(Selection[])
extern void MiniGameMaster_set_selections_mFC22C4EC11FADC18A737A9C4D69657CA8BA5C32B ();
// 0x00000098 Selection[] MiniGameMaster::ShuffleSelection(Selection[])
extern void MiniGameMaster_ShuffleSelection_m3A1DCF94DEC52AC9B2416DD26F53022C59C963A3 ();
// 0x00000099 System.Void MiniGameMaster::selection_selected(System.Int32)
extern void MiniGameMaster_selection_selected_m8DFBF5C658FD093B3CB9DA9309E4D2BEC1A38F9E ();
// 0x0000009A System.Void MiniGameMaster::do_remove_hints()
extern void MiniGameMaster_do_remove_hints_m2A6334F7F21558CE7028211DCF2CA2D66E4365E3 ();
// 0x0000009B System.Void MiniGameMaster::do_selection_clicked(System.Int32)
extern void MiniGameMaster_do_selection_clicked_mF209C783B4DAA0AA2EA92788292C48B337240DA0 ();
// 0x0000009C System.Void MiniGameMaster::do_show_hints(UnityEngine.GameObject,System.Int32)
extern void MiniGameMaster_do_show_hints_m15531A5E781B042D35AA8B9CB75CAFC302AE9C25 ();
// 0x0000009D System.Void MiniGameMaster::confirmButton_enable(System.Boolean)
extern void MiniGameMaster_confirmButton_enable_m28040F8385FD85D371E8AB6BF5494AC1F7DB8EA3 ();
// 0x0000009E System.Void MiniGameMaster::refButton_enable(System.Boolean)
extern void MiniGameMaster_refButton_enable_m39639ACFE99F3B9CCBF1B5BC2A3EF51D395BC2BE ();
// 0x0000009F System.Void MiniGameMaster::judgeButton_enable(System.Boolean)
extern void MiniGameMaster_judgeButton_enable_mC90EC2CDF694D4F1D76F0AA2C6322CF59766EE56 ();
// 0x000000A0 System.Void MiniGameMaster::confirm_clicked()
extern void MiniGameMaster_confirm_clicked_m52F620B9494474609BA73117B13F5CA0ED5C03C7 ();
// 0x000000A1 System.Boolean MiniGameMaster::do_confirmClicked(System.Int32)
extern void MiniGameMaster_do_confirmClicked_m5ADB3E630BEA8A3FBB8AD829F46D80FB161C9BAF ();
// 0x000000A2 System.Void MiniGameMaster::judge_clicked(System.Boolean)
extern void MiniGameMaster_judge_clicked_m35E2D80E7C898F7A75B5CB72FD1C8D3ED878EFF6 ();
// 0x000000A3 System.Void MiniGameMaster::do_judgeClicked(System.Boolean)
extern void MiniGameMaster_do_judgeClicked_mBBC992410AA114175526D6DFD2280BB79146738F ();
// 0x000000A4 System.Void MiniGameMaster::reference_clicked()
extern void MiniGameMaster_reference_clicked_m7D92E7FD102DD86088301C37636EE4D89FB4495B ();
// 0x000000A5 System.Void MiniGameMaster::timesUp()
extern void MiniGameMaster_timesUp_m8002078ED7D1E5DF06A7A70992F7981924103C09 ();
// 0x000000A6 System.Void MiniGameMaster::do_timesUp()
extern void MiniGameMaster_do_timesUp_m1857CC34B4D80B36CA1170C79B7CBFD92EFAE7F2 ();
// 0x000000A7 System.Void MiniGameMaster::show_wrong()
extern void MiniGameMaster_show_wrong_m26F0EEE90D9CDD557A6285CEB51C00FC56837CFE ();
// 0x000000A8 System.Void MiniGameMaster::show_correct()
extern void MiniGameMaster_show_correct_m3B38EAE630D9B1BD24D7F762C63E9075A32C66A3 ();
// 0x000000A9 System.Void MiniGameMaster::get_NumOfSelections_range()
extern void MiniGameMaster_get_NumOfSelections_range_m38A3A0B6E1DAAC5624E599FB5C3D084481EEF629 ();
// 0x000000AA UnityEngine.GameObject MiniGameMaster::get_selectedObject()
extern void MiniGameMaster_get_selectedObject_m7AB4DE756FEE1A2541E1AB3EADA2205765365B6E ();
// 0x000000AB System.Void MiniGameMaster::handleAnswerdResult(System.Boolean)
extern void MiniGameMaster_handleAnswerdResult_mB836DBB0C87A34A7E7F67794924E61F1D47ACBFC ();
// 0x000000AC System.Void MiniGameMaster::do_lessSelection_lowerDifficulity()
extern void MiniGameMaster_do_lessSelection_lowerDifficulity_m0E6D0D1BB0D0C3BD1CB8D270599698C8735E032F ();
// 0x000000AD System.Void MiniGameMaster::do_moreSelections_higherDifficulity()
extern void MiniGameMaster_do_moreSelections_higherDifficulity_mCB396613BA5199F804BC38C5D28C009412C1BCF5 ();
// 0x000000AE System.Void MiniGameMaster::showResultAnimation()
extern void MiniGameMaster_showResultAnimation_m4B38D0974059B354276CD12F6F5167A10C6A2FA1 ();
// 0x000000AF System.Void MiniGameMaster::change_difficulity(System.Boolean)
extern void MiniGameMaster_change_difficulity_m1EF22CA574188E51EF3E7C84CA4210A790D32577 ();
// 0x000000B0 System.Void MiniGameMaster::update_difficulity()
extern void MiniGameMaster_update_difficulity_mF7AF5C4F94D748F61337F9F7FD4CC1368E702671 ();
// 0x000000B1 System.Collections.IEnumerator MiniGameMaster::waitAndGoNext()
extern void MiniGameMaster_waitAndGoNext_m163D07CD967D222CA5651369E9AA5263B4F2BB8F ();
// 0x000000B2 System.Void MiniGameMaster::showCoveredText()
extern void MiniGameMaster_showCoveredText_mA8DED299EC8FA66F18DCE94B1039A5E3A5EC68AC ();
// 0x000000B3 System.Void MiniGameMaster::play_audio()
extern void MiniGameMaster_play_audio_m7A0110B765DF88B9CBA620F33586194170E0EF9A ();
// 0x000000B4 System.Void MiniGameMaster::play_word_audio(System.Int32)
extern void MiniGameMaster_play_word_audio_m4943C7A34D49007ED28D0962DA4E70CB81DEC8C3 ();
// 0x000000B5 System.String MiniGameMaster::get_word_eng(System.Int32)
extern void MiniGameMaster_get_word_eng_m9FCD4B6BF6940BC181C5A3787230708AEB8755C9 ();
// 0x000000B6 System.Void MiniGameMaster::do_play_word_audio(System.String)
extern void MiniGameMaster_do_play_word_audio_m2D1EDAFEAB72EA8E85767D1540EAF3057F7DC488 ();
// 0x000000B7 System.Void MiniGameMaster::exitClicked()
extern void MiniGameMaster_exitClicked_m8992C18E90B751E51D95568DA0E1669556A31B5E ();
// 0x000000B8 System.Void MiniGameMaster::do_gotoStory()
extern void MiniGameMaster_do_gotoStory_mDCD0E3ABEF086E2C272580F7DA55952F4AB7F8D5 ();
// 0x000000B9 System.Void MiniGameMaster::do_exitClicked()
extern void MiniGameMaster_do_exitClicked_mBA21EA0BAF5ED8C74F4E4B34FC0D32467FB9F3C2 ();
// 0x000000BA System.String MiniGameMaster::get_csvFolderPath()
extern void MiniGameMaster_get_csvFolderPath_mBC1D8B245D44E848D43B8DF5261DC3696061B18E ();
// 0x000000BB System.Void MiniGameMaster::do_load_tellData(System.String)
extern void MiniGameMaster_do_load_tellData_m2198C30A2159A917EE0B0353E673EB53663B4D59 ();
// 0x000000BC System.Collections.IEnumerator MiniGameMaster::load_tellData()
extern void MiniGameMaster_load_tellData_mAE2AD35E54C955656F473792D6FD01E9BCA18200 ();
// 0x000000BD System.Void MiniGameMaster::ProcessTellData(System.String[0...,0...])
extern void MiniGameMaster_ProcessTellData_m938AF3DE9BBB0C0EC6DF3FF15A4F085DB962457B ();
// 0x000000BE System.Void MiniGameMaster::do_load_sameData(System.String)
extern void MiniGameMaster_do_load_sameData_m1759E6677D78A90592AC5A7EFD2E775802F6B806 ();
// 0x000000BF System.Collections.IEnumerator MiniGameMaster::load_sameData()
extern void MiniGameMaster_load_sameData_m870F0FD9F6C0D7C8B9436EF61A1694B088F89BEE ();
// 0x000000C0 System.Void MiniGameMaster::ProcessSameData(System.String[0...,0...])
extern void MiniGameMaster_ProcessSameData_m8209DE40774607D9315B65BDE2BC26EA5361F38F ();
// 0x000000C1 System.Void MiniGameMaster::do_load_differentData(System.String)
extern void MiniGameMaster_do_load_differentData_mD63F4891947B840B6EAEB96754CBB17FF4EBB636 ();
// 0x000000C2 System.Collections.IEnumerator MiniGameMaster::load_differentData()
extern void MiniGameMaster_load_differentData_m16A5EF8CF003DD533EF63CE2CEFB2AF60C3DD32C ();
// 0x000000C3 System.Void MiniGameMaster::ProcessDifferentData(System.String[0...,0...])
extern void MiniGameMaster_ProcessDifferentData_m39904DF803083593372DAA048564C4B322F6B193 ();
// 0x000000C4 System.String MiniGameMaster::get_csvRawData(System.String,MiniGameController_MiniGameQuestionType)
extern void MiniGameMaster_get_csvRawData_mC41AE6B42C6048E0DBDA1730D018B5EB0C88F661 ();
// 0x000000C5 System.Void MiniGameMaster::run_testing_load()
extern void MiniGameMaster_run_testing_load_mE02A5BA34302691A906A6F8EDE66AD38420E4CAF ();
// 0x000000C6 System.Void MiniGameMaster::.ctor()
extern void MiniGameMaster__ctor_m8481E1BCB290FB3617C4735D489B93EDC7721327 ();
// 0x000000C7 System.Void MiniGameMaster::<setup_graphicUI>b__56_0()
extern void MiniGameMaster_U3Csetup_graphicUIU3Eb__56_0_m81E8C33425508771348C7EA01C4B436DFB14FF9A ();
// 0x000000C8 System.Void MiniGameMaster::<setup_graphicUI>b__56_1()
extern void MiniGameMaster_U3Csetup_graphicUIU3Eb__56_1_mB7072DED31F3DFB7742BCCA334EF6C4A5834DAB6 ();
// 0x000000C9 System.Void MiniGame_Move_forward::Start()
extern void MiniGame_Move_forward_Start_mADDA051C5879444AD8C16225A41747CA4CB2FCAC ();
// 0x000000CA System.Void MiniGame_Move_forward::FixedUpdate()
extern void MiniGame_Move_forward_FixedUpdate_m2782C648C8BD4238025E1EA7183FBEE54183E5E9 ();
// 0x000000CB System.Void MiniGame_Move_forward::setup_Minigame_Doge_Normal()
extern void MiniGame_Move_forward_setup_Minigame_Doge_Normal_m7970A12896F82FECC5F4608FC46864DEC537D473 ();
// 0x000000CC System.Void MiniGame_Move_forward::do_move_anim()
extern void MiniGame_Move_forward_do_move_anim_m31F7DA6390978D57ACB6DA4D498097BDFB036A40 ();
// 0x000000CD System.Void MiniGame_Move_forward::do_pause_anim()
extern void MiniGame_Move_forward_do_pause_anim_mA40F218780750920FEA7B559F3DE582FA411621E ();
// 0x000000CE System.Void MiniGame_Move_forward::generateQuestion_and_setAnimation()
extern void MiniGame_Move_forward_generateQuestion_and_setAnimation_mF3606E901897B576AA254733F9D3B8C745EB00F9 ();
// 0x000000CF System.Void MiniGame_Move_forward::confirm_clicked()
extern void MiniGame_Move_forward_confirm_clicked_m0E649A18BFF541DC5B0F4154360B96A9B38F5BE2 ();
// 0x000000D0 System.Void MiniGame_Move_forward::judge_clicked(System.Boolean)
extern void MiniGame_Move_forward_judge_clicked_m97CD7B1D7FDD51A0651EE4D92AA2E7C14138D814 ();
// 0x000000D1 System.Void MiniGame_Move_forward::.ctor()
extern void MiniGame_Move_forward__ctor_m54AC42A7AD8254D4E05CFB2FA4718EEFB95CC729 ();
// 0x000000D2 System.Void Minigame_Appear::Start()
extern void Minigame_Appear_Start_m6529F84F941C5502554656A6D686268D4CBF64E1 ();
// 0x000000D3 System.Void Minigame_Appear::FixedUpdate()
extern void Minigame_Appear_FixedUpdate_m46942C70D32E9AD20AD27046D7FB19360E020904 ();
// 0x000000D4 System.Void Minigame_Appear::setup_appearUI()
extern void Minigame_Appear_setup_appearUI_m9CAF2DD4581ABD0C600D30197C2BF04E4A900611 ();
// 0x000000D5 System.Void Minigame_Appear::setup_gridView()
extern void Minigame_Appear_setup_gridView_m6CC0467A09505320DD1F5B36204E66A5F5238440 ();
// 0x000000D6 System.Void Minigame_Appear::update_objToShow()
extern void Minigame_Appear_update_objToShow_m133F72C03A2A6C170E5A4D4EE765DC19E34F6C43 ();
// 0x000000D7 System.Void Minigame_Appear::.ctor()
extern void Minigame_Appear__ctor_m1FE765870AD4A66EA410E2939CC769B86B62658D ();
// 0x000000D8 System.Void Minigame_Chase_Normal::Start()
extern void Minigame_Chase_Normal_Start_mA957148E401CFB33A58CAA4B474C89A3F5D04524 ();
// 0x000000D9 System.Void Minigame_Chase_Normal::FixedUpdate()
extern void Minigame_Chase_Normal_FixedUpdate_m1327D33C3E941B96A9994FF677DA67BB49EAAE40 ();
// 0x000000DA System.Void Minigame_Chase_Normal::setup_Minigame_Chase_Normal()
extern void Minigame_Chase_Normal_setup_Minigame_Chase_Normal_m5D9DF300978966BB903D54520BEC09A01C3DB267 ();
// 0x000000DB System.Void Minigame_Chase_Normal::do_move_anim()
extern void Minigame_Chase_Normal_do_move_anim_m1F7EC7DAF97F84155CF10C7D051058997CFCFA5A ();
// 0x000000DC System.Void Minigame_Chase_Normal::do_pause_anim()
extern void Minigame_Chase_Normal_do_pause_anim_m13AD24666E836F29140DB3198FAA2760107518B3 ();
// 0x000000DD System.Void Minigame_Chase_Normal::generateQuestion_and_setAnimation()
extern void Minigame_Chase_Normal_generateQuestion_and_setAnimation_mDB0E834A03DB003521AE62A528A74FE4DCC9DF97 ();
// 0x000000DE System.Void Minigame_Chase_Normal::confirm_clicked()
extern void Minigame_Chase_Normal_confirm_clicked_mE199E87C87E7CC1623F1F87C17C05F2AFFC6C1B0 ();
// 0x000000DF System.Void Minigame_Chase_Normal::judge_clicked(System.Boolean)
extern void Minigame_Chase_Normal_judge_clicked_m53F557BE5C7AF649DABEAAC4972196D838818155 ();
// 0x000000E0 System.Void Minigame_Chase_Normal::timesUp()
extern void Minigame_Chase_Normal_timesUp_m875D3A229E26A924BCAD2D9862A460C1DE0AA3A4 ();
// 0x000000E1 System.Void Minigame_Chase_Normal::.ctor()
extern void Minigame_Chase_Normal__ctor_mCFE8B011E6B8E6ED632579F2A7F7A15DFF91D3C4 ();
// 0x000000E2 System.Void Minigame_Dodge_TimeLimited::Start()
extern void Minigame_Dodge_TimeLimited_Start_m0EF038EB6C3E28DDE34BA51D4042118917AAD133 ();
// 0x000000E3 System.Void Minigame_Dodge_TimeLimited::FixedUpdate()
extern void Minigame_Dodge_TimeLimited_FixedUpdate_mA089772C5911D1ADD61AD83FF460AAAD0749AA0F ();
// 0x000000E4 System.Void Minigame_Dodge_TimeLimited::setup_Minigame_Doge_Normal()
extern void Minigame_Dodge_TimeLimited_setup_Minigame_Doge_Normal_m5976E22B9791BCDF56FD9208F89DB6A889C45AD4 ();
// 0x000000E5 System.Void Minigame_Dodge_TimeLimited::do_move_anim()
extern void Minigame_Dodge_TimeLimited_do_move_anim_mD8AB51CA20EC5E8E9EA8C88BDAA3FBB7F8028D7B ();
// 0x000000E6 System.Void Minigame_Dodge_TimeLimited::do_pause_anim()
extern void Minigame_Dodge_TimeLimited_do_pause_anim_m22D0F20D7451B5929F16843DD8FC6562F71521B4 ();
// 0x000000E7 System.Void Minigame_Dodge_TimeLimited::generateQuestion_and_setAnimation()
extern void Minigame_Dodge_TimeLimited_generateQuestion_and_setAnimation_mA8989522ABED082C9679B68EF06E5C173D0D00A2 ();
// 0x000000E8 System.Void Minigame_Dodge_TimeLimited::confirm_clicked()
extern void Minigame_Dodge_TimeLimited_confirm_clicked_m76E626763D7E889FB5B624D6E18FEBFABD47631A ();
// 0x000000E9 System.Void Minigame_Dodge_TimeLimited::judge_clicked(System.Boolean)
extern void Minigame_Dodge_TimeLimited_judge_clicked_m58090535DC80D8DA28043C49F4FC3ED3BC7DCB57 ();
// 0x000000EA System.Void Minigame_Dodge_TimeLimited::.ctor()
extern void Minigame_Dodge_TimeLimited__ctor_m55835DC477A897883C885A2BDE363789A3B1A9A8 ();
// 0x000000EB System.Void Minigame_Doge_Normal::Start()
extern void Minigame_Doge_Normal_Start_mE8AFA01EC4DE55099022D94210087C9A3A7FBA45 ();
// 0x000000EC System.Void Minigame_Doge_Normal::FixedUpdate()
extern void Minigame_Doge_Normal_FixedUpdate_m5BD5B0DA6C814C6A2579267A97FB1ECCA0AF8D94 ();
// 0x000000ED System.Void Minigame_Doge_Normal::setup_Minigame_Doge_Normal()
extern void Minigame_Doge_Normal_setup_Minigame_Doge_Normal_mBD7C925517A27EF9DC137B26E161BCE8786223EF ();
// 0x000000EE System.Void Minigame_Doge_Normal::do_move_anim()
extern void Minigame_Doge_Normal_do_move_anim_m880B5B599B79DE2C625FE7FB289ADF444B936C25 ();
// 0x000000EF System.Void Minigame_Doge_Normal::do_pause_anim()
extern void Minigame_Doge_Normal_do_pause_anim_m6636EDAF03CA903692DE94000E6EDB3DD6A987A7 ();
// 0x000000F0 System.Void Minigame_Doge_Normal::generateQuestion_and_setAnimation()
extern void Minigame_Doge_Normal_generateQuestion_and_setAnimation_m78A24B715A7944A0442877EB958513E9626DAF21 ();
// 0x000000F1 System.Void Minigame_Doge_Normal::confirm_clicked()
extern void Minigame_Doge_Normal_confirm_clicked_m6C0A3A339DFCB93FAE9DEDE0972C9E1173D1133E ();
// 0x000000F2 System.Void Minigame_Doge_Normal::judge_clicked(System.Boolean)
extern void Minigame_Doge_Normal_judge_clicked_mB8C35717E652E8C8E06DB414E6D7AF4586B9D6FB ();
// 0x000000F3 System.Void Minigame_Doge_Normal::.ctor()
extern void Minigame_Doge_Normal__ctor_m452F1B165CBB24F907E5E27E4FCA6CB095361F84 ();
// 0x000000F4 System.Void Minigame_Hamster::Start()
extern void Minigame_Hamster_Start_m55E0E4EEA4CDDB7629ECB34E7494C144A47528A1 ();
// 0x000000F5 System.Void Minigame_Hamster::FixedUpdate()
extern void Minigame_Hamster_FixedUpdate_m0470EDCD7AB0ACD3B9C5D88A87681BD18888244D ();
// 0x000000F6 System.Void Minigame_Hamster::setup_gridView()
extern void Minigame_Hamster_setup_gridView_m7538522D5E1736AC9D043148B020285E9F75D40A ();
// 0x000000F7 System.Void Minigame_Hamster::showResultAnimation()
extern void Minigame_Hamster_showResultAnimation_m53A3DD3101F62488EDCBA4D5F7F4CBAC74207359 ();
// 0x000000F8 UnityEngine.GameObject Minigame_Hamster::get_selectedObject()
extern void Minigame_Hamster_get_selectedObject_mB48FB0B3F23ED75E14B7F46A515C5780FB9DC832 ();
// 0x000000F9 System.Void Minigame_Hamster::set_selections(Selection[])
extern void Minigame_Hamster_set_selections_mBFE1F0716BA9BC7016A424254457B83E835B1DE4 ();
// 0x000000FA System.Void Minigame_Hamster::setup_Minigame_Hamster()
extern void Minigame_Hamster_setup_Minigame_Hamster_m084C4FC7B9FAE98EBEE4DFD8B781CAC161FADE70 ();
// 0x000000FB System.Void Minigame_Hamster::do_move_anim()
extern void Minigame_Hamster_do_move_anim_m85F16E99DE1300EE046B949649A04EBD8BE0DFFB ();
// 0x000000FC System.Void Minigame_Hamster::do_pause_anim()
extern void Minigame_Hamster_do_pause_anim_mE893DBC28D3B39B6B65A87BD39CAFB229E45EF3B ();
// 0x000000FD System.Void Minigame_Hamster::timesUp()
extern void Minigame_Hamster_timesUp_m6D2E8F77A3CC4A4A6CDF8194FFB1BAB4EDFCA412 ();
// 0x000000FE System.Void Minigame_Hamster::generateQuestion_and_setAnimation()
extern void Minigame_Hamster_generateQuestion_and_setAnimation_mBDA89F8284327DF14335BB85E7A23A0824F72D17 ();
// 0x000000FF System.Void Minigame_Hamster::confirm_clicked()
extern void Minigame_Hamster_confirm_clicked_m2B1156E65A036A4BCD36EE2D8369C433E677E4DA ();
// 0x00000100 System.Int32 Minigame_Hamster::get_real_currentSelected()
extern void Minigame_Hamster_get_real_currentSelected_m778EB1935E53D2FA3139048BC240B7C9E452DC9B ();
// 0x00000101 System.Void Minigame_Hamster::play_word_audio(System.Int32)
extern void Minigame_Hamster_play_word_audio_mC4B64F70C5B1121826DA1FDB1D7CB283FC177F7C ();
// 0x00000102 System.String Minigame_Hamster::get_word_eng(System.Int32)
extern void Minigame_Hamster_get_word_eng_mB5A1C4D71C68E4A18F773A4C80FD12695B68FABC ();
// 0x00000103 System.Void Minigame_Hamster::.ctor()
extern void Minigame_Hamster__ctor_mED6C22B0DDC2E17A6B8F6C0FAE1930BEA7BA0588 ();
// 0x00000104 System.Void Minigame_Replace::Start()
extern void Minigame_Replace_Start_m69069BEEE808A9408B4B6D5836C64BAB355E3441 ();
// 0x00000105 System.Void Minigame_Replace::FixedUpdate()
extern void Minigame_Replace_FixedUpdate_mA312355F27C087FA2320D816F55132FB2B765D43 ();
// 0x00000106 System.Void Minigame_Replace::Update()
extern void Minigame_Replace_Update_mB27D5237BA1C570E939F35A52175A411981C39A6 ();
// 0x00000107 System.Void Minigame_Replace::setup_gridView()
extern void Minigame_Replace_setup_gridView_mF50AE7BA21F3DB92F8B08BB45DDE168BD024FEE8 ();
// 0x00000108 System.Void Minigame_Replace::set_selections(Selection[])
extern void Minigame_Replace_set_selections_mF56A720AA4217BF10DEF4C70E0F23D8A3AE7FFF6 ();
// 0x00000109 System.Void Minigame_Replace::init_12_selections()
extern void Minigame_Replace_init_12_selections_mDABE0BEB2D66DED147C92F1C203711AAF56DFEE5 ();
// 0x0000010A System.Void Minigame_Replace::confirm_clicked()
extern void Minigame_Replace_confirm_clicked_mA066EDAE92BE8B9795E7533980801B4AB731F6FB ();
// 0x0000010B System.Void Minigame_Replace::play_word_audio(System.Int32)
extern void Minigame_Replace_play_word_audio_m0D8B32B1D511F77562485A76EF2B6D0B3A271FD8 ();
// 0x0000010C System.String Minigame_Replace::get_word_eng(System.Int32)
extern void Minigame_Replace_get_word_eng_mCF980C5CBDA977F7F949E6135CBB0AF06B9CD3EC ();
// 0x0000010D System.Void Minigame_Replace::start_game()
extern void Minigame_Replace_start_game_m8F064BD195244B68555FD5611713A5354C3516D0 ();
// 0x0000010E System.Void Minigame_Replace::.ctor()
extern void Minigame_Replace__ctor_m60851BEAD0B0D908F2351DC80E1772D7F77739BA ();
// 0x0000010F System.Void Minigame_Select_1::Start()
extern void Minigame_Select_1_Start_m4468D5A62272AD3309F674A6CF312869920F3E78 ();
// 0x00000110 System.Void Minigame_Select_1::FixedUpdate()
extern void Minigame_Select_1_FixedUpdate_m369BDB93B6F2C4512F93D4225F2494940DA2AE53 ();
// 0x00000111 System.Void Minigame_Select_1::setup_gridView()
extern void Minigame_Select_1_setup_gridView_m01A47EE2DA79AF6111BE50B1CE365762398CC329 ();
// 0x00000112 System.Void Minigame_Select_1::.ctor()
extern void Minigame_Select_1__ctor_m8B6FC13CB230D326D644FC03BF606869B4D34753 ();
// 0x00000113 System.Void Minigame_Select_2::Start()
extern void Minigame_Select_2_Start_mD1AC1782B4518E273B340D2923ED23DF853300D4 ();
// 0x00000114 System.Void Minigame_Select_2::FixedUpdate()
extern void Minigame_Select_2_FixedUpdate_m3A38414D45286DC8390660AD3E99D8AFFFB0C2DA ();
// 0x00000115 System.Void Minigame_Select_2::setup_Minigame_Select_3()
extern void Minigame_Select_2_setup_Minigame_Select_3_m1CEE9F6826549028611523E1C7BF4FF66650DC34 ();
// 0x00000116 System.Void Minigame_Select_2::setup_gridView()
extern void Minigame_Select_2_setup_gridView_mCE6698811EA7EF44EA2A88E42F9414921164F822 ();
// 0x00000117 System.Void Minigame_Select_2::do_move_anim()
extern void Minigame_Select_2_do_move_anim_mA30678F74D34B37645B1438815FE0623CEFB10E8 ();
// 0x00000118 System.Void Minigame_Select_2::do_pause_anim()
extern void Minigame_Select_2_do_pause_anim_m9E1B534717CF7CE2BBBA5AF71BA358CA7458B2EF ();
// 0x00000119 System.Void Minigame_Select_2::generateQuestion_and_setAnimation()
extern void Minigame_Select_2_generateQuestion_and_setAnimation_m6320555D162A59DEA628AE1148559A4B21FF3F30 ();
// 0x0000011A System.Void Minigame_Select_2::confirm_clicked()
extern void Minigame_Select_2_confirm_clicked_mE4265C3D0A6CD0C569A571A4362B9F12F16B9C12 ();
// 0x0000011B System.Void Minigame_Select_2::judge_clicked(System.Boolean)
extern void Minigame_Select_2_judge_clicked_m724D2F88BE26CA0428B1B38C28616D56C3F733F7 ();
// 0x0000011C System.Void Minigame_Select_2::.ctor()
extern void Minigame_Select_2__ctor_mD628721A8BA4A06D8C0D491B5F1C71FBA06F2E03 ();
// 0x0000011D System.Void Minigame_Select_3::Start()
extern void Minigame_Select_3_Start_m508A7E91F27BE33B12DEE4D6B40627E233D44903 ();
// 0x0000011E System.Void Minigame_Select_3::FixedUpdate()
extern void Minigame_Select_3_FixedUpdate_mCBB9AD0C35622C4AEF0681319553B79D6295E087 ();
// 0x0000011F System.Void Minigame_Select_3::setup_Minigame_Select_3()
extern void Minigame_Select_3_setup_Minigame_Select_3_m3C29A5FBC1ADDAE2C5C342D720FEE12AD197F10A ();
// 0x00000120 System.Void Minigame_Select_3::do_move_anim()
extern void Minigame_Select_3_do_move_anim_m1ADA20D3CAE7F61B77D229DF5F9C7778E5764C10 ();
// 0x00000121 System.Void Minigame_Select_3::do_pause_anim()
extern void Minigame_Select_3_do_pause_anim_m8A312B0A60154FE76E11FA4E0F75E80E761674F5 ();
// 0x00000122 System.Void Minigame_Select_3::generateQuestion_and_setAnimation()
extern void Minigame_Select_3_generateQuestion_and_setAnimation_m8DDE1B33EFE655F8D23FC0113BDC18BCCA5FB150 ();
// 0x00000123 System.Void Minigame_Select_3::confirm_clicked()
extern void Minigame_Select_3_confirm_clicked_mBAAC6688896EF2B9E49CB7380D3BD36118ECBEC0 ();
// 0x00000124 System.Void Minigame_Select_3::judge_clicked(System.Boolean)
extern void Minigame_Select_3_judge_clicked_m2EE9FD0A158F2F57934AB67B6B9A870C439F2FC1 ();
// 0x00000125 System.Void Minigame_Select_3::.ctor()
extern void Minigame_Select_3__ctor_m3448BFD1510E104950F64717BEA01B4D10884AEC ();
// 0x00000126 System.Void MinigameData::.ctor()
extern void MinigameData__ctor_m9B753574783B1EB79561BEEC8EF516BF7A5C34CD ();
// 0x00000127 System.Void StoryData::.ctor()
extern void StoryData__ctor_m4F857607C65B6807BA3F6CFCEDE42B9F1A006593 ();
// 0x00000128 System.Void ProgressBarController::setup(System.Int32)
extern void ProgressBarController_setup_mCB1D34288242A429580F3F5CC398C71F2AF01F12 ();
// 0x00000129 System.Void ProgressBarController::set_prgress(System.Int32)
extern void ProgressBarController_set_prgress_mD824D57061A5113FBF5108ECA54C5094DFB84C2E ();
// 0x0000012A System.Void ProgressBarController::progress_add()
extern void ProgressBarController_progress_add_mB03C011D65AC7484EAE02A8ABC2211BDD236352D ();
// 0x0000012B System.Void ProgressBarController::.ctor()
extern void ProgressBarController__ctor_m22A11E98EB0E4002D6D57A103AE0F72A480F89FE ();
// 0x0000012C System.Void RegisterController::Awake()
extern void RegisterController_Awake_m5B95FDA20AFB84556BDFADE8B9AD8AF3E5E90F0E ();
// 0x0000012D System.Void RegisterController::Start()
extern void RegisterController_Start_mB19B92CC7BF1961B627B23894434CB7666EE609A ();
// 0x0000012E System.Void RegisterController::Update()
extern void RegisterController_Update_mC5087B4BD4A83A2161914BBC882CB68FE37546E3 ();
// 0x0000012F System.Void RegisterController::genderClicked(System.Boolean)
extern void RegisterController_genderClicked_m7D913A0AF4B5DF534BC662BDDB29543FD2652416 ();
// 0x00000130 System.Boolean RegisterController::checkInput()
extern void RegisterController_checkInput_mF0FAC4E13320C9E4088FEE25F0CDFD8077942738 ();
// 0x00000131 System.Void RegisterController::closeClicked()
extern void RegisterController_closeClicked_mBEF5DE052076578D41C21DB6631208DCBA6B14B4 ();
// 0x00000132 System.Void RegisterController::confirmClicked()
extern void RegisterController_confirmClicked_m5FC879624550CEEFB3A4AA791BF41BC1ECD06789 ();
// 0x00000133 System.Void RegisterController::do_register()
extern void RegisterController_do_register_m56ABA6500CCA2478EC10C3DFB089E8B8D0F92988 ();
// 0x00000134 System.Void RegisterController::setButtonsInteractable(System.Boolean)
extern void RegisterController_setButtonsInteractable_m050947B95D357741C062A3BDD12A22E0036B5950 ();
// 0x00000135 System.Void RegisterController::.ctor()
extern void RegisterController__ctor_mC180EE13451B04B44C077E3730DAA4A094E3B508 ();
// 0x00000136 System.Void RegisterController::<do_register>b__20_0(Models.User)
extern void RegisterController_U3Cdo_registerU3Eb__20_0_m6D950A96F1A233CAE641774FA1D73DA1E986CEDD ();
// 0x00000137 System.Void RegisterController::<do_register>b__20_1(System.Exception)
extern void RegisterController_U3Cdo_registerU3Eb__20_1_m3AF7152C6140C51C5ACA8267F2CB8A15399C4037 ();
// 0x00000138 System.Void ScoreBoardController::set_score(System.Int32)
extern void ScoreBoardController_set_score_m25A29BA2FEF6F3FF10F647B68F89D5E41801A1A4 ();
// 0x00000139 System.Void ScoreBoardController::add_score()
extern void ScoreBoardController_add_score_mE371C7B6C29AB6C24454CCC12BFC9A6554EE149D ();
// 0x0000013A System.Void ScoreBoardController::do_add_score()
extern void ScoreBoardController_do_add_score_mD8106AB057DE8AA1A63AF2560F53329BE4899FA6 ();
// 0x0000013B System.Void ScoreBoardController::.ctor()
extern void ScoreBoardController__ctor_m4673D5DA5884829DEA5BD4041DCE1806B4450631 ();
// 0x0000013C System.Void SelectionObject::setup(Selection,UnityEngine.Sprite)
extern void SelectionObject_setup_mF6FB575CE7C7612A444C055505DFC50F27586DC2 ();
// 0x0000013D System.Void SelectionObject::showCoveredText()
extern void SelectionObject_showCoveredText_m8C1A67A3C531E646A037404F0596D6FF624432FF ();
// 0x0000013E System.Void SelectionObject::setSprite(UnityEngine.Sprite)
extern void SelectionObject_setSprite_m8B8FC6EF4E0EB866F6E0C5B6B52D00794A928ED1 ();
// 0x0000013F System.Void SelectionObject::showSprite(System.Boolean)
extern void SelectionObject_showSprite_m9FDEA2F158928A94BCE2FABC6ADE09CC861E7299 ();
// 0x00000140 System.Void SelectionObject::.ctor()
extern void SelectionObject__ctor_m7D87658FB8D67C607A47AA079E3A6F39075D4205 ();
// 0x00000141 System.Void Word::.ctor()
extern void Word__ctor_mF8464EF52C9F984F8365366F1A6459B7285ADEE5 ();
// 0x00000142 Word[] Word::process_Words(System.String)
extern void Word_process_Words_m0A72B0D380151194A7ACC19F8BF25836709403E6 ();
// 0x00000143 System.Void Selection::.ctor()
extern void Selection__ctor_m16E6DADCDF18AA6090CD83EEB1AE1A8265FFE8A3 ();
// 0x00000144 System.Void Selection::.ctor(System.String,Word[],System.Boolean)
extern void Selection__ctor_m972BB9B0B67A219CCEE4B5DAEF6AFC771FC91601 ();
// 0x00000145 System.Void Question_Tell::.ctor()
extern void Question_Tell__ctor_m3BC26B40527DC9771344C580A2A317FD79B9E177 ();
// 0x00000146 System.Void Question_Same::.ctor()
extern void Question_Same__ctor_m475211008883E416EB265AACF338A466B6D9A312 ();
// 0x00000147 System.Void Question_Different::.ctor()
extern void Question_Different__ctor_mEAE745CBD226DE5C276E54AAFB7EA987D21064CB ();
// 0x00000148 System.Void SettingController::Awake()
extern void SettingController_Awake_m150964C5DC48A05C5C6CC9AD4C4B2893D51249BE ();
// 0x00000149 System.Void SettingController::Start()
extern void SettingController_Start_mFF2A2A98C7658D187B675AF0AC32E4BA37394F6B ();
// 0x0000014A System.Void SettingController::setup_sliders()
extern void SettingController_setup_sliders_m416AA95143A1756DBDB8A453E82901015E7A92DB ();
// 0x0000014B System.Void SettingController::save_sliderValues()
extern void SettingController_save_sliderValues_mDD26D753ACF103A4B707E8C63B448C28291D8C61 ();
// 0x0000014C System.Void SettingController::sliders_changed()
extern void SettingController_sliders_changed_mE5C5EA85BE101986A4E92730A1344AFC003C3636 ();
// 0x0000014D System.Void SettingController::.ctor()
extern void SettingController__ctor_mA8AA8CB0AF797CBECDB0FFB2AFE2BF6A1318D3E8 ();
// 0x0000014E System.Void StorySlideController::Start()
extern void StorySlideController_Start_mE2E46B96D3938208D577EEBB6BEB7F96CB109D5A ();
// 0x0000014F System.Void StorySlideController::Update()
extern void StorySlideController_Update_mC31181CE6172A625119E98E7EDD92126D4F2D121 ();
// 0x00000150 System.Void StorySlideController::call_nextSlide()
extern void StorySlideController_call_nextSlide_m87F8430AB8BED81E049C03C03AE648844C80F2A0 ();
// 0x00000151 System.Void StorySlideController::call_previousSlide()
extern void StorySlideController_call_previousSlide_m6C2B970CD0881B888CFCFC8A1EF4B74293F40B20 ();
// 0x00000152 System.Void StorySlideController::.ctor()
extern void StorySlideController__ctor_mC28A0EE9DC7CDD54CF4AB6B640F0901EB7F8FC76 ();
// 0x00000153 System.Void TimeLimitedController::Start()
extern void TimeLimitedController_Start_m2C7C11D2C30EA9B8893E22D247A8864FC9775750 ();
// 0x00000154 System.Void TimeLimitedController::did_end_time()
extern void TimeLimitedController_did_end_time_m2E6AE05485BB803AE6095A02207562D4E52B8AF5 ();
// 0x00000155 System.Void TimeLimitedController::.ctor()
extern void TimeLimitedController__ctor_m1B68715926A2B7AF738FF63847908AFBB8DF444E ();
// 0x00000156 System.Void TimerController::Start()
extern void TimerController_Start_m496216FB4A17FC12489A48F8871D2BC2DA10B80A ();
// 0x00000157 System.Void TimerController::Update()
extern void TimerController_Update_m339A914063B02EB04A70CB576CE20CE7F7458163 ();
// 0x00000158 System.Void TimerController::setup(System.Int32)
extern void TimerController_setup_m9DC3BDD7669B98399EBE3D902E5B14AFC834F010 ();
// 0x00000159 System.Void TimerController::startCountdown()
extern void TimerController_startCountdown_m5D076F0D9213B19F584FEFB57D50A1FB7AA0B79A ();
// 0x0000015A System.Void TimerController::pauseCountdown()
extern void TimerController_pauseCountdown_mEF6FE5059DE10E4190DB904FD02A5DB2D0833F3F ();
// 0x0000015B System.Void TimerController::.ctor()
extern void TimerController__ctor_m929586B0B39C6399C05B9B9EE33F7C03C6618BC3 ();
// 0x0000015C System.String Models.GameAnswer::ToString()
extern void GameAnswer_ToString_m840AF72CF2659D1EFC2DCEA0B2C3ACD9CE2366BE ();
// 0x0000015D System.Void Models.GameAnswer::.ctor()
extern void GameAnswer__ctor_m67CDDD5A963F68F8201C1FE2FE2A2468BAF6EF16 ();
// 0x0000015E System.String Models.GameProgress::ToString()
extern void GameProgress_ToString_mEC37BAE8FB736A29F23583051BB02FA1F046481D ();
// 0x0000015F System.Void Models.GameProgress::.ctor()
extern void GameProgress__ctor_m39A768BF00BB7442246629E960B8B38D78392F8C ();
// 0x00000160 System.String Models.GameTime::ToString()
extern void GameTime_ToString_m14A83635B8F46DBA276955753C91A62164C3691D ();
// 0x00000161 System.Void Models.GameTime::.ctor()
extern void GameTime__ctor_mA83F5B3079B683405341BE0883B2AB005B14F1B1 ();
// 0x00000162 System.String Models.User::ToString()
extern void User_ToString_m32C56202AB63DDD85E5FFA39AAC30A8A1FB93BA6 ();
// 0x00000163 System.Void Models.User::.ctor()
extern void User__ctor_m7E99B614CF1352555325351DAD7FA08FD3909DCA ();
// 0x00000164 System.Int32 RSG.IPromise`1::get_Id()
// 0x00000165 RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x00000166 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000167 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x00000168 System.Void RSG.IPromise`1::Done()
// 0x00000169 RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x0000016A RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x0000016B RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x0000016C RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x0000016D RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x0000016E RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000016F RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000170 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000171 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000172 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000173 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000174 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x00000175 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000176 RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000177 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000178 RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000179 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x0000017A RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000017B RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000017C RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x0000017D System.Void RSG.IRejectable::Reject(System.Exception)
// 0x0000017E System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x0000017F System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x00000180 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x00000181 System.Int32 RSG.Promise`1::get_Id()
// 0x00000182 System.String RSG.Promise`1::get_Name()
// 0x00000183 System.Void RSG.Promise`1::set_Name(System.String)
// 0x00000184 RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x00000185 System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x00000186 System.Void RSG.Promise`1::.ctor()
// 0x00000187 System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x00000188 System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x00000189 System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x0000018A System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x0000018B System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x0000018C System.Void RSG.Promise`1::ClearHandlers()
// 0x0000018D System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x0000018E System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x0000018F System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x00000190 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x00000191 System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x00000192 System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x00000193 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000194 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x00000195 System.Void RSG.Promise`1::Done()
// 0x00000196 RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x00000197 RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x00000198 RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000199 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x0000019A RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x0000019B RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x0000019C RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000019D RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x0000019E RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000019F RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000001A0 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001A1 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001A2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000001A3 System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000001A4 System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x000001A5 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001A6 RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001A7 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x000001A8 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000001A9 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001AA RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001AB RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x000001AC RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000001AD RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x000001AE RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x000001AF RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x000001B0 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000001B1 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000001B2 RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x000001B3 System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x000001B4 System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x000001B5 System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x000001B6 RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x000001B7 RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x000001B8 RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x000001B9 System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_mF03EB8E9CE6B9CF884725E1A97A2CFB5C1BD48F5 ();
// 0x000001BA System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m9FB8F5C40E7B5AD572CEEB797FBE3938ECC56A12 ();
// 0x000001BB System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_mFE953BCE9224945D264E92CDB5FA552D2FF0CB2C ();
// 0x000001BC RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x000001BD RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x000001BE RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x000001BF System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x000001C0 System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x000001C1 RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_m8412D2C5990A2FBB010B67B1EF94960317D2CE72 ();
// 0x000001C2 RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m9FF70ED4587C8EDDFC0D5F445F6BB7EA44CC78C2 ();
// 0x000001C3 RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m75EC23820851DCC0C7C49B061BB2B5F2C4BD04D0 ();
// 0x000001C4 System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_mB3EABB157CC031CE655D2F83068DF650CF5F6C81 ();
// 0x000001C5 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_m472F369396FC318965681B6A7CD98D22A92B3E22 ();
// 0x000001C6 System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_mD3B803E765306098FE2FAD00CFC47FBF0B8127B8 ();
// 0x000001C7 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_m518F9D576BA81BDA44AE922199C6F4F5029A9814 ();
// 0x000001C8 System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_mA04F6931B30C76E74FB87E764AC4FFD5DCC11B49 ();
// 0x000001C9 System.Int32 RSG.IPromise::get_Id()
// 0x000001CA RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x000001CB System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x000001CC System.Void RSG.IPromise::Done(System.Action)
// 0x000001CD System.Void RSG.IPromise::Done()
// 0x000001CE RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x000001CF RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000001D0 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x000001D1 RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x000001D2 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000001D3 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000001D4 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x000001D5 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000001D6 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001D7 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000001D8 RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001D9 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001DA RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x000001DB RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000001DC RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000001DD RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x000001DE RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000001DF RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000001E0 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x000001E1 System.Int32 RSG.IPendingPromise::get_Id()
// 0x000001E2 System.Void RSG.IPendingPromise::Resolve()
// 0x000001E3 System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x000001E4 System.Int32 RSG.IPromiseInfo::get_Id()
// 0x000001E5 System.String RSG.IPromiseInfo::get_Name()
// 0x000001E6 System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m359F24318D921B164DCE03B853505F366192B62E ();
// 0x000001E7 System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_m63D0BBF0DE96140F87BABADFA6367E9678B420FC ();
// 0x000001E8 System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_m89846D7BA324C3D5FF463016A076BFD88F7DE6ED ();
// 0x000001E9 System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_mBC5DDDAB7103CD4B2D7930E5226F8584441D4701 ();
// 0x000001EA System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m0473C8E9F3D670601D9761BD229833F01DCBD892 ();
// 0x000001EB System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_m75911C604D3D1E70DEA44687755A6481553558ED ();
// 0x000001EC System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m830859BC68EF160D9F61904117DBCA0725C07F57 ();
// 0x000001ED System.String RSG.Promise::get_Name()
extern void Promise_get_Name_mCF196DE952307DB05A83EE49C791D09869A08EA5 ();
// 0x000001EE System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_mCE385D3759A515925145B96030786C9B14510175 ();
// 0x000001EF RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_m6BBB58E657D13BF0CD04C9655F6F21B9ED43C41D ();
// 0x000001F0 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_mAF0DC100A33BF72C66E61A3023CBC7369BAFD661 ();
// 0x000001F1 System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m93DB3381EEAC4C29D49E9651801FBED07208AE7F ();
// 0x000001F2 System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_mF24B852646A4C43475D8E5099381627C427B586D ();
// 0x000001F3 System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_mB2BEBFBDB7C5D345707CB4707707AA3BE413FB68 ();
// 0x000001F4 System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m35C60C296FEDDFEB0E51A000ACD26E57DD29DF1C ();
// 0x000001F5 System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m952A85C20177C5642FF0E9856B77926C9AAA443A ();
// 0x000001F6 System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_mC5B25B11324ABB848AF8F5E84CBA079895A16A7F ();
// 0x000001F7 System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mFD81E39BD9AC3129DEAF1B7F550E47A7D3BFA196 ();
// 0x000001F8 System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_mAD5D536AC291D012A34445143678C0C26AF07561 ();
// 0x000001F9 System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m108B1623CE8E85AD2FB979CE3D4AAB75AB13CC7E ();
// 0x000001FA System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_m8593EBA08219023BF4B146D6E68166030CE5B452 ();
// 0x000001FB System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m89D97A64CA3C5B6CA39A6C1EAA4FE3586F992B68 ();
// 0x000001FC System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m664E671FB00D803F07B0A7427E3298F800BE8FD1 ();
// 0x000001FD System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_m0440A2D41D00D7F51F6FD7E1A8D252895E25BF4F ();
// 0x000001FE System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_m01D6E7306276BF06D642E997243D6AD2ACF326CF ();
// 0x000001FF System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_m59D1433DF29C576911F452840EDB571548835A3A ();
// 0x00000200 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m6D514945624B2662DB5DC6A4C3D6E680AC355089 ();
// 0x00000201 System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_m832048ABFC33D1BC29CE0A0231015027D85C3289 ();
// 0x00000202 System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_m88E1CFF52436F0417A0558EA121FE7C4376E45AC ();
// 0x00000203 System.Void RSG.Promise::Done()
extern void Promise_Done_m353F8D2F2949E07CFA7885E3A31B909567CDFB80 ();
// 0x00000204 RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m37E0038809F81F116B01F7D207D7BF0EF56CB32B ();
// 0x00000205 RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_mFA95CEAFDDF70B8126F07202236BBA6CE5EF6351 ();
// 0x00000206 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000207 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_mEABC93568E27F2D1DABF8C25D8AD0429B8A33AAA ();
// 0x00000208 RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_m8078BFDE1C12539AB4C525043DE09AF6BD1F7114 ();
// 0x00000209 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000020A RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_mA06445A003F509C82BA059519A1B5384C56E88D8 ();
// 0x0000020B RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_mB2C2921F69AE85A4416EC3702245186AF1ED009B ();
// 0x0000020C RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000020D RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m834055F7479F392C55074DD054A88488CB7B95E0 ();
// 0x0000020E RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m076C9BEC0083BFC6D0D983371F31DEC78A7DF73D ();
// 0x0000020F System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m021FB595899D1438A462754976F48E5E3AEAB1D0 ();
// 0x00000210 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m5F31C1A1CDD46F46CC51A611D9671A37D0DB2924 ();
// 0x00000211 RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m792F1F92799BFC664B0F4E52B22DF43F5E7BB660 ();
// 0x00000212 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000213 RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m82839CDADB53DD91569988C65EC287EED839ECF4 ();
// 0x00000214 RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_mB56B9EF290A3A4757AC72F12E2762927B9CECCAD ();
// 0x00000215 RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_m32CBED7E921C74273C0BCCC2CACC52E1FAD3D4C6 ();
// 0x00000216 RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_m254CD5AF651DF447FDDB39C519D93E06C315341C ();
// 0x00000217 RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_m8F1D0A45B6025929F3E0487D8B6D02D356C33A0B ();
// 0x00000218 RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_m2C6DAA1F0BB7F9B43E4B1ABA154C4E0E8F51B984 ();
// 0x00000219 RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000021A RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m78DEF2E794FF3713EB41095CB056B7DE90F61329 ();
// 0x0000021B RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_mD1F684B8E4BFCC4D93855F2F424924C418B9B1FA ();
// 0x0000021C RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m68DB50577F8EEF1CA2536EDDB26928A5A85DE6A3 ();
// 0x0000021D RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_mC5D063790E663AB3316D5F04B42C8391DF3CBFD2 ();
// 0x0000021E RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_m19FFF998F5923B3853C81D1F2E62FF3351FDD17D ();
// 0x0000021F RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_mBE702F11D902A0425F8B6B8FC47FEB2368A216A1 ();
// 0x00000220 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000221 RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_m9FAD5899A128C5FE45441510B8D9279C2B9D68C0 ();
// 0x00000222 System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_m686471F60398EBD66ED81FA0C8F271425D395C9B ();
// 0x00000223 System.Void RSG.Promise::.cctor()
extern void Promise__cctor_m7D11D8D35308AB1BF00A1EEA1D73C7B2A2BF36BA ();
// 0x00000224 System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise_ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mEA9C3917D21BE93956790DE4FB5AE8F187338D40 ();
// 0x00000225 System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_mCEA4BB2C7A81AD2F6FDC39AADF8EADC75A9FE9A4 ();
// 0x00000226 System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mF316DF93F6B59969F89BBE93BA44836FC8788240 ();
// 0x00000227 System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m35FDB6387B276AB86787B30636AD0B5CFFB157F2 ();
// 0x00000228 RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x00000229 RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x0000022A RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x0000022B System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_mA23590A932F2B85BC150832BC7F706546FB25D47 ();
// 0x0000022C System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x0000022D T1 RSG.Tuple`2::get_Item1()
// 0x0000022E System.Void RSG.Tuple`2::set_Item1(T1)
// 0x0000022F T2 RSG.Tuple`2::get_Item2()
// 0x00000230 System.Void RSG.Tuple`2::set_Item2(T2)
// 0x00000231 System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x00000232 T1 RSG.Tuple`3::get_Item1()
// 0x00000233 System.Void RSG.Tuple`3::set_Item1(T1)
// 0x00000234 T2 RSG.Tuple`3::get_Item2()
// 0x00000235 System.Void RSG.Tuple`3::set_Item2(T2)
// 0x00000236 T3 RSG.Tuple`3::get_Item3()
// 0x00000237 System.Void RSG.Tuple`3::set_Item3(T3)
// 0x00000238 System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x00000239 T1 RSG.Tuple`4::get_Item1()
// 0x0000023A System.Void RSG.Tuple`4::set_Item1(T1)
// 0x0000023B T2 RSG.Tuple`4::get_Item2()
// 0x0000023C System.Void RSG.Tuple`4::set_Item2(T2)
// 0x0000023D T3 RSG.Tuple`4::get_Item3()
// 0x0000023E System.Void RSG.Tuple`4::set_Item3(T3)
// 0x0000023F T4 RSG.Tuple`4::get_Item4()
// 0x00000240 System.Void RSG.Tuple`4::set_Item4(T4)
// 0x00000241 System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_mB91AE2B45F7CC73C6DCA81CB98112845FA4409E8 ();
// 0x00000242 System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_mBC58F51E1B9AC68089E2B1ADA370E6947A66EC31 ();
// 0x00000243 System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m003442558D9CD25D706CCD0B8E9C2BE0A317CB59 ();
// 0x00000244 System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m5B99DA75AF22A642A7DBF73015B2760F1D65B869 ();
// 0x00000245 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mF799A9CCC8CF7A46CD4D912A2B42E6704893E06A ();
// 0x00000246 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m278DF7EBD7800BCF611AF38606A845C59273B993 ();
// 0x00000247 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x00000248 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x00000249 System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x0000024A System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_m73576498BB671104F1924185B7AFDED86F738BC1 ();
// 0x0000024B System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_m4A36485992F5F58F8289087494D63DFE0CCBD533 ();
// 0x0000024C System.Void Proyecto26.RestClient::CleanDefaultHeaders()
extern void RestClient_CleanDefaultHeaders_m80B5F4367E694ABB283F5471692C45D92997A3BC ();
// 0x0000024D System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_m77411140A0B8D05FF504D3719B2D4AC80DAD6011 ();
// 0x0000024E System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000024F System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m9D04DEA397B934DEAF99EE63660399AF288E281B ();
// 0x00000250 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m1F189EE04932CF41EB403312DABAE3DC640D0947 ();
// 0x00000251 System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000252 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000253 System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000254 System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000255 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mAE9ED3C9408189D921AAA1D3B1F0E84FB9B15A3C ();
// 0x00000256 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m31BA981121E787A6AA1E4056E4F6BB3CAC5C38BF ();
// 0x00000257 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mCFC824D62A526DF3454565BEA0D3E0EC3478785F ();
// 0x00000258 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000259 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000025A System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000025B System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m09EA31A50DCC7963D03FDB086F4BB887C7D56A08 ();
// 0x0000025C System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m1C2F4AFC5DBD84B3E7B9E6B75E3BE71221C74E2D ();
// 0x0000025D System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_mE19F5C2E68BA18C3E138976FBBAD882BE0F2A431 ();
// 0x0000025E System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000025F System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000260 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000261 System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_mAA28A9FAD6A8683C444E4A6020D1ECA1E51D984F ();
// 0x00000262 System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m78568521DA2274A69750EEB00D4A227B5789DD30 ();
// 0x00000263 System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m639942A37D623080C9D7B373EF4920F62953E587 ();
// 0x00000264 System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_mB16B5D694F1A3E19012D3BF5E2690DB6AAD77CE6 ();
// 0x00000265 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_m0BA554BC9023E4726CE1777F868B0FD55F623206 ();
// 0x00000266 RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x00000267 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_m67CE396FE5FCDF6A8BB4BE0D154D6956D7D3787E ();
// 0x00000268 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_m0D9E2C1EB049974F78BE7BA8F92063BC842AAB88 ();
// 0x00000269 RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x0000026A RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x0000026B RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x0000026C RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x0000026D RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_mB1911EA9ED87562F73D651395C98195ED8D556F3 ();
// 0x0000026E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_m0B0551A7A2466485EEA56C6487DC59FA3514462B ();
// 0x0000026F RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_mC632B22BFA78D0B3C346CC608EAC730173AB69B3 ();
// 0x00000270 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x00000271 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x00000272 RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x00000273 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_m02F26FA57C32EA1D57715FEB0B12E2E3D12FC971 ();
// 0x00000274 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m999201C12FDCF2380BCBAF4EF55CDC777A38F84F ();
// 0x00000275 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_mF3DFA649A055F6C9B3ADB56226F63089F279DACE ();
// 0x00000276 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x00000277 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x00000278 RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x00000279 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m434271AAFAC5AAA7214074653C7479B4F7022F60 ();
// 0x0000027A RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m6AA933C3F377A10BBB0E7E7A848B2A53C7AEFD8D ();
// 0x0000027B RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_m29C35E12D8F6398DAF346470AF1BF472DFB105E2 ();
// 0x0000027C RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_m559D8243A556974E9D847DB3E3128F051A666564 ();
// 0x0000027D System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x0000027E System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x0000027F System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_mF250059013E350E0C8DA48AF5AA8035F482DC528 ();
// 0x00000280 UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_m275DB6EB32A338391A7422933B486595D82DB9D2 ();
// 0x00000281 Proyecto26.RequestException Proyecto26.HttpBase::CreateException(UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_m194CF2E26B1376590E2E20D91A012AF5556DE4F3 ();
// 0x00000282 System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_m8B106A424CDEAEEF6C414D29836973BED9491463 ();
// 0x00000283 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_m39FF5B3AED81F8EF18159E085321E6BE8ACBB39D ();
// 0x00000284 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x00000285 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x00000286 System.Collections.IEnumerator Proyecto26.HttpBase::SendWebRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void HttpBase_SendWebRequest_m1F4B19B32BBDCC28D0ADD4DFF2AD4FF4C2E86C5A ();
// 0x00000287 T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x00000288 T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x00000289 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x0000028A System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x0000028B System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_m6F5B175FD7F371D7C8B5452F3F180C9BD40838E8 ();
// 0x0000028C System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_m3A0E4DA9CB25A2C2BD7396CC9ACF8D151F49093F ();
// 0x0000028D System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_m28F83E20065738CD7ED5B79350D4A2587B7D7D4A ();
// 0x0000028E System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_m7F784979F60FCD85F411E1726F5807E2DAE5D081 ();
// 0x0000028F System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m30BFC33E4BCA4FC7BD9F7D4BCDB249C4267A4846 ();
// 0x00000290 System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mBDA182B36C645A65F32F15C74DDE06AB9483CCE4 ();
// 0x00000291 System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_m8453E139F1B8E33C6AB6BC426BAF4568C176A88D ();
// 0x00000292 System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m7E86E1CB3013133D8C579476B7FA39B136A6460B ();
// 0x00000293 System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_m8C986C735E11B633EB4D30816AB0117EDA3FEB82 ();
// 0x00000294 System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_m4845F1344B303A08A44897B8791E86F9A0897225 ();
// 0x00000295 System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_mA3553611EDEBD35D428B0FBD28CFE0E3C8833E32 ();
// 0x00000296 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m1FCAFE2A3491DC22C4A2D9069CA8A20D5F7F8F9F ();
// 0x00000297 System.Void Proyecto26.RequestException::.ctor(System.String,System.Object[])
extern void RequestException__ctor_mA1DE75816294F39B5DFB4921FF8D1C5C8F73A184 ();
// 0x00000298 System.Void Proyecto26.RequestException::.ctor(System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_m82EE05E9A81904EE7195C9E5D3702FE8ED8C2870 ();
// 0x00000299 System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_m7047BABE1656F650A3E58440071D39E6358BBBCD ();
// 0x0000029A System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_m6E2922D5CBDA4F030EF21D0F1B6E699C9C633181 ();
// 0x0000029B System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_mDBA0530E15674F8879E8701A98C9B9F77D24C080 ();
// 0x0000029C System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_mE2D6611FC528306E7D7082CF64AF8487FF819145 ();
// 0x0000029D System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_mBA3C18F541933DEBD3CC581C8ACFF4B8D2F4F0D2 ();
// 0x0000029E System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m433497F39CA7EFD389E4279E5EAA193CD20727DF ();
// 0x0000029F System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m48E4781621899E33C88E7196660BC1B1CA5C8045 ();
// 0x000002A0 System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_mC41C456244C97F162E28923F7C1DCBE5092A28D0 ();
// 0x000002A1 System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m98668878BAC82EB76FF3096174A7A9DC72A5256F ();
// 0x000002A2 System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_mD198470BD3EB1111781DACF9CA4AA0B30FB11E3D ();
// 0x000002A3 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_mCBFD1D92C750D50AE122FB01F831862390C1F37A ();
// 0x000002A4 System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m006234BB2B771FDAA02519B9786569C1D7F3F7AE ();
// 0x000002A5 System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_m61D38DF3A1CA06F73B9426B17BAE72C43DF7606F ();
// 0x000002A6 System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_m544341AC1AC74CA96F491CFA07D5373CEB820AC0 ();
// 0x000002A7 System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_mB4E2AA7AD1E2EEDB89632B199CD5123EDD773594 ();
// 0x000002A8 System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m78DC002BEAA7B5B66EAA9D6D22F927DA1B726962 ();
// 0x000002A9 System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_mB277C902129CF2BFC2CC6D48CC4B72ADC08F8809 ();
// 0x000002AA System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_m9BF0BC026039D48FFDAF69FE5237D7931339CAC2 ();
// 0x000002AB System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_mC17EB6DA6D24F71FEF672E1A024CED74540E083A ();
// 0x000002AC System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_m66950AE73265F6ACDD858663EF2C4C9C81886DC9 ();
// 0x000002AD System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_m0B4B78D687DFAF5082865206E0C6E269AE332B4F ();
// 0x000002AE System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_mEF93B1B869A6675E019987880BDC07F561EE437E ();
// 0x000002AF System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_ChunkedTransfer()
extern void RequestHelper_get_ChunkedTransfer_m5C809E092B3FAE32F3CDF2B529A4C84916605445 ();
// 0x000002B0 System.Void Proyecto26.RequestHelper::set_ChunkedTransfer(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_ChunkedTransfer_mCCEDBBB93AC79320B91DCA044EF46A8B2278F3B9 ();
// 0x000002B1 System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_mDB2E3AB378AE5D40728CDDF202C6C796EB5F6E28 ();
// 0x000002B2 System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_m0A952F887927E26CA2178A1E562D6CED387A076B ();
// 0x000002B3 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_mFE6B3D6DE8F20BF62583BB6D93CC246DDFC545C2 ();
// 0x000002B4 System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_m4674912A7CD2BD48E770EC75F6D19C7160A1014A ();
// 0x000002B5 System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m956B4E7736CC62E1592207ED65CAEB3E7B8859CF ();
// 0x000002B6 System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_m402DD8BF79EBB0B2B0BC56DE9D1D41B2FC6D418B ();
// 0x000002B7 UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_m0207D1A53E755E7031AC4C4B5B8D511071F4F478 ();
// 0x000002B8 System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_m3AF0C050E7AD128B8D7A947043D7548CAC2A6EBE ();
// 0x000002B9 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_mC51EC658CC52DE0D89F5720F94A6E7E2726CD82B ();
// 0x000002BA System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_mD5EDA0B445F702562BFDC3078B67CDCE7109D548 ();
// 0x000002BB System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_m3C759886C38B35F03F6DCAB64DA6DEB52F5678FD ();
// 0x000002BC System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_mF6BD0288F15A8FB9B219ADE963DCD6A6280574CF ();
// 0x000002BD UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_m7D5D164ACD676DC4B29297D75A4EC3E191F9A5E5 ();
// 0x000002BE System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m7D0ACC92FA6698CE156FF64A7A23F934190CBD2D ();
// 0x000002BF UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_mC39A192F8F8205937A2E3429A3585B23C6179815 ();
// 0x000002C0 System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mA8317AC94BB2DB1D9D4DB733F403CCA1514C8E52 ();
// 0x000002C1 UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m16A51AA814F360D0B86C9AD5AB1487931049603B ();
// 0x000002C2 System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_m82CED39A618786A2E78012A9A854ED26651BDC03 ();
// 0x000002C3 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_mAD303FE22CC5ADD010B2F619334B75EB515FA5EA ();
// 0x000002C4 System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_mF83A88C5AC2BAFA16854EB612512B8AAF811A174 ();
// 0x000002C5 System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m9B8077CD6671F1E401A80121D570762CF020C804 ();
// 0x000002C6 System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m25284A06292160526A29BDD7EB164AA42C75BC08 ();
// 0x000002C7 System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_m709EEA6072D4B2372E0853EF87BB8740B3FD0F56 ();
// 0x000002C8 System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_mDB750F682594960D558F0B01818693579B8FC9BB ();
// 0x000002C9 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_mC869E47DC8EEEFF9894D23786D2BF266FAC6E254 ();
// 0x000002CA System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_mEBDD05B95E50DF961BBDD4380134473F42067788 ();
// 0x000002CB System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_m08659C9B95866076DC0EB8A0D01A18923B1A607C ();
// 0x000002CC System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_m4790D319014D3F027D6FD2C65C370CD848EBB497 ();
// 0x000002CD System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_mCD1E24FAE4BD36AC25D9359ED375E49CBCC26963 ();
// 0x000002CE System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_mAE80593708D7AB18F6199B8AEAFBE087AACA5EF5 ();
// 0x000002CF System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_mED46A9A073D1FAAFA8FFE85C9C3370CB5327B429 ();
// 0x000002D0 UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_m7D2000B276199E7012E28DF03E784C7862CFA55B ();
// 0x000002D1 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_mC273CE8E26B2C957D5CC0606D3D7186595B14A4B ();
// 0x000002D2 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m8561F3821A5C30E9E2156E22BE79CA8F40948319 ();
// 0x000002D3 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_mE0FF05AC9EE31E65180DE37E092CCDAED803A4C2 ();
// 0x000002D4 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_mBA0C42CC9DCDCF89561A0DA92279B1D4CDA99521 ();
// 0x000002D5 System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_m59261DF2C55AE0CF78D6569F9E1F7934219901C5 ();
// 0x000002D6 System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mC1421D13A6B6B7AD19637171BE92A182F461DC0D ();
// 0x000002D7 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_mCFFDF858F43F0DD4FFC1C24E2E5CAF66AD2185EF ();
// 0x000002D8 System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_m4E7511CC23112AA61E05EAF49289A25FA04017A3 ();
// 0x000002D9 Proyecto26.StaticCoroutine_CoroutineHolder Proyecto26.StaticCoroutine::get_runner()
extern void StaticCoroutine_get_runner_mCDEDC96828F412E908516930AA0A07628AD45941 ();
// 0x000002DA UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_m30B1BE08E4A8F5A4FBB32EBAE184AD175B0865B4 ();
// 0x000002DB Proyecto26.ResponseHelper Proyecto26.Common.Extensions.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_m0E720BE1DF47D7B1A94C6F79C39AE18938B8A009 ();
// 0x000002DC System.Boolean Proyecto26.Common.Extensions.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_m97BFC46C89323A6BBC3BBCC3B289794171EC61C7 ();
// 0x000002DD System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m082960ACC7C4FD71A9A2131DE82E8147B002CE2C ();
// 0x000002DE System.Void CSVReader_<>c::.cctor()
extern void U3CU3Ec__cctor_m066714F45A18E9A21E29891970A928EA3EC69AF3 ();
// 0x000002DF System.Void CSVReader_<>c::.ctor()
extern void U3CU3Ec__ctor_mAE46C38757FB3E4266F7625CD099D47363839CED ();
// 0x000002E0 System.String CSVReader_<>c::<SplitCsvLine>b__4_0(System.Text.RegularExpressions.Match)
extern void U3CU3Ec_U3CSplitCsvLineU3Eb__4_0_m76650B704D0CA5A8767E6ACF506EC144AF6EFC91 ();
// 0x000002E1 System.Void GameManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m725B5E395984C44EF285A5452E010DFDFDDCC9AC ();
// 0x000002E2 System.Void GameManager_<>c::.ctor()
extern void U3CU3Ec__ctor_m68F187E8662A2870EFEA590075764CE38FAB29EE ();
// 0x000002E3 System.Void GameManager_<>c::<do_upload_answer>b__25_0(Models.GameAnswer)
extern void U3CU3Ec_U3Cdo_upload_answerU3Eb__25_0_mECCCCC7A3C52743132850D472F85BCCB88F41FD7 ();
// 0x000002E4 System.Void GameManager_<>c::<do_upload_answer>b__25_1(System.Exception)
extern void U3CU3Ec_U3Cdo_upload_answerU3Eb__25_1_m265672C2E1A0CD9A3662BA3FE461FA62B9FFE0D3 ();
// 0x000002E5 System.Void GameManager_<>c::<do_upload_gameTime>b__26_0(Models.GameTime)
extern void U3CU3Ec_U3Cdo_upload_gameTimeU3Eb__26_0_m7AAA8EBF0B92C566750CBE6EE204B44AB46A50C9 ();
// 0x000002E6 System.Void GameManager_<>c::<do_upload_gameTime>b__26_1(System.Exception)
extern void U3CU3Ec_U3Cdo_upload_gameTimeU3Eb__26_1_m0913071FDD31DC89FAA3288CF665503B6D1CFA28 ();
// 0x000002E7 System.Void GameManager_<>c::<do_upload_gameProgress>b__27_0(Models.GameProgress)
extern void U3CU3Ec_U3Cdo_upload_gameProgressU3Eb__27_0_m49A72FEFB52488DCC7F3DC186128FF96DEE21296 ();
// 0x000002E8 System.Void GameManager_<>c::<do_upload_gameProgress>b__27_1(System.Exception)
extern void U3CU3Ec_U3Cdo_upload_gameProgressU3Eb__27_1_m6102E0FB64C23FB30C1E17F99DD42E58712448BC ();
// 0x000002E9 System.Void GameManager_<>c::<do_update_difficulity>b__28_0(Models.User)
extern void U3CU3Ec_U3Cdo_update_difficulityU3Eb__28_0_m68B7C8DA9971A764576A6B24EB60085EF54F5250 ();
// 0x000002EA System.Void GameManager_<>c::<do_update_difficulity>b__28_1(System.Exception)
extern void U3CU3Ec_U3Cdo_update_difficulityU3Eb__28_1_m6EA660286612B2ACAAB388C048FFCDA58E461F6F ();
// 0x000002EB System.Void LoginController_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mEFFCF16430EC9C1D7D5CA8E3898A88283E7B7300 ();
// 0x000002EC System.Void LoginController_<>c__DisplayClass12_0::<do_login>b__0(Models.User)
extern void U3CU3Ec__DisplayClass12_0_U3Cdo_loginU3Eb__0_m6745D937CE5FDA3F7143FCBE55DF415A3D78CEFC ();
// 0x000002ED System.Void MainController_<load_storyData>d__102::.ctor(System.Int32)
extern void U3Cload_storyDataU3Ed__102__ctor_m7AF515A9672D9EE5D78C38DDEE1F7A9F4FCAE1F1 ();
// 0x000002EE System.Void MainController_<load_storyData>d__102::System.IDisposable.Dispose()
extern void U3Cload_storyDataU3Ed__102_System_IDisposable_Dispose_mD25E86FD804FA098170F6C6BDFE6C8C85F044BAF ();
// 0x000002EF System.Boolean MainController_<load_storyData>d__102::MoveNext()
extern void U3Cload_storyDataU3Ed__102_MoveNext_m688F6EF1395FB552701345839C33830F36790CCF ();
// 0x000002F0 System.Object MainController_<load_storyData>d__102::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload_storyDataU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42D50C14729A29806BCE9A779857AC9D2F6B61E ();
// 0x000002F1 System.Void MainController_<load_storyData>d__102::System.Collections.IEnumerator.Reset()
extern void U3Cload_storyDataU3Ed__102_System_Collections_IEnumerator_Reset_m01165EAED5971F1AAD3335A601FF2C8D755437EC ();
// 0x000002F2 System.Object MainController_<load_storyData>d__102::System.Collections.IEnumerator.get_Current()
extern void U3Cload_storyDataU3Ed__102_System_Collections_IEnumerator_get_Current_mBEE4D266AA6B949DDF9174C0E3D38F7B77153EE2 ();
// 0x000002F3 System.Void MainController_<load_minigameData>d__105::.ctor(System.Int32)
extern void U3Cload_minigameDataU3Ed__105__ctor_m32650C4842BC5C38754F7A1FE1AF9EB9B7B7C2A8 ();
// 0x000002F4 System.Void MainController_<load_minigameData>d__105::System.IDisposable.Dispose()
extern void U3Cload_minigameDataU3Ed__105_System_IDisposable_Dispose_m096DA4C46507F6078791B3F861DAB9B682EE8014 ();
// 0x000002F5 System.Boolean MainController_<load_minigameData>d__105::MoveNext()
extern void U3Cload_minigameDataU3Ed__105_MoveNext_m4D7234257A341EEDC5E202D6AD96096682A96FB2 ();
// 0x000002F6 System.Object MainController_<load_minigameData>d__105::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload_minigameDataU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8377BED7B792FE41D3E2EEB3C7CC3367309DA1EC ();
// 0x000002F7 System.Void MainController_<load_minigameData>d__105::System.Collections.IEnumerator.Reset()
extern void U3Cload_minigameDataU3Ed__105_System_Collections_IEnumerator_Reset_mC426E8CA00A7A1AB1829B03D31B8C72CCF6A8209 ();
// 0x000002F8 System.Object MainController_<load_minigameData>d__105::System.Collections.IEnumerator.get_Current()
extern void U3Cload_minigameDataU3Ed__105_System_Collections_IEnumerator_get_Current_mB75587A3589C8167B8D6650430FA12248718E676 ();
// 0x000002F9 System.Void MiniGameMaster_<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_mEBD87BE56A8A36CC5B1B0B7F699E56166E8B9F00 ();
// 0x000002FA System.Void MiniGameMaster_<>c__DisplayClass73_0::<set_selections>b__0()
extern void U3CU3Ec__DisplayClass73_0_U3Cset_selectionsU3Eb__0_mE25C9BE0CF3DCCCFD267BE85F23ED4C70AB36B55 ();
// 0x000002FB System.Void MiniGameMaster_<waitAndGoNext>d__99::.ctor(System.Int32)
extern void U3CwaitAndGoNextU3Ed__99__ctor_m0511E24A7B6D03262D71FD6194060C05E5969658 ();
// 0x000002FC System.Void MiniGameMaster_<waitAndGoNext>d__99::System.IDisposable.Dispose()
extern void U3CwaitAndGoNextU3Ed__99_System_IDisposable_Dispose_m03566FE2BCECA1EE5219599D2BCCFABD912F54B8 ();
// 0x000002FD System.Boolean MiniGameMaster_<waitAndGoNext>d__99::MoveNext()
extern void U3CwaitAndGoNextU3Ed__99_MoveNext_mCACAED435E2BA9145719E99C0F8D5A2AAA3453C3 ();
// 0x000002FE System.Object MiniGameMaster_<waitAndGoNext>d__99::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitAndGoNextU3Ed__99_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C91FC70F5FF4FBC093829EB82F7BBFB684960AC ();
// 0x000002FF System.Void MiniGameMaster_<waitAndGoNext>d__99::System.Collections.IEnumerator.Reset()
extern void U3CwaitAndGoNextU3Ed__99_System_Collections_IEnumerator_Reset_m6CF586177BB342B41D1E4B79A7959F6AE61BA4E0 ();
// 0x00000300 System.Object MiniGameMaster_<waitAndGoNext>d__99::System.Collections.IEnumerator.get_Current()
extern void U3CwaitAndGoNextU3Ed__99_System_Collections_IEnumerator_get_Current_m1814DB2F9759FF6A55BC303B1711ACA6395C3C89 ();
// 0x00000301 System.Void MiniGameMaster_<load_tellData>d__110::.ctor(System.Int32)
extern void U3Cload_tellDataU3Ed__110__ctor_mB0909C1BFEF572056FF0FA844A4FAC9EF909FDFF ();
// 0x00000302 System.Void MiniGameMaster_<load_tellData>d__110::System.IDisposable.Dispose()
extern void U3Cload_tellDataU3Ed__110_System_IDisposable_Dispose_m780BD3899544E272F868CF2BC3B4CA14DB7B7777 ();
// 0x00000303 System.Boolean MiniGameMaster_<load_tellData>d__110::MoveNext()
extern void U3Cload_tellDataU3Ed__110_MoveNext_m0A970C191EAF671C69D35ED4A98F93E29721A9CF ();
// 0x00000304 System.Object MiniGameMaster_<load_tellData>d__110::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload_tellDataU3Ed__110_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB02DC678D93231EF3CE710C62C2D71C8ECFF6DD8 ();
// 0x00000305 System.Void MiniGameMaster_<load_tellData>d__110::System.Collections.IEnumerator.Reset()
extern void U3Cload_tellDataU3Ed__110_System_Collections_IEnumerator_Reset_m46B1F6D1143257BC4976E34CF9614D96030E7118 ();
// 0x00000306 System.Object MiniGameMaster_<load_tellData>d__110::System.Collections.IEnumerator.get_Current()
extern void U3Cload_tellDataU3Ed__110_System_Collections_IEnumerator_get_Current_mAC2FD332C4D9771783A8D1BA4444389C50D61B9C ();
// 0x00000307 System.Void MiniGameMaster_<>c__DisplayClass111_0::.ctor()
extern void U3CU3Ec__DisplayClass111_0__ctor_m79D2312D7C16999C0BBD48C6E0395448E15A986E ();
// 0x00000308 System.Boolean MiniGameMaster_<>c__DisplayClass111_0::<ProcessTellData>b__2(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass111_0_U3CProcessTellDataU3Eb__2_m1AB5E841ED0C0EA4DA85878686397E083E266251 ();
// 0x00000309 System.Void MiniGameMaster_<>c::.cctor()
extern void U3CU3Ec__cctor_m02ABDC199916A71DC65C56B14EA56A213DBA6CC8 ();
// 0x0000030A System.Void MiniGameMaster_<>c::.ctor()
extern void U3CU3Ec__ctor_m9326332BB75666D4F363102BB244B110B7497F03 ();
// 0x0000030B System.String MiniGameMaster_<>c::<ProcessTellData>b__111_0(System.String)
extern void U3CU3Ec_U3CProcessTellDataU3Eb__111_0_m399848B5E33F36ABBC02163E56E1F7BA8805CB8C ();
// 0x0000030C System.Int32 MiniGameMaster_<>c::<ProcessTellData>b__111_1(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessTellDataU3Eb__111_1_m59F0F8CCA9D5D05A708C2A6489C0F7964BACBFF9 ();
// 0x0000030D System.String MiniGameMaster_<>c::<ProcessTellData>b__111_3(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessTellDataU3Eb__111_3_m44A8033EC3429D55CD9FBF24EF749A8604CD3D91 ();
// 0x0000030E System.String MiniGameMaster_<>c::<ProcessSameData>b__114_0(System.String)
extern void U3CU3Ec_U3CProcessSameDataU3Eb__114_0_mAD2690ED531F453A56FA02DCBC9D48FF66C5FBD9 ();
// 0x0000030F System.Int32 MiniGameMaster_<>c::<ProcessSameData>b__114_1(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessSameDataU3Eb__114_1_mAC2754970F3A2FBBD968B3D2D2ADA6496DE4FF02 ();
// 0x00000310 System.String MiniGameMaster_<>c::<ProcessSameData>b__114_3(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessSameDataU3Eb__114_3_mCA00CE157CA7F2360CB246BB302C24E89A865E1D ();
// 0x00000311 System.String MiniGameMaster_<>c::<ProcessDifferentData>b__117_0(System.String)
extern void U3CU3Ec_U3CProcessDifferentDataU3Eb__117_0_mAE3F43EAEF7577CD644D8F101156DDF1D9F9865B ();
// 0x00000312 System.Int32 MiniGameMaster_<>c::<ProcessDifferentData>b__117_1(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessDifferentDataU3Eb__117_1_mF7C818D23595575F338BF7DA52CBEDDB5CEF9D08 ();
// 0x00000313 System.String MiniGameMaster_<>c::<ProcessDifferentData>b__117_3(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec_U3CProcessDifferentDataU3Eb__117_3_m139FCCB6B0D9AB3480699C6596798A2E12BEEA01 ();
// 0x00000314 System.Void MiniGameMaster_<load_sameData>d__113::.ctor(System.Int32)
extern void U3Cload_sameDataU3Ed__113__ctor_mBB7E61F0F9F67915AA3E6BF9D0EA818594C642B1 ();
// 0x00000315 System.Void MiniGameMaster_<load_sameData>d__113::System.IDisposable.Dispose()
extern void U3Cload_sameDataU3Ed__113_System_IDisposable_Dispose_m9B004B481E13E68554435083A71BD996C2B9854C ();
// 0x00000316 System.Boolean MiniGameMaster_<load_sameData>d__113::MoveNext()
extern void U3Cload_sameDataU3Ed__113_MoveNext_m88609F717DB0EAFC4865BA1C3357EB181B9C45F3 ();
// 0x00000317 System.Object MiniGameMaster_<load_sameData>d__113::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload_sameDataU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57628D57473219973EE45498BCDB2882680B80CA ();
// 0x00000318 System.Void MiniGameMaster_<load_sameData>d__113::System.Collections.IEnumerator.Reset()
extern void U3Cload_sameDataU3Ed__113_System_Collections_IEnumerator_Reset_m00EF51309F49704C4B9B75AE6043F45F698BC113 ();
// 0x00000319 System.Object MiniGameMaster_<load_sameData>d__113::System.Collections.IEnumerator.get_Current()
extern void U3Cload_sameDataU3Ed__113_System_Collections_IEnumerator_get_Current_mB5F0BE7F5AB035DD4BF62A7C40D5F0E3B1AEC718 ();
// 0x0000031A System.Void MiniGameMaster_<>c__DisplayClass114_0::.ctor()
extern void U3CU3Ec__DisplayClass114_0__ctor_m6FC6B92F2405F7F9D6A709ED54AC323E5456F0AF ();
// 0x0000031B System.Boolean MiniGameMaster_<>c__DisplayClass114_0::<ProcessSameData>b__2(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass114_0_U3CProcessSameDataU3Eb__2_m476F4828EC9056DF6927ED96B5CC89444DECBA60 ();
// 0x0000031C System.Void MiniGameMaster_<load_differentData>d__116::.ctor(System.Int32)
extern void U3Cload_differentDataU3Ed__116__ctor_m680EA44B3AB2DD748FD8F66DCE4819ED348FE6E2 ();
// 0x0000031D System.Void MiniGameMaster_<load_differentData>d__116::System.IDisposable.Dispose()
extern void U3Cload_differentDataU3Ed__116_System_IDisposable_Dispose_m0EF143158D4604487DC9360098010355A43C5211 ();
// 0x0000031E System.Boolean MiniGameMaster_<load_differentData>d__116::MoveNext()
extern void U3Cload_differentDataU3Ed__116_MoveNext_mCD1ECCD30B123244C1DE7D396A06F8173B3164F5 ();
// 0x0000031F System.Object MiniGameMaster_<load_differentData>d__116::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cload_differentDataU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15170ED172C4D96C90A8ED14C654236C8264E459 ();
// 0x00000320 System.Void MiniGameMaster_<load_differentData>d__116::System.Collections.IEnumerator.Reset()
extern void U3Cload_differentDataU3Ed__116_System_Collections_IEnumerator_Reset_m808B02B85A984072081ADF1157A3C5D2050E519C ();
// 0x00000321 System.Object MiniGameMaster_<load_differentData>d__116::System.Collections.IEnumerator.get_Current()
extern void U3Cload_differentDataU3Ed__116_System_Collections_IEnumerator_get_Current_mBD9FD5DE0826006026D8C02862E5274C7F022398 ();
// 0x00000322 System.Void MiniGameMaster_<>c__DisplayClass117_0::.ctor()
extern void U3CU3Ec__DisplayClass117_0__ctor_m34CF63F70109F030A27F49FA59774A7FA4B25F98 ();
// 0x00000323 System.Boolean MiniGameMaster_<>c__DisplayClass117_0::<ProcessDifferentData>b__2(System.Linq.IGrouping`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass117_0_U3CProcessDifferentDataU3Eb__2_m93941B492B7EDD1EAA1CC851AE7182E0E725185A ();
// 0x00000324 System.Void Minigame_Hamster_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m333AE8BA81BA94EE7FDC63841924BD2E8026A43C ();
// 0x00000325 System.Void Minigame_Hamster_<>c__DisplayClass8_0::<set_selections>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3Cset_selectionsU3Eb__0_m7F9C50322420A47366CE8303B9A0BC42EB721181 ();
// 0x00000326 System.Void Minigame_Replace_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m96C4031FA07F1C96D1BC3D00F7AB9200C918FB76 ();
// 0x00000327 System.Void Minigame_Replace_<>c__DisplayClass8_0::<init_12_selections>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3Cinit_12_selectionsU3Eb__0_m24916E8853302E705AB2ADD4BA7097F21AFAD708 ();
// 0x00000328 System.Void RSG.Promise`1_<>c__DisplayClass24_0::.ctor()
// 0x00000329 System.Void RSG.Promise`1_<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x0000032A System.Void RSG.Promise`1_<>c__DisplayClass26_0::.ctor()
// 0x0000032B System.Void RSG.Promise`1_<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x0000032C System.Void RSG.Promise`1_<>c__DisplayClass34_0::.ctor()
// 0x0000032D System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x0000032E System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x0000032F System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x00000330 System.Void RSG.Promise`1_<>c__DisplayClass35_0::.ctor()
// 0x00000331 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x00000332 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x00000333 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x00000334 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::.ctor()
// 0x00000335 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x00000336 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x00000337 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x00000338 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x00000339 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x0000033A System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x0000033B System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x0000033C System.Void RSG.Promise`1_<>c__DisplayClass43_0::.ctor()
// 0x0000033D System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x0000033E System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x0000033F System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__3()
// 0x00000340 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x00000341 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x00000342 System.Void RSG.Promise`1_<>c__DisplayClass44_0::.ctor()
// 0x00000343 System.Void RSG.Promise`1_<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x00000344 System.Void RSG.Promise`1_<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x00000345 System.Void RSG.Promise`1_<>c__DisplayClass45_0`1::.ctor()
// 0x00000346 RSG.IPromise`1<ConvertedT> RSG.Promise`1_<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x00000347 System.Void RSG.Promise`1_<>c__DisplayClass48_0`1::.ctor()
// 0x00000348 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1_<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x00000349 System.Void RSG.Promise`1_<>c__DisplayClass49_0::.ctor()
// 0x0000034A RSG.IPromise RSG.Promise`1_<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x0000034B System.Void RSG.Promise`1_<>c__DisplayClass51_0::.ctor()
// 0x0000034C System.Void RSG.Promise`1_<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x0000034D System.Void RSG.Promise`1_<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x0000034E System.Void RSG.Promise`1_<>c__DisplayClass51_1::.ctor()
// 0x0000034F System.Void RSG.Promise`1_<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x00000350 System.Void RSG.Promise`1_<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x00000351 System.Void RSG.Promise`1_<>c__DisplayClass52_0`1::.ctor()
// 0x00000352 RSG.IPromise`1<ConvertedT> RSG.Promise`1_<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x00000353 System.Void RSG.Promise`1_<>c__DisplayClass53_0::.ctor()
// 0x00000354 RSG.IPromise RSG.Promise`1_<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x00000355 System.Void RSG.Promise`1_<>c__DisplayClass55_0::.ctor()
// 0x00000356 System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000357 System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x00000358 System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x00000359 System.Void RSG.Promise`1_<>c__DisplayClass55_1::.ctor()
// 0x0000035A System.Void RSG.Promise`1_<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x0000035B System.Void RSG.Promise`1_<>c__DisplayClass58_0::.ctor()
// 0x0000035C System.Void RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x0000035D System.Void RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x0000035E PromisedT RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x0000035F System.Void RSG.Promise`1_<>c__DisplayClass59_0::.ctor()
// 0x00000360 System.Void RSG.Promise`1_<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000361 System.Void RSG.Promise`1_<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000362 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::.ctor()
// 0x00000363 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x00000364 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000365 System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::.ctor()
// 0x00000366 System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x00000367 System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x00000368 System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x00000369 System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x0000036A System.Void RSG.PromiseHelpers_<>c__1`3::.cctor()
// 0x0000036B System.Void RSG.PromiseHelpers_<>c__1`3::.ctor()
// 0x0000036C RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers_<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x0000036D System.Void RSG.PromiseHelpers_<>c__2`4::.cctor()
// 0x0000036E System.Void RSG.PromiseHelpers_<>c__2`4::.ctor()
// 0x0000036F RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers_<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x00000370 System.Void RSG.PromiseTimer_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m26D280878886D32A48286566B4B2F8B756603652 ();
// 0x00000371 System.Boolean RSG.PromiseTimer_<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mFE0792D8CD3AEA252F554CFACC88FDF94EE6496A ();
// 0x00000372 System.Void RSG.PromiseTimer_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m2416E4D52DA50D980F71698E4F8ED22385712AB6 ();
// 0x00000373 System.Boolean RSG.PromiseTimer_<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m9C67DD62BC0EC33C51BCFB1204B61D8E9CF513C7 ();
// 0x00000374 System.Void RSG.Promise_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mC3B94F40E42254AD21D74448FB5714D30792E61F ();
// 0x00000375 System.Void RSG.Promise_<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_m8366E4096A77E72722DCEC234A20EFCF26BEA228 ();
// 0x00000376 System.Void RSG.Promise_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m2A1C12E7C94E9C2C2CDD7185F5A155A3E302D9C9 ();
// 0x00000377 System.Void RSG.Promise_<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mBBBE632CA7790E7D865234EA41252C8AB82C130A ();
// 0x00000378 System.Void RSG.Promise_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mBDDCA7051B60BC924212BDDF8A3F84AD98E5C5BF ();
// 0x00000379 System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mDEC5F90187DBFDE06666EA852CB47E338BFD2388 ();
// 0x0000037A System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m96E6BD454A47F5DBE5E5C38F68FEC88A62EEA090 ();
// 0x0000037B System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_mE8F5E78214BB7803877E1FE7B307ACC6A106711A ();
// 0x0000037C System.Void RSG.Promise_<>c__DisplayClass51_0`1::.ctor()
// 0x0000037D System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__0()
// 0x0000037E System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x0000037F System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x00000380 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x00000381 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x00000382 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x00000383 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x00000384 System.Void RSG.Promise_<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mF9C690306B3AE84201C2EC20318765F98E909FC0 ();
// 0x00000385 System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m4F024045E1EA15EE42591A2122E92422434E7A6E ();
// 0x00000386 System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m7481C9198D465A81D20C81DCD945E1C343D183E0 ();
// 0x00000387 System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m6FC04810CF220E80FA1FB86DE9EE6DA40279C643 ();
// 0x00000388 System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m2E5E50D9671B3DD1529F9B88DBB0EEDB0376D6D2 ();
// 0x00000389 System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m6206ED102A42205C3A95F7861E5683D45BC6A52D ();
// 0x0000038A System.Void RSG.Promise_<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m89EEA98D995A51B55F9F0D970060619B967EB069 ();
// 0x0000038B System.Void RSG.Promise_<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m4411CA79718E3EECCF18F5421FC0FCFA17DD99DB ();
// 0x0000038C System.Void RSG.Promise_<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m0C5AC78DD8387929C87E55411C88B2A2DA552D83 ();
// 0x0000038D System.Void RSG.Promise_<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_m75748B5C7A1DAC9739F44CDA7C566EC4E438052B ();
// 0x0000038E RSG.IPromise RSG.Promise_<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_mA21AFB4C21A37853627FF773D4D1404AD6786740 ();
// 0x0000038F System.Void RSG.Promise_<>c__DisplayClass57_0`1::.ctor()
// 0x00000390 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise_<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x00000391 System.Void RSG.Promise_<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mF6ED2C62B52C0E842F620C2D15EED73EA9895930 ();
// 0x00000392 System.Void RSG.Promise_<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m1E0AC79E95B447B8DBE44680332B4FF23A22C30D ();
// 0x00000393 System.Void RSG.Promise_<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m4A097F664029C979CD260045C3EECEE4DEE60055 ();
// 0x00000394 System.Void RSG.Promise_<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m53D188D578D87BC21AEA8392DC24CBCBE86CFA4F ();
// 0x00000395 System.Void RSG.Promise_<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_mD7CD24E66E0F6C5F079026E87762BBECD9EA35C8 ();
// 0x00000396 System.Void RSG.Promise_<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m78CDE6104253276761A7D5E1B2B76537AF41B521 ();
// 0x00000397 System.Void RSG.Promise_<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m01CE686EF3AB87D0AD517F072BF8B4FC825795D9 ();
// 0x00000398 RSG.IPromise RSG.Promise_<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mDADCE479295647D84A792C1F706EA461171D991A ();
// 0x00000399 System.Void RSG.Promise_<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m9AD0C767F70704AB09F5DA1D604AE87E73C004E7 ();
// 0x0000039A RSG.IPromise RSG.Promise_<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3DDD73CC4AA576C4627158BF8287ECC08017C26C ();
// 0x0000039B System.Void RSG.Promise_<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mB9121BC6031646355C2868E98033EC665015449A ();
// 0x0000039C System.Void RSG.Promise_<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_mE13A6232DC6B9EF40571FFCCA8DEEDA80EAE12B4 ();
// 0x0000039D RSG.IPromise RSG.Promise_<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m5360F807690CF5E74F146D584D34DE677EE93B33 ();
// 0x0000039E System.Void RSG.Promise_<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m978B803D5B00C4FB7E29005A1AEA280682C8CBDE ();
// 0x0000039F System.Void RSG.Promise_<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_mD99259F46A8A0BE917B9BA389318BFAA2B77C0CC ();
// 0x000003A0 RSG.IPromise RSG.Promise_<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_mC8980C10FD965277E8B6C4B1039DD4F6E26E3219 ();
// 0x000003A1 System.Void RSG.Promise_<>c__DisplayClass64_0`1::.ctor()
// 0x000003A2 RSG.IPromise`1<ConvertedT> RSG.Promise_<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x000003A3 System.Void RSG.Promise_<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_mCC556B1AA030A584CE9D66B3F13D480C747675BD ();
// 0x000003A4 System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_mADE355E511D1E0EFFEBFE70F11EB9BCACB21586B ();
// 0x000003A5 System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m2C1315D69B11F613A330D0DD029A51375F1FBCFE ();
// 0x000003A6 System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5F54CB92ECDBBD2470E74A78905406BA8D4CD146 ();
// 0x000003A7 System.Void RSG.Promise_<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m0F8A91358F07DBB8670518B68D65F9D197F3314A ();
// 0x000003A8 System.Void RSG.Promise_<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mB5D75654E9BE47760F86D72D07B71AA4AA49AC69 ();
// 0x000003A9 System.Void RSG.Promise_<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m227932624168BCE05AA61C2B4F0926B2F4185D05 ();
// 0x000003AA System.Void RSG.Promise_<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m9A2AEA9FF14C7062C0D2516781358E97343C89DA ();
// 0x000003AB System.Void RSG.Promise_<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mD6E3B098E7DEA66DB62DFEF299E7179D418C2816 ();
// 0x000003AC System.Void RSG.Promise_<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_m762186B3D6B0DE50498C280C5B0473B21E10B931 ();
// 0x000003AD System.Void RSG.Promise_<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m8DDFCD018962A0D70E644B2724D83ADE7C56FB0E ();
// 0x000003AE System.Void RSG.Promise_<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mA4F54EF7D51474C5AC4C9175DC0D5C485A9EACB7 ();
// 0x000003AF System.Void RSG.Promise_<>c__DisplayClass71_0`1::.ctor()
// 0x000003B0 System.Void RSG.Promise_<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x000003B1 System.Void RSG.Promise_<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x000003B2 System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::.ctor(System.Int32)
// 0x000003B3 System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x000003B4 System.Boolean RSG.Promises.EnumerableExt_<FromItems>d__2`1::MoveNext()
// 0x000003B5 T RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000003B6 System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x000003B7 System.Object RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x000003B8 System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000003B9 System.Collections.IEnumerator RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003BA System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__2__ctor_mDBAE36C45CF4004B97C30F95C1791BA4AF169622 ();
// 0x000003BB System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__2_System_IDisposable_Dispose_mA9BF134894D193D7D13294E9DA2C6B716151F54A ();
// 0x000003BC System.Boolean Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__2_MoveNext_mDA1690BC7C7F380815AE96BC6135DE8E930700D2 ();
// 0x000003BD System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__2_U3CU3Em__Finally1_m09F7B2D91D58A0DBBF872760D5B489C8F6858957 ();
// 0x000003BE System.Object Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m461D5C5A254E3CC14FAC6B064B80D60B3A4550C6 ();
// 0x000003BF System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__2_System_Collections_IEnumerator_Reset_m7A2662E883A4243F08B2293489832C10977AF790 ();
// 0x000003C0 System.Object Proyecto26.HttpBase_<CreateRequestAndRetry>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__2_System_Collections_IEnumerator_get_Current_m6C8046E47CF3487494779EF4A0FB33503464BE42 ();
// 0x000003C1 System.Void Proyecto26.HttpBase_<>c__DisplayClass7_0`1::.ctor()
// 0x000003C2 System.Void Proyecto26.HttpBase_<>c__DisplayClass7_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x000003C3 System.Void Proyecto26.HttpBase_<>c__DisplayClass8_0`1::.ctor()
// 0x000003C4 System.Void Proyecto26.HttpBase_<>c__DisplayClass8_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x000003C5 System.Void Proyecto26.HttpBase_<SendWebRequest>d__9::.ctor(System.Int32)
extern void U3CSendWebRequestU3Ed__9__ctor_mD9787FCFC0283C43D781F09DA3DD357F33C94CCC ();
// 0x000003C6 System.Void Proyecto26.HttpBase_<SendWebRequest>d__9::System.IDisposable.Dispose()
extern void U3CSendWebRequestU3Ed__9_System_IDisposable_Dispose_m339A6E45448CE0827CA9F67E4BBB70F1CA464E84 ();
// 0x000003C7 System.Boolean Proyecto26.HttpBase_<SendWebRequest>d__9::MoveNext()
extern void U3CSendWebRequestU3Ed__9_MoveNext_mB50578A11EFF61C59D6218E615F9D90E07E74BD9 ();
// 0x000003C8 System.Object Proyecto26.HttpBase_<SendWebRequest>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDFE68A2E51947221751C0614554D7DD4E9490C7 ();
// 0x000003C9 System.Void Proyecto26.HttpBase_<SendWebRequest>d__9::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestU3Ed__9_System_Collections_IEnumerator_Reset_m0C92F5BEDC9A9E14615D977358EC5A7B599874A7 ();
// 0x000003CA System.Object Proyecto26.HttpBase_<SendWebRequest>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestU3Ed__9_System_Collections_IEnumerator_get_Current_mC951AAAF2B5DAB2BAF1ED03A2E696090C5E94371 ();
// 0x000003CB System.Void Proyecto26.JsonHelper_Wrapper`1::.ctor()
// 0x000003CC System.Void Proyecto26.StaticCoroutine_CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_m47D58BEAAE6AB0AF0FFFA8897651D4455D142FF2 ();
static Il2CppMethodPointer s_methodPointers[972] = 
{
	AlertController_setup_mFF5316D39F0AFE3DBF01B5C7EF93A86B182B9EDB,
	AlertController_dismiss_self_mC8130B326B05CB1DDC040907AAE02D196401A888,
	AlertController_confirmClicked_mC1DB32214E30E22EA249A0FC58C3EB425F8A2AC1,
	AlertController_cancelClicked_mECC295CDB1084460C3CC1F631F97C12FC98F4CF4,
	AlertController__ctor_mC0A30B81BC06B5848E7F1C4B3ED48604F837BED3,
	OnBoardTutorialController_highlight_obj_mE2C519C293022C49175CBB24FC11CE1A52E23ECB,
	OnBoardTutorialController_reset_toturialContent_m60122051EB9DD895AC21023A1B4528D5D7C5F036,
	OnBoardTutorialController__ctor_mE8EA536872E07D6FC07747653F8282F3F1F94F57,
	APISetting__ctor_m5782FF5B51B10E5F7A0A2EFD78B8716934723F78,
	APISetting__cctor_mAE553E712FA6762FCD32388C6C348E41D4AC182C,
	Achievement_effect_Start_m30F79934EB54F2996DFA82090D2863366D6A7536,
	Achievement_effect_Update_m693D97691F3F9CDDED95CDAEF09FC90EFFC6F855,
	Achievement_effect_change_page_effect_mA96BCA876A9755E0E9B9751B8CF0CC0CA461AB6F,
	Achievement_effect_call_reloadStickers_mEB0332373044C1DF40E2F3C41F5104BA990344D0,
	Achievement_effect__ctor_m1FBEF5188E15D581A09976DC4C671A96B6D8AD2B,
	AchivementController_Awake_mF6B74651CDE3BF441814A7E8A1DE7541C49A443C,
	AchivementController_Start_mDE70F8245789BE75D79590B8ECA33430E10EEC7A,
	AchivementController_Update_mB9B8A5DBD5A698732B845B712622315613F66E01,
	AchivementController_setup_gridView_mA13343802BC685D90F9E238E39EE674D956C48C0,
	AchivementController_backClicked_m1B1CEABB42FF1911EA6D5339E76490D052CFC728,
	AchivementController_clean_stickers_m52396708D8C21651859628E7ADEB708AED3AF512,
	AchivementController_show_sticker_small_clicked_m1317914E5FBB07EB6CB57651E9E5838BF9F921BC,
	AchivementController_load_smallSticker_m065D91461A6EE2C9449D428A59F13D9B9C76FE1B,
	AchivementController_init_sticker_m2709427B5FDD7815F2903D90AF557339A7DDCF1A,
	AchivementController_show_sticker_large_clicked_m7A6DBECA18A808C870A2F840AC67C538D064A726,
	AchivementController_load_largeSticker_m7DEAB10DDBAFAF209CC372514505BC7B91ECB349,
	AchivementController_set_changePageButtons_m883FF3B4FCC2018E439B4F4137EE9A8CA32E3DE1,
	AchivementController_next_page_clicked_mC363B2F69137424893ADCE4858CF4536D64B8E02,
	AchivementController_previous_page_clicked_m0AE7CDF97A6D6D07DCAD4F395569AEE1380B7F50,
	AchivementController__ctor_m12C63F41598CFBE1BE253002DFD1C1F4F16DC178,
	CSVReader_Start_mBA3D657B973751B9D936A4DA4788FF1C75D26B31,
	CSVReader_DebugOutputGrid_m6A48E539290678F3EC61E05F76ABBA36561B9A3A,
	CSVReader_SplitCsvGrid_m8D6CDD28EF1E2C26B448F5691AE7D5401184ED0A,
	CSVReader_SplitCsvLine_m0A1A26B97C16EFB18357E1B7316C3BA98115A195,
	CSVReader__ctor_mC259EFA1874891B2736A0559B5614CCB8C1BD9AC,
	CollectionObjectController_setup_m74F60CF14FA85249055BC458E31DE1438D82DEEB,
	CollectionObjectController__ctor_mA2EBEAFCD6310770D6D37F09ED32C6F9A93427F6,
	EffectController_Start_m44EF5B5E5700D7D9F3EAD8DDE6A80A2F52E9C8A2,
	EffectController_Update_mE64CFF76F4DCFEB3391DA7095A0F94D0D802D9B3,
	EffectController_DestroyGameObject_m1EBACBF4864FB3230997F394ECB9BF2168743AC7,
	EffectController__ctor_m4D367DCBC8B71FDB6EE65A6ED43C44F56B6CF649,
	GameManager_Awake_mE60F41F3186E80B2BAB293918745366D18508C0F,
	GameManager_Start_mD77CCDBF1DA8EC5C3AE7ED955DE4E7F54B79C88E,
	GameManager_Update_m07DC32583BF09EB71183725B7B95FA7B4716988A,
	GameManager_set_audios_volume_m91E50913CB4855E23B6745CB405E61E2730D7FC5,
	GameManager_change_scene_mF1B55AAA457C1A91196BC077BBCC913E25779291,
	GameManager_GetTimeStamp_m86A31AF5E65C2E1A37E0C845F98EB9ABFB1652C0,
	GameManager_UnixTimestampToDateTime_m873D959D049FDAF7B9A7FC6C191AD2EE1F845390,
	GameManager_do_upload_answer_m1D04DECDCD2CB7E1571B45B81FE0E1DACEA40DC7,
	GameManager_do_upload_gameTime_m75EAB05DD93538BFB0C424CB0BC8401FEC2E5728,
	GameManager_do_upload_gameProgress_mA10061CA6D0242836679FA1BB5F9CD2068C30737,
	GameManager_do_update_difficulity_m2A42C621C32AAA0D1135727B6341F0A85D1B4FA7,
	GameManager_get_current_week_m9DF0FE2697B7CCA4F74A81B3A975554108EA675F,
	GameManager_get_current_week_FromProgress_mD6295276A5E7A45BE348449442A3B7845E4B1BF7,
	GameManager_play_SE_m37D30E734731D1B8F5685C8EEF3434F0B6E01894,
	GameManager_play_BGM_m9787BA7006E57AD327CEC9C1CC4656A590963816,
	GameManager_group_score_by_day_m7DF02170EFA5AC235EC7402A74170E41DD7A4119,
	GameManager_group_score_by_day_mAD10CE899BE8BFFCE28A1187087A55FE3E067219,
	GameManager_get_maxDayFromProgress_m59EB0F5F3192F655ABA5DEED080AF67B5FD8658D,
	GameManager_get_scoresOfDay_m8EFED0F1FD12F3839F95D1A1C3E49B9C8068143C,
	GameManager_get_progressOfDay_m87085B411E75D31A09ADA283FD9CC999226FE7B4,
	GameManager_compare_dayDiffToNow_mB3074477EEBC0E56387C5C4AB23439957E40ABA4,
	GameManager_showAlert_m94EF799509708BCF3663061A65EA4503F86B30A0,
	GameManager_show_loading_mB73335C43295F6C02ADD9EC002415E58540E38BE,
	GameManager_play_video_mDAF6E08A9A26E80124D6FD939009EFB241FB591B,
	GameManager_DidEndVideoPlay_m91DADA8DDA03A52F20C90A80FAE31D1CE8FA2650,
	GameManager_check_networkStatus_m4C391B60C4A7D1033943E2704BD97986E3FAB516,
	GameManager_highlight_obj_mC8A9AAAA97A4FC61F2FA3E5C7B5FC39F9F3E5E8E,
	GameManager__ctor_mF7F1107D38DE91EB8A57C1C3BB1A932C50CD9693,
	GameSetting__ctor_mE3E9CB53C4483431CC85C718E24B2FA164A48CB2,
	GameSetting__cctor_mAF6F0BFEEC54791457175C733644D1AC2E6D966F,
	LoginController_Awake_m13813482EB515C6BF0E1F200EC65169729DCBE9A,
	LoginController_Start_mFB2204778D7EA0F9DDA0EB6877C51AF1407F55CA,
	LoginController_Update_m7DE707345A79196417B91B5F5B0622667E8B2A3C,
	LoginController_loginClicked_m572560CF7A6A7190D111F9F39E4281377CAE106C,
	LoginController_do_login_m33D291048A633CB7EEAF431DE62707BE942D09CF,
	LoginController_setButtonsInteractable_m5F4BFB86A9F201E23808ACF8E7ECCFA32EE05087,
	LoginController_checkInput_m042CC0E05608CBC9A26E3A9589DF3086C6D78F61,
	LoginController_registerClicked_m54F0B278502570ED3372A02E58E0120ACD29C6E5,
	LoginController__ctor_m82192FD3AC4B3431849B96EABB27850484FF3695,
	LoginController_U3Cdo_loginU3Eb__12_1_m62524ACDC6FE964DAB67651ECC8958F4267483AF,
	MainController_Awake_mCB019C420771675CC0A6A3F28973467E6B3CE32B,
	MainController_Start_m35DA7A10A3616E68633F7BB8D2C562D05D8CBFA1,
	MainController_didLoadMiniGameData_mD73F5CDCFBFEFD79642F6D8F78279F30728F3FF3,
	MainController_didLoadStoryData_m5E9F565BEBE16916A07B7654D34D6940A53C2FCE,
	MainController_check_and_startStory_m1829B8040FF88D119996091FD87F096C72AB0A46,
	MainController_checkShowTutorialContent_mFD6825752BED1FCC690D8F0547C25221C1FD886C,
	MainController_Update_m263887AAE4B665CD62FC9C0B80B884E8E7B4D524,
	MainController_do_nextPage_m0FBEC278FAFB56B4616F3D78039C4F3A8C99BABD,
	MainController_do_previousPage_m743D096A720C7ECCC3D00BA405AE702987DCDD95,
	MainController_setup_map_UI_m2545AF67769AA32F0E7652BD68222BC66C422E0E,
	MainController_playTutorialVideo_m250CAB57CFDE5B5AE576BA68F5ED852B65404768,
	MainController_DidEndTutorialVideo_m0C5BD6BD5973BB5EFF81BAA692CEF614012B3BE1,
	MainController_logout_clicked_mF99F7526CBD7CFF55A9BCF7898C48B2DBC39A342,
	MainController_do_logout_m243E5D9BDBD9589B3E5897811942F562E1E119C0,
	MainController_setup_days_UI_m36568D55F6EF8F70D483A5C409F9F8594D938C6D,
	MainController_gotoSettingClicked_m45C8B383B2E6FE5D1DB08655C7B7130A167C6C0E,
	MainController_closeSettingClicked_mE46C0783E30B9D9397EFB26DDAC666A4B7388451,
	MainController_gotoAchivementClicked_mB1B3773D236E2486D19D6D825D7E6EDC34F3E5AF,
	MainController_startGameClicked_mEC96807EBE0B1AE185A1F0FD503AE22484F7F4E6,
	MainController_daysClicked_m57D7C59BDD6D4329C930074B9F4D48E0DEFB98B5,
	MainController_check_playTutorialVideo_mDC85C8F5B0BB24417B1E7B24C68BE9CF995305D1,
	MainController_showDaysClicked_mC68F0D65F7F905CE314534F2A0E0B4851A0AEE94,
	MainController_hideDaysClicked_mD1A9B340023261809E46CFDEA775108D3CF7DABE,
	MainController_readStoryClicked_mF1CCE42D7BBC75153C888D6C3920AD76E630E9D5,
	MainController_next_storySlide_mB0B70BC52C9BC78A7E32131EDBF599FCEF69F08F,
	MainController_previous_storySlide_m6ACCA48A8927A2E616CE6961CA2B27C59F1F9F22,
	MainController_checkAndShowAchievement_m821BFAF3DDBA4736025C10699A8BAAFAAF18A927,
	MainController_check_new_largeSticker_mCDD5A2895CA13B9857AD195E02CEBA11E9DC1DDD,
	MainController_get_lastSmallStickerImage_m1788989F13AE1892CFCA965AEEC60CF812CE4DCF,
	MainController_get_smallSticker_count_mCA2786A94937326824FDEE7205C2AD426EDA4A5E,
	MainController_get_largeSticker_count_mAE286AFFDACB396B2055ECAB7D3263E5DE20DC56,
	MainController_switch_page_m5BBE7FD5061F6E6A6E4CFE8524B0A19BE63B7D5E,
	MainController_gotoMapClicked_mB9C165983221BC06BB3AD9C333598DCDF9B4399A,
	MainController_endStoryToMap_mAF94C8F0CA846FFD85687C586204BF32DCEC8D74,
	MainController_endOneDayTraining_m60B0487F1263F7C6D3835FCC8E60DD7608133564,
	MainController_gotoStoryClicked_mF516F24512FB94B9AE6CDA35C93F8A9ACD38A1F0,
	MainController_upload_storyTime_m48DFE89FE4AE75EA334435167D7FDD7A6B3ACE1C,
	MainController_gotoMiniGameClicked_m336CD764C929FB20450F981EE005D278DA5F480D,
	MainController_get_currentMinigameData_mFC5CA5C17180A5772B8E5377917A48BAC36CACD6,
	MainController_show_loading_mE511E527F2AF3A642FDAC8E5E4DEADEDAF992394,
	MainController_do_load_storyData_mD32540EF6C996512284AECFB7B56A5D3B79AEEF5,
	MainController_load_storyData_m98A217ED7CF21E87F1009C7A4A0F921EA08299AC,
	MainController_ProcessStoryData_m3B5E36F0EE497F74EEAB6CF3A5619CEE2CF6D3B7,
	MainController_do_load_minigameData_m12179D423AB77D5111A54A8A9F74505F5D995D3A,
	MainController_load_minigameData_m3624EC5B2355FA3F87F83BDFAB74C1E58A680FE2,
	MainController_ProcessMinigameData_m53DA52616E02B8A76AB7F11B273471EB6D1E4A89,
	MainController__ctor_m74A09948F6E80BEFF77C13D185CCBB07899CE96E,
	MainController_U3Clogout_clickedU3Eb__73_0_mD3F7D2B3BC103F80C1F9529CAFAD0A115E078816,
	MainController_U3CendStoryToMapU3Eb__94_0_mA004DDE054A6B4287A5829500E84C49FCE64C804,
	MainController_U3CendStoryToMapU3Eb__94_1_mAF8BF36823466391BF52BC5DE3153D9D2ECAF838,
	MiniGameController_load_minigame_m3B155ACC0E9BA2D595FFD6DD60B5C58131E7D34E,
	MiniGameController__ctor_m0C22708AE971B4F8ADD322934839B632331E5082,
	MiniGameMaster_setup_graphicUI_m40C5AB58A3880FCE86D4F771B33FB039D60769D9,
	MiniGameMaster_checkShowTutorialContent_m688C36CA6F847171CC91232488A9C6867B3EB90A,
	MiniGameMaster_reset_selectionsAnim_m2B85B2AAA4C4683F9D22A8FD16BF766DFFCD0FFC,
	MiniGameMaster_setup_questionTypeUI_mB472643AA6CCFEC882DEE3C4365B77C8FD1D68F9,
	MiniGameMaster_showNextQuestion_m676343EA248251066E54182F57B74D69A17EA6DC,
	MiniGameMaster_do_showNextQuestion_m075A292874587FB0B92358703078FB478FEB6850,
	MiniGameMaster_check_playingAudio_m02CA46B4E146547F6ABAF3309855AEE86D126771,
	MiniGameMaster_start_game_m0FB68EC2AF2D193471447DB460E4D15D06E1894A,
	MiniGameMaster_do_start_game_mFA4CEB81017E604E1BCBE24A43C295288DBF6CBE,
	MiniGameMaster_update_gameTime_m40F5CFF02A95BBBD5BCF0BAE62888C2DA258290B,
	MiniGameMaster_update_gameProgress_m71710839A543B0020E9A1AC3440444711FEE3721,
	MiniGameMaster_generateQuestion_and_setAnimation_m2AFB9E06614240B0570579D4FC3FCC142FF7E622,
	MiniGameMaster_check_countOfSelections_mE97F542F4C8924669F2776A82D8E121D4A9F9562,
	MiniGameMaster_get_questionSetByDifficulity_different_mF361EB7A70374B10BC09386E1F54A0E4375FF09C,
	MiniGameMaster_get_questionSetByDifficulity_same_mCA9EE3BCC580E9D5C66F1DAEA0EF20C9EFDC4EA3,
	MiniGameMaster_get_questionSetByDifficulity_judge_mA4495A6290D12DA7572EB68A41051409535686F3,
	MiniGameMaster_generateQuestion_m7415900A23C154DA7D3DE5BE33F6F7501AEB65F0,
	MiniGameMaster_set_selections_mFC22C4EC11FADC18A737A9C4D69657CA8BA5C32B,
	MiniGameMaster_ShuffleSelection_m3A1DCF94DEC52AC9B2416DD26F53022C59C963A3,
	MiniGameMaster_selection_selected_m8DFBF5C658FD093B3CB9DA9309E4D2BEC1A38F9E,
	MiniGameMaster_do_remove_hints_m2A6334F7F21558CE7028211DCF2CA2D66E4365E3,
	MiniGameMaster_do_selection_clicked_mF209C783B4DAA0AA2EA92788292C48B337240DA0,
	MiniGameMaster_do_show_hints_m15531A5E781B042D35AA8B9CB75CAFC302AE9C25,
	MiniGameMaster_confirmButton_enable_m28040F8385FD85D371E8AB6BF5494AC1F7DB8EA3,
	MiniGameMaster_refButton_enable_m39639ACFE99F3B9CCBF1B5BC2A3EF51D395BC2BE,
	MiniGameMaster_judgeButton_enable_mC90EC2CDF694D4F1D76F0AA2C6322CF59766EE56,
	MiniGameMaster_confirm_clicked_m52F620B9494474609BA73117B13F5CA0ED5C03C7,
	MiniGameMaster_do_confirmClicked_m5ADB3E630BEA8A3FBB8AD829F46D80FB161C9BAF,
	MiniGameMaster_judge_clicked_m35E2D80E7C898F7A75B5CB72FD1C8D3ED878EFF6,
	MiniGameMaster_do_judgeClicked_mBBC992410AA114175526D6DFD2280BB79146738F,
	MiniGameMaster_reference_clicked_m7D92E7FD102DD86088301C37636EE4D89FB4495B,
	MiniGameMaster_timesUp_m8002078ED7D1E5DF06A7A70992F7981924103C09,
	MiniGameMaster_do_timesUp_m1857CC34B4D80B36CA1170C79B7CBFD92EFAE7F2,
	MiniGameMaster_show_wrong_m26F0EEE90D9CDD557A6285CEB51C00FC56837CFE,
	MiniGameMaster_show_correct_m3B38EAE630D9B1BD24D7F762C63E9075A32C66A3,
	MiniGameMaster_get_NumOfSelections_range_m38A3A0B6E1DAAC5624E599FB5C3D084481EEF629,
	MiniGameMaster_get_selectedObject_m7AB4DE756FEE1A2541E1AB3EADA2205765365B6E,
	MiniGameMaster_handleAnswerdResult_mB836DBB0C87A34A7E7F67794924E61F1D47ACBFC,
	MiniGameMaster_do_lessSelection_lowerDifficulity_m0E6D0D1BB0D0C3BD1CB8D270599698C8735E032F,
	MiniGameMaster_do_moreSelections_higherDifficulity_mCB396613BA5199F804BC38C5D28C009412C1BCF5,
	MiniGameMaster_showResultAnimation_m4B38D0974059B354276CD12F6F5167A10C6A2FA1,
	MiniGameMaster_change_difficulity_m1EF22CA574188E51EF3E7C84CA4210A790D32577,
	MiniGameMaster_update_difficulity_mF7AF5C4F94D748F61337F9F7FD4CC1368E702671,
	MiniGameMaster_waitAndGoNext_m163D07CD967D222CA5651369E9AA5263B4F2BB8F,
	MiniGameMaster_showCoveredText_mA8DED299EC8FA66F18DCE94B1039A5E3A5EC68AC,
	MiniGameMaster_play_audio_m7A0110B765DF88B9CBA620F33586194170E0EF9A,
	MiniGameMaster_play_word_audio_m4943C7A34D49007ED28D0962DA4E70CB81DEC8C3,
	MiniGameMaster_get_word_eng_m9FCD4B6BF6940BC181C5A3787230708AEB8755C9,
	MiniGameMaster_do_play_word_audio_m2D1EDAFEAB72EA8E85767D1540EAF3057F7DC488,
	MiniGameMaster_exitClicked_m8992C18E90B751E51D95568DA0E1669556A31B5E,
	MiniGameMaster_do_gotoStory_mDCD0E3ABEF086E2C272580F7DA55952F4AB7F8D5,
	MiniGameMaster_do_exitClicked_mBA21EA0BAF5ED8C74F4E4B34FC0D32467FB9F3C2,
	MiniGameMaster_get_csvFolderPath_mBC1D8B245D44E848D43B8DF5261DC3696061B18E,
	MiniGameMaster_do_load_tellData_m2198C30A2159A917EE0B0353E673EB53663B4D59,
	MiniGameMaster_load_tellData_mAE2AD35E54C955656F473792D6FD01E9BCA18200,
	MiniGameMaster_ProcessTellData_m938AF3DE9BBB0C0EC6DF3FF15A4F085DB962457B,
	MiniGameMaster_do_load_sameData_m1759E6677D78A90592AC5A7EFD2E775802F6B806,
	MiniGameMaster_load_sameData_m870F0FD9F6C0D7C8B9436EF61A1694B088F89BEE,
	MiniGameMaster_ProcessSameData_m8209DE40774607D9315B65BDE2BC26EA5361F38F,
	MiniGameMaster_do_load_differentData_mD63F4891947B840B6EAEB96754CBB17FF4EBB636,
	MiniGameMaster_load_differentData_m16A5EF8CF003DD533EF63CE2CEFB2AF60C3DD32C,
	MiniGameMaster_ProcessDifferentData_m39904DF803083593372DAA048564C4B322F6B193,
	MiniGameMaster_get_csvRawData_mC41AE6B42C6048E0DBDA1730D018B5EB0C88F661,
	MiniGameMaster_run_testing_load_mE02A5BA34302691A906A6F8EDE66AD38420E4CAF,
	MiniGameMaster__ctor_m8481E1BCB290FB3617C4735D489B93EDC7721327,
	MiniGameMaster_U3Csetup_graphicUIU3Eb__56_0_m81E8C33425508771348C7EA01C4B436DFB14FF9A,
	MiniGameMaster_U3Csetup_graphicUIU3Eb__56_1_mB7072DED31F3DFB7742BCCA334EF6C4A5834DAB6,
	MiniGame_Move_forward_Start_mADDA051C5879444AD8C16225A41747CA4CB2FCAC,
	MiniGame_Move_forward_FixedUpdate_m2782C648C8BD4238025E1EA7183FBEE54183E5E9,
	MiniGame_Move_forward_setup_Minigame_Doge_Normal_m7970A12896F82FECC5F4608FC46864DEC537D473,
	MiniGame_Move_forward_do_move_anim_m31F7DA6390978D57ACB6DA4D498097BDFB036A40,
	MiniGame_Move_forward_do_pause_anim_mA40F218780750920FEA7B559F3DE582FA411621E,
	MiniGame_Move_forward_generateQuestion_and_setAnimation_mF3606E901897B576AA254733F9D3B8C745EB00F9,
	MiniGame_Move_forward_confirm_clicked_m0E649A18BFF541DC5B0F4154360B96A9B38F5BE2,
	MiniGame_Move_forward_judge_clicked_m97CD7B1D7FDD51A0651EE4D92AA2E7C14138D814,
	MiniGame_Move_forward__ctor_m54AC42A7AD8254D4E05CFB2FA4718EEFB95CC729,
	Minigame_Appear_Start_m6529F84F941C5502554656A6D686268D4CBF64E1,
	Minigame_Appear_FixedUpdate_m46942C70D32E9AD20AD27046D7FB19360E020904,
	Minigame_Appear_setup_appearUI_m9CAF2DD4581ABD0C600D30197C2BF04E4A900611,
	Minigame_Appear_setup_gridView_m6CC0467A09505320DD1F5B36204E66A5F5238440,
	Minigame_Appear_update_objToShow_m133F72C03A2A6C170E5A4D4EE765DC19E34F6C43,
	Minigame_Appear__ctor_m1FE765870AD4A66EA410E2939CC769B86B62658D,
	Minigame_Chase_Normal_Start_mA957148E401CFB33A58CAA4B474C89A3F5D04524,
	Minigame_Chase_Normal_FixedUpdate_m1327D33C3E941B96A9994FF677DA67BB49EAAE40,
	Minigame_Chase_Normal_setup_Minigame_Chase_Normal_m5D9DF300978966BB903D54520BEC09A01C3DB267,
	Minigame_Chase_Normal_do_move_anim_m1F7EC7DAF97F84155CF10C7D051058997CFCFA5A,
	Minigame_Chase_Normal_do_pause_anim_m13AD24666E836F29140DB3198FAA2760107518B3,
	Minigame_Chase_Normal_generateQuestion_and_setAnimation_mDB0E834A03DB003521AE62A528A74FE4DCC9DF97,
	Minigame_Chase_Normal_confirm_clicked_mE199E87C87E7CC1623F1F87C17C05F2AFFC6C1B0,
	Minigame_Chase_Normal_judge_clicked_m53F557BE5C7AF649DABEAAC4972196D838818155,
	Minigame_Chase_Normal_timesUp_m875D3A229E26A924BCAD2D9862A460C1DE0AA3A4,
	Minigame_Chase_Normal__ctor_mCFE8B011E6B8E6ED632579F2A7F7A15DFF91D3C4,
	Minigame_Dodge_TimeLimited_Start_m0EF038EB6C3E28DDE34BA51D4042118917AAD133,
	Minigame_Dodge_TimeLimited_FixedUpdate_mA089772C5911D1ADD61AD83FF460AAAD0749AA0F,
	Minigame_Dodge_TimeLimited_setup_Minigame_Doge_Normal_m5976E22B9791BCDF56FD9208F89DB6A889C45AD4,
	Minigame_Dodge_TimeLimited_do_move_anim_mD8AB51CA20EC5E8E9EA8C88BDAA3FBB7F8028D7B,
	Minigame_Dodge_TimeLimited_do_pause_anim_m22D0F20D7451B5929F16843DD8FC6562F71521B4,
	Minigame_Dodge_TimeLimited_generateQuestion_and_setAnimation_mA8989522ABED082C9679B68EF06E5C173D0D00A2,
	Minigame_Dodge_TimeLimited_confirm_clicked_m76E626763D7E889FB5B624D6E18FEBFABD47631A,
	Minigame_Dodge_TimeLimited_judge_clicked_m58090535DC80D8DA28043C49F4FC3ED3BC7DCB57,
	Minigame_Dodge_TimeLimited__ctor_m55835DC477A897883C885A2BDE363789A3B1A9A8,
	Minigame_Doge_Normal_Start_mE8AFA01EC4DE55099022D94210087C9A3A7FBA45,
	Minigame_Doge_Normal_FixedUpdate_m5BD5B0DA6C814C6A2579267A97FB1ECCA0AF8D94,
	Minigame_Doge_Normal_setup_Minigame_Doge_Normal_mBD7C925517A27EF9DC137B26E161BCE8786223EF,
	Minigame_Doge_Normal_do_move_anim_m880B5B599B79DE2C625FE7FB289ADF444B936C25,
	Minigame_Doge_Normal_do_pause_anim_m6636EDAF03CA903692DE94000E6EDB3DD6A987A7,
	Minigame_Doge_Normal_generateQuestion_and_setAnimation_m78A24B715A7944A0442877EB958513E9626DAF21,
	Minigame_Doge_Normal_confirm_clicked_m6C0A3A339DFCB93FAE9DEDE0972C9E1173D1133E,
	Minigame_Doge_Normal_judge_clicked_mB8C35717E652E8C8E06DB414E6D7AF4586B9D6FB,
	Minigame_Doge_Normal__ctor_m452F1B165CBB24F907E5E27E4FCA6CB095361F84,
	Minigame_Hamster_Start_m55E0E4EEA4CDDB7629ECB34E7494C144A47528A1,
	Minigame_Hamster_FixedUpdate_m0470EDCD7AB0ACD3B9C5D88A87681BD18888244D,
	Minigame_Hamster_setup_gridView_m7538522D5E1736AC9D043148B020285E9F75D40A,
	Minigame_Hamster_showResultAnimation_m53A3DD3101F62488EDCBA4D5F7F4CBAC74207359,
	Minigame_Hamster_get_selectedObject_mB48FB0B3F23ED75E14B7F46A515C5780FB9DC832,
	Minigame_Hamster_set_selections_mBFE1F0716BA9BC7016A424254457B83E835B1DE4,
	Minigame_Hamster_setup_Minigame_Hamster_m084C4FC7B9FAE98EBEE4DFD8B781CAC161FADE70,
	Minigame_Hamster_do_move_anim_m85F16E99DE1300EE046B949649A04EBD8BE0DFFB,
	Minigame_Hamster_do_pause_anim_mE893DBC28D3B39B6B65A87BD39CAFB229E45EF3B,
	Minigame_Hamster_timesUp_m6D2E8F77A3CC4A4A6CDF8194FFB1BAB4EDFCA412,
	Minigame_Hamster_generateQuestion_and_setAnimation_mBDA89F8284327DF14335BB85E7A23A0824F72D17,
	Minigame_Hamster_confirm_clicked_m2B1156E65A036A4BCD36EE2D8369C433E677E4DA,
	Minigame_Hamster_get_real_currentSelected_m778EB1935E53D2FA3139048BC240B7C9E452DC9B,
	Minigame_Hamster_play_word_audio_mC4B64F70C5B1121826DA1FDB1D7CB283FC177F7C,
	Minigame_Hamster_get_word_eng_mB5A1C4D71C68E4A18F773A4C80FD12695B68FABC,
	Minigame_Hamster__ctor_mED6C22B0DDC2E17A6B8F6C0FAE1930BEA7BA0588,
	Minigame_Replace_Start_m69069BEEE808A9408B4B6D5836C64BAB355E3441,
	Minigame_Replace_FixedUpdate_mA312355F27C087FA2320D816F55132FB2B765D43,
	Minigame_Replace_Update_mB27D5237BA1C570E939F35A52175A411981C39A6,
	Minigame_Replace_setup_gridView_mF50AE7BA21F3DB92F8B08BB45DDE168BD024FEE8,
	Minigame_Replace_set_selections_mF56A720AA4217BF10DEF4C70E0F23D8A3AE7FFF6,
	Minigame_Replace_init_12_selections_mDABE0BEB2D66DED147C92F1C203711AAF56DFEE5,
	Minigame_Replace_confirm_clicked_mA066EDAE92BE8B9795E7533980801B4AB731F6FB,
	Minigame_Replace_play_word_audio_m0D8B32B1D511F77562485A76EF2B6D0B3A271FD8,
	Minigame_Replace_get_word_eng_mCF980C5CBDA977F7F949E6135CBB0AF06B9CD3EC,
	Minigame_Replace_start_game_m8F064BD195244B68555FD5611713A5354C3516D0,
	Minigame_Replace__ctor_m60851BEAD0B0D908F2351DC80E1772D7F77739BA,
	Minigame_Select_1_Start_m4468D5A62272AD3309F674A6CF312869920F3E78,
	Minigame_Select_1_FixedUpdate_m369BDB93B6F2C4512F93D4225F2494940DA2AE53,
	Minigame_Select_1_setup_gridView_m01A47EE2DA79AF6111BE50B1CE365762398CC329,
	Minigame_Select_1__ctor_m8B6FC13CB230D326D644FC03BF606869B4D34753,
	Minigame_Select_2_Start_mD1AC1782B4518E273B340D2923ED23DF853300D4,
	Minigame_Select_2_FixedUpdate_m3A38414D45286DC8390660AD3E99D8AFFFB0C2DA,
	Minigame_Select_2_setup_Minigame_Select_3_m1CEE9F6826549028611523E1C7BF4FF66650DC34,
	Minigame_Select_2_setup_gridView_mCE6698811EA7EF44EA2A88E42F9414921164F822,
	Minigame_Select_2_do_move_anim_mA30678F74D34B37645B1438815FE0623CEFB10E8,
	Minigame_Select_2_do_pause_anim_m9E1B534717CF7CE2BBBA5AF71BA358CA7458B2EF,
	Minigame_Select_2_generateQuestion_and_setAnimation_m6320555D162A59DEA628AE1148559A4B21FF3F30,
	Minigame_Select_2_confirm_clicked_mE4265C3D0A6CD0C569A571A4362B9F12F16B9C12,
	Minigame_Select_2_judge_clicked_m724D2F88BE26CA0428B1B38C28616D56C3F733F7,
	Minigame_Select_2__ctor_mD628721A8BA4A06D8C0D491B5F1C71FBA06F2E03,
	Minigame_Select_3_Start_m508A7E91F27BE33B12DEE4D6B40627E233D44903,
	Minigame_Select_3_FixedUpdate_mCBB9AD0C35622C4AEF0681319553B79D6295E087,
	Minigame_Select_3_setup_Minigame_Select_3_m3C29A5FBC1ADDAE2C5C342D720FEE12AD197F10A,
	Minigame_Select_3_do_move_anim_m1ADA20D3CAE7F61B77D229DF5F9C7778E5764C10,
	Minigame_Select_3_do_pause_anim_m8A312B0A60154FE76E11FA4E0F75E80E761674F5,
	Minigame_Select_3_generateQuestion_and_setAnimation_m8DDE1B33EFE655F8D23FC0113BDC18BCCA5FB150,
	Minigame_Select_3_confirm_clicked_mBAAC6688896EF2B9E49CB7380D3BD36118ECBEC0,
	Minigame_Select_3_judge_clicked_m2EE9FD0A158F2F57934AB67B6B9A870C439F2FC1,
	Minigame_Select_3__ctor_m3448BFD1510E104950F64717BEA01B4D10884AEC,
	MinigameData__ctor_m9B753574783B1EB79561BEEC8EF516BF7A5C34CD,
	StoryData__ctor_m4F857607C65B6807BA3F6CFCEDE42B9F1A006593,
	ProgressBarController_setup_mCB1D34288242A429580F3F5CC398C71F2AF01F12,
	ProgressBarController_set_prgress_mD824D57061A5113FBF5108ECA54C5094DFB84C2E,
	ProgressBarController_progress_add_mB03C011D65AC7484EAE02A8ABC2211BDD236352D,
	ProgressBarController__ctor_m22A11E98EB0E4002D6D57A103AE0F72A480F89FE,
	RegisterController_Awake_m5B95FDA20AFB84556BDFADE8B9AD8AF3E5E90F0E,
	RegisterController_Start_mB19B92CC7BF1961B627B23894434CB7666EE609A,
	RegisterController_Update_mC5087B4BD4A83A2161914BBC882CB68FE37546E3,
	RegisterController_genderClicked_m7D913A0AF4B5DF534BC662BDDB29543FD2652416,
	RegisterController_checkInput_mF0FAC4E13320C9E4088FEE25F0CDFD8077942738,
	RegisterController_closeClicked_mBEF5DE052076578D41C21DB6631208DCBA6B14B4,
	RegisterController_confirmClicked_m5FC879624550CEEFB3A4AA791BF41BC1ECD06789,
	RegisterController_do_register_m56ABA6500CCA2478EC10C3DFB089E8B8D0F92988,
	RegisterController_setButtonsInteractable_m050947B95D357741C062A3BDD12A22E0036B5950,
	RegisterController__ctor_mC180EE13451B04B44C077E3730DAA4A094E3B508,
	RegisterController_U3Cdo_registerU3Eb__20_0_m6D950A96F1A233CAE641774FA1D73DA1E986CEDD,
	RegisterController_U3Cdo_registerU3Eb__20_1_m3AF7152C6140C51C5ACA8267F2CB8A15399C4037,
	ScoreBoardController_set_score_m25A29BA2FEF6F3FF10F647B68F89D5E41801A1A4,
	ScoreBoardController_add_score_mE371C7B6C29AB6C24454CCC12BFC9A6554EE149D,
	ScoreBoardController_do_add_score_mD8106AB057DE8AA1A63AF2560F53329BE4899FA6,
	ScoreBoardController__ctor_m4673D5DA5884829DEA5BD4041DCE1806B4450631,
	SelectionObject_setup_mF6FB575CE7C7612A444C055505DFC50F27586DC2,
	SelectionObject_showCoveredText_m8C1A67A3C531E646A037404F0596D6FF624432FF,
	SelectionObject_setSprite_m8B8FC6EF4E0EB866F6E0C5B6B52D00794A928ED1,
	SelectionObject_showSprite_m9FDEA2F158928A94BCE2FABC6ADE09CC861E7299,
	SelectionObject__ctor_m7D87658FB8D67C607A47AA079E3A6F39075D4205,
	Word__ctor_mF8464EF52C9F984F8365366F1A6459B7285ADEE5,
	Word_process_Words_m0A72B0D380151194A7ACC19F8BF25836709403E6,
	Selection__ctor_m16E6DADCDF18AA6090CD83EEB1AE1A8265FFE8A3,
	Selection__ctor_m972BB9B0B67A219CCEE4B5DAEF6AFC771FC91601,
	Question_Tell__ctor_m3BC26B40527DC9771344C580A2A317FD79B9E177,
	Question_Same__ctor_m475211008883E416EB265AACF338A466B6D9A312,
	Question_Different__ctor_mEAE745CBD226DE5C276E54AAFB7EA987D21064CB,
	SettingController_Awake_m150964C5DC48A05C5C6CC9AD4C4B2893D51249BE,
	SettingController_Start_mFF2A2A98C7658D187B675AF0AC32E4BA37394F6B,
	SettingController_setup_sliders_m416AA95143A1756DBDB8A453E82901015E7A92DB,
	SettingController_save_sliderValues_mDD26D753ACF103A4B707E8C63B448C28291D8C61,
	SettingController_sliders_changed_mE5C5EA85BE101986A4E92730A1344AFC003C3636,
	SettingController__ctor_mA8AA8CB0AF797CBECDB0FFB2AFE2BF6A1318D3E8,
	StorySlideController_Start_mE2E46B96D3938208D577EEBB6BEB7F96CB109D5A,
	StorySlideController_Update_mC31181CE6172A625119E98E7EDD92126D4F2D121,
	StorySlideController_call_nextSlide_m87F8430AB8BED81E049C03C03AE648844C80F2A0,
	StorySlideController_call_previousSlide_m6C2B970CD0881B888CFCFC8A1EF4B74293F40B20,
	StorySlideController__ctor_mC28A0EE9DC7CDD54CF4AB6B640F0901EB7F8FC76,
	TimeLimitedController_Start_m2C7C11D2C30EA9B8893E22D247A8864FC9775750,
	TimeLimitedController_did_end_time_m2E6AE05485BB803AE6095A02207562D4E52B8AF5,
	TimeLimitedController__ctor_m1B68715926A2B7AF738FF63847908AFBB8DF444E,
	TimerController_Start_m496216FB4A17FC12489A48F8871D2BC2DA10B80A,
	TimerController_Update_m339A914063B02EB04A70CB576CE20CE7F7458163,
	TimerController_setup_m9DC3BDD7669B98399EBE3D902E5B14AFC834F010,
	TimerController_startCountdown_m5D076F0D9213B19F584FEFB57D50A1FB7AA0B79A,
	TimerController_pauseCountdown_mEF6FE5059DE10E4190DB904FD02A5DB2D0833F3F,
	TimerController__ctor_m929586B0B39C6399C05B9B9EE33F7C03C6618BC3,
	GameAnswer_ToString_m840AF72CF2659D1EFC2DCEA0B2C3ACD9CE2366BE,
	GameAnswer__ctor_m67CDDD5A963F68F8201C1FE2FE2A2468BAF6EF16,
	GameProgress_ToString_mEC37BAE8FB736A29F23583051BB02FA1F046481D,
	GameProgress__ctor_m39A768BF00BB7442246629E960B8B38D78392F8C,
	GameTime_ToString_m14A83635B8F46DBA276955753C91A62164C3691D,
	GameTime__ctor_mA83F5B3079B683405341BE0883B2AB005B14F1B1,
	User_ToString_m32C56202AB63DDD85E5FFA39AAC30A8A1FB93BA6,
	User__ctor_m7E99B614CF1352555325351DAD7FA08FD3909DCA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_mF03EB8E9CE6B9CF884725E1A97A2CFB5C1BD48F5,
	PromiseCancelledException__ctor_m9FB8F5C40E7B5AD572CEEB797FBE3938ECC56A12,
	PredicateWait__ctor_mFE953BCE9224945D264E92CDB5FA552D2FF0CB2C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_m8412D2C5990A2FBB010B67B1EF94960317D2CE72,
	PromiseTimer_WaitWhile_m9FF70ED4587C8EDDFC0D5F445F6BB7EA44CC78C2,
	PromiseTimer_WaitUntil_m75EC23820851DCC0C7C49B061BB2B5F2C4BD04D0,
	PromiseTimer_Cancel_mB3EABB157CC031CE655D2F83068DF650CF5F6C81,
	PromiseTimer_FindInWaiting_m472F369396FC318965681B6A7CD98D22A92B3E22,
	PromiseTimer_Update_mD3B803E765306098FE2FAD00CFC47FBF0B8127B8,
	PromiseTimer_RemoveNode_m518F9D576BA81BDA44AE922199C6F4F5029A9814,
	PromiseTimer__ctor_mA04F6931B30C76E74FB87E764AC4FFD5DCC11B49,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m359F24318D921B164DCE03B853505F366192B62E,
	ExceptionEventArgs_get_Exception_m63D0BBF0DE96140F87BABADFA6367E9678B420FC,
	ExceptionEventArgs_set_Exception_m89846D7BA324C3D5FF463016A076BFD88F7DE6ED,
	Promise_add_UnhandledException_mBC5DDDAB7103CD4B2D7930E5226F8584441D4701,
	Promise_remove_UnhandledException_m0473C8E9F3D670601D9761BD229833F01DCBD892,
	Promise_GetPendingPromises_m75911C604D3D1E70DEA44687755A6481553558ED,
	Promise_get_Id_m830859BC68EF160D9F61904117DBCA0725C07F57,
	Promise_get_Name_mCF196DE952307DB05A83EE49C791D09869A08EA5,
	Promise_set_Name_mCE385D3759A515925145B96030786C9B14510175,
	Promise_get_CurState_m6BBB58E657D13BF0CD04C9655F6F21B9ED43C41D,
	Promise_set_CurState_mAF0DC100A33BF72C66E61A3023CBC7369BAFD661,
	Promise__ctor_m93DB3381EEAC4C29D49E9651801FBED07208AE7F,
	Promise__ctor_mF24B852646A4C43475D8E5099381627C427B586D,
	Promise_NextId_mB2BEBFBDB7C5D345707CB4707707AA3BE413FB68,
	Promise_AddRejectHandler_m35C60C296FEDDFEB0E51A000ACD26E57DD29DF1C,
	Promise_AddResolveHandler_m952A85C20177C5642FF0E9856B77926C9AAA443A,
	Promise_AddProgressHandler_mC5B25B11324ABB848AF8F5E84CBA079895A16A7F,
	Promise_InvokeRejectHandler_mFD81E39BD9AC3129DEAF1B7F550E47A7D3BFA196,
	Promise_InvokeResolveHandler_mAD5D536AC291D012A34445143678C0C26AF07561,
	Promise_InvokeProgressHandler_m108B1623CE8E85AD2FB979CE3D4AAB75AB13CC7E,
	Promise_ClearHandlers_m8593EBA08219023BF4B146D6E68166030CE5B452,
	Promise_InvokeRejectHandlers_m89D97A64CA3C5B6CA39A6C1EAA4FE3586F992B68,
	Promise_InvokeResolveHandlers_m664E671FB00D803F07B0A7427E3298F800BE8FD1,
	Promise_InvokeProgressHandlers_m0440A2D41D00D7F51F6FD7E1A8D252895E25BF4F,
	Promise_Reject_m01D6E7306276BF06D642E997243D6AD2ACF326CF,
	Promise_Resolve_m59D1433DF29C576911F452840EDB571548835A3A,
	Promise_ReportProgress_m6D514945624B2662DB5DC6A4C3D6E680AC355089,
	Promise_Done_m832048ABFC33D1BC29CE0A0231015027D85C3289,
	Promise_Done_m88E1CFF52436F0417A0558EA121FE7C4376E45AC,
	Promise_Done_m353F8D2F2949E07CFA7885E3A31B909567CDFB80,
	Promise_WithName_m37E0038809F81F116B01F7D207D7BF0EF56CB32B,
	Promise_Catch_mFA95CEAFDDF70B8126F07202236BBA6CE5EF6351,
	NULL,
	Promise_Then_mEABC93568E27F2D1DABF8C25D8AD0429B8A33AAA,
	Promise_Then_m8078BFDE1C12539AB4C525043DE09AF6BD1F7114,
	NULL,
	Promise_Then_mA06445A003F509C82BA059519A1B5384C56E88D8,
	Promise_Then_mB2C2921F69AE85A4416EC3702245186AF1ED009B,
	NULL,
	Promise_Then_m834055F7479F392C55074DD054A88488CB7B95E0,
	Promise_Then_m076C9BEC0083BFC6D0D983371F31DEC78A7DF73D,
	Promise_ActionHandlers_m021FB595899D1438A462754976F48E5E3AEAB1D0,
	Promise_ProgressHandlers_m5F31C1A1CDD46F46CC51A611D9671A37D0DB2924,
	Promise_ThenAll_m792F1F92799BFC664B0F4E52B22DF43F5E7BB660,
	NULL,
	Promise_All_m82839CDADB53DD91569988C65EC287EED839ECF4,
	Promise_All_mB56B9EF290A3A4757AC72F12E2762927B9CECCAD,
	Promise_ThenSequence_m32CBED7E921C74273C0BCCC2CACC52E1FAD3D4C6,
	Promise_Sequence_m254CD5AF651DF447FDDB39C519D93E06C315341C,
	Promise_Sequence_m8F1D0A45B6025929F3E0487D8B6D02D356C33A0B,
	Promise_ThenRace_m2C6DAA1F0BB7F9B43E4B1ABA154C4E0E8F51B984,
	NULL,
	Promise_Race_m78DEF2E794FF3713EB41095CB056B7DE90F61329,
	Promise_Race_mD1F684B8E4BFCC4D93855F2F424924C418B9B1FA,
	Promise_Resolved_m68DB50577F8EEF1CA2536EDDB26928A5A85DE6A3,
	Promise_Rejected_mC5D063790E663AB3316D5F04B42C8391DF3CBFD2,
	Promise_Finally_m19FFF998F5923B3853C81D1F2E62FF3351FDD17D,
	Promise_ContinueWith_mBE702F11D902A0425F8B6B8FC47FEB2368A216A1,
	NULL,
	Promise_Progress_m9FAD5899A128C5FE45441510B8D9279C2B9D68C0,
	Promise_PropagateUnhandledException_m686471F60398EBD66ED81FA0C8F271425D395C9B,
	Promise__cctor_m7D11D8D35308AB1BF00A1EEA1D73C7B2A2BF36BA,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mEA9C3917D21BE93956790DE4FB5AE8F187338D40,
	Promise_U3CDoneU3Eb__40_0_mCEA4BB2C7A81AD2F6FDC39AADF8EADC75A9FE9A4,
	Promise_U3CDoneU3Eb__41_0_mF316DF93F6B59969F89BBE93BA44836FC8788240,
	Promise_U3CDoneU3Eb__42_0_m35FDB6387B276AB86787B30636AD0B5CFFB157F2,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_mA23590A932F2B85BC150832BC7F706546FB25D47,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_mB91AE2B45F7CC73C6DCA81CB98112845FA4409E8,
	PromiseException__ctor_mBC58F51E1B9AC68089E2B1ADA370E6947A66EC31,
	PromiseException__ctor_m003442558D9CD25D706CCD0B8E9C2BE0A317CB59,
	PromiseStateException__ctor_m5B99DA75AF22A642A7DBF73015B2760F1D65B869,
	PromiseStateException__ctor_mF799A9CCC8CF7A46CD4D912A2B42E6704893E06A,
	PromiseStateException__ctor_m278DF7EBD7800BCF611AF38606A845C59273B993,
	NULL,
	NULL,
	NULL,
	RestClient_get_DefaultRequestHeaders_m73576498BB671104F1924185B7AFDED86F738BC1,
	RestClient_set_DefaultRequestHeaders_m4A36485992F5F58F8289087494D63DFE0CCBD533,
	RestClient_CleanDefaultHeaders_m80B5F4367E694ABB283F5471692C45D92997A3BC,
	RestClient_Request_m77411140A0B8D05FF504D3719B2D4AC80DAD6011,
	NULL,
	RestClient_Get_m9D04DEA397B934DEAF99EE63660399AF288E281B,
	RestClient_Get_m1F189EE04932CF41EB403312DABAE3DC640D0947,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_mAE9ED3C9408189D921AAA1D3B1F0E84FB9B15A3C,
	RestClient_Post_m31BA981121E787A6AA1E4056E4F6BB3CAC5C38BF,
	RestClient_Post_mCFC824D62A526DF3454565BEA0D3E0EC3478785F,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m09EA31A50DCC7963D03FDB086F4BB887C7D56A08,
	RestClient_Put_m1C2F4AFC5DBD84B3E7B9E6B75E3BE71221C74E2D,
	RestClient_Put_mE19F5C2E68BA18C3E138976FBBAD882BE0F2A431,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_mAA28A9FAD6A8683C444E4A6020D1ECA1E51D984F,
	RestClient_Delete_m78568521DA2274A69750EEB00D4A227B5789DD30,
	RestClient_Head_m639942A37D623080C9D7B373EF4920F62953E587,
	RestClient_Head_mB16B5D694F1A3E19012D3BF5E2690DB6AAD77CE6,
	RestClient_Request_m0BA554BC9023E4726CE1777F868B0FD55F623206,
	NULL,
	RestClient_Get_m67CE396FE5FCDF6A8BB4BE0D154D6956D7D3787E,
	RestClient_Get_m0D9E2C1EB049974F78BE7BA8F92063BC842AAB88,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_mB1911EA9ED87562F73D651395C98195ED8D556F3,
	RestClient_Post_m0B0551A7A2466485EEA56C6487DC59FA3514462B,
	RestClient_Post_mC632B22BFA78D0B3C346CC608EAC730173AB69B3,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m02F26FA57C32EA1D57715FEB0B12E2E3D12FC971,
	RestClient_Put_m999201C12FDCF2380BCBAF4EF55CDC777A38F84F,
	RestClient_Put_mF3DFA649A055F6C9B3ADB56226F63089F279DACE,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m434271AAFAC5AAA7214074653C7479B4F7022F60,
	RestClient_Delete_m6AA933C3F377A10BBB0E7E7A848B2A53C7AEFD8D,
	RestClient_Head_m29C35E12D8F6398DAF346470AF1BF472DFB105E2,
	RestClient_Head_m559D8243A556974E9D847DB3E3128F051A666564,
	NULL,
	NULL,
	HttpBase_CreateRequestAndRetry_mF250059013E350E0C8DA48AF5AA8035F482DC528,
	HttpBase_CreateRequest_m275DB6EB32A338391A7422933B486595D82DB9D2,
	HttpBase_CreateException_m194CF2E26B1376590E2E20D91A012AF5556DE4F3,
	HttpBase_DebugLog_m8B106A424CDEAEEF6C414D29836973BED9491463,
	HttpBase_DefaultUnityWebRequest_m39FF5B3AED81F8EF18159E085321E6BE8ACBB39D,
	NULL,
	NULL,
	HttpBase_SendWebRequest_m1F4B19B32BBDCC28D0ADD4DFF2AD4FF4C2E86C5A,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_IsHttpError_m6F5B175FD7F371D7C8B5452F3F180C9BD40838E8,
	RequestException_set_IsHttpError_m3A0E4DA9CB25A2C2BD7396CC9ACF8D151F49093F,
	RequestException_get_IsNetworkError_m28F83E20065738CD7ED5B79350D4A2587B7D7D4A,
	RequestException_set_IsNetworkError_m7F784979F60FCD85F411E1726F5807E2DAE5D081,
	RequestException_get_StatusCode_m30BFC33E4BCA4FC7BD9F7D4BCDB249C4267A4846,
	RequestException_set_StatusCode_mBDA182B36C645A65F32F15C74DDE06AB9483CCE4,
	RequestException_get_ServerMessage_m8453E139F1B8E33C6AB6BC426BAF4568C176A88D,
	RequestException_set_ServerMessage_m7E86E1CB3013133D8C579476B7FA39B136A6460B,
	RequestException_get_Response_m8C986C735E11B633EB4D30816AB0117EDA3FEB82,
	RequestException_set_Response_m4845F1344B303A08A44897B8791E86F9A0897225,
	RequestException__ctor_mA3553611EDEBD35D428B0FBD28CFE0E3C8833E32,
	RequestException__ctor_m1FCAFE2A3491DC22C4A2D9069CA8A20D5F7F8F9F,
	RequestException__ctor_mA1DE75816294F39B5DFB4921FF8D1C5C8F73A184,
	RequestException__ctor_m82EE05E9A81904EE7195C9E5D3702FE8ED8C2870,
	RequestHelper_get_Uri_m7047BABE1656F650A3E58440071D39E6358BBBCD,
	RequestHelper_set_Uri_m6E2922D5CBDA4F030EF21D0F1B6E699C9C633181,
	RequestHelper_get_Method_mDBA0530E15674F8879E8701A98C9B9F77D24C080,
	RequestHelper_set_Method_mE2D6611FC528306E7D7082CF64AF8487FF819145,
	RequestHelper_get_Body_mBA3C18F541933DEBD3CC581C8ACFF4B8D2F4F0D2,
	RequestHelper_set_Body_m433497F39CA7EFD389E4279E5EAA193CD20727DF,
	RequestHelper_get_BodyString_m48E4781621899E33C88E7196660BC1B1CA5C8045,
	RequestHelper_set_BodyString_mC41C456244C97F162E28923F7C1DCBE5092A28D0,
	RequestHelper_get_BodyRaw_m98668878BAC82EB76FF3096174A7A9DC72A5256F,
	RequestHelper_set_BodyRaw_mD198470BD3EB1111781DACF9CA4AA0B30FB11E3D,
	RequestHelper_get_Timeout_mCBFD1D92C750D50AE122FB01F831862390C1F37A,
	RequestHelper_set_Timeout_m006234BB2B771FDAA02519B9786569C1D7F3F7AE,
	RequestHelper_get_ContentType_m61D38DF3A1CA06F73B9426B17BAE72C43DF7606F,
	RequestHelper_set_ContentType_m544341AC1AC74CA96F491CFA07D5373CEB820AC0,
	RequestHelper_get_Retries_mB4E2AA7AD1E2EEDB89632B199CD5123EDD773594,
	RequestHelper_set_Retries_m78DC002BEAA7B5B66EAA9D6D22F927DA1B726962,
	RequestHelper_get_RetrySecondsDelay_mB277C902129CF2BFC2CC6D48CC4B72ADC08F8809,
	RequestHelper_set_RetrySecondsDelay_m9BF0BC026039D48FFDAF69FE5237D7931339CAC2,
	RequestHelper_get_RetryCallback_mC17EB6DA6D24F71FEF672E1A024CED74540E083A,
	RequestHelper_set_RetryCallback_m66950AE73265F6ACDD858663EF2C4C9C81886DC9,
	RequestHelper_get_EnableDebug_m0B4B78D687DFAF5082865206E0C6E269AE332B4F,
	RequestHelper_set_EnableDebug_mEF93B1B869A6675E019987880BDC07F561EE437E,
	RequestHelper_get_ChunkedTransfer_m5C809E092B3FAE32F3CDF2B529A4C84916605445,
	RequestHelper_set_ChunkedTransfer_mCCEDBBB93AC79320B91DCA044EF46A8B2278F3B9,
	RequestHelper_get_UseHttpContinue_mDB2E3AB378AE5D40728CDDF202C6C796EB5F6E28,
	RequestHelper_set_UseHttpContinue_m0A952F887927E26CA2178A1E562D6CED387A076B,
	RequestHelper_get_RedirectLimit_mFE6B3D6DE8F20BF62583BB6D93CC246DDFC545C2,
	RequestHelper_set_RedirectLimit_m4674912A7CD2BD48E770EC75F6D19C7160A1014A,
	RequestHelper_get_IgnoreHttpException_m956B4E7736CC62E1592207ED65CAEB3E7B8859CF,
	RequestHelper_set_IgnoreHttpException_m402DD8BF79EBB0B2B0BC56DE9D1D41B2FC6D418B,
	RequestHelper_get_FormData_m0207D1A53E755E7031AC4C4B5B8D511071F4F478,
	RequestHelper_set_FormData_m3AF0C050E7AD128B8D7A947043D7548CAC2A6EBE,
	RequestHelper_get_SimpleForm_mC51EC658CC52DE0D89F5720F94A6E7E2726CD82B,
	RequestHelper_set_SimpleForm_mD5EDA0B445F702562BFDC3078B67CDCE7109D548,
	RequestHelper_get_FormSections_m3C759886C38B35F03F6DCAB64DA6DEB52F5678FD,
	RequestHelper_set_FormSections_mF6BD0288F15A8FB9B219ADE963DCD6A6280574CF,
	RequestHelper_get_CertificateHandler_m7D5D164ACD676DC4B29297D75A4EC3E191F9A5E5,
	RequestHelper_set_CertificateHandler_m7D0ACC92FA6698CE156FF64A7A23F934190CBD2D,
	RequestHelper_get_UploadHandler_mC39A192F8F8205937A2E3429A3585B23C6179815,
	RequestHelper_set_UploadHandler_mA8317AC94BB2DB1D9D4DB733F403CCA1514C8E52,
	RequestHelper_get_DownloadHandler_m16A51AA814F360D0B86C9AD5AB1487931049603B,
	RequestHelper_set_DownloadHandler_m82CED39A618786A2E78012A9A854ED26651BDC03,
	RequestHelper_get_Headers_mAD303FE22CC5ADD010B2F619334B75EB515FA5EA,
	RequestHelper_set_Headers_mF83A88C5AC2BAFA16854EB612512B8AAF811A174,
	RequestHelper_get_UploadProgress_m9B8077CD6671F1E401A80121D570762CF020C804,
	RequestHelper_get_UploadedBytes_m25284A06292160526A29BDD7EB164AA42C75BC08,
	RequestHelper_get_DownloadProgress_m709EEA6072D4B2372E0853EF87BB8740B3FD0F56,
	RequestHelper_get_DownloadedBytes_mDB750F682594960D558F0B01818693579B8FC9BB,
	RequestHelper_get_Request_mC869E47DC8EEEFF9894D23786D2BF266FAC6E254,
	RequestHelper_set_Request_mEBDD05B95E50DF961BBDD4380134473F42067788,
	RequestHelper_GetHeader_m08659C9B95866076DC0EB8A0D01A18923B1A607C,
	RequestHelper_get_IsAborted_m4790D319014D3F027D6FD2C65C370CD848EBB497,
	RequestHelper_set_IsAborted_mCD1E24FAE4BD36AC25D9359ED375E49CBCC26963,
	RequestHelper_Abort_mAE80593708D7AB18F6199B8AEAFBE087AACA5EF5,
	RequestHelper__ctor_mED46A9A073D1FAAFA8FFE85C9C3370CB5327B429,
	ResponseHelper_get_Request_m7D2000B276199E7012E28DF03E784C7862CFA55B,
	ResponseHelper_set_Request_mC273CE8E26B2C957D5CC0606D3D7186595B14A4B,
	ResponseHelper__ctor_m8561F3821A5C30E9E2156E22BE79CA8F40948319,
	ResponseHelper_get_StatusCode_mE0FF05AC9EE31E65180DE37E092CCDAED803A4C2,
	ResponseHelper_get_Data_mBA0C42CC9DCDCF89561A0DA92279B1D4CDA99521,
	ResponseHelper_get_Text_m59261DF2C55AE0CF78D6569F9E1F7934219901C5,
	ResponseHelper_get_Error_mC1421D13A6B6B7AD19637171BE92A182F461DC0D,
	ResponseHelper_get_Headers_mCFFDF858F43F0DD4FFC1C24E2E5CAF66AD2185EF,
	ResponseHelper_ToString_m4E7511CC23112AA61E05EAF49289A25FA04017A3,
	StaticCoroutine_get_runner_mCDEDC96828F412E908516930AA0A07628AD45941,
	StaticCoroutine_StartCoroutine_m30B1BE08E4A8F5A4FBB32EBAE184AD175B0865B4,
	Extensions_CreateWebResponse_m0E720BE1DF47D7B1A94C6F79C39AE18938B8A009,
	Extensions_IsValidRequest_m97BFC46C89323A6BBC3BBCC3B289794171EC61C7,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m082960ACC7C4FD71A9A2131DE82E8147B002CE2C,
	U3CU3Ec__cctor_m066714F45A18E9A21E29891970A928EA3EC69AF3,
	U3CU3Ec__ctor_mAE46C38757FB3E4266F7625CD099D47363839CED,
	U3CU3Ec_U3CSplitCsvLineU3Eb__4_0_m76650B704D0CA5A8767E6ACF506EC144AF6EFC91,
	U3CU3Ec__cctor_m725B5E395984C44EF285A5452E010DFDFDDCC9AC,
	U3CU3Ec__ctor_m68F187E8662A2870EFEA590075764CE38FAB29EE,
	U3CU3Ec_U3Cdo_upload_answerU3Eb__25_0_mECCCCC7A3C52743132850D472F85BCCB88F41FD7,
	U3CU3Ec_U3Cdo_upload_answerU3Eb__25_1_m265672C2E1A0CD9A3662BA3FE461FA62B9FFE0D3,
	U3CU3Ec_U3Cdo_upload_gameTimeU3Eb__26_0_m7AAA8EBF0B92C566750CBE6EE204B44AB46A50C9,
	U3CU3Ec_U3Cdo_upload_gameTimeU3Eb__26_1_m0913071FDD31DC89FAA3288CF665503B6D1CFA28,
	U3CU3Ec_U3Cdo_upload_gameProgressU3Eb__27_0_m49A72FEFB52488DCC7F3DC186128FF96DEE21296,
	U3CU3Ec_U3Cdo_upload_gameProgressU3Eb__27_1_m6102E0FB64C23FB30C1E17F99DD42E58712448BC,
	U3CU3Ec_U3Cdo_update_difficulityU3Eb__28_0_m68B7C8DA9971A764576A6B24EB60085EF54F5250,
	U3CU3Ec_U3Cdo_update_difficulityU3Eb__28_1_m6EA660286612B2ACAAB388C048FFCDA58E461F6F,
	U3CU3Ec__DisplayClass12_0__ctor_mEFFCF16430EC9C1D7D5CA8E3898A88283E7B7300,
	U3CU3Ec__DisplayClass12_0_U3Cdo_loginU3Eb__0_m6745D937CE5FDA3F7143FCBE55DF415A3D78CEFC,
	U3Cload_storyDataU3Ed__102__ctor_m7AF515A9672D9EE5D78C38DDEE1F7A9F4FCAE1F1,
	U3Cload_storyDataU3Ed__102_System_IDisposable_Dispose_mD25E86FD804FA098170F6C6BDFE6C8C85F044BAF,
	U3Cload_storyDataU3Ed__102_MoveNext_m688F6EF1395FB552701345839C33830F36790CCF,
	U3Cload_storyDataU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42D50C14729A29806BCE9A779857AC9D2F6B61E,
	U3Cload_storyDataU3Ed__102_System_Collections_IEnumerator_Reset_m01165EAED5971F1AAD3335A601FF2C8D755437EC,
	U3Cload_storyDataU3Ed__102_System_Collections_IEnumerator_get_Current_mBEE4D266AA6B949DDF9174C0E3D38F7B77153EE2,
	U3Cload_minigameDataU3Ed__105__ctor_m32650C4842BC5C38754F7A1FE1AF9EB9B7B7C2A8,
	U3Cload_minigameDataU3Ed__105_System_IDisposable_Dispose_m096DA4C46507F6078791B3F861DAB9B682EE8014,
	U3Cload_minigameDataU3Ed__105_MoveNext_m4D7234257A341EEDC5E202D6AD96096682A96FB2,
	U3Cload_minigameDataU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8377BED7B792FE41D3E2EEB3C7CC3367309DA1EC,
	U3Cload_minigameDataU3Ed__105_System_Collections_IEnumerator_Reset_mC426E8CA00A7A1AB1829B03D31B8C72CCF6A8209,
	U3Cload_minigameDataU3Ed__105_System_Collections_IEnumerator_get_Current_mB75587A3589C8167B8D6650430FA12248718E676,
	U3CU3Ec__DisplayClass73_0__ctor_mEBD87BE56A8A36CC5B1B0B7F699E56166E8B9F00,
	U3CU3Ec__DisplayClass73_0_U3Cset_selectionsU3Eb__0_mE25C9BE0CF3DCCCFD267BE85F23ED4C70AB36B55,
	U3CwaitAndGoNextU3Ed__99__ctor_m0511E24A7B6D03262D71FD6194060C05E5969658,
	U3CwaitAndGoNextU3Ed__99_System_IDisposable_Dispose_m03566FE2BCECA1EE5219599D2BCCFABD912F54B8,
	U3CwaitAndGoNextU3Ed__99_MoveNext_mCACAED435E2BA9145719E99C0F8D5A2AAA3453C3,
	U3CwaitAndGoNextU3Ed__99_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C91FC70F5FF4FBC093829EB82F7BBFB684960AC,
	U3CwaitAndGoNextU3Ed__99_System_Collections_IEnumerator_Reset_m6CF586177BB342B41D1E4B79A7959F6AE61BA4E0,
	U3CwaitAndGoNextU3Ed__99_System_Collections_IEnumerator_get_Current_m1814DB2F9759FF6A55BC303B1711ACA6395C3C89,
	U3Cload_tellDataU3Ed__110__ctor_mB0909C1BFEF572056FF0FA844A4FAC9EF909FDFF,
	U3Cload_tellDataU3Ed__110_System_IDisposable_Dispose_m780BD3899544E272F868CF2BC3B4CA14DB7B7777,
	U3Cload_tellDataU3Ed__110_MoveNext_m0A970C191EAF671C69D35ED4A98F93E29721A9CF,
	U3Cload_tellDataU3Ed__110_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB02DC678D93231EF3CE710C62C2D71C8ECFF6DD8,
	U3Cload_tellDataU3Ed__110_System_Collections_IEnumerator_Reset_m46B1F6D1143257BC4976E34CF9614D96030E7118,
	U3Cload_tellDataU3Ed__110_System_Collections_IEnumerator_get_Current_mAC2FD332C4D9771783A8D1BA4444389C50D61B9C,
	U3CU3Ec__DisplayClass111_0__ctor_m79D2312D7C16999C0BBD48C6E0395448E15A986E,
	U3CU3Ec__DisplayClass111_0_U3CProcessTellDataU3Eb__2_m1AB5E841ED0C0EA4DA85878686397E083E266251,
	U3CU3Ec__cctor_m02ABDC199916A71DC65C56B14EA56A213DBA6CC8,
	U3CU3Ec__ctor_m9326332BB75666D4F363102BB244B110B7497F03,
	U3CU3Ec_U3CProcessTellDataU3Eb__111_0_m399848B5E33F36ABBC02163E56E1F7BA8805CB8C,
	U3CU3Ec_U3CProcessTellDataU3Eb__111_1_m59F0F8CCA9D5D05A708C2A6489C0F7964BACBFF9,
	U3CU3Ec_U3CProcessTellDataU3Eb__111_3_m44A8033EC3429D55CD9FBF24EF749A8604CD3D91,
	U3CU3Ec_U3CProcessSameDataU3Eb__114_0_mAD2690ED531F453A56FA02DCBC9D48FF66C5FBD9,
	U3CU3Ec_U3CProcessSameDataU3Eb__114_1_mAC2754970F3A2FBBD968B3D2D2ADA6496DE4FF02,
	U3CU3Ec_U3CProcessSameDataU3Eb__114_3_mCA00CE157CA7F2360CB246BB302C24E89A865E1D,
	U3CU3Ec_U3CProcessDifferentDataU3Eb__117_0_mAE3F43EAEF7577CD644D8F101156DDF1D9F9865B,
	U3CU3Ec_U3CProcessDifferentDataU3Eb__117_1_mF7C818D23595575F338BF7DA52CBEDDB5CEF9D08,
	U3CU3Ec_U3CProcessDifferentDataU3Eb__117_3_m139FCCB6B0D9AB3480699C6596798A2E12BEEA01,
	U3Cload_sameDataU3Ed__113__ctor_mBB7E61F0F9F67915AA3E6BF9D0EA818594C642B1,
	U3Cload_sameDataU3Ed__113_System_IDisposable_Dispose_m9B004B481E13E68554435083A71BD996C2B9854C,
	U3Cload_sameDataU3Ed__113_MoveNext_m88609F717DB0EAFC4865BA1C3357EB181B9C45F3,
	U3Cload_sameDataU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57628D57473219973EE45498BCDB2882680B80CA,
	U3Cload_sameDataU3Ed__113_System_Collections_IEnumerator_Reset_m00EF51309F49704C4B9B75AE6043F45F698BC113,
	U3Cload_sameDataU3Ed__113_System_Collections_IEnumerator_get_Current_mB5F0BE7F5AB035DD4BF62A7C40D5F0E3B1AEC718,
	U3CU3Ec__DisplayClass114_0__ctor_m6FC6B92F2405F7F9D6A709ED54AC323E5456F0AF,
	U3CU3Ec__DisplayClass114_0_U3CProcessSameDataU3Eb__2_m476F4828EC9056DF6927ED96B5CC89444DECBA60,
	U3Cload_differentDataU3Ed__116__ctor_m680EA44B3AB2DD748FD8F66DCE4819ED348FE6E2,
	U3Cload_differentDataU3Ed__116_System_IDisposable_Dispose_m0EF143158D4604487DC9360098010355A43C5211,
	U3Cload_differentDataU3Ed__116_MoveNext_mCD1ECCD30B123244C1DE7D396A06F8173B3164F5,
	U3Cload_differentDataU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15170ED172C4D96C90A8ED14C654236C8264E459,
	U3Cload_differentDataU3Ed__116_System_Collections_IEnumerator_Reset_m808B02B85A984072081ADF1157A3C5D2050E519C,
	U3Cload_differentDataU3Ed__116_System_Collections_IEnumerator_get_Current_mBD9FD5DE0826006026D8C02862E5274C7F022398,
	U3CU3Ec__DisplayClass117_0__ctor_m34CF63F70109F030A27F49FA59774A7FA4B25F98,
	U3CU3Ec__DisplayClass117_0_U3CProcessDifferentDataU3Eb__2_m93941B492B7EDD1EAA1CC851AE7182E0E725185A,
	U3CU3Ec__DisplayClass8_0__ctor_m333AE8BA81BA94EE7FDC63841924BD2E8026A43C,
	U3CU3Ec__DisplayClass8_0_U3Cset_selectionsU3Eb__0_m7F9C50322420A47366CE8303B9A0BC42EB721181,
	U3CU3Ec__DisplayClass8_0__ctor_m96C4031FA07F1C96D1BC3D00F7AB9200C918FB76,
	U3CU3Ec__DisplayClass8_0_U3Cinit_12_selectionsU3Eb__0_m24916E8853302E705AB2ADD4BA7097F21AFAD708,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_m26D280878886D32A48286566B4B2F8B756603652,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mFE0792D8CD3AEA252F554CFACC88FDF94EE6496A,
	U3CU3Ec__DisplayClass4_0__ctor_m2416E4D52DA50D980F71698E4F8ED22385712AB6,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m9C67DD62BC0EC33C51BCFB1204B61D8E9CF513C7,
	U3CU3Ec__DisplayClass34_0__ctor_mC3B94F40E42254AD21D74448FB5714D30792E61F,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_m8366E4096A77E72722DCEC234A20EFCF26BEA228,
	U3CU3Ec__DisplayClass36_0__ctor_m2A1C12E7C94E9C2C2CDD7185F5A155A3E302D9C9,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mBBBE632CA7790E7D865234EA41252C8AB82C130A,
	U3CU3Ec__DisplayClass44_0__ctor_mBDDCA7051B60BC924212BDDF8A3F84AD98E5C5BF,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mDEC5F90187DBFDE06666EA852CB47E338BFD2388,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m96E6BD454A47F5DBE5E5C38F68FEC88A62EEA090,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_mE8F5E78214BB7803877E1FE7B307ACC6A106711A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mF9C690306B3AE84201C2EC20318765F98E909FC0,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m4F024045E1EA15EE42591A2122E92422434E7A6E,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m7481C9198D465A81D20C81DCD945E1C343D183E0,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m6FC04810CF220E80FA1FB86DE9EE6DA40279C643,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m2E5E50D9671B3DD1529F9B88DBB0EEDB0376D6D2,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_m6206ED102A42205C3A95F7861E5683D45BC6A52D,
	U3CU3Ec__DisplayClass53_0__ctor_m89EEA98D995A51B55F9F0D970060619B967EB069,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m4411CA79718E3EECCF18F5421FC0FCFA17DD99DB,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m0C5AC78DD8387929C87E55411C88B2A2DA552D83,
	U3CU3Ec__DisplayClass56_0__ctor_m75748B5C7A1DAC9739F44CDA7C566EC4E438052B,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_mA21AFB4C21A37853627FF773D4D1404AD6786740,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_mF6ED2C62B52C0E842F620C2D15EED73EA9895930,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_m1E0AC79E95B447B8DBE44680332B4FF23A22C30D,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m4A097F664029C979CD260045C3EECEE4DEE60055,
	U3CU3Ec__DisplayClass59_1__ctor_m53D188D578D87BC21AEA8392DC24CBCBE86CFA4F,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_mD7CD24E66E0F6C5F079026E87762BBECD9EA35C8,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m78CDE6104253276761A7D5E1B2B76537AF41B521,
	U3CU3Ec__DisplayClass60_0__ctor_m01CE686EF3AB87D0AD517F072BF8B4FC825795D9,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mDADCE479295647D84A792C1F706EA461171D991A,
	U3CU3Ec__DisplayClass62_0__ctor_m9AD0C767F70704AB09F5DA1D604AE87E73C004E7,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3DDD73CC4AA576C4627158BF8287ECC08017C26C,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mB9121BC6031646355C2868E98033EC665015449A,
	U3CU3Ec__DisplayClass62_1__ctor_mE13A6232DC6B9EF40571FFCCA8DEEDA80EAE12B4,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m5360F807690CF5E74F146D584D34DE677EE93B33,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m978B803D5B00C4FB7E29005A1AEA280682C8CBDE,
	U3CU3Ec__DisplayClass63_0__ctor_mD99259F46A8A0BE917B9BA389318BFAA2B77C0CC,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_mC8980C10FD965277E8B6C4B1039DD4F6E26E3219,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_mCC556B1AA030A584CE9D66B3F13D480C747675BD,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_mADE355E511D1E0EFFEBFE70F11EB9BCACB21586B,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m2C1315D69B11F613A330D0DD029A51375F1FBCFE,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5F54CB92ECDBBD2470E74A78905406BA8D4CD146,
	U3CU3Ec__DisplayClass66_1__ctor_m0F8A91358F07DBB8670518B68D65F9D197F3314A,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mB5D75654E9BE47760F86D72D07B71AA4AA49AC69,
	U3CU3Ec__DisplayClass69_0__ctor_m227932624168BCE05AA61C2B4F0926B2F4185D05,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m9A2AEA9FF14C7062C0D2516781358E97343C89DA,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mD6E3B098E7DEA66DB62DFEF299E7179D418C2816,
	U3CU3Ec__DisplayClass70_0__ctor_m762186B3D6B0DE50498C280C5B0473B21E10B931,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m8DDFCD018962A0D70E644B2724D83ADE7C56FB0E,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_mA4F54EF7D51474C5AC4C9175DC0D5C485A9EACB7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__2__ctor_mDBAE36C45CF4004B97C30F95C1791BA4AF169622,
	U3CCreateRequestAndRetryU3Ed__2_System_IDisposable_Dispose_mA9BF134894D193D7D13294E9DA2C6B716151F54A,
	U3CCreateRequestAndRetryU3Ed__2_MoveNext_mDA1690BC7C7F380815AE96BC6135DE8E930700D2,
	U3CCreateRequestAndRetryU3Ed__2_U3CU3Em__Finally1_m09F7B2D91D58A0DBBF872760D5B489C8F6858957,
	U3CCreateRequestAndRetryU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m461D5C5A254E3CC14FAC6B064B80D60B3A4550C6,
	U3CCreateRequestAndRetryU3Ed__2_System_Collections_IEnumerator_Reset_m7A2662E883A4243F08B2293489832C10977AF790,
	U3CCreateRequestAndRetryU3Ed__2_System_Collections_IEnumerator_get_Current_m6C8046E47CF3487494779EF4A0FB33503464BE42,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CSendWebRequestU3Ed__9__ctor_mD9787FCFC0283C43D781F09DA3DD357F33C94CCC,
	U3CSendWebRequestU3Ed__9_System_IDisposable_Dispose_m339A6E45448CE0827CA9F67E4BBB70F1CA464E84,
	U3CSendWebRequestU3Ed__9_MoveNext_mB50578A11EFF61C59D6218E615F9D90E07E74BD9,
	U3CSendWebRequestU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDFE68A2E51947221751C0614554D7DD4E9490C7,
	U3CSendWebRequestU3Ed__9_System_Collections_IEnumerator_Reset_m0C92F5BEDC9A9E14615D977358EC5A7B599874A7,
	U3CSendWebRequestU3Ed__9_System_Collections_IEnumerator_get_Current_mC951AAAF2B5DAB2BAF1ED03A2E696090C5E94371,
	NULL,
	CoroutineHolder__ctor_m47D58BEAAE6AB0AF0FFFA8897651D4455D142FF2,
};
static const int32_t s_InvokerIndices[972] = 
{
	1448,
	23,
	23,
	23,
	23,
	168,
	23,
	23,
	23,
	3,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	1449,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	122,
	0,
	0,
	23,
	1450,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	232,
	1451,
	26,
	26,
	26,
	26,
	10,
	10,
	855,
	26,
	14,
	34,
	10,
	34,
	34,
	97,
	1452,
	31,
	26,
	26,
	114,
	168,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	31,
	114,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	32,
	114,
	32,
	23,
	23,
	31,
	23,
	23,
	23,
	14,
	10,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	160,
	31,
	23,
	14,
	26,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	1138,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	34,
	34,
	34,
	23,
	26,
	28,
	32,
	23,
	32,
	136,
	31,
	31,
	31,
	23,
	30,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	31,
	23,
	23,
	23,
	31,
	23,
	14,
	23,
	23,
	32,
	34,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	26,
	14,
	26,
	58,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	34,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	32,
	34,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	31,
	114,
	23,
	23,
	23,
	31,
	23,
	26,
	26,
	32,
	23,
	23,
	23,
	27,
	23,
	26,
	31,
	23,
	23,
	0,
	23,
	110,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	14,
	23,
	14,
	23,
	14,
	23,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	23,
	1351,
	28,
	28,
	290,
	9,
	1351,
	28,
	28,
	9,
	28,
	290,
	28,
	23,
	10,
	28,
	27,
	26,
	23,
	28,
	-1,
	28,
	28,
	-1,
	113,
	113,
	-1,
	177,
	177,
	28,
	-1,
	28,
	28,
	-1,
	28,
	28,
	-1,
	28,
	10,
	23,
	290,
	10,
	14,
	26,
	14,
	26,
	122,
	122,
	4,
	10,
	14,
	26,
	10,
	32,
	23,
	26,
	131,
	27,
	27,
	27,
	168,
	27,
	1454,
	23,
	26,
	23,
	290,
	26,
	23,
	290,
	27,
	26,
	23,
	28,
	28,
	-1,
	28,
	28,
	-1,
	113,
	113,
	-1,
	177,
	177,
	168,
	27,
	28,
	-1,
	0,
	0,
	28,
	0,
	0,
	28,
	-1,
	0,
	0,
	4,
	0,
	28,
	28,
	-1,
	28,
	134,
	3,
	1455,
	26,
	26,
	26,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	27,
	23,
	26,
	27,
	-1,
	-1,
	-1,
	4,
	122,
	3,
	134,
	-1,
	134,
	134,
	-1,
	-1,
	-1,
	-1,
	156,
	156,
	134,
	-1,
	-1,
	-1,
	156,
	156,
	134,
	-1,
	-1,
	-1,
	134,
	134,
	134,
	134,
	0,
	-1,
	0,
	0,
	-1,
	-1,
	-1,
	-1,
	1,
	1,
	0,
	-1,
	-1,
	-1,
	1,
	1,
	0,
	-1,
	-1,
	-1,
	0,
	0,
	0,
	0,
	-1,
	-1,
	1,
	0,
	0,
	1458,
	1,
	-1,
	-1,
	1,
	-1,
	-1,
	-1,
	-1,
	114,
	31,
	114,
	31,
	142,
	172,
	14,
	26,
	14,
	26,
	23,
	26,
	27,
	1459,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	720,
	1460,
	14,
	26,
	10,
	32,
	677,
	290,
	14,
	26,
	114,
	31,
	1461,
	1462,
	1461,
	1462,
	720,
	1460,
	114,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	677,
	142,
	677,
	142,
	14,
	26,
	28,
	114,
	31,
	23,
	23,
	14,
	26,
	26,
	142,
	14,
	14,
	14,
	14,
	14,
	4,
	0,
	0,
	111,
	95,
	3,
	23,
	28,
	3,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	9,
	3,
	23,
	28,
	116,
	28,
	28,
	116,
	28,
	28,
	116,
	28,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	9,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	9,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	1453,
	23,
	1453,
	23,
	1456,
	23,
	1457,
	23,
	23,
	26,
	290,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	290,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	14,
	-1,
	-1,
	23,
	136,
	26,
	23,
	290,
	23,
	23,
	14,
	23,
	113,
	23,
	23,
	14,
	290,
	23,
	14,
	-1,
	-1,
	23,
	136,
	26,
	23,
	23,
	290,
	23,
	23,
	26,
	23,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	114,
	23,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	114,
	14,
	23,
	14,
	-1,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[82] = 
{
	{ 0x02000031, { 0, 101 } },
	{ 0x02000040, { 298, 2 } },
	{ 0x02000041, { 300, 3 } },
	{ 0x02000042, { 303, 4 } },
	{ 0x02000065, { 134, 1 } },
	{ 0x02000066, { 135, 1 } },
	{ 0x02000068, { 136, 4 } },
	{ 0x02000069, { 140, 13 } },
	{ 0x0200006A, { 153, 4 } },
	{ 0x0200006B, { 157, 1 } },
	{ 0x0200006C, { 158, 3 } },
	{ 0x0200006D, { 161, 3 } },
	{ 0x0200006E, { 164, 1 } },
	{ 0x0200006F, { 165, 10 } },
	{ 0x02000070, { 175, 3 } },
	{ 0x02000071, { 178, 3 } },
	{ 0x02000072, { 181, 1 } },
	{ 0x02000073, { 182, 11 } },
	{ 0x02000074, { 193, 2 } },
	{ 0x02000075, { 195, 2 } },
	{ 0x02000078, { 226, 3 } },
	{ 0x02000079, { 229, 8 } },
	{ 0x0200007A, { 237, 10 } },
	{ 0x02000081, { 273, 13 } },
	{ 0x02000085, { 286, 3 } },
	{ 0x0200008C, { 289, 3 } },
	{ 0x02000092, { 315, 4 } },
	{ 0x02000094, { 375, 2 } },
	{ 0x02000095, { 377, 2 } },
	{ 0x0600018B, { 101, 1 } },
	{ 0x06000199, { 102, 1 } },
	{ 0x0600019C, { 103, 1 } },
	{ 0x0600019F, { 104, 7 } },
	{ 0x060001A2, { 111, 6 } },
	{ 0x060001A5, { 117, 6 } },
	{ 0x060001A9, { 123, 6 } },
	{ 0x060001B1, { 129, 5 } },
	{ 0x060001B6, { 197, 14 } },
	{ 0x060001B7, { 211, 7 } },
	{ 0x060001B8, { 218, 8 } },
	{ 0x06000206, { 247, 1 } },
	{ 0x06000209, { 248, 1 } },
	{ 0x0600020C, { 249, 7 } },
	{ 0x06000212, { 256, 6 } },
	{ 0x06000219, { 262, 6 } },
	{ 0x06000220, { 268, 5 } },
	{ 0x06000228, { 292, 2 } },
	{ 0x06000229, { 294, 2 } },
	{ 0x0600022A, { 296, 2 } },
	{ 0x06000247, { 307, 3 } },
	{ 0x06000248, { 310, 3 } },
	{ 0x06000249, { 313, 2 } },
	{ 0x0600024E, { 319, 1 } },
	{ 0x06000251, { 320, 1 } },
	{ 0x06000252, { 321, 1 } },
	{ 0x06000253, { 322, 1 } },
	{ 0x06000254, { 323, 1 } },
	{ 0x06000258, { 324, 1 } },
	{ 0x06000259, { 325, 1 } },
	{ 0x0600025A, { 326, 1 } },
	{ 0x0600025E, { 327, 1 } },
	{ 0x0600025F, { 328, 1 } },
	{ 0x06000260, { 329, 1 } },
	{ 0x06000266, { 330, 6 } },
	{ 0x06000269, { 336, 1 } },
	{ 0x0600026A, { 337, 6 } },
	{ 0x0600026B, { 343, 1 } },
	{ 0x0600026C, { 344, 6 } },
	{ 0x06000270, { 350, 1 } },
	{ 0x06000271, { 351, 1 } },
	{ 0x06000272, { 352, 6 } },
	{ 0x06000276, { 358, 1 } },
	{ 0x06000277, { 359, 1 } },
	{ 0x06000278, { 360, 6 } },
	{ 0x0600027D, { 366, 2 } },
	{ 0x0600027E, { 368, 1 } },
	{ 0x06000284, { 369, 3 } },
	{ 0x06000285, { 372, 3 } },
	{ 0x06000287, { 379, 1 } },
	{ 0x06000288, { 380, 1 } },
	{ 0x06000289, { 381, 2 } },
	{ 0x0600028A, { 383, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[385] = 
{
	{ (Il2CppRGCTXDataType)3, 10808 },
	{ (Il2CppRGCTXDataType)3, 10809 },
	{ (Il2CppRGCTXDataType)2, 15265 },
	{ (Il2CppRGCTXDataType)3, 10810 },
	{ (Il2CppRGCTXDataType)3, 10811 },
	{ (Il2CppRGCTXDataType)3, 10812 },
	{ (Il2CppRGCTXDataType)2, 16018 },
	{ (Il2CppRGCTXDataType)3, 10813 },
	{ (Il2CppRGCTXDataType)3, 10814 },
	{ (Il2CppRGCTXDataType)2, 16019 },
	{ (Il2CppRGCTXDataType)3, 10815 },
	{ (Il2CppRGCTXDataType)3, 10816 },
	{ (Il2CppRGCTXDataType)3, 10817 },
	{ (Il2CppRGCTXDataType)3, 10818 },
	{ (Il2CppRGCTXDataType)3, 10819 },
	{ (Il2CppRGCTXDataType)3, 10820 },
	{ (Il2CppRGCTXDataType)2, 16020 },
	{ (Il2CppRGCTXDataType)3, 10821 },
	{ (Il2CppRGCTXDataType)3, 10822 },
	{ (Il2CppRGCTXDataType)3, 10823 },
	{ (Il2CppRGCTXDataType)3, 10824 },
	{ (Il2CppRGCTXDataType)3, 10825 },
	{ (Il2CppRGCTXDataType)3, 10826 },
	{ (Il2CppRGCTXDataType)3, 10827 },
	{ (Il2CppRGCTXDataType)3, 10828 },
	{ (Il2CppRGCTXDataType)3, 10829 },
	{ (Il2CppRGCTXDataType)3, 10830 },
	{ (Il2CppRGCTXDataType)3, 10831 },
	{ (Il2CppRGCTXDataType)3, 10832 },
	{ (Il2CppRGCTXDataType)3, 10833 },
	{ (Il2CppRGCTXDataType)2, 16021 },
	{ (Il2CppRGCTXDataType)3, 10834 },
	{ (Il2CppRGCTXDataType)3, 10835 },
	{ (Il2CppRGCTXDataType)3, 10836 },
	{ (Il2CppRGCTXDataType)3, 10837 },
	{ (Il2CppRGCTXDataType)3, 10838 },
	{ (Il2CppRGCTXDataType)3, 10839 },
	{ (Il2CppRGCTXDataType)3, 10840 },
	{ (Il2CppRGCTXDataType)2, 16022 },
	{ (Il2CppRGCTXDataType)3, 10841 },
	{ (Il2CppRGCTXDataType)2, 16023 },
	{ (Il2CppRGCTXDataType)3, 10842 },
	{ (Il2CppRGCTXDataType)3, 10843 },
	{ (Il2CppRGCTXDataType)3, 10844 },
	{ (Il2CppRGCTXDataType)3, 10845 },
	{ (Il2CppRGCTXDataType)3, 10846 },
	{ (Il2CppRGCTXDataType)3, 10847 },
	{ (Il2CppRGCTXDataType)3, 10848 },
	{ (Il2CppRGCTXDataType)2, 16024 },
	{ (Il2CppRGCTXDataType)3, 10849 },
	{ (Il2CppRGCTXDataType)3, 10850 },
	{ (Il2CppRGCTXDataType)3, 10851 },
	{ (Il2CppRGCTXDataType)2, 16025 },
	{ (Il2CppRGCTXDataType)3, 10852 },
	{ (Il2CppRGCTXDataType)3, 10853 },
	{ (Il2CppRGCTXDataType)3, 10854 },
	{ (Il2CppRGCTXDataType)3, 10855 },
	{ (Il2CppRGCTXDataType)3, 10856 },
	{ (Il2CppRGCTXDataType)3, 10857 },
	{ (Il2CppRGCTXDataType)3, 10858 },
	{ (Il2CppRGCTXDataType)2, 16026 },
	{ (Il2CppRGCTXDataType)3, 10859 },
	{ (Il2CppRGCTXDataType)3, 10860 },
	{ (Il2CppRGCTXDataType)2, 15274 },
	{ (Il2CppRGCTXDataType)3, 10861 },
	{ (Il2CppRGCTXDataType)3, 10862 },
	{ (Il2CppRGCTXDataType)3, 10863 },
	{ (Il2CppRGCTXDataType)2, 16023 },
	{ (Il2CppRGCTXDataType)2, 16027 },
	{ (Il2CppRGCTXDataType)3, 10864 },
	{ (Il2CppRGCTXDataType)3, 10865 },
	{ (Il2CppRGCTXDataType)3, 10866 },
	{ (Il2CppRGCTXDataType)3, 10867 },
	{ (Il2CppRGCTXDataType)2, 16028 },
	{ (Il2CppRGCTXDataType)2, 16029 },
	{ (Il2CppRGCTXDataType)2, 16028 },
	{ (Il2CppRGCTXDataType)3, 10868 },
	{ (Il2CppRGCTXDataType)3, 10869 },
	{ (Il2CppRGCTXDataType)3, 10870 },
	{ (Il2CppRGCTXDataType)2, 16030 },
	{ (Il2CppRGCTXDataType)3, 10871 },
	{ (Il2CppRGCTXDataType)3, 10872 },
	{ (Il2CppRGCTXDataType)2, 16031 },
	{ (Il2CppRGCTXDataType)3, 10873 },
	{ (Il2CppRGCTXDataType)3, 10874 },
	{ (Il2CppRGCTXDataType)3, 10875 },
	{ (Il2CppRGCTXDataType)2, 16032 },
	{ (Il2CppRGCTXDataType)3, 10876 },
	{ (Il2CppRGCTXDataType)3, 10877 },
	{ (Il2CppRGCTXDataType)2, 16033 },
	{ (Il2CppRGCTXDataType)3, 10878 },
	{ (Il2CppRGCTXDataType)3, 10879 },
	{ (Il2CppRGCTXDataType)3, 10880 },
	{ (Il2CppRGCTXDataType)3, 10881 },
	{ (Il2CppRGCTXDataType)2, 16034 },
	{ (Il2CppRGCTXDataType)3, 10882 },
	{ (Il2CppRGCTXDataType)3, 10883 },
	{ (Il2CppRGCTXDataType)2, 16035 },
	{ (Il2CppRGCTXDataType)3, 10884 },
	{ (Il2CppRGCTXDataType)3, 10885 },
	{ (Il2CppRGCTXDataType)3, 10886 },
	{ (Il2CppRGCTXDataType)3, 10887 },
	{ (Il2CppRGCTXDataType)3, 10888 },
	{ (Il2CppRGCTXDataType)3, 10889 },
	{ (Il2CppRGCTXDataType)2, 16036 },
	{ (Il2CppRGCTXDataType)3, 10890 },
	{ (Il2CppRGCTXDataType)2, 16037 },
	{ (Il2CppRGCTXDataType)3, 10891 },
	{ (Il2CppRGCTXDataType)3, 10892 },
	{ (Il2CppRGCTXDataType)3, 10893 },
	{ (Il2CppRGCTXDataType)3, 10894 },
	{ (Il2CppRGCTXDataType)2, 16038 },
	{ (Il2CppRGCTXDataType)3, 10895 },
	{ (Il2CppRGCTXDataType)3, 10896 },
	{ (Il2CppRGCTXDataType)2, 16039 },
	{ (Il2CppRGCTXDataType)3, 10897 },
	{ (Il2CppRGCTXDataType)3, 10898 },
	{ (Il2CppRGCTXDataType)2, 16040 },
	{ (Il2CppRGCTXDataType)3, 10899 },
	{ (Il2CppRGCTXDataType)3, 10900 },
	{ (Il2CppRGCTXDataType)2, 16041 },
	{ (Il2CppRGCTXDataType)3, 10901 },
	{ (Il2CppRGCTXDataType)3, 10902 },
	{ (Il2CppRGCTXDataType)2, 16042 },
	{ (Il2CppRGCTXDataType)3, 10903 },
	{ (Il2CppRGCTXDataType)3, 10904 },
	{ (Il2CppRGCTXDataType)2, 16043 },
	{ (Il2CppRGCTXDataType)3, 10905 },
	{ (Il2CppRGCTXDataType)3, 10906 },
	{ (Il2CppRGCTXDataType)2, 16044 },
	{ (Il2CppRGCTXDataType)3, 10907 },
	{ (Il2CppRGCTXDataType)3, 10908 },
	{ (Il2CppRGCTXDataType)3, 10909 },
	{ (Il2CppRGCTXDataType)3, 10910 },
	{ (Il2CppRGCTXDataType)3, 10911 },
	{ (Il2CppRGCTXDataType)3, 10912 },
	{ (Il2CppRGCTXDataType)3, 10913 },
	{ (Il2CppRGCTXDataType)3, 10914 },
	{ (Il2CppRGCTXDataType)3, 10915 },
	{ (Il2CppRGCTXDataType)3, 10916 },
	{ (Il2CppRGCTXDataType)3, 10917 },
	{ (Il2CppRGCTXDataType)3, 10918 },
	{ (Il2CppRGCTXDataType)2, 15333 },
	{ (Il2CppRGCTXDataType)3, 10919 },
	{ (Il2CppRGCTXDataType)2, 16045 },
	{ (Il2CppRGCTXDataType)3, 10920 },
	{ (Il2CppRGCTXDataType)3, 10921 },
	{ (Il2CppRGCTXDataType)3, 10922 },
	{ (Il2CppRGCTXDataType)3, 10923 },
	{ (Il2CppRGCTXDataType)3, 10924 },
	{ (Il2CppRGCTXDataType)3, 10925 },
	{ (Il2CppRGCTXDataType)3, 10926 },
	{ (Il2CppRGCTXDataType)3, 10927 },
	{ (Il2CppRGCTXDataType)3, 10928 },
	{ (Il2CppRGCTXDataType)3, 10929 },
	{ (Il2CppRGCTXDataType)3, 10930 },
	{ (Il2CppRGCTXDataType)3, 10931 },
	{ (Il2CppRGCTXDataType)3, 10932 },
	{ (Il2CppRGCTXDataType)3, 10933 },
	{ (Il2CppRGCTXDataType)3, 10934 },
	{ (Il2CppRGCTXDataType)2, 16046 },
	{ (Il2CppRGCTXDataType)3, 10935 },
	{ (Il2CppRGCTXDataType)3, 10936 },
	{ (Il2CppRGCTXDataType)2, 16047 },
	{ (Il2CppRGCTXDataType)3, 10937 },
	{ (Il2CppRGCTXDataType)2, 16048 },
	{ (Il2CppRGCTXDataType)3, 10938 },
	{ (Il2CppRGCTXDataType)3, 10939 },
	{ (Il2CppRGCTXDataType)2, 15367 },
	{ (Il2CppRGCTXDataType)3, 10940 },
	{ (Il2CppRGCTXDataType)2, 16049 },
	{ (Il2CppRGCTXDataType)3, 10941 },
	{ (Il2CppRGCTXDataType)3, 10942 },
	{ (Il2CppRGCTXDataType)3, 10943 },
	{ (Il2CppRGCTXDataType)3, 10944 },
	{ (Il2CppRGCTXDataType)3, 10945 },
	{ (Il2CppRGCTXDataType)3, 10946 },
	{ (Il2CppRGCTXDataType)3, 10947 },
	{ (Il2CppRGCTXDataType)3, 10948 },
	{ (Il2CppRGCTXDataType)3, 10949 },
	{ (Il2CppRGCTXDataType)2, 16051 },
	{ (Il2CppRGCTXDataType)3, 10950 },
	{ (Il2CppRGCTXDataType)2, 16052 },
	{ (Il2CppRGCTXDataType)3, 10951 },
	{ (Il2CppRGCTXDataType)3, 10952 },
	{ (Il2CppRGCTXDataType)2, 15390 },
	{ (Il2CppRGCTXDataType)3, 10953 },
	{ (Il2CppRGCTXDataType)2, 16053 },
	{ (Il2CppRGCTXDataType)3, 10954 },
	{ (Il2CppRGCTXDataType)3, 10955 },
	{ (Il2CppRGCTXDataType)3, 10956 },
	{ (Il2CppRGCTXDataType)3, 10957 },
	{ (Il2CppRGCTXDataType)3, 10958 },
	{ (Il2CppRGCTXDataType)3, 10959 },
	{ (Il2CppRGCTXDataType)3, 10960 },
	{ (Il2CppRGCTXDataType)3, 10961 },
	{ (Il2CppRGCTXDataType)3, 10962 },
	{ (Il2CppRGCTXDataType)2, 16054 },
	{ (Il2CppRGCTXDataType)3, 10963 },
	{ (Il2CppRGCTXDataType)2, 16055 },
	{ (Il2CppRGCTXDataType)3, 10964 },
	{ (Il2CppRGCTXDataType)3, 10965 },
	{ (Il2CppRGCTXDataType)2, 16056 },
	{ (Il2CppRGCTXDataType)3, 10966 },
	{ (Il2CppRGCTXDataType)2, 15413 },
	{ (Il2CppRGCTXDataType)3, 10967 },
	{ (Il2CppRGCTXDataType)3, 10968 },
	{ (Il2CppRGCTXDataType)2, 16057 },
	{ (Il2CppRGCTXDataType)3, 10969 },
	{ (Il2CppRGCTXDataType)2, 15415 },
	{ (Il2CppRGCTXDataType)3, 10970 },
	{ (Il2CppRGCTXDataType)3, 10971 },
	{ (Il2CppRGCTXDataType)3, 10972 },
	{ (Il2CppRGCTXDataType)2, 16059 },
	{ (Il2CppRGCTXDataType)3, 10973 },
	{ (Il2CppRGCTXDataType)2, 16060 },
	{ (Il2CppRGCTXDataType)3, 10974 },
	{ (Il2CppRGCTXDataType)3, 10975 },
	{ (Il2CppRGCTXDataType)3, 10976 },
	{ (Il2CppRGCTXDataType)3, 10977 },
	{ (Il2CppRGCTXDataType)3, 10978 },
	{ (Il2CppRGCTXDataType)2, 16064 },
	{ (Il2CppRGCTXDataType)3, 10979 },
	{ (Il2CppRGCTXDataType)2, 16065 },
	{ (Il2CppRGCTXDataType)3, 10980 },
	{ (Il2CppRGCTXDataType)3, 10981 },
	{ (Il2CppRGCTXDataType)3, 10982 },
	{ (Il2CppRGCTXDataType)3, 10983 },
	{ (Il2CppRGCTXDataType)3, 10984 },
	{ (Il2CppRGCTXDataType)2, 16067 },
	{ (Il2CppRGCTXDataType)3, 10985 },
	{ (Il2CppRGCTXDataType)2, 16067 },
	{ (Il2CppRGCTXDataType)3, 10986 },
	{ (Il2CppRGCTXDataType)3, 10987 },
	{ (Il2CppRGCTXDataType)3, 10988 },
	{ (Il2CppRGCTXDataType)3, 10989 },
	{ (Il2CppRGCTXDataType)3, 10990 },
	{ (Il2CppRGCTXDataType)2, 16068 },
	{ (Il2CppRGCTXDataType)3, 10991 },
	{ (Il2CppRGCTXDataType)2, 16068 },
	{ (Il2CppRGCTXDataType)3, 10992 },
	{ (Il2CppRGCTXDataType)3, 10993 },
	{ (Il2CppRGCTXDataType)3, 10994 },
	{ (Il2CppRGCTXDataType)3, 10995 },
	{ (Il2CppRGCTXDataType)3, 10996 },
	{ (Il2CppRGCTXDataType)3, 10997 },
	{ (Il2CppRGCTXDataType)3, 10998 },
	{ (Il2CppRGCTXDataType)3, 10999 },
	{ (Il2CppRGCTXDataType)3, 11000 },
	{ (Il2CppRGCTXDataType)2, 16069 },
	{ (Il2CppRGCTXDataType)3, 11001 },
	{ (Il2CppRGCTXDataType)2, 16070 },
	{ (Il2CppRGCTXDataType)3, 11002 },
	{ (Il2CppRGCTXDataType)3, 11003 },
	{ (Il2CppRGCTXDataType)3, 11004 },
	{ (Il2CppRGCTXDataType)3, 11005 },
	{ (Il2CppRGCTXDataType)2, 16071 },
	{ (Il2CppRGCTXDataType)3, 11006 },
	{ (Il2CppRGCTXDataType)3, 11007 },
	{ (Il2CppRGCTXDataType)2, 16072 },
	{ (Il2CppRGCTXDataType)3, 11008 },
	{ (Il2CppRGCTXDataType)3, 11009 },
	{ (Il2CppRGCTXDataType)2, 16073 },
	{ (Il2CppRGCTXDataType)3, 11010 },
	{ (Il2CppRGCTXDataType)3, 11011 },
	{ (Il2CppRGCTXDataType)2, 16074 },
	{ (Il2CppRGCTXDataType)3, 11012 },
	{ (Il2CppRGCTXDataType)3, 11013 },
	{ (Il2CppRGCTXDataType)2, 16075 },
	{ (Il2CppRGCTXDataType)3, 11014 },
	{ (Il2CppRGCTXDataType)3, 11015 },
	{ (Il2CppRGCTXDataType)3, 11016 },
	{ (Il2CppRGCTXDataType)3, 11017 },
	{ (Il2CppRGCTXDataType)3, 11018 },
	{ (Il2CppRGCTXDataType)3, 11019 },
	{ (Il2CppRGCTXDataType)2, 15557 },
	{ (Il2CppRGCTXDataType)3, 11020 },
	{ (Il2CppRGCTXDataType)2, 16076 },
	{ (Il2CppRGCTXDataType)3, 11021 },
	{ (Il2CppRGCTXDataType)3, 11022 },
	{ (Il2CppRGCTXDataType)3, 11023 },
	{ (Il2CppRGCTXDataType)3, 11024 },
	{ (Il2CppRGCTXDataType)3, 11025 },
	{ (Il2CppRGCTXDataType)3, 11026 },
	{ (Il2CppRGCTXDataType)3, 11027 },
	{ (Il2CppRGCTXDataType)3, 11028 },
	{ (Il2CppRGCTXDataType)3, 11029 },
	{ (Il2CppRGCTXDataType)3, 11030 },
	{ (Il2CppRGCTXDataType)2, 16077 },
	{ (Il2CppRGCTXDataType)3, 11031 },
	{ (Il2CppRGCTXDataType)3, 11032 },
	{ (Il2CppRGCTXDataType)2, 16078 },
	{ (Il2CppRGCTXDataType)2, 15614 },
	{ (Il2CppRGCTXDataType)3, 11033 },
	{ (Il2CppRGCTXDataType)2, 15618 },
	{ (Il2CppRGCTXDataType)3, 11034 },
	{ (Il2CppRGCTXDataType)2, 15623 },
	{ (Il2CppRGCTXDataType)3, 11035 },
	{ (Il2CppRGCTXDataType)3, 11036 },
	{ (Il2CppRGCTXDataType)3, 11037 },
	{ (Il2CppRGCTXDataType)3, 11038 },
	{ (Il2CppRGCTXDataType)3, 11039 },
	{ (Il2CppRGCTXDataType)3, 11040 },
	{ (Il2CppRGCTXDataType)3, 11041 },
	{ (Il2CppRGCTXDataType)3, 11042 },
	{ (Il2CppRGCTXDataType)3, 11043 },
	{ (Il2CppRGCTXDataType)3, 11044 },
	{ (Il2CppRGCTXDataType)2, 15654 },
	{ (Il2CppRGCTXDataType)2, 16079 },
	{ (Il2CppRGCTXDataType)3, 11045 },
	{ (Il2CppRGCTXDataType)2, 15657 },
	{ (Il2CppRGCTXDataType)2, 16080 },
	{ (Il2CppRGCTXDataType)3, 11046 },
	{ (Il2CppRGCTXDataType)2, 16081 },
	{ (Il2CppRGCTXDataType)3, 11047 },
	{ (Il2CppRGCTXDataType)2, 15665 },
	{ (Il2CppRGCTXDataType)2, 16082 },
	{ (Il2CppRGCTXDataType)3, 11048 },
	{ (Il2CppRGCTXDataType)3, 11049 },
	{ (Il2CppRGCTXDataType)3, 11050 },
	{ (Il2CppRGCTXDataType)3, 11051 },
	{ (Il2CppRGCTXDataType)3, 11052 },
	{ (Il2CppRGCTXDataType)3, 11053 },
	{ (Il2CppRGCTXDataType)3, 11054 },
	{ (Il2CppRGCTXDataType)3, 11055 },
	{ (Il2CppRGCTXDataType)3, 11056 },
	{ (Il2CppRGCTXDataType)3, 11057 },
	{ (Il2CppRGCTXDataType)3, 11058 },
	{ (Il2CppRGCTXDataType)3, 11059 },
	{ (Il2CppRGCTXDataType)3, 11060 },
	{ (Il2CppRGCTXDataType)2, 16083 },
	{ (Il2CppRGCTXDataType)3, 11061 },
	{ (Il2CppRGCTXDataType)3, 11062 },
	{ (Il2CppRGCTXDataType)2, 16084 },
	{ (Il2CppRGCTXDataType)3, 11063 },
	{ (Il2CppRGCTXDataType)3, 11064 },
	{ (Il2CppRGCTXDataType)3, 11065 },
	{ (Il2CppRGCTXDataType)2, 16085 },
	{ (Il2CppRGCTXDataType)3, 11066 },
	{ (Il2CppRGCTXDataType)3, 11067 },
	{ (Il2CppRGCTXDataType)2, 16086 },
	{ (Il2CppRGCTXDataType)3, 11068 },
	{ (Il2CppRGCTXDataType)3, 11069 },
	{ (Il2CppRGCTXDataType)3, 11070 },
	{ (Il2CppRGCTXDataType)2, 16087 },
	{ (Il2CppRGCTXDataType)3, 11071 },
	{ (Il2CppRGCTXDataType)3, 11072 },
	{ (Il2CppRGCTXDataType)2, 16088 },
	{ (Il2CppRGCTXDataType)3, 11073 },
	{ (Il2CppRGCTXDataType)3, 11074 },
	{ (Il2CppRGCTXDataType)3, 11075 },
	{ (Il2CppRGCTXDataType)3, 11076 },
	{ (Il2CppRGCTXDataType)2, 16089 },
	{ (Il2CppRGCTXDataType)3, 11077 },
	{ (Il2CppRGCTXDataType)3, 11078 },
	{ (Il2CppRGCTXDataType)2, 16090 },
	{ (Il2CppRGCTXDataType)3, 11079 },
	{ (Il2CppRGCTXDataType)3, 11080 },
	{ (Il2CppRGCTXDataType)3, 11081 },
	{ (Il2CppRGCTXDataType)3, 11082 },
	{ (Il2CppRGCTXDataType)2, 16091 },
	{ (Il2CppRGCTXDataType)3, 11083 },
	{ (Il2CppRGCTXDataType)3, 11084 },
	{ (Il2CppRGCTXDataType)2, 16092 },
	{ (Il2CppRGCTXDataType)3, 11085 },
	{ (Il2CppRGCTXDataType)3, 11086 },
	{ (Il2CppRGCTXDataType)3, 11087 },
	{ (Il2CppRGCTXDataType)3, 11088 },
	{ (Il2CppRGCTXDataType)3, 11089 },
	{ (Il2CppRGCTXDataType)2, 16093 },
	{ (Il2CppRGCTXDataType)3, 11090 },
	{ (Il2CppRGCTXDataType)3, 11091 },
	{ (Il2CppRGCTXDataType)2, 16094 },
	{ (Il2CppRGCTXDataType)3, 11092 },
	{ (Il2CppRGCTXDataType)3, 11093 },
	{ (Il2CppRGCTXDataType)3, 11094 },
	{ (Il2CppRGCTXDataType)3, 11095 },
	{ (Il2CppRGCTXDataType)3, 11096 },
	{ (Il2CppRGCTXDataType)3, 11097 },
	{ (Il2CppRGCTXDataType)3, 11098 },
	{ (Il2CppRGCTXDataType)3, 11099 },
	{ (Il2CppRGCTXDataType)2, 16097 },
	{ (Il2CppRGCTXDataType)3, 11100 },
	{ (Il2CppRGCTXDataType)2, 16098 },
	{ (Il2CppRGCTXDataType)3, 11101 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	972,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	82,
	s_rgctxIndices,
	385,
	s_rgctxValues,
	NULL,
};
