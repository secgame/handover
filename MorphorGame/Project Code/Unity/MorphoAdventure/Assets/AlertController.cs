﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertController : MonoBehaviour
{
    public Text lab_title;
    public Text lab_content;
    public Button btn_confirm;
    public Button btn_cancel;
    public Image img_content;
    public Text lab_btn_confirm;
    public Text lab_btn_cancel;

    public void setup(string str_title, string str_content, Sprite img, string str_confirm, string str_cancel, bool isShowCancel)
    {
        lab_title.text = str_title;
        lab_content.text = str_content;
        if (img != null)
        {
            img_content.sprite = img;
            img_content.gameObject.SetActive(true);
            lab_content.gameObject.SetActive(false);
        }
        else
        {
            img_content.gameObject.SetActive(false);
            lab_content.gameObject.SetActive(true);
        }
        lab_btn_confirm.text = str_confirm;
        lab_btn_cancel.text = str_cancel;

        if (isShowCancel)
        {
            btn_cancel.gameObject.SetActive(true);
        }
        else
        {
            btn_cancel.gameObject.SetActive(false);
        }
    }

    public void dismiss_self()
    {
        Destroy(this.gameObject);
    }
    public void confirmClicked()
    {
        GameManager gm = GameObject.Find("GM").GetComponent<GameManager>();
        gm.play_SE("se_confirm", 1f);
    }
    public void cancelClicked()
    {
        GameManager gm = GameObject.Find("GM").GetComponent<GameManager>();
        gm.play_SE("se_cancel", 1f);
    }
}
