﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnBoardTutorialController : MonoBehaviour
{
    public GameObject bgObjects;
    public GameObject highlightObjects;
    public Text lab_tutorialText;
    GameObject NextObject;

    // Start is called before the first frame update
    public void highlight_obj(GameObject obj, string displayText, GameObject nextCallObject)
    {
        // copy obj
        GameObject CopyObject = (GameObject)Instantiate(obj, highlightObjects.transform);

        // set position
        CopyObject.transform.position = obj.transform.position;

        // set parent
        CopyObject.transform.parent = highlightObjects.transform;

        // // add reset click effect
        // if (CopyObject.GetComponent<Button>() == null)
        // {
        //     CopyObject.AddComponent<Button>();
        // }
        // CopyObject.GetComponent<Button>().onClick.AddListener(reset_toturialContent);

        // show bg panel
        bgObjects.SetActive(true);

        // set tutorial text
        lab_tutorialText.text = displayText;

        NextObject = nextCallObject;
    }
    public void reset_toturialContent()
    {
        lab_tutorialText.text = "";
        foreach (Transform t in highlightObjects.transform)
        {
            Destroy(t.gameObject);
        }
        bgObjects.SetActive(false);

        if (NextObject != null)
        {
            NextObject.SendMessage("checkShowTutorialContent");
        }
    }
}
