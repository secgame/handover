﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Hamster : MiniGameMaster
{
    List<int> random_pos = new List<int>();
    int move_time=10;
    public Sprite sp_selection_bg;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup grid view
        setup_gridView();

        // setup hamster view
        setup_Minigame_Hamster();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }
    void setup_gridView()
    {
        var gridLayoutGroup_1 = panel_selections.transform.GetChild(0).gameObject.GetComponent<GridLayoutGroup> ();
        var gridLayoutGroup_2 = panel_selections.transform.GetChild(1).gameObject.GetComponent<GridLayoutGroup> ();
        var rect = panel_selections.GetComponent<RectTransform>();
        var spacing = 20;
        var padding = 20;

        Vector2 newCellSize = new Vector2((rect.rect.height - 50)/2, (rect.rect.height - 50)/2);
        gridLayoutGroup_1.cellSize = newCellSize;
        gridLayoutGroup_2.cellSize = newCellSize;
    }

    public override void showResultAnimation()
    {
        foreach(Transform t in panel_selections.transform.GetChild(0).transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("show_result");
        }

        foreach(Transform t in panel_selections.transform.GetChild(1).transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("show_result");
        }
    }

    public override GameObject get_selectedObject()
    {
        return array_selectionsObj[current_selected];
    }

    public override void set_selections(Selection[] array_selections)
    {
        reset_selectionsAnim(panel_selections.transform.GetChild(0).gameObject);
        reset_selectionsAnim(panel_selections.transform.GetChild(1).gameObject);
        // reset
        random_pos = new List<int>();
        List<int> setPos = new List<int>();
        for (int i=0; i<array_selectionsObj.Length; i++)
        {
            GameObject obj = array_selectionsObj[i];
            // set as not selected color
            obj.GetComponent<Image>().color = new Color32(255,255,255,255);
        }

        // generate some random position
        do
        {
            // new random pos
            int newRandomInt = Random.Range(0,array_selectionsObj.Length);
            bool isUsedPos = false;

            foreach (int i in random_pos.ToArray())
            {
                if (newRandomInt == i)
                {
                    isUsedPos = true;
                    break;
                }
            }
            if (!isUsedPos)
            {
                // add
                // UnityEngine.Debug.Log("newRandomInt: " + newRandomInt);
                random_pos.Add(newRandomInt);
            }
        } while (random_pos.Count != (array_selections.Length));

        // sort
        random_pos.Sort();

        // set selections on ramdom position
        for (int i=0; i<array_selectionsObj.Length; i++)
        {
            bool isCurrentPos = false;
            // check -> current selection position
            foreach (int currentPos in random_pos)
            {
                bool isSet = false;
                // check set or not
                foreach(int p in setPos)
                {
                    if (currentPos == p)
                    {
                        isSet = true;
                    }
                }
                if ((!isSet) && (currentPos == i))
                {
                    isCurrentPos = true;
                    break;
                }
            }

            SelectionObject selectionObject = array_selectionsObj[i].GetComponent<SelectionObject>();
            Button btn = selectionObject.gameObject.GetComponent<Button>();
            int targetSelected = i;
            btn.onClick.AddListener(() => this.selection_selected(targetSelected));
            selectionObject.gameObject.transform.GetChild(2).gameObject.SetActive(false);
            selectionObject.setSprite(sprite_selection);

            if (isCurrentPos)
            {
                selectionObject.setup(array_selections[setPos.Count], sprite_selection);
                btn.interactable = true;
                setPos.Add(i);
                selectionObject.showSprite(true);
            }
            // set as default empty selection
            else
            {
                selectionObject.panel_text.SetActive(false);
                btn.interactable = false;
                selectionObject.showSprite(false);
            }
        }

        // show pop animation
        foreach(Transform t in panel_selections.transform.GetChild(0).transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("idle");
        }

        foreach(Transform t in panel_selections.transform.GetChild(1).transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("idle");
        }
    }

    public void setup_Minigame_Hamster()
    {
        // get sp_selection_bg
        sp_selection_bg = Resources.Load<Sprite>(sourcePath + "btn_bg");
        
        List<GameObject> objs = new List<GameObject>();

        foreach(Transform t in panel_selections.transform.GetChild(0))
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<SelectionObject>().showSprite(false);
            obj.GetComponent<Image>().sprite = sp_selection_bg;
            objs.Add(obj);
        }
        foreach(Transform t in panel_selections.transform.GetChild(1))
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<SelectionObject>().showSprite(false);
            obj.GetComponent<Image>().sprite = sp_selection_bg;
            objs.Add(obj);
        }
        array_selectionsObj = objs.ToArray();

        // set object move time
        move_time = (limited_time != 0) ? limited_time : move_time; 
        UnityEngine.Debug.Log("move_time: " + move_time);
        
    }
    void do_move_anim()
    {
        Animator anim = panel_selections.GetComponent<Animator>();
        anim.speed = 60f/(float)move_time;
        UnityEngine.Debug.Log("anim speed: " + ((float)move_time/60f));

        anim.SetTrigger("hide");
    }
    void do_pause_anim()
    {
        Animator anim = panel_selections.GetComponent<Animator>();
        anim.speed = 0f;
    }
    public override void timesUp()
    {
        UnityEngine.Debug.Log("timesUp called");
        do_timesUp();
    }
    // =====================================================================================================
    // override functions
    // =====================================================================================================
    public override void generateQuestion_and_setAnimation()
    {
        // call generate question
        generateQuestion();

        // call set movement
        if (limited_time != 0)
        {
            do_move_anim();
        }
    }
    public override void confirm_clicked()
    {
        // pause aniamtion
        if (limited_time != 0)
        {
            do_pause_anim();
        }

        if (current_selected != -1)
        {
            int selectedIndex = get_real_currentSelected();

            bool result = do_confirmClicked(selectedIndex);
        }
    }

    int get_real_currentSelected()
    {
        int selectedIndex = 0;
        for (int i=0; i<random_pos.ToArray().Length; i++)
        {
            if (random_pos[i] == current_selected)
            {
                selectedIndex = i;
                break;
            }
        }

        return selectedIndex;
    }

    public override void play_word_audio(int selected)
    {
        string word_eng = get_word_eng(selected);        

        do_play_word_audio(word_eng);
    }
    public override string get_word_eng(int selected)
    {
        string word_eng = "";
        int realIndex = 0;
        for (int i=0; i<random_pos.ToArray().Length; i++)
        {
            if (random_pos[i] == selected)
            {
                realIndex = i;
                break;
            }
        }
        
        Word[] array_words = list_selections[realIndex].array_word;
        foreach(Word w in array_words)
        {
            word_eng += w.word_eng;
        }
        return word_eng;
    }
    // =====================================================================================================
}
