﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievement_effect : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator anim;
    public AchivementController achivementController;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void change_page_effect(bool isNext)
    {
        if (isNext)
        {
            anim.SetTrigger("change_next");
        }
        else
        {
            anim.SetTrigger("change_last");
        }
    }

    public void call_reloadStickers()
    {
        Debug.Log("call_reloadStickers called");
        if (achivementController.isShowing_smallStickers)
        {
            achivementController.load_smallSticker();
        }
        else
        {
            achivementController.load_largeSticker();
        }
    }
}
