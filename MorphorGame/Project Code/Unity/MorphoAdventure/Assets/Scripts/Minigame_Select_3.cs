﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Select_3 : MiniGameMaster
{
    public MiniGameController.MiniGameMoveDirection move_direction = MiniGameController.MiniGameMoveDirection.none;
    int move_time=10;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup Minigame_Chase_Normal
        setup_Minigame_Select_3();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    public void setup_Minigame_Select_3()
    {
        // set object move time
        move_time = (limited_time != 0) ? limited_time : move_time;
        UnityEngine.Debug.Log("limited_time: " + limited_time);
    }
    void do_move_anim()
    {
        Animator anim = panel_selections.GetComponent<Animator>();
        anim.speed = 60f/(float)move_time;

        string animationName = "";
        switch (move_direction)
        {
            case MiniGameController.MiniGameMoveDirection.ToUP:
                animationName = "up_toEnd";
            break;

            case MiniGameController.MiniGameMoveDirection.ToDown:
                animationName = "down_toEnd";
            break;

            case MiniGameController.MiniGameMoveDirection.ToLeft:
                animationName = "left_toEnd";
            break;

            case MiniGameController.MiniGameMoveDirection.ToRight:
                animationName = "right_toEnd";
            break;

            case MiniGameController.MiniGameMoveDirection.none:
                animationName = "right_toEnd";
            break;
        }

        anim.SetTrigger(animationName);
        Debug.Log("anim.speed: " + anim.speed);
        Debug.Log("move_time: " + move_time);
    }
    void do_pause_anim()
    {
        Animator anim = panel_selections.GetComponent<Animator>();
        anim.speed = 0f;
    }

    // =====================================================================================================
    // override functions
    // =====================================================================================================
    public override void generateQuestion_and_setAnimation()
    {
        if (move_direction == MiniGameController.MiniGameMoveDirection.ToUP || move_direction == MiniGameController.MiniGameMoveDirection.ToDown)
        {
            panel_selections =  GameObject.Find("Panel_selections_hor");
        }

        // call generate question
        generateQuestion();

        // call set movement
        do_move_anim();
    }
    public override void confirm_clicked()
    {
        if (current_selected != -1 && (obj_progressBar.progress_current != obj_progressBar.progress_max))
        {
            // pause aniamtion
            do_pause_anim();

            // do check confirm
            do_confirmClicked(current_selected);
        }
    }
    public override void judge_clicked(bool isCorrect)
    {
        if (obj_progressBar.progress_current != obj_progressBar.progress_max)
        {
            // pause animation
            do_pause_anim();

            // do check judge
            do_judgeClicked(isCorrect);
        }
    }
    // =====================================================================================================
}
