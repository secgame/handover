﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorySlideController : MonoBehaviour
{
    public MainController obj_MainController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void call_nextSlide()
    {
        obj_MainController.next_storySlide(false);
        this.GetComponent<Animator>().SetBool("idle", true);
    }
    public void call_previousSlide()
    {
        obj_MainController.previous_storySlide();
        this.GetComponent<Animator>().SetBool("idle", true);
    }
}
