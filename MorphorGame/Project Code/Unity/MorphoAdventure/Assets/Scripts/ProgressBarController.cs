﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarController : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider slider;
    public int progress_max;
    public int progress_current;
    public Text lab_progress;
    public void setup(int max)
    {
        progress_max = max;
        slider.value = 0;
        lab_progress.text = progress_current + " / " + progress_max;
    }
    public void set_prgress(int val)
    {
        progress_current = val;
        slider.value = (float)progress_current/(float)progress_max;;
        lab_progress.text = progress_current + " / " + progress_max;
    }
    public void progress_add()
    {
        progress_current += 1;
        slider.value = (float)progress_current/(float)progress_max;;
        lab_progress.text = progress_current + " / " + progress_max;
    }
}
