﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGame_Move_forward : MiniGameMaster
{

    int move_time=10;
    public GameObject obj_runner;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup Minigame_Doge_Normal
        setup_Minigame_Doge_Normal();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    public void setup_Minigame_Doge_Normal()
    {
        // set objects
        obj_runner = GameObject.Find("obj_runner");
        obj_runner.GetComponent<Image>().sprite = Resources.Load<Sprite>(sourcePath + "runner");

        // set object move time
        move_time = (limited_time != 0) ? limited_time : move_time;
    }
    void do_move_anim()
    {
        Animator anim_selection = panel_selections.GetComponent<Animator>();
        anim_selection.speed = 60f/(float)move_time;
        anim_selection.SetTrigger("down");

        Animator anim_bg = img_bg.gameObject.GetComponent<Animator>();
        anim_bg.speed = 60f/(float)move_time;
        string triggername = "zoom_" + (obj_progressBar.progress_current + 1);
        anim_bg.SetTrigger(triggername);
        Debug.Log("triggername: " + triggername);
    }
    void do_pause_anim()
    {
        Animator anim_selection = panel_selections.GetComponent<Animator>();
        anim_selection.speed = 0f;

        Animator anim_bg = img_bg.gameObject.GetComponent<Animator>();
        anim_bg.speed = 0f;
    }

    // =====================================================================================================
    // override functions
    // =====================================================================================================
    public override void generateQuestion_and_setAnimation()
    {
        // call generate question
        generateQuestion();

        // call set movement
        do_move_anim();
    }
    public override void confirm_clicked()
    {
        // pause aniamtion
        do_pause_anim();

        // do check confirm
        do_confirmClicked(current_selected);
    }
    public override void judge_clicked(bool isCorrect)
    {
        // pause animation
        do_pause_anim();

        // do check judge
        do_judgeClicked(isCorrect);
    }
    // =====================================================================================================
}
