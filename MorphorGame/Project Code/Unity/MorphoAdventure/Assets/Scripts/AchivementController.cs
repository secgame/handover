﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Models;

public class AchivementController : MonoBehaviour
{
    // Start is called before the first frame update
    GameManager gm;
    public GridLayoutGroup gridLayoutGroup_1;
    public GridLayoutGroup gridLayoutGroup_2;
    public Achievement_effect achi_eff;
    public GameObject prefab_collectionObject;
    public Button btn_smallSticker;
    public Button btn_largeSticker;
    public Sprite sp_stickerTag_enable;
    public Sprite sp_stickerTag_disable;
    public Sprite sp_stickerSmall_bg;
    public Sprite sp_stickerlarge_bg;
    public Sprite sp_nextPage_enable;
    public Sprite sp_nextPage_disable;
    public Sprite sp_lastPage_enable;
    public Sprite sp_lastPage_disable;
    public Button btn_nextPage;
    public Button btn_lastPage;

    public bool isShowing_smallStickers = true;
    int current_page_sticker = 0;
    int max_week = 0;
    void Awake()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
    }
    void Start()
    {
        // show small sticker by default
        show_sticker_small_clicked();
        load_smallSticker();

        // set buttons
        set_changePageButtons();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setup_gridView(bool isSmall)
    {
        if (isSmall)
        {
            var rect = gridLayoutGroup_1.gameObject.GetComponent<RectTransform> ();
            var spacing = 0;
            var padding = 10;
            var numOfColum = 3;
            var cell_w = (rect.rect.width - 20) / numOfColum;
            Vector2 cellSize = new Vector2(cell_w, cell_w);
            gridLayoutGroup_1.cellSize = cellSize;
            gridLayoutGroup_2.cellSize = cellSize;
            gridLayoutGroup_1.constraintCount = numOfColum;
            gridLayoutGroup_2.constraintCount = numOfColum;
            gridLayoutGroup_1.padding.top = padding;
            gridLayoutGroup_1.padding.left = padding;
            gridLayoutGroup_1.padding.right = padding;
            gridLayoutGroup_2.padding.top = padding;
            gridLayoutGroup_2.padding.left = padding;
            gridLayoutGroup_2.padding.right = padding;
        }
        else
        {
            var rect = gridLayoutGroup_1.gameObject.GetComponent<RectTransform> ();
            var spacing = 0;
            int padding = (int)(rect.rect.width/4);
            int numOfColum = 1;
            int cell_w = (int)(rect.rect.width/2);
            Vector2 cellSize = new Vector2(cell_w, cell_w*1.147f);
            gridLayoutGroup_1.cellSize = cellSize;
            gridLayoutGroup_2.cellSize = cellSize;
            gridLayoutGroup_1.constraintCount = numOfColum;
            gridLayoutGroup_2.constraintCount = numOfColum;
            gridLayoutGroup_1.padding.top = padding;
            gridLayoutGroup_1.padding.left = padding;
            gridLayoutGroup_1.padding.right = padding;
            gridLayoutGroup_2.padding.top = padding;
            gridLayoutGroup_2.padding.left = padding;
            gridLayoutGroup_2.padding.right = padding;
        }
    }
    public void backClicked()
    {
        gm.play_SE("se_cancel", 1f);
        MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
        mainController.gotoMapClicked();
    }

    void clean_stickers()
    {
        foreach(Transform t in gridLayoutGroup_1.transform)
        {
            GameObject.Destroy(t.gameObject);
        }
        foreach(Transform t in gridLayoutGroup_2.transform)
        {
            GameObject.Destroy(t.gameObject);
        }
    }

    public void show_sticker_small_clicked()
    {
        if (isShowing_smallStickers != true)
        {
            achi_eff.change_page_effect(true);
        }
        isShowing_smallStickers = true;
        current_page_sticker = 0;
        btn_smallSticker.GetComponent<Image>().sprite = sp_stickerTag_enable;
        btn_largeSticker.GetComponent<Image>().sprite = sp_stickerTag_disable;
        
    }

    public void load_smallSticker()
    {
        // setup grid view
        setup_gridView(true);

        clean_stickers();
        float[][] socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();
        // RULES: counting scores in week, >=2, >=4, >=5
        int numOfDays = socres_groupByDay.Length;
        max_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();

        int count_week_shown = 0;
        int checking_week_start = 0 + GameSetting.sticker_small_weekToShow * current_page_sticker;
        for (int checking_week = checking_week_start; checking_week < max_week; checking_week++)
        {
            // counting num of scores in the week
            int numOfScoresInWeek = 0;
            for (int day=1; day<=7; day++)
            {
                int dayToGet = day + checking_week*7;
                if (socres_groupByDay.Length > dayToGet)
                {
                    if (socres_groupByDay[dayToGet].Length >= 3)        // must have 3 score in 1 day to be calculated as finished day
                    {
                        numOfScoresInWeek++;
                    }
                }
                else
                {
                    break;
                }
            }

            // get sticker sprite of the week
            Sprite[] sp_stickerFront = Resources.LoadAll<Sprite>("StickerImages/small/Week_" + (checking_week+1));
            bool isLeft = (count_week_shown < GameSetting.sticker_small_weekToShow / 2);
            // setting stickers
            int emptyStickerToFill = 3;
            if (numOfScoresInWeek >=2)  // first sticker
            {
                init_sticker(isLeft, sp_stickerFront[0], sp_stickerFront[0], 1f, false);
                emptyStickerToFill -= 1;
            }
            if (numOfScoresInWeek >=4)  // second sticker
            {
                init_sticker(isLeft, sp_stickerFront[1], sp_stickerFront[1], 1f, false);
                emptyStickerToFill -= 1;
            }
            if (numOfScoresInWeek >=5)  // third sticker
            {
                init_sticker(isLeft, sp_stickerFront[2], sp_stickerFront[2], 1f, false);
                emptyStickerToFill -= 1;
            }

            for (int i=0; i<emptyStickerToFill; i++)
            {
                init_sticker(isLeft, sp_stickerSmall_bg, sp_stickerSmall_bg, 1f, false);
            }

            // counter ++
            count_week_shown++;
            // break when more than 8 week shown
            if (count_week_shown >= GameSetting.sticker_small_weekToShow)
            {
                break;
            }
        }
        // set page buttons layout
        set_changePageButtons();
    }

    public void init_sticker(bool isLeftSide, Sprite sp_bg, Sprite sp_front, float fillAmount, bool isLarge)
    {
        GameObject newObj = (GameObject)Instantiate(prefab_collectionObject, ((isLeftSide) ? gridLayoutGroup_1 : gridLayoutGroup_2).transform);
        newObj.GetComponent<CollectionObjectController>().setup(sp_bg, sp_front, fillAmount, isLarge);
    }

    public void show_sticker_large_clicked()
    {
        if (isShowing_smallStickers != false)
        {
            achi_eff.change_page_effect(true);
        }
        isShowing_smallStickers = false;
        current_page_sticker = 0;
        btn_smallSticker.GetComponent<Image>().sprite = sp_stickerTag_disable;
        btn_largeSticker.GetComponent<Image>().sprite = sp_stickerTag_enable;
        
    }
    public void load_largeSticker()
    {
        // setup grid view
        setup_gridView(false);

        clean_stickers();
        float[][] socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();
        // RULES: counting number of score above % in week, every 5 count + 33.3% of fill mount
        int numOfDays = socres_groupByDay.Length;
        max_week = (int)(System.Math.Ceiling((float)numOfDays / 7f));
        int count_week_shown = 0;
        int checking_week_start = 0 + GameSetting.sticker_large_weekToShow * current_page_sticker;
        for (int checking_week = checking_week_start; checking_week < max_week; checking_week++)
        {
            // counting num of star in the week
            int numOfStarInWeek = 0;
            for (int day=1; day<=7; day++)
            {
                int dayToGet = day + checking_week*7;
                if (socres_groupByDay.Length > dayToGet)
                {
                    float[] day_scores = socres_groupByDay[dayToGet];
                    foreach(float score_float in day_scores)
                    {
                        if (score_float >= GameSetting.minigame_getStarScore)
                        {
                            numOfStarInWeek++;
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            // calculate fill amount
            float fillAmount = 0f;
            if (numOfStarInWeek >= 15)
            {
                fillAmount = 1f;
            }
            else if (numOfStarInWeek >= 10)
            {
                fillAmount = 0.66f;
            }
            else if (numOfStarInWeek >= 5)
            {
                fillAmount = 0.33f;
            }

            // get sticker sprite of the week
            Sprite sp_stickerFront = Resources.Load<Sprite>("StickerImages/large/Week_" + (checking_week+1));

            // init sticker
            bool isLeft = (count_week_shown < GameSetting.sticker_large_weekToShow / 2);
            init_sticker(isLeft, sp_stickerFront, sp_stickerFront, fillAmount, true);

            // counter ++
            count_week_shown++;
            // break when more than 8 week shown
            if (count_week_shown >= GameSetting.sticker_large_weekToShow)
            {
                break;
            }
        }
        // set page buttons layout
        set_changePageButtons();
    }

    void set_changePageButtons()
    {
        // set last page button
        if (current_page_sticker == 0)
        {
            btn_lastPage.GetComponent<Image>().sprite = sp_lastPage_disable;
            btn_lastPage.interactable = false;
        }
        else
        {
            btn_lastPage.GetComponent<Image>().sprite = sp_lastPage_enable;
            btn_lastPage.interactable = true;
        }

        // set next page button
        if(max_week <= ((isShowing_smallStickers) ? GameSetting.sticker_small_weekToShow : GameSetting.sticker_large_weekToShow) * (current_page_sticker+1))
        {
            btn_nextPage.GetComponent<Image>().sprite = sp_nextPage_disable;
            btn_nextPage.interactable = false;
        }
        else
        {
            btn_nextPage.GetComponent<Image>().sprite = sp_nextPage_enable;
            btn_nextPage.interactable = true;
        }
    }
    

    public void next_page_clicked()
    {
        current_page_sticker ++;
        achi_eff.change_page_effect(true);
    }
    public void previous_page_clicked()
    {
        current_page_sticker --;
        achi_eff.change_page_effect(false);
    }
}
