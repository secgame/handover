﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Replace : MiniGameMaster
{
    // Start is called before the first frame update
    public List<int> list_used_pos = new List<int>();
    public Sprite sprite_replace;
    List<int> random_pos = new List<int>();
    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup grid view
        setup_gridView();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        // get replace sprite
        sprite_replace = Resources.Load<Sprite>(sourcePath + "btn_option_replace");

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void setup_gridView()
    {
        var gridLayoutGroup = panel_selections.GetComponent<GridLayoutGroup> ();
        var rect = panel_selections.GetComponent<RectTransform> ();
        var spacing = 20;
        var padding = 20;
        var numOfColum = 4;

        // gridLayoutGroup.spacing = new Vector2(spacing, spacing);
        gridLayoutGroup.cellSize = new Vector2 ((rect.rect.width- 2 * padding - spacing*(numOfColum-1)) / numOfColum, (rect.rect.height - 2 * padding - spacing*2) / 3);
    }

    public override void set_selections(Selection[] array_selections)
    {
        reset_selectionsAnim(panel_selections);
        // reset random pos
        random_pos = new List<int>();
        List<int> setPos = new List<int>();
        for (int i=0; i<array_selectionsObj.Length; i++)
        {
            GameObject obj = array_selectionsObj[i];
            // set as not selected color
            obj.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255,255,255,255);
        }
        
        // get some random position int
        do
        {
            // UnityEngine.Debug.Log("start do while");
            // new random pos
            int newRandomInt = Random.Range(0,12);
            // check if used or not
            bool isUsedPos = false;
            foreach (int i in list_used_pos.ToArray())
            {
                if (newRandomInt == i)
                {
                    isUsedPos = true;
                    break;
                }
            }
            foreach (int i in random_pos.ToArray())
            {
                if (newRandomInt == i)
                {
                    isUsedPos = true;
                    break;
                }
            }
            if (!isUsedPos)
            {
                // add
                // UnityEngine.Debug.Log("newRandomInt: " + newRandomInt);
                random_pos.Add(newRandomInt);
            }
        } while (random_pos.Count != (array_selections.Length));

        // sort
        random_pos.Sort();

        // set each random position content
        for (int i=0; i<12; i++)
        {
            bool isUsedPos = false;
            bool isCurrentPos = false;
            // check -> used pos
            foreach (int usedPos in list_used_pos)
            {
                if (i == usedPos)
                {
                    isUsedPos = true;
                    break;
                }
            }
            // check -> current selection position
            foreach (int currentPos in random_pos)
            {
                bool isSet = false;
                // check set or not
                foreach(int p in setPos)
                {
                    if (currentPos == p)
                    {
                        isSet = true;
                    }
                }
                if ((!isSet) && (currentPos == i))
                {
                    isCurrentPos = true;
                    break;
                }
            }

            SelectionObject selectionObject = panel_selections.transform.GetChild(i).gameObject.GetComponent<SelectionObject>();
            Button btn = selectionObject.gameObject.GetComponent<Button>();
            selectionObject.gameObject.transform.GetChild(2).gameObject.SetActive(false);

            // set used selction
            if (isUsedPos)
            {
                selectionObject.panel_text.SetActive(false);
                btn.interactable = false;
                // selectionObject.setSprite(sprite_replace);
            }
            // set current active selection content
            else if (isCurrentPos)
            {
                selectionObject.setup(array_selections[setPos.Count], sprite_selection);
                btn.interactable = true;
                setPos.Add(i);
            }
            // set as default empty selection
            else
            {
                selectionObject.panel_text.SetActive(false);
                btn.interactable = false;
                selectionObject.setSprite(sprite_selection);
            }
        }
        // for (int i=0; i<random_pos.Count; i++)
        // {
            
        // }
    }

    public void init_12_selections()
    {
        // remove all childs
        foreach (Transform child in panel_selections.transform) {
            GameObject.Destroy(child.gameObject);
        }
        List<GameObject> objs = new List<GameObject>();

        for (int i=0; i<12; i++)
        {
            // init new selection object
            GameObject newObj = (GameObject)Instantiate(prefab_selection, panel_selections.transform);
            objs.Add(newObj);
            SelectionObject selectionObject = newObj.GetComponent<SelectionObject>();
            Button btn = newObj.GetComponent<Button>();
            int targetSelected = i;
            btn.onClick.AddListener(() => this.selection_selected(targetSelected));
        }     
        array_selectionsObj = objs.ToArray();   
    }

    public override void confirm_clicked()
    {
        if (current_selected != -1)
        {
            int selectedIndex = 0;
            for (int i=0; i<random_pos.ToArray().Length; i++)
            {
                // UnityEngine.Debug.Log("i: " + i + " random_pos[i]: " + random_pos[i]);
                if (random_pos[i] == (current_selected))
                {
                    selectedIndex = i;
                    break;
                }
            }
            // UnityEngine.Debug.Log("selectedIndex: " + selectedIndex);
            bool result = do_confirmClicked(selectedIndex);

            if (result)
            {
                // add used position
                list_used_pos.Add(current_selected);

                // change used position sprite
                SelectionObject obj_used = panel_selections.transform.GetChild(current_selected).gameObject.GetComponent<SelectionObject>();
                obj_used.setSprite(sprite_replace);
            }
        }
    }

    public override void play_word_audio(int selected)
    {
        string word_eng = get_word_eng(selected);

        do_play_word_audio(word_eng);
    }

    public override string get_word_eng(int selected)
    {
        foreach (Selection s in list_selections)
        {
            string w_chi = "";
            foreach (Word w in s.array_word)
            {
                w_chi += w.word_chi;
            }
        }

        int realIndex = 0;
        for (int i=0; i<random_pos.ToArray().Length; i++)
        {
            if (random_pos[i] == (selected))
            {
                realIndex = i;
                break;
            }
        }
        string word_eng = "";
        Word[] array_words = list_selections[realIndex].array_word;
        foreach(Word w in array_words)
        {
            word_eng += w.word_eng;
        }

        return word_eng;
    }

    public override void start_game()
    {
        init_12_selections();
        
        do_start_game();
    }
}
