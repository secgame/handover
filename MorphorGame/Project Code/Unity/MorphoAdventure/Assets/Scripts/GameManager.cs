﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Models;
using System;
using Proyecto26;

public class GameManager : MonoBehaviour
{
    public bool isTestingMode = true;
    public User playerData;
    public int current_day;
    public int current_story_part = 0;
    public MiniGameController.MiniGameType curret_temp_name;
    public MiniGameController.MiniGameQuestionType curret_question_type;
    public int current_progress_day = 0;
    public AudioSource audio_se;
    public AudioSource audio_bgm;
    private static GameObject instance;
    public GameObject prefab_alert;
    public GameObject prefab_loading;
    GameObject canvas;

    public UnityEngine.Video.VideoPlayer videoPlayer;

    public bool isPlaying_video = false;
    // ################################################################################################
    // Server Side
    // ################################################################################################
    private RequestHelper currentRequest;
    private readonly string basePath_gameProgress = APISetting.basePath + "/api/gameProgress";
    private readonly string basePath_User = APISetting.basePath + "/api/user";
    // ################################################################################################


    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
         instance = gameObject;
        else
         Destroy(gameObject);
    }
    void Start()
    {
        set_audios_volume();
        // start game bgm
        play_BGM("bgm_main");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void set_audios_volume()
    {
        float f_bgm = PlayerPrefs.GetFloat("setting_bgm", GameSetting.DEFAULT_VOLUME_BGM);
        float f_se = PlayerPrefs.GetFloat("setting_se", GameSetting.DEFAULT_VOLUME_SE);

        audio_bgm.volume = f_bgm;
        audio_se.volume = f_se;
    }

    public void change_scene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public static long GetTimeStamp(bool bflag = true)
    {
        TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        long ret;
        if (bflag)
            ret = Convert.ToInt64(ts.TotalSeconds);
        else
            ret = Convert.ToInt64(ts.TotalMilliseconds);
        return ret;
    }
    public static DateTime UnixTimestampToDateTime(double unixTime)
    {
        DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        long unixTimeStampInTicks = (long) (unixTime * TimeSpan.TicksPerSecond);
        return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
    }

    public void do_upload_answer(GameAnswer gameAnswer)
    {
        currentRequest = new RequestHelper {
            Uri = basePath_gameProgress + "/add_gameAnswer.php",
            Body = gameAnswer
        };
        RestClient.Post<GameAnswer>(currentRequest)
        .Then(res => {
            UnityEngine.Debug.Log("POST Success: " + JsonUtility.ToJson(res, true));
        })
        .Catch(err => 
        {
            UnityEngine.Debug.Log("POST Fail: " + err.Message);
        });
    }
    
    public void do_upload_gameTime(GameTime gameTime)
    {
        currentRequest = new RequestHelper {
            Uri = basePath_gameProgress + "/add_gameTime.php",
            Body = gameTime
        };
        RestClient.Post<GameTime>(currentRequest)
        .Then(res => {
            UnityEngine.Debug.Log("POST Success: " + JsonUtility.ToJson(res, true));
        })
        .Catch(err => 
        {
            UnityEngine.Debug.Log("POST Fail: " + err.Message);
        });
    }
    public void do_upload_gameProgress(GameProgress gameProgress)
    {
        currentRequest = new RequestHelper {
            Uri = basePath_gameProgress + "/add_progress.php",
            Body = gameProgress
        };
        RestClient.Post<GameProgress>(currentRequest)
        .Then(res => {
            UnityEngine.Debug.Log("POST Success: " + JsonUtility.ToJson(res, true));
        })
        .Catch(err => 
        {
            UnityEngine.Debug.Log("POST Fail: " + err.Message);
        });
    }
    public void do_update_difficulity(User obj)
    {
        currentRequest = new RequestHelper {
            Uri = basePath_User + "/update_difficulity.php",
            Body = obj
        };
        RestClient.Post<User>(currentRequest)
        .Then(res => {
            UnityEngine.Debug.Log("POST Success: " + JsonUtility.ToJson(res, true));
        })
        .Catch(err => 
        {
            UnityEngine.Debug.Log("POST Fail: " + err.Message);
        });
    }
    public int get_current_week()
    {
        return (int)(System.Math.Ceiling((float)current_progress_day/(float)7));
    }
    public int get_current_week_FromProgress()
    {
        return (int)(System.Math.Ceiling((float)get_maxDayFromProgress()/(float)7));
    }

    public void play_SE(string fileName, float pitch)
    {
        var clip_se = Resources.Load<AudioClip>("Sounds/SE/" + fileName);
        audio_se.clip = clip_se;
        audio_se.pitch = pitch;
        audio_se.Play();
    }

    public void play_BGM(string fileName)
    {
        var clip_bgm = Resources.Load<AudioClip>("Sounds/BGM/" + fileName);
        audio_bgm.clip = clip_bgm;
        audio_bgm.Play();
    }

    public float[][] group_score_by_day()
    {
        List<float[]> score_by_day = new List<float[]>();
        int max_day = current_progress_day;
        // UnityEngine.Debug.Log("max_day: " + max_day);
        if(max_day >= 0)
        {
            // find and group
            for (int i=0; i <= max_day; i++)
            {
                float[] score_day = get_scoresOfDay(i);
                score_by_day.Add(score_day);
            }
        }
        return score_by_day.ToArray();
    }

    public float[][] group_score_by_day(int max_day)
    {
        List<float[]> score_by_day = new List<float[]>();

        // UnityEngine.Debug.Log("max_day: " + max_day);
        if(max_day >= 0)
        {
            // find and group
            for (int i=0; i <= max_day; i++)
            {
                float[] score_day = get_scoresOfDay(i);
                score_by_day.Add(score_day);
            }
        }
        return score_by_day.ToArray();
    }

    public int get_maxDayFromProgress()
    {
        int max_day = 0;
        foreach(GameProgress gp in playerData.progress)
        {
            if (gp.day > max_day)
            {
                max_day = gp.day;
            }
        }

        // UnityEngine.Debug.Log("Max day before: " + max_day);

        if (get_scoresOfDay(max_day).Length >= 3)
        {
            max_day += 1;
        }

        // UnityEngine.Debug.Log("Max day after: " + max_day);

        // Set current_progress_day


        return max_day;
    }

    public float[] get_scoresOfDay(int target_day)
    {
        List<float> list_score = new List<float>();
        foreach(GameProgress gp in playerData.progress)
        {
            if (gp.day == target_day)
            {
                float score_float = (float)gp.score / (float)gp.score_max;
                list_score.Add(score_float);

                if (list_score.Count == 3)
                {
                    break;
                }
            }
        }
        return list_score.ToArray();
    }

    public GameProgress[] get_progressOfDay(int target_day)
    {
        List<GameProgress> list_progress = new List<GameProgress>();
        foreach(GameProgress gp in playerData.progress)
        {
            if (gp.day == target_day)
            {
                list_progress.Add(gp);
                if (list_progress.Count == 3)
                {
                    break;
                }
            }
        }
        return list_progress.ToArray();
    }
    public static int compare_dayDiffToNow(long timestamp_targetDay)
    {
        DateTime time_now = DateTime.UtcNow;
        DateTime time_target = UnixTimestampToDateTime(timestamp_targetDay);
        return (time_now - time_target).Days;
    }

    public AlertController showAlert(string str_title, string str_content, Sprite img_content, string str_confirm, string str_cancel, bool isShowCancel)
    {
        canvas = GameObject.Find("Canvas");
        GameObject obj = (GameObject)Instantiate(prefab_alert, canvas.transform);
        AlertController obj_alert = obj.GetComponent<AlertController>();
        obj_alert.setup(str_title, str_content, img_content, str_confirm, str_cancel, isShowCancel);
        return obj_alert;
    }

    public void show_loading(bool isShowLoading)
    {
        canvas = GameObject.Find("Canvas");

        if (isShowLoading)
        {
            // show only one loading
            if (GameObject.Find("Panel_loading") == null)
            {
                GameObject obj = (GameObject)Instantiate(prefab_loading, canvas.transform);
                obj.name = "Panel_loading";
            }
            else
            {
                UnityEngine.Debug.Log("Only one Loading obj can be show");
            }
        }
        else
        {
            if (GameObject.Find("Panel_loading") != null)
            {
                Destroy(GameObject.Find("Panel_loading"));
            }
            else
            {
                UnityEngine.Debug.Log("No Loading obj found");
            }
        }
    }

    public void play_video(string video_path)
    {
        // Will attach a VideoPlayer to the main camera.
        GameObject camera = GameObject.Find("Main Camera");

        // VideoPlayer automatically targets the camera backplane when it is added
        // to a camera object, no need to change videoPlayer.targetCamera.
        if (camera.GetComponent<UnityEngine.Video.VideoPlayer>() == null)
        {
            camera.AddComponent<UnityEngine.Video.VideoPlayer>();
        }
        videoPlayer = camera.GetComponent<UnityEngine.Video.VideoPlayer>();

        // Play on awake defaults to true. Set it to false to avoid the url set
        videoPlayer.playOnAwake = false;

        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        // This will set the visible.
        videoPlayer.targetCameraAlpha = 1f;

        // Set the video to play. URL supports local absolute or relative paths.
        videoPlayer.url = video_path;

        // // Skip the first 100 frames.
        // videoPlayer.frame = 100;

        // Restart from beginning when done.
        videoPlayer.isLooping = false;

        // Each time we reach the end, we slow down the playback by a factor of 10.
        videoPlayer.loopPointReached += DidEndVideoPlay;

        // stop bgm
        if (audio_bgm.isPlaying)
        {
            audio_bgm.Stop();
        }

        // hide canvas
        canvas = GameObject.Find("Canvas");
        canvas.SetActive(false);

        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.
        isPlaying_video = true;
        videoPlayer.Play();
    }

    void DidEndVideoPlay(UnityEngine.Video.VideoPlayer videoPlayer)
    {
        videoPlayer.targetCameraAlpha = 0f;
        isPlaying_video = false;

        // play bgm
        audio_bgm.Play();

        // show canvas
        canvas.SetActive(true);
    }

    public bool check_networkStatus()
    {
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            AlertController alert =  showAlert("網絡狀態", "請確認網絡連接狀態", null, "確定", "取消", false);
            return false;
        }
        else
        {
            return true;
        }
    }

    public void highlight_obj(GameObject obj, string displayText, GameObject nextCallObject)
    {
        OnBoardTutorialController OBT = GameObject.Find("Panel_Tutorial").GetComponent<OnBoardTutorialController>();
        OBT.highlight_obj(obj, displayText, nextCallObject);
    }
}
