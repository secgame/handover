﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreBoardController : MonoBehaviour
{
    public Text lab_score;
    public int current_score = 0;
    public Animator anim;
    
    
    public void set_score(int newScore)
    {
        current_score = newScore;
        lab_score.text = current_score.ToString();
    }

    public void add_score()
    {
        anim.SetTrigger("add_score");
    }

    public void do_add_score()
    {
        current_score += 1;
        lab_score.text = current_score.ToString();
    }
}
