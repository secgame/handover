﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Models;


public class MiniGameMaster : MonoBehaviour
{
    // ################################################################################################
    // COMMON USE Var
    // ################################################################################################
    public GameManager gm;
    public GameObject panel_selections;
    public GameObject panel_selections_left;
    public GameObject panel_selections_right;
    public GameObject panel_selections_up;
    public GameObject prefab_selection;
    public ProgressBarController obj_progressBar;
    public ScoreBoardController obj_scoreBoard;
    public int current_selected;
    public Selection[] list_selections;
    public MiniGameController.MiniGameQuestionType current_questionType;
    public GameObject btn_confirm;
    public GameObject btn_reference;
    public GameObject btn_reference_left;
    public GameObject btn_reference_right;
    public GameObject btn_reference_up;
    public GameObject btn_yes;
    public GameObject btn_no;
    public Button btn_exit;
    public GameObject btn_playAudio;
    public Image img_bg;
    public Sprite sprite_selection;
    public string sourcePath;
    public AudioSource audio_story;
    public AudioSource audio_word;

    public TimerController obj_counter;

    string[,] data_items; 
    public TextAsset csv_tell;
    public Question_Tell[] array_TellData;
    public TextAsset csv_same;
    public Question_Same[] array_SameData;
    public TextAsset csv_different;
    public Question_Different[] array_DifferentData;
    public int num_questions;
    public int limited_time=0;
    public GameObject[] array_selectionsObj;

    public Question_Different current_question_different;
    public Question_Same current_question_same;
    public Question_Tell current_question_tell;

    string current_question_csv;
    string current_answer_csv;
    bool isplaying_Audio = false;
    bool isFirstPlayAudio = true;

    public Sprite sp_selectionResult_correct;
    public Sprite sp_selectionResult_wrong;
    public Sprite sp_selectionResult_normal;

    int count_numOfWrongAnswer = 0;
    int count_numOfCorrectAnswer = 0;
    int num_selections_max = 0;
    int num_selections_min = 0;
    int num_selections_current = 0;
    public MiniGameController.MiniGameSelectionsPosition selectionPosition = MiniGameController.MiniGameSelectionsPosition.none;
    
    // ################################################################################################
    // Server Side
    // ################################################################################################
    double start_answer_time;
    double end_answer_time;
    double start_minigame_time;
    double end_minigame_time;
    // ################################################################################################

    // ################################################################################################
    // COMMON USE FUNCTION
    // ################################################################################################
    
    public void setup_graphicUI()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
        MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();

        // show loading
        gm.show_loading(true);

        // get common use object
        obj_scoreBoard = GameObject.Find("obj_scoreBoard").GetComponent<ScoreBoardController>();
        obj_progressBar = GameObject.Find("obj_progressBar").GetComponent<ProgressBarController>();
        
        switch (selectionPosition)
        {
            case MiniGameController.MiniGameSelectionsPosition.Up_center:
                panel_selections = panel_selections_up;
                panel_selections.SetActive(true);
                btn_reference = btn_reference_up;
                btn_reference.SetActive(true);
            break;

            case MiniGameController.MiniGameSelectionsPosition.Left_center:
                panel_selections = panel_selections_left;
                panel_selections.SetActive(true);
                btn_reference = btn_reference_left;
                btn_reference.SetActive(true);
            break;

            case MiniGameController.MiniGameSelectionsPosition.Right_center:
                panel_selections = panel_selections_right;
                panel_selections.SetActive(true);
                btn_reference = btn_reference_right;
                btn_reference.SetActive(true);
            break;

            default:
                panel_selections = GameObject.Find("Panel_selections");
                btn_reference = GameObject.Find("btn_ref");
            break;
        }
        
        btn_exit = GameObject.Find("btn_minigame_back").GetComponent<Button>();
        btn_exit.onClick.AddListener(exitClicked);
        btn_playAudio = GameObject.Find("btn_playSound");
        btn_playAudio.GetComponent<Button>().onClick.AddListener(play_audio);
        img_bg = GameObject.Find("BG").GetComponent<Image>();       

        // set timer
        obj_counter = GameObject.Find("obj_timer").GetComponent<TimerController>(); 
        if (limited_time != 0)
        {
            obj_counter.gameObject.SetActive(true);
            obj_counter.setup(limited_time);
        }
        else
        {
            obj_counter.gameObject.SetActive(false);
        }
        
        int current_story_part = 0;
        int current_numOfDay = 0;
        current_story_part = gm.current_story_part+1;
        // current_numOfDay = mainController.current_numOfDay;
        sourcePath = "MinigameImages/Day_" + (gm.current_day) + "/game" + (current_story_part) + "/";       // folder in resource starting with day 1

        

        // 背景，選項
        // UnityEngine.Debug.Log("sourcePath: " + sourcePath);
        img_bg.sprite = Resources.Load<Sprite>(sourcePath + "bg");
        sprite_selection = Resources.Load<Sprite>(sourcePath + "btn_option");
        Sprite sprite_confirm = Resources.Load<Sprite>(sourcePath + "btn_confirm");
        Sprite sprite_ref = Resources.Load<Sprite>(sourcePath + "btn_ref");
        sp_selectionResult_correct = Resources.Load<Sprite>("UI/common/btn_right");
        sp_selectionResult_wrong = Resources.Load<Sprite>("UI/common/btn_wrong");
        sp_selectionResult_normal = Resources.Load<Sprite>("UI/common/btn_normal");

        switch (current_questionType)
        {
            case MiniGameController.MiniGameQuestionType.Find_different:
                // 確定按鈕
                btn_confirm = GameObject.Find("btn_confirm");
                btn_confirm.GetComponent<Button>().onClick.AddListener(confirm_clicked);
                btn_confirm.GetComponent<Image>().sprite = sprite_confirm;
                if (GameObject.Find("btn_yes") != null)
                {
                    GameObject.Find("btn_yes").SetActive(false);
                }
                if (GameObject.Find("btn_no") != null)
                {
                    GameObject.Find("btn_no").SetActive(false);
                }

                // load questions different
                do_load_differentData(get_csvFolderPath() + "/");
            break;

            case MiniGameController.MiniGameQuestionType.Find_same:
                // 確定按鈕，參考按鈕
                btn_confirm = GameObject.Find("btn_confirm");
                btn_confirm.GetComponent<Image>().sprite = sprite_confirm;
                btn_confirm.GetComponent<Button>().onClick.AddListener(confirm_clicked);
                btn_reference.GetComponent<Button>().onClick.AddListener(reference_clicked);
                btn_reference.GetComponent<Image>().sprite = sprite_ref;
                if (GameObject.Find("btn_yes") != null)
                {
                    GameObject.Find("btn_yes").SetActive(false);
                }
                if (GameObject.Find("btn_no") != null)
                {
                    GameObject.Find("btn_no").SetActive(false);
                }

                // load questions same
                do_load_sameData(get_csvFolderPath() + "/");
            break;
            case MiniGameController.MiniGameQuestionType.Judge:
                btn_yes = GameObject.Find("btn_yes");
                btn_no = GameObject.Find("btn_no");
                GameObject obj_judgeButtons = new GameObject();
                obj_judgeButtons.name = "obj_judgeButtons";
                obj_judgeButtons.transform.parent = btn_yes.transform.parent;
                btn_yes.transform.parent = obj_judgeButtons.transform;
                btn_no.transform.parent = obj_judgeButtons.transform;

                btn_yes.GetComponent<Button>().onClick.AddListener(() => judge_clicked(true));
                btn_no.GetComponent<Button>().onClick.AddListener(() => judge_clicked(false));
                
                if (GameObject.Find("btn_confirm") != null)
                {
                    GameObject.Find("btn_confirm").SetActive(false);
                }
                

                // load questions Tell
                do_load_tellData(get_csvFolderPath() + "/");
            break;
        }

        obj_scoreBoard.set_score(0);
        // UnityEngine.Debug.Log("obj_progressBar.setup(num_questions): " + num_questions);
        obj_progressBar.setup(num_questions);

        // get start minigame time
        start_minigame_time = (double)GameManager.GetTimeStamp(false) / (double)1000;

        // disable  buttons at begining
        judgeButton_enable(false);
        confirmButton_enable(false);
        refButton_enable(false);

        get_NumOfSelections_range();

        // hide loading
        gm.show_loading(false);

        
    }
    void checkShowTutorialContent()
    {
        MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();

        if (gm.current_story_part != 0)
        {
            mainController.tutorial_step = 999;
            mainController.isTutorial = false;
        }

        switch (mainController.tutorial_step)
        {
            case 2:
                gm.highlight_obj(btn_playAudio.gameObject, "點擊可重新播放遊戲指導", this.gameObject);
            break;

            case 3:
                gm.highlight_obj(obj_scoreBoard.gameObject, "此處是已獲得分數", this.gameObject);
            break;

            case 4:
                gm.highlight_obj(obj_progressBar.gameObject, "此處是進度條", this.gameObject);
            break;

            case 5:
                gm.highlight_obj(panel_selections.gameObject, "點擊每個選項播放讀音", this.gameObject);
            break;

            case 6:
                GameObject obj_confirmButtons = GameObject.Find("btn_confirm");
                gm.highlight_obj(obj_confirmButtons.gameObject, "點擊提交答案", null);
            break;

            default:
                mainController.isTutorial = false;
            break;
        }
        mainController.tutorial_step++;
    }
    public void reset_selectionsAnim(GameObject gameObj)
    {
        foreach(Transform t in gameObj.transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("idle");
            obj.transform.GetChild(1).GetComponent<Image>().sprite = sp_selectionResult_normal;
        }
    }
    public void setup_questionTypeUI()
    {
        if (current_questionType == MiniGameController.MiniGameQuestionType.Find_same)
        {
            btn_confirm.SetActive(true);
            if (btn_reference != null)             
            {                 
                btn_reference.SetActive(true);             
            }
            if (btn_yes != null)             
            {        
                btn_yes.SetActive(false);
            }
            if (btn_no != null)             
            {        
                btn_no.SetActive(false);
            }
        }
        else if (current_questionType == MiniGameController.MiniGameQuestionType.Find_different)
        {
            if (btn_confirm != null)             
            {                 
                btn_confirm.SetActive(true);     
            }
            
            if (btn_reference != null)             
            {                 
                btn_reference.SetActive(false);             
            }
            if (btn_yes != null)             
            {                 
                btn_yes.SetActive(false);             
            }
            if (btn_no != null)             
            {                 
                btn_no.SetActive(false);             
            }
        }
        else if (current_questionType == MiniGameController.MiniGameQuestionType.Judge)
        {
            if (btn_confirm != null)             
            {                 
                btn_confirm.SetActive(false);     
            }
            if (btn_reference != null)             
            {                 
                btn_reference.SetActive(false);             
            }
            if (btn_yes != null)             
            {                 
                btn_yes.SetActive(true);             
            }
            if (btn_no != null)             
            {                 
                btn_no.SetActive(true);             
            }
        }
    }
    public void showNextQuestion()
    {
        // check if is last question
        if (obj_progressBar.progress_current == obj_progressBar.progress_max)
        {
            MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
            gm.current_story_part += 1;

            mainController.gotoStoryClicked();

            // change to next page
            mainController.next_storySlide(true);

            

            // // play story
            // mainController.readStoryClicked();

            // get end minigame time
            end_minigame_time = (double)GameManager.GetTimeStamp(false) / (double)1000;

            // update data
            // update_gameTime();
            /*andded by ivy*/
            //在這裡計算一下遊戲的時間有沒有超過14分鐘，如果超過就要停止今天的訓練了。
            System.TimeSpan timespan = mainController.stopwatch.Elapsed;
            mainController.minutes = timespan.TotalMinutes;
            if (mainController.minutes > 14)
            {
                mainController.isendtime = true;
                mainController.testing_needToLogout = true;
                update_gameProgress(1);
            }
            else {
                update_gameProgress(0);
            }
            

            // ************************************************************************
            // For Testing lock account only
            // ************************************************************************
            if (gm.current_story_part == 3 && gm.current_day != 0 && gm.current_day % 7 == 0)
            {
                mainController.testing_needToLogout = true;
            }
            
            // ************************************************************************

            // update stars and map
            mainController.setup_map_UI();
            mainController.setup_days_UI();

            // exit
            do_exitClicked();

            mainController.checkAndShowAchievement();
        }
        else
        {
            // show loading
            gm.show_loading(true);

            do_showNextQuestion();
        }
    }

    void do_showNextQuestion()
    {
        // reset selected
        current_selected = -1;

        // generate question and animation
        generateQuestion_and_setAnimation();

        // show question UI
        set_selections(list_selections);

        // start counter
        if(limited_time != 0)
        {
            // reset
            obj_counter.setup(limited_time);
            // start
            obj_counter.startCountdown();
        }

        // hide loading
        gm.show_loading(false);
    }

    public void check_playingAudio()
    {
        if (isplaying_Audio)
        {
            if (!audio_story.isPlaying)
            {
                // set audio button animation
                btn_playAudio.GetComponent<Animator>().SetTrigger("idle");
                isplaying_Audio = false;

                if (isFirstPlayAudio)
                {
                    isFirstPlayAudio = false;
                    start_game();

                    // check show tutorial
                    MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
                    if(mainController.isTutorial)
                    {
                        checkShowTutorialContent();
                    }
                }
            }
        } 
    }
    public virtual void start_game()
    {
        do_start_game();
    }
    public void do_start_game()
    {
        // show start game animation and start
        GameObject prefab_effect_startGame = Resources.Load<GameObject>("Prefab/obj_startGame");
        GameObject obj_eff = (GameObject)Instantiate(prefab_effect_startGame, this.gameObject.transform);

        // set selections UI
        showNextQuestion();
    }
    public void update_gameTime()
    {
        // create game time object
        GameTime obj = new GameTime{
            user_id = gm.playerData.id,
            day = gm.current_day,
            part = gm.current_story_part,
            time_type = MiniGameController.MiniGameTimeType.Minigame.ToString(),
            start_time = start_minigame_time,
            end_time = end_minigame_time
        };
        // upload game time to server
        gm.do_upload_gameTime(obj);
    }
    public void update_gameProgress(int willlock = 0)
    {
        Debug.Log("willlock:"+willlock);
        // create game time object
        GameProgress obj = new GameProgress {
            user_id = gm.playerData.id,
            day = gm.current_day,
            part = gm.current_story_part,
            score = obj_scoreBoard.current_score,
            score_max = obj_progressBar.progress_max,
            start_time = start_minigame_time,
            end_time = end_minigame_time,
            islock = willlock
        };
        // upload game time to server
        gm.do_upload_gameProgress(obj);
        // update local progress
        List<GameProgress> newList = new List<GameProgress>();
        newList = gm.playerData.progress.ToList();
        newList.Add(obj);
        gm.playerData.progress = newList.ToArray();
    }
    public virtual void generateQuestion_and_setAnimation()
    {
        // generate question
        generateQuestion();
    }

    public bool check_countOfSelections()
    {
        bool isSelectionsCountInRange = true;

        switch(current_questionType)
        {
            case MiniGameController.MiniGameQuestionType.Find_different:
                Question_Different[] selected_levelQuestion_different = get_questionSetByDifficulity_different(gm.playerData.current_difficulity);
                isSelectionsCountInRange = (selected_levelQuestion_different.Length != 0);
                UnityEngine.Debug.Log("checking num of selections: " + selected_levelQuestion_different.Length);
            break;

            case MiniGameController.MiniGameQuestionType.Find_same:
                Question_Same[] selected_levelQuestion_same = get_questionSetByDifficulity_same(gm.playerData.current_difficulity);
                isSelectionsCountInRange = (selected_levelQuestion_same.Length != 0);
                UnityEngine.Debug.Log("checking num of selections: " + selected_levelQuestion_same.Length);
            break;

            case MiniGameController.MiniGameQuestionType.Judge:
                Question_Tell[] selected_levelQuestion = get_questionSetByDifficulity_judge(gm.playerData.current_difficulity);
                isSelectionsCountInRange = (selected_levelQuestion.Length != 0);
                UnityEngine.Debug.Log("checking num of selections: " + selected_levelQuestion.Length);
            break;
        }

        return isSelectionsCountInRange;
    }

    public Question_Different[] get_questionSetByDifficulity_different(int targetDifficulityLevel)
    {
        List<Question_Different> list_result = new List<Question_Different>();
        // UnityEngine.Debug.Log("array_DifferentData.Length: " + array_DifferentData.Length);
        foreach(Question_Different q in array_DifferentData)
        {
            // UnityEngine.Debug.Log("id: " + q.id);
            
            if (q.difficulityLevel == targetDifficulityLevel)
            {
                // check num of selections
                if (q.selections.Length == num_selections_current)
                {
                    list_result.Add(q);
                }
                // if (q.id == "13")
                // {
                //     // UnityEngine.Debug.Log("num of selections: " + q.selections.Length);
                //     foreach (Selection s in q.selections)
                //     {
                //         string wholeWord = "";
                //         foreach (Word w in s.array_word)
                //         {
                //             wholeWord+=w.word_chi;
                //         }
                //         UnityEngine.Debug.Log("selection: " + wholeWord);
                //     }
                //     // UnityEngine.Debug.Log("list_result.count: " + list_result.Count);
                // }
                
            }
        }

        return list_result.ToArray();
    }
    public Question_Same[] get_questionSetByDifficulity_same(int targetDifficulityLevel)
    {
        // UnityEngine.Debug.Log("targetDifficulityLevel: " + targetDifficulityLevel);
        // UnityEngine.Debug.Log("num_selections_current: " + num_selections_current);
        List<Question_Same> list_result = new List<Question_Same>();
        foreach(Question_Same q in array_SameData)
        {
            if (q.difficulityLevel == targetDifficulityLevel)
            {
                // if (targetDifficulityLevel == 2)
                // {
                //     UnityEngine.Debug.Log("checking numOfSelection: " + q.selections.Length);
                // }
                // check num of selections
                if (q.selections.Length == num_selections_current)
                {
                    list_result.Add(q);
                }
            }
        }

        return list_result.ToArray();
    }  
    public Question_Tell[] get_questionSetByDifficulity_judge(int targetDifficulityLevel)
    {
        List<Question_Tell> list_result = new List<Question_Tell>();
        foreach(Question_Tell q in array_TellData)
        {
            if (q.difficulityLevel == targetDifficulityLevel)
            {
                // check num of selections
                if (q.selections.Length == num_selections_current)
                {
                    list_result.Add(q);
                }
            }
        }

        return list_result.ToArray();
    }

    

    public void generateQuestion()
    {
        // set buttons enable
        confirmButton_enable(true);
        judgeButton_enable(true);
        refButton_enable(true);

        // get current time
        start_answer_time = (double)GameManager.GetTimeStamp(false) / (double)1000;


        switch(current_questionType)
        {
            case MiniGameController.MiniGameQuestionType.Find_different:
                // get question in difficulity level
                Question_Different[] selected_levelQuestion_different = get_questionSetByDifficulity_different(gm.playerData.current_difficulity);
                // get random question
                int randomInt_different = Random.Range(0, selected_levelQuestion_different.Length);
                current_question_different = selected_levelQuestion_different[randomInt_different];
                // UnityEngine.Debug.Log("Current question difficulity: " + current_question_different.difficulityLevel);
                // UnityEngine.Debug.Log("current question id: " + current_question_tell.id);
                list_selections = ShuffleSelection(current_question_different.selections);

                current_question_csv = get_csvRawData(current_question_different.id, MiniGameController.MiniGameQuestionType.Find_different);
            break;
            case MiniGameController.MiniGameQuestionType.Find_same:
                
                // get question in difficulity level
                Question_Same[] selected_levelQuestion_same = get_questionSetByDifficulity_same(gm.playerData.current_difficulity);
                // get random question
                int randomInt_same = Random.Range(0, selected_levelQuestion_same.Length);
                // UnityEngine.Debug.Log("selected_levelQuestion_same length: " + selected_levelQuestion_same.Length);
                // UnityEngine.Debug.Log("randomInt_same: " + randomInt_same);
                current_question_same = selected_levelQuestion_same[randomInt_same];
                // UnityEngine.Debug.Log("Current question difficulity: " + current_question_same.difficulityLevel);
                // UnityEngine.Debug.Log("current question id: " + current_question_tell.id);
                list_selections = ShuffleSelection(current_question_same.selections);

                current_question_csv = get_csvRawData(current_question_same.id, MiniGameController.MiniGameQuestionType.Find_same);
            break;
            case MiniGameController.MiniGameQuestionType.Judge:
                
                // get question in difficulity level
                Question_Tell[] selected_levelQuestion = get_questionSetByDifficulity_judge(gm.playerData.current_difficulity);
                // get random question
                int randomInt = Random.Range(0, selected_levelQuestion.Length);
                current_question_tell = selected_levelQuestion[randomInt];
                // UnityEngine.Debug.Log("Current question difficulity: " + current_question_tell.difficulityLevel);
                // UnityEngine.Debug.Log("current question id: " + current_question_tell.id);
                list_selections = ShuffleSelection(current_question_tell.selections);

                current_question_csv = get_csvRawData(current_question_tell.id, MiniGameController.MiniGameQuestionType.Judge);
            break;
        }
    }

    public virtual void set_selections(Selection[] array_selections)
    {
        // remove all childs
        foreach (Transform child in panel_selections.transform) {
            GameObject.Destroy(child.gameObject);
        }
        List<GameObject> objs = new List<GameObject>();

        // init new selection child
        for (int i=0; i<array_selections.Length; i++)
        {
            GameObject newObj = (GameObject)Instantiate(prefab_selection, panel_selections.transform);
            objs.Add(newObj);
            SelectionObject selectionObject = newObj.GetComponent<SelectionObject>();
            selectionObject.setup(array_selections[i], sprite_selection);

            Button btn = newObj.GetComponent<Button>();
            int targetSelected = i;
            btn.onClick.AddListener(() => this.selection_selected(targetSelected));

            // set selected
            if (current_selected != -1)
            {
                if (i == current_selected)
                {
                    // show selected cover
                    newObj.transform.GetChild(2).gameObject.SetActive(true);
                }
                else
                {
                    // hide selected cover
                    newObj.transform.GetChild(2).gameObject.SetActive(false);
                }
            }
        }

        array_selectionsObj = objs.ToArray();
    }

    public Selection[] ShuffleSelection(Selection[] objList) 
    {
        Selection[] result_objList = objList;
        Selection tempGO;
        for (int i = 0; i < result_objList.Length; i++) 
        {
            int rnd = Random.Range(0, result_objList.Length);
            tempGO = result_objList[rnd];
            result_objList[rnd] = result_objList[i];
            result_objList[i] = tempGO;
        }

        return result_objList;
    }

    public virtual void selection_selected(int selected)
    {
        do_selection_clicked(selected);
    }

    public void do_remove_hints()
    {
        foreach(GameObject G_obj in array_selectionsObj)
        {
            foreach (Transform t2 in G_obj.transform)
            {
                if (t2.gameObject.name == "effect_wordHints" + "(Clone)")
                {
                    Destroy(t2.gameObject);
                    break;
                }
            }
        }
    }

    public void do_selection_clicked(int selected)
    {
        gm.play_SE("se_confirm", 1f);
        int last_selected = current_selected;
        current_selected = selected;

        // remove hints
        if (last_selected != current_selected)
        {
            do_remove_hints();
        }

        // set selected effect
        for (int i=0; i<array_selectionsObj.Length; i++)
        {
            GameObject obj = array_selectionsObj[i];
            if (i == current_selected)
            {
                if (current_questionType == MiniGameController.MiniGameQuestionType.Find_different || current_questionType == MiniGameController.MiniGameQuestionType.Find_same)
                {
                    // show selected cover
                    obj.transform.GetChild(2).gameObject.SetActive(true);
                }

                // show hints images
                bool isShowingHints = false;
                foreach (Transform t in obj.transform)
                {
                    if (t.gameObject.name == "effect_wordHints" + "(Clone)")
                    {
                        isShowingHints = true;
                        break;
                    }
                }

                if (!isShowingHints)
                {
                    do_show_hints(obj, selected);
                }
          

                // set as selected color
                if (obj.tag == "selection_hamster")
                {
                    obj.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Image>().color = new Color32(150,150,150,255);
                }
                else
                {
                    obj.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(150,150,150,255);
                }
            }
            else
            {
                if (current_questionType == MiniGameController.MiniGameQuestionType.Find_different || current_questionType == MiniGameController.MiniGameQuestionType.Find_same)
                {
                    // hide selected cover
                    obj.transform.GetChild(3).gameObject.SetActive(false);
                }

                // set as not selected color
                // set as selected color
                if (obj.tag == "selection_hamster")
                {
                    obj.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255,255,255,255);
                }
                else
                {
                    obj.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255,255,255,255);
                }
                
            }
        }

        // play selected audio
        play_word_audio(selected);
    }

    void do_show_hints(GameObject obj, int selected)
    {
        Sprite sp_hints = Resources.Load<Sprite>("HintsImages/" + get_word_eng(selected));
        // UnityEngine.Debug.Log("sp name: " + get_word_eng(selected));
        if (sp_hints != null)
        {
            GameObject prefab_effect_wordHints = Resources.Load<GameObject>("Prefab/effect_wordHints");
            GameObject obj_wordHints = (GameObject)Instantiate(prefab_effect_wordHints, obj.transform.transform);
            obj_wordHints.transform.GetChild(0).GetComponent<Image>().sprite = sp_hints;
        }
    }

    public void confirmButton_enable(bool isEnable)
    {
        if (btn_confirm != null)
        {
            btn_confirm.GetComponent<Button>().interactable = isEnable;
        }
    }
    public void refButton_enable(bool isEnable)
    {
        if (btn_reference != null)
        {
            btn_reference.GetComponent<Button>().interactable = isEnable;
        }
    }
    public void judgeButton_enable(bool isEnable)
    {
        if (btn_yes != null)
        {
            btn_yes.GetComponent<Button>().interactable = isEnable;
        }
        if (btn_no != null)
        {
            btn_no.GetComponent<Button>().interactable = isEnable;
        }
    }

    public virtual void confirm_clicked()
    {
        if (current_selected != -1 && (obj_progressBar.progress_current != obj_progressBar.progress_max))
        {
            bool result = do_confirmClicked(current_selected);
        }
    }
    public bool do_confirmClicked(int selectedIndex)
    {
        // remove hints
        do_remove_hints();

        confirmButton_enable(false);
        // submit to server
        // get answer time
        end_answer_time = (double)GameManager.GetTimeStamp(false) / (double)1000;
        // create obj
        string csv_answer_raw = "";
        Selection current_selection = list_selections[selectedIndex];
        for (int i=0; i<current_selection.array_word.Length; i++)
        {
            Word w = current_selection.array_word[i];
            csv_answer_raw += w.word_chi + "(" + w.word_eng + ")" + ((i == current_selection.array_word.Length-1)? "" : " ");
        }

        GameAnswer obj = new GameAnswer{
            user_id = gm.playerData.id,
            day = gm.current_day,
            game_section = gm.current_story_part,
            game_temp = gm.curret_temp_name.ToString(),
            question_type = gm.curret_question_type.ToString(),
            answer_start_time = start_answer_time,
            answer_end_time = end_answer_time,
            question = current_question_csv,
            answer = csv_answer_raw
        };
        // upload answer to server
        gm.do_upload_answer(obj);

        // do reaction
        if (current_questionType == MiniGameController.MiniGameQuestionType.Find_different)
        {
            handleAnswerdResult(list_selections[selectedIndex].isCorrect);
        }
        else if (current_questionType == MiniGameController.MiniGameQuestionType.Find_same)
        {
            handleAnswerdResult(list_selections[selectedIndex].isCorrect);
        }
        else
        {
            UnityEngine.Debug.Log("Wrong question type");
        }
        return list_selections[selectedIndex].isCorrect;
    }

    public virtual void judge_clicked(bool isCorrect)
    {
        if (obj_progressBar.progress_current != obj_progressBar.progress_max)
        {
            do_judgeClicked(isCorrect);
        }
    }
    public void do_judgeClicked(bool answer)
    {
        // remove hints
        do_remove_hints();
        
        judgeButton_enable(false);
        end_answer_time = (double)GameManager.GetTimeStamp(false) / (double)1000;
        GameAnswer obj = new GameAnswer{
            user_id = gm.playerData.id,
            day = gm.current_day,
            game_section = gm.current_story_part,
            game_temp = gm.curret_temp_name.ToString(),
            question_type = gm.curret_question_type.ToString(),
            answer_start_time = start_answer_time,
            answer_end_time = end_answer_time,
            question = current_question_csv,
            answer = answer.ToString()
        };
        // upload answer to server
        gm.do_upload_answer(obj);

        if (current_questionType == MiniGameController.MiniGameQuestionType.Judge)
        {
            bool questionResult = list_selections[0].isCorrect;

            if (answer == questionResult)
            {
                UnityEngine.Debug.Log("判斷：正確");
                handleAnswerdResult(true);
            }
            else
            {
                UnityEngine.Debug.Log("判斷：錯誤");
                handleAnswerdResult(false);
            }
        }
    }

    


    public void reference_clicked()
    {
        // UnityEngine.Debug.Log("reference_clicked called");
        // reference always first one
        string ref_word_end = "";
        string ref_word_chi = "";
        Word[] array_words = current_question_same.selection_ref.array_word;
        foreach(Word w in array_words)
        {
            ref_word_end += w.word_eng;
            ref_word_chi += w.word_chi;
        }
        // UnityEngine.Debug.Log("ref_word_end: " + ref_word_end);
        // UnityEngine.Debug.Log("ref_word_chi: " + ref_word_chi);
        do_play_word_audio(ref_word_end);
    }
    public virtual void timesUp()
    {
        do_timesUp();
    }
    public void do_timesUp()
    {
        if (limited_time != 0)
        {
            judgeButton_enable(false);
            confirmButton_enable(false);
            show_wrong();
            gm.play_SE("se_timeout", 1f);

            // next question
            StartCoroutine(waitAndGoNext());
            // add progress
            obj_progressBar.progress_add();
        }
    }
    public void show_wrong()
    {
        gm.play_SE("se_wrong", 1f);
    }
    public void show_correct()
    {
        gm.play_SE("se_correct", 1f);
    }

    public void get_NumOfSelections_range()
    {
        if (current_questionType == MiniGameController.MiniGameQuestionType.Find_different)
        {
            num_selections_max = 5;
            num_selections_min = 3;
            num_selections_current = 4;
        }
        else if (current_questionType == MiniGameController.MiniGameQuestionType.Find_same)
        {
            num_selections_max = 4;
            num_selections_min = 2;
            num_selections_current = 3;
        }
        else if (current_questionType == MiniGameController.MiniGameQuestionType.Judge)
        {
            num_selections_max = 4;
            num_selections_min = 2;
            num_selections_current = 3;
        }
    }

    public virtual GameObject get_selectedObject()
    {
        return panel_selections.transform.GetChild(current_selected).gameObject;
    }

    public void handleAnswerdResult(bool isCorrect)
    {
        if (limited_time != 0)
        {
            obj_counter.pauseCountdown();
        }
        
        GameObject SelectedObj = new GameObject();
        if (current_questionType != MiniGameController.MiniGameQuestionType.Judge)
        {
            SelectedObj = get_selectedObject();
        }

        if (isCorrect)
        {
            // show correct effect
            show_correct();

            // add score
            obj_scoreBoard.add_score();

            if (current_questionType != MiniGameController.MiniGameQuestionType.Judge)
            {
                SelectedObj.transform.GetChild(1).GetComponent<Image>().sprite = sp_selectionResult_correct;
            }

            // change answer count
            count_numOfWrongAnswer = 0;
            count_numOfCorrectAnswer ++;

            if (count_numOfCorrectAnswer >= GameSetting.difficulity_RightchangeCount[gm.playerData.current_difficulity])
            {
                // reset count
                count_numOfCorrectAnswer = 0;
                do_moreSelections_higherDifficulity();
            }
        }
        else
        {
            // show wrong effect
            show_wrong();
            if (current_questionType != MiniGameController.MiniGameQuestionType.Judge)
            {
                SelectedObj.transform.GetChild(1).GetComponent<Image>().sprite = sp_selectionResult_wrong;
            }
            // change answer count
            count_numOfWrongAnswer ++;
            count_numOfCorrectAnswer = 0;

            if (count_numOfWrongAnswer >= GameSetting.difficulity_WrongchangeCount[gm.playerData.current_difficulity])
            {
                // reset count
                count_numOfWrongAnswer = 0;
                do_lessSelection_lowerDifficulity();
            }
        }

        StartCoroutine(waitAndGoNext());

        // add progress
        obj_progressBar.progress_add();

        showResultAnimation();
    }

    void do_lessSelection_lowerDifficulity()
    {
        // UnityEngine.Debug.Log("do_lessSelection_lowerDifficulity called");
        int old_num_selections = num_selections_current;
        int old_difficulity = gm.playerData.current_difficulity;

        bool isNeedReGen = false;
        do
        {
            // less selections
            if (num_selections_current > num_selections_min)
            {
                UnityEngine.Debug.Log("less selections");
                num_selections_current --;
            }
            // lower difficulity
            else
            {
                num_selections_current = num_selections_max;
                change_difficulity(true);
            }
            isNeedReGen = !check_countOfSelections();

            if (isNeedReGen)
            {
                if (gm.playerData.current_difficulity == 1 && num_selections_current == num_selections_min)
                {
                    UnityEngine.Debug.Log("Reach lowest level and still not enough selections");
                    isNeedReGen = false;
                    num_selections_current = old_num_selections;
                    gm.playerData.current_difficulity = old_difficulity;
                }
            }

        } while (isNeedReGen);
        
        update_difficulity();
    }
    void do_moreSelections_higherDifficulity()
    {
        int old_num_selections = num_selections_current;
        int old_difficulity = gm.playerData.current_difficulity;

        // UnityEngine.Debug.Log("do_moreSelections_higherDifficulity called");
        bool isNeedReGen = false;
        do
        {
            // more selections
            if (num_selections_current < num_selections_max)
            {
                UnityEngine.Debug.Log("more selections");
                num_selections_current ++;
            }
            // higher difficulity
            else
            {
                num_selections_current = num_selections_min;
                change_difficulity(false);
            }
            isNeedReGen = !check_countOfSelections();

            if (isNeedReGen)
            {
                if (gm.playerData.current_difficulity == 10 && num_selections_current == num_selections_max)
                {
                    UnityEngine.Debug.Log("Reach highest level and still not enough selections");
                    isNeedReGen = false;
                    num_selections_current = old_num_selections;
                    gm.playerData.current_difficulity = old_difficulity;
                }
            }
        } while (isNeedReGen);

        update_difficulity();
    }

    public virtual void showResultAnimation()
    {
        foreach(Transform t in panel_selections.transform)
        {
            GameObject obj = t.gameObject;
            obj.GetComponent<Animator>().SetTrigger("show_result");
        }
    }

    void change_difficulity(bool isLower)
    {
        UnityEngine.Debug.Log("change_difficulity called");
        // change local value
        if (isLower)
        {
            if (gm.playerData.current_difficulity > 1)
            {
                // update local difficulity
                gm.playerData.current_difficulity--;
            }
        }
        else
        {
            if (gm.playerData.current_difficulity < 10)
            {
                // update local difficulity
                gm.playerData.current_difficulity++;
            }
        }
    }

    void update_difficulity()
    {
        // update server difficulity record
        User objToUpdate = new User();
        objToUpdate.id = gm.playerData.id;
        objToUpdate.current_difficulity = gm.playerData.current_difficulity;
        gm.do_update_difficulity(objToUpdate);
    }
    IEnumerator waitAndGoNext()
    {
        // show result
        showCoveredText();
        yield return new WaitForSeconds(4);
        showNextQuestion();
    }
    public void showCoveredText()
    {
        foreach (GameObject child in array_selectionsObj) {
            SelectionObject selectionObject = child.GetComponent<SelectionObject>();
            selectionObject.showCoveredText();
        }
    }

    public void play_audio()
    {
        if (!isplaying_Audio)
        {
            audio_story = GameObject.Find("audio_story").GetComponent<AudioSource>();
        
            MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
            int days = (7 * (mainController.selected_week - 1)) + mainController.selected_days;

            // get audio source
            string filename = (days).ToString("000") + "_game_" + (gm.current_story_part+1).ToString("00");
            // UnityEngine.Debug.Log("filename: " + filename);
            AudioClip storyClip = Resources.Load<AudioClip>("Sounds/game/" + filename);
            // set source
            audio_story.clip = storyClip;
            //play story
            audio_story.Play();

            // set audio button animation
            btn_playAudio.GetComponent<Animator>().SetTrigger("play");
            isplaying_Audio = true;
        }
    }

    public virtual void play_word_audio(int selected)
    {
        // UnityEngine.Debug.Log("play_word_audio selected: " + selected);
        string word_eng = "";
        Word[] array_words = list_selections[selected].array_word;
        foreach(Word w in array_words)
        {
            word_eng += w.word_eng;
        }

        UnityEngine.Debug.Log("word_eng: " + word_eng);
        do_play_word_audio(word_eng);
    }

    public virtual string get_word_eng(int selected)
    {
        string word_eng = "";
        Word[] array_words = list_selections[selected].array_word;
        foreach(Word w in array_words)
        {
            word_eng += w.word_eng;
        }

        return word_eng;
    }

    public void do_play_word_audio(string filename)
    {
        float f_story = PlayerPrefs.GetFloat("setting_story", GameSetting.DEFAULT_VOLUME_STORY);
        
        audio_word = GameObject.Find("audio_word").GetComponent<AudioSource>();
        audio_word.volume = f_story;

        AudioClip storyClip = Resources.Load<AudioClip>("Sounds/word/" + filename);
        // set source
        audio_word.clip = storyClip;
        //play story
        audio_word.Play();
    }

    public void exitClicked()
    {
        gm.play_SE("se_confirm", 1f);
        AlertController alert =  gm.showAlert("提示", "確定要退出嗎?", null, "退出", "取消", true);
        alert.btn_confirm.onClick.AddListener(do_exitClicked); 
        alert.btn_confirm.onClick.AddListener(do_gotoStory); 
    }
    public void do_gotoStory()
    {
        MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
        mainController.gotoStoryClicked(); 
    }

    public void do_exitClicked()
    {
        MainController mainController = GameObject.Find("MainController").GetComponent<MainController>();
        mainController.setup_map_UI();
        mainController.setup_days_UI();
        // remove this temp
        GameObject.Destroy(this.gameObject);

        gm.play_BGM("bgm_main");
    }
    public string get_csvFolderPath()
    {
        int current_week = gm.get_current_week_FromProgress();
        if (current_week<=GameSetting.week_Homophone)
        {
            return "Homophone";
        }
        else if(current_week<=GameSetting.week_Construction)
        {
            return "Construction";
        }
        else
        {
            return "Homograph";
            // int randomInt = Random.Range(0,2);
            // switch (randomInt)
            // {
            //     case 0:
            //         return "Homophone";
            //     case 1:
            //         return "Construction";

            //     default:
            //         return "";
            //     // break;
            // }
        }
    }

    // ################################################################################################

    // ************************************************************************************************
    // Tell QUESTION LOADING
    // ************************************************************************************************
    public void do_load_tellData(string csvFolder)
    {
        // string csvFolder = get_csvFolderPath() + "/";
        // UnityEngine.Debug.Log("csv path: " + "csv/" + csvFolder + "Tell");
        csv_tell = Resources.Load<TextAsset>("csv/" + csvFolder + "Tell");
        StartCoroutine(load_tellData());
    }
    IEnumerator load_tellData()
    {
        string[,] data_items = CSVReader.SplitCsvGrid(csv_tell.text);
        ProcessTellData(data_items);
        yield return null;
    }

    void ProcessTellData(string[,] data_items)
    {
        // difficulty	result	word_1	word_2	word_3	word_4
        const string key_id = "id";
        const string key_difficulty = "difficulty";
        const string key_result = "result";
        const string key_word_1 = "word_1";
        const string key_word_2 = "word_2";
        const string key_word_3 = "word_3";
        const string key_word_4 = "word_4";

        int index_id=0;
        int index_difficulty=0;
        int index_result=0;
        int index_word_1=0;
        int index_word_2=0;
        int index_word_3=0;
        int index_word_4=0;

        array_TellData = new Question_Tell[data_items.GetLength(1) - 3];

        for (int i = 0; i<data_items.GetLength(0); i++){
            string theKey = data_items[i,0];
            switch (theKey)
            {
                case key_id:
                    index_id = i;
                break;
                
                case key_difficulty:
                    index_difficulty = i;
                break;
                case key_result:
                    index_result = i;
                break;
                case key_word_1:
                    index_word_1 = i;
                break;
                case key_word_2:
                    index_word_2 = i;
                break;
                case key_word_3:
                    index_word_3 = i;
                break;
                case key_word_4:
                    index_word_4 = i;
                break;

                default:
                break;
            }
        }

        for(int j = 1; j < data_items.GetLength(1) - 2; j++) {
            Question_Tell item = new Question_Tell();
            item.id = data_items[index_id, j];
            item.isCorrect = (int.Parse(data_items[index_result, j])==1);   // 1: true 0: false
            // UnityEngine.Debug.Log("Tell question isCorrect: " + item.isCorrect);
            
            // get word string
            char[] charsToTrim = {' '};
            string word_1 = data_items[index_word_1, j].Trim(charsToTrim);
            string word_2 = data_items[index_word_2, j].Trim(charsToTrim);
            string word_3 = data_items[index_word_3, j].Trim(charsToTrim);
            string word_4 = data_items[index_word_4, j].Trim(charsToTrim);
            string[] array_word = {word_1, word_2, word_3, word_4};


            // UnityEngine.Debug.Log("ID: " + item.id + " word_1: " + word_1 + " word_2: " + word_2 + " word_3: " + word_3 + " word_4: " + word_4);
            // process string to Word array
            Word[] array_word_1 = (word_1 != "") ? Word.process_Words(word_1) : new Word[0];
            Word[] array_word_2 = (word_2 != "") ? Word.process_Words(word_2) : new Word[0];
            Word[] array_word_3 = (word_3 != "") ? Word.process_Words(word_3) : new Word[0];
            Word[] array_word_4 = (word_4 != "") ? Word.process_Words(word_4) : new Word[0];

            // find hidden target
            string str_hiddenTarget = "";
            List<string> list_allEng = new List<string>();

            Word[][] all_array_word = new Word[][]{array_word_1, array_word_2, array_word_3, array_word_4};
            foreach (Word[] arr_w in all_array_word)
            {
                foreach (Word w in arr_w)
                {
                    list_allEng.Add(w.word_eng);
                }
            }

            var wordGroup = list_allEng.ToArray().GroupBy(x => x);
            var maxCount = wordGroup.Max(g => g.Count());
            string mostCommons = (wordGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray())[0];
            str_hiddenTarget = mostCommons;
            // UnityEngine.Debug.Log("str_hiddenTarget: " + str_hiddenTarget);

            // create Selections
            Selection selection_1 = new Selection(str_hiddenTarget, array_word_1, item.isCorrect);
            Selection selection_2 = new Selection(str_hiddenTarget, array_word_2, item.isCorrect);
            Selection selection_3 = new Selection(str_hiddenTarget, array_word_3, item.isCorrect);
            Selection selection_4 = new Selection(str_hiddenTarget, array_word_4, item.isCorrect);
            Selection[] array_selections = {selection_1, selection_2, selection_3, selection_4};

            List<Selection> list_selections = new List<Selection>();
            for (int i=0; i<array_word.Length; i++)
            {
                if (array_word[i] != "")
                {
                    list_selections.Add(array_selections[i]);
                }
            }

            // find the difficulity level
            item.difficulityLevel = int.Parse(data_items[index_difficulty, j]);
            
            
            item.selections = list_selections.ToArray();
            array_TellData[j - 1] = item;
        }
    }
    // ************************************************************************************************
    // Same QUESTION LOADING
    // ************************************************************************************************
    public void do_load_sameData(string csvFolder)
    {
        // string csvFolder = get_csvFolderPath() + "/";
        // UnityEngine.Debug.Log("csv path: " + "csv/" + csvFolder + "Same");
        csv_same = Resources.Load<TextAsset>("csv/" + csvFolder + "Same");
        StartCoroutine(load_sameData());
    }
    IEnumerator load_sameData()
    {
        string[,] data_items = CSVReader.SplitCsvGrid(csv_same.text);
        ProcessSameData(data_items);
        yield return null;
    }

    void ProcessSameData(string[,] data_items)
    {
        // difficulty	word_1	word_2	word_3	word_4
        const string key_id = "id";
        const string key_difficulty = "difficulty";
        const string key_word_1 = "word_1";
        const string key_word_2 = "word_2";
        const string key_word_3 = "word_3";
        const string key_word_4 = "word_4";
        const string key_word_5 = "word_5";

        int index_id=0;
        int index_difficulty=0;
        int index_word_1=0;
        int index_word_2=0;
        int index_word_3=0;
        int index_word_4=0;
        int index_word_5=0;

        array_SameData = new Question_Same[data_items.GetLength(1) - 3];

        for (int i = 0; i<data_items.GetLength(0); i++){
            string theKey = data_items[i,0];
            switch (theKey)
            {
                case key_id:
                    index_id = i;
                break;
                case key_difficulty:
                    index_difficulty = i;
                break;
                case key_word_1:
                    index_word_1 = i;
                break;
                case key_word_2:
                    index_word_2 = i;
                break;
                case key_word_3:
                    index_word_3 = i;
                break;
                case key_word_4:
                    index_word_4 = i;
                break;
                case key_word_5:
                    index_word_5 = i;
                break;

                default:
                break;
            }
        }

        for(int j = 1; j < data_items.GetLength(1) - 2; j++) {
            Question_Same item = new Question_Same();
            item.id = data_items[index_id, j];
            
            // get word string
            char[] charsToTrim = {' '};
            string word_1 = data_items[index_word_1, j].Trim(charsToTrim);
            string word_2 = data_items[index_word_2, j].Trim(charsToTrim);
            string word_3 = data_items[index_word_3, j].Trim(charsToTrim);
            string word_4 = data_items[index_word_4, j].Trim(charsToTrim);
            string word_5 = data_items[index_word_5, j].Trim(charsToTrim);
            string[] array_word = {word_2, word_3, word_4, word_5};

            // UnityEngine.Debug.Log("ID: " + item.id + " word_1: " + word_1 + " word_2: " + word_2 + " word_3: " + word_3 + " word_4: " + word_4);
            // process string to Word array
            Word[] array_word_1 = (word_1 != "") ? Word.process_Words(word_1) : new Word[0];
            Word[] array_word_2 = (word_2 != "") ? Word.process_Words(word_2) : new Word[0];
            Word[] array_word_3 = (word_3 != "") ? Word.process_Words(word_3) : new Word[0];
            Word[] array_word_4 = (word_4 != "") ? Word.process_Words(word_4) : new Word[0];
            Word[] array_word_5 = (word_5 != "") ? Word.process_Words(word_5) : new Word[0];

            // find hidden target
            string str_hiddenTarget = "";
            List<string> list_allEng = new List<string>();
            Word[][] all_array_word = new Word[][]{array_word_1, array_word_2, array_word_3, array_word_4, array_word_5};
            foreach (Word[] arr_w in all_array_word)
            {
                foreach (Word w in arr_w)
                {
                    list_allEng.Add(w.word_eng);
                }
            }

            var wordGroup = list_allEng.ToArray().GroupBy(x => x);
            var maxCount = wordGroup.Max(g => g.Count());
            string mostCommons = (wordGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray())[0];
            str_hiddenTarget = mostCommons;
            // UnityEngine.Debug.Log("str_hiddenTarget: " + str_hiddenTarget);

            // create Selections
            Selection selection_ref = new Selection(str_hiddenTarget, array_word_1, false);               // word_1 is reference
            item.selection_ref = selection_ref;
            
            Selection selection_2 = new Selection(str_hiddenTarget, array_word_2, true);        // word_2 must be correct
            Selection selection_3 = new Selection(str_hiddenTarget, array_word_3, false);
            Selection selection_4 = new Selection(str_hiddenTarget, array_word_4, false);
            Selection selection_5 = new Selection(str_hiddenTarget, array_word_5, false);
            Selection[] array_selections = {selection_2, selection_3, selection_4, selection_5};

            List<Selection> list_selections = new List<Selection>();
            for (int i=0; i<array_word.Length; i++)
            {
                if (array_word[i] != "")
                {
                    list_selections.Add(array_selections[i]);
                }
            }

            // find the difficulity level
            item.difficulityLevel = int.Parse(data_items[index_difficulty, j]);
            
            item.selections = list_selections.ToArray();
            array_SameData[j - 1] = item;
        }
    }
    // ************************************************************************************************
    // Different QUESTION LOADING
    // ************************************************************************************************
    public void do_load_differentData(string csvFolder)
    {
        // string csvFolder = get_csvFolderPath() + "/";
        // UnityEngine.Debug.Log("csv path: " + "csv/" + csvFolder + "Differ");
        csv_different = Resources.Load<TextAsset>("csv/" + csvFolder + "Differ");
        StartCoroutine(load_differentData());
    }
    IEnumerator load_differentData()
    {
        string[,] data_items = CSVReader.SplitCsvGrid(csv_different.text);
        ProcessDifferentData(data_items);
        yield return null;
    }

    void ProcessDifferentData(string[,] data_items)
    {
        // difficulty	word_1	word_2	word_3	word_4
        const string key_id = "id";
        const string key_difficulty = "difficulty";
        const string key_word_1 = "word_1";
        const string key_word_2 = "word_2";
        const string key_word_3 = "word_3";
        const string key_word_4 = "word_4";
        const string key_word_5 = "word_5";

        int index_id=0;
        int index_difficulty=0;
        int index_word_1=0;
        int index_word_2=0;
        int index_word_3=0;
        int index_word_4=0;
        int index_word_5=0;

        array_DifferentData = new Question_Different[data_items.GetLength(1) - 3];

        for (int i = 0; i<data_items.GetLength(0); i++){
            string theKey = data_items[i,0];
            switch (theKey)
            {
                case key_id:
                    index_id = i;
                break;
                case key_difficulty:
                    index_difficulty = i;
                break;
                case key_word_1:
                    index_word_1 = i;
                break;
                case key_word_2:
                    index_word_2 = i;
                break;
                case key_word_3:
                    index_word_3 = i;
                break;
                case key_word_4:
                    index_word_4 = i;
                break;
                case key_word_5:
                    index_word_5 = i;
                break;

                default:
                break;
            }
        }

        for(int j = 1; j < data_items.GetLength(1) - 2; j++) {
            Question_Different item = new Question_Different();

            item.id = data_items[index_id, j];
            
            // get word string
            char[] charsToTrim = {' '};
            string word_1 = data_items[index_word_1, j].Trim(charsToTrim);
            string word_2 = data_items[index_word_2, j].Trim(charsToTrim);
            string word_3 = data_items[index_word_3, j].Trim(charsToTrim);
            string word_4 = data_items[index_word_4, j].Trim(charsToTrim);
            string word_5 = data_items[index_word_5, j].Trim(charsToTrim);
            string[] array_word = {word_1, word_2, word_3, word_4, word_5};
            // UnityEngine.Debug.Log("ID: " + item.id + " word_1: " + word_1 + " word_2: " + word_2 + " word_3: " + word_3 + " word_4: " + word_4);


            // UnityEngine.Debug.Log("ID: " + item.id + " word_1: " + word_1 + " word_2: " + word_2 + " word_3: " + word_3 + " word_4: " + word_4);
            // process string to Word array
            Word[] array_word_1 = (word_1 != "") ? Word.process_Words(word_1) : new Word[0];
            Word[] array_word_2 = (word_2 != "") ? Word.process_Words(word_2) : new Word[0];
            Word[] array_word_3 = (word_3 != "") ? Word.process_Words(word_3) : new Word[0];
            Word[] array_word_4 = (word_4 != "") ? Word.process_Words(word_4) : new Word[0];
            Word[] array_word_5 = (word_5 != "") ? Word.process_Words(word_5) : new Word[0];

            // find hidden target
            string str_hiddenTarget = "";
            List<string> list_allEng = new List<string>();
            Word[][] all_array_word = new Word[][]{array_word_1, array_word_2, array_word_3, array_word_4, array_word_5};
            foreach (Word[] arr_w in all_array_word)
            {
                foreach (Word w in arr_w)
                {
                    list_allEng.Add(w.word_eng);
                }
            }

            var wordGroup = list_allEng.ToArray().GroupBy(x => x);
            var maxCount = wordGroup.Max(g => g.Count());
            string mostCommons = (wordGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray())[0];
            str_hiddenTarget = mostCommons;
            // UnityEngine.Debug.Log("str_hiddenTarget: " + str_hiddenTarget);

            // create Selections
            Selection selection_1 = new Selection(str_hiddenTarget, array_word_1, true);            // first one must be correct
            Selection selection_2 = new Selection(str_hiddenTarget, array_word_2, false);
            Selection selection_3 = new Selection(str_hiddenTarget, array_word_3, false);
            Selection selection_4 = new Selection(str_hiddenTarget, array_word_4, false);
            Selection selection_5 = new Selection(str_hiddenTarget, array_word_5, false);
            Selection[] array_selections = {selection_1, selection_2, selection_3, selection_4, selection_5};

            List<Selection> list_selections = new List<Selection>();
            for (int i=0; i<array_word.Length; i++)
            {
                if (array_word[i] != "")
                {
                    list_selections.Add(array_selections[i]);
                }
            }

            // find the difficulity level
            item.difficulityLevel = int.Parse(data_items[index_difficulty, j]);
            
            item.selections = list_selections.ToArray();
            array_DifferentData[j - 1] = item;
        }
    }
    // ************************************************************************************************

    public string get_csvRawData(string id, MiniGameController.MiniGameQuestionType questionType)
    {
        string csvName = "";
        if (questionType == MiniGameController.MiniGameQuestionType.Find_different)
        {
            csvName = "Differ";
        }
        else if (questionType == MiniGameController.MiniGameQuestionType.Find_same)
        {
            csvName = "Same";
        }
        else if (questionType == MiniGameController.MiniGameQuestionType.Judge)
        {
            csvName = "Tell";
        }

        string csvFolder = get_csvFolderPath() + "/";
        TextAsset csv = Resources.Load<TextAsset>("csv/" + csvFolder + csvName);
        string[,] data_items = CSVReader.SplitCsvGrid(csv.text);
        string result = "";

        for(int i = 1; i < data_items.GetLength(1) - 2; i++) 
        {
            // data_items[index_word_1, j].Trim(charsToTrim);
            if (data_items[0, i] == id)
            {
                // UnityEngine.Debug.Log("id " + id + " found in row " + i);
                for(int j = 0; j < data_items.GetLength(0) - 2; j++)
                {
                    result += data_items[j, i] + ((j == data_items.GetLength(0) - 3) ? "" : ",");
                }
                // UnityEngine.Debug.Log("csv raw result: " + result);
                return result;
            }
        }
        return "";
    }

    public void run_testing_load()
    {
        string testingText = "";

        UnityEngine.Debug.Log("=========================Testing start=========================");
        testingText +=("[Homophone]\n");
        do_load_differentData("Homophone/");
        do_load_sameData("Homophone/");
        do_load_tellData("Homophone/");
        
        testingText +=("Num DifferentData: " + array_DifferentData.Length) + "\n";
        testingText +=("Num SameData: " + array_SameData.Length) + "\n";
        testingText +=("Num TellData: " + array_TellData.Length) + "\n";

        testingText += ("************************Checking DifferentData************************\n");
        
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 3;
            num_selections_max = 5;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Different[] selected_levelQuestion_different = get_questionSetByDifficulity_different(diff_lv);
                int numOfSelection = selected_levelQuestion_different.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
            // testingText += ("Total: " +  + "\n");
            
        }
        testingText += ("**********************************************************************\n");

        testingText += ("************************Checking SameData************************\n");
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 2;
            num_selections_max = 4;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Same[] selected_levelQuestion_same = get_questionSetByDifficulity_same(diff_lv);
                int numOfSelection = selected_levelQuestion_same.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
        }
        testingText += ("**********************************************************************\n");

        testingText += ("************************Checking TellData************************\n");
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 2;
            num_selections_max = 4;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Tell[] selected_levelQuestion_tell = get_questionSetByDifficulity_judge(diff_lv);
                int numOfSelection = selected_levelQuestion_tell.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
            
        }
        testingText += ("**********************************************************************\n");
        
        UnityEngine.Debug.Log(testingText);

        testingText = "";
        testingText += ("[Construction]\n");
        do_load_differentData("Construction/");
        do_load_sameData("Construction/");
        do_load_tellData("Construction/");
        
        testingText += ("Num DifferentData: " + array_DifferentData.Length) + "\n";
        testingText += ("Num SameData: " + array_SameData.Length) + "\n";
        testingText += ("Num TellData: " + array_TellData.Length) + "\n";

        testingText += ("************************Checking DifferentData************************\n");
        
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 3;
            num_selections_max = 5;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Different[] selected_levelQuestion_different = get_questionSetByDifficulity_different(diff_lv);
                int numOfSelection = selected_levelQuestion_different.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
        }
        testingText += ("**********************************************************************\n");

        testingText += ("************************Checking SameData************************\n");
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 2;
            num_selections_max = 4;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Same[] selected_levelQuestion_same = get_questionSetByDifficulity_same(diff_lv);
                int numOfSelection = selected_levelQuestion_same.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
        }
        testingText += ("**********************************************************************\n");

        testingText += ("************************Checking TellData************************\n");
        for (int diff_lv =1; diff_lv <= 10; diff_lv++)
        {
            testingText += ("=====lv [" + diff_lv + "]=====\n");
            num_selections_min = 2;
            num_selections_max = 4;
            for(int i=num_selections_min;i<=num_selections_max; i++)
            {
                num_selections_current = i;
                Question_Tell[] selected_levelQuestion_tell = get_questionSetByDifficulity_judge(diff_lv);
                int numOfSelection = selected_levelQuestion_tell.Length;
                testingText += "<" + i + ">: " + numOfSelection + "\n";
            }
        }
        testingText += ("**********************************************************************\n");
        
        UnityEngine.Debug.Log(testingText);
    }
}
