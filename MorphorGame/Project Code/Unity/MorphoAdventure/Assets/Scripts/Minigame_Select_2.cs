﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Select_2 : MiniGameMaster
{
    int move_time=10;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup grid view
        setup_gridView();

        // setup Minigame_Chase_Normal
        setup_Minigame_Select_3();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    public void setup_Minigame_Select_3()
    {
        // set object move time
        move_time = (limited_time != 0) ? limited_time : move_time;
    }
    void setup_gridView()
    {
        var gridLayoutGroup = panel_selections.GetComponent<GridLayoutGroup> ();
        var rect = panel_selections.GetComponent<RectTransform> ();
        var spacing = 20;
        var padding = 20;
        var numOfColum = 5;

        gridLayoutGroup.spacing = new Vector2(spacing, spacing);
        gridLayoutGroup.cellSize = new Vector2 ((rect.rect.width- 2 * padding - spacing*(numOfColum-1)) / numOfColum, (rect.rect.width- 2 * padding - spacing*(numOfColum - 1)) / numOfColum);
    }
    void do_move_anim()
    {
        Animator anim = this.GetComponent<Animator>();
        anim.speed = 60f/(float)move_time;
        anim.SetTrigger("right");
    }
    void do_pause_anim()
    {
       Debug.Log("do_pause_anim called");
        Animator anim = this.GetComponent<Animator>();
        anim.speed = 0f;
    }

    // =====================================================================================================
    // override functions
    // =====================================================================================================
    public override void generateQuestion_and_setAnimation()
    {
        // call generate question
        generateQuestion();

        // call set movement
        do_move_anim();
    }
    public override void confirm_clicked()
    {
        if (current_selected != -1 && (obj_progressBar.progress_current != obj_progressBar.progress_max))
        {
            // pause aniamtion
            do_pause_anim();

            // do check confirm
            do_confirmClicked(current_selected);
        }
    }
    public override void judge_clicked(bool isCorrect)
    {
        if (obj_progressBar.progress_current != obj_progressBar.progress_max)
        {
            // pause animation
            do_pause_anim();

            // do check judge
            do_judgeClicked(isCorrect);
        }
    }
    // =====================================================================================================
}
