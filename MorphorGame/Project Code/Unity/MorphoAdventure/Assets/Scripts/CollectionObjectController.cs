﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionObjectController : MonoBehaviour
{
    public Image img_bg;
    public Image img_front;
    public Image img_stampBG;

    public void setup(Sprite sp_bg, Sprite sp_front, float fill_percentage, bool isLarge)
    {
        img_bg.sprite = sp_bg;
        img_front.sprite = sp_front;
        img_front.fillAmount = fill_percentage;
        img_stampBG.gameObject.SetActive(isLarge);
    }
}
