﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameController : MonoBehaviour
{
    public enum MiniGameQuestionType
    {
        none,
        Find_different,
        Find_same,
        Judge
    }
    public enum MiniGameType 
    {
        none,
        Select_1,       //  選擇1
        Select_2,       //  選擇2
        Select_3,       //  選擇3
        Dodge,          //  躲避
        Chase,          //  追逐
        Move_forward,   //  前進
        Replace,        //  佔位
        Appear,         //  顯現
        Hamster         //  打地鼠
    };
    public enum MiniGameSelectionsPosition
    {
        none,
        Up_center,
        Left_center,
        Right_center,
    }
    public enum MiniGameMoveDirection
    {
        none,
        ToUP,
        ToDown,
        ToLeft,
        ToRight
    }
    public enum MiniGameTimeType
    {
        none,
        Story,
        Minigame
    }

    public MiniGameType current_miniGameType;

    public void load_minigame(MiniGameType miniGameType, MiniGameSelectionsPosition selectionsPos, MiniGameQuestionType questionType, MiniGameMoveDirection move_direction, int limited_time, int num_questions)
    {
        current_miniGameType = miniGameType;
        GameObject obj_minigame;

        switch (miniGameType)
        {
            case MiniGameType.Appear:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Appear>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Appear>().num_questions = num_questions;
                GameObject obj_appear = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Chase:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Chase_Normal>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Chase_Normal>().num_questions = num_questions;
                if (limited_time != 0)
                {
                    obj_minigame.GetComponent<Minigame_Chase_Normal>().limited_time = limited_time;
                }
                GameObject obj_chase = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Dodge:
                
                if (limited_time == 0)
                {
                    obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString() + "_Normal");
                    obj_minigame.GetComponent<Minigame_Doge_Normal>().current_questionType = questionType;
                    obj_minigame.GetComponent<Minigame_Doge_Normal>().num_questions = num_questions;
                }
                else
                {
                    obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString() + "_TimeLimited");
                    obj_minigame.GetComponent<Minigame_Dodge_TimeLimited>().current_questionType = questionType;
                    obj_minigame.GetComponent<Minigame_Dodge_TimeLimited>().num_questions = num_questions;
                    obj_minigame.GetComponent<Minigame_Dodge_TimeLimited>().limited_time = limited_time;
                }
                
                GameObject obj_dodge = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Hamster:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Hamster>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Hamster>().num_questions = num_questions;
                if (limited_time != 0)
                {
                    obj_minigame.GetComponent<Minigame_Hamster>().limited_time = limited_time;
                }
                GameObject obj_hamster = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Move_forward:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<MiniGame_Move_forward>().current_questionType = questionType;
                obj_minigame.GetComponent<MiniGame_Move_forward>().num_questions = num_questions;
                if (limited_time != 0)
                {
                    obj_minigame.GetComponent<MiniGame_Move_forward>().limited_time = limited_time;
                }
                GameObject obj_moveForward = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Replace:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Replace>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Replace>().num_questions = num_questions;
                GameObject obj_replace = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Select_1:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Select_1>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Select_1>().num_questions = num_questions;
                obj_minigame.GetComponent<Minigame_Select_1>().selectionPosition = selectionsPos;
                GameObject obj_select_1 = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Select_2:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Select_2>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Select_2>().num_questions = num_questions;
                
                if (limited_time != 0)
                {
                    obj_minigame.GetComponent<Minigame_Select_2>().limited_time = limited_time;
                }
                GameObject obj_select_2 = (GameObject)Instantiate(obj_minigame, this.transform);
            break;

            case MiniGameType.Select_3:
                obj_minigame = Resources.Load<GameObject>("Prefab/games/Minigame_" + current_miniGameType.ToString());
                obj_minigame.GetComponent<Minigame_Select_3>().current_questionType = questionType;
                obj_minigame.GetComponent<Minigame_Select_3>().num_questions = num_questions;
                obj_minigame.GetComponent<Minigame_Select_3>().move_direction = move_direction;
                
                if (limited_time != 0)
                {
                    obj_minigame.GetComponent<Minigame_Select_3>().limited_time = limited_time;
                }
                GameObject obj_select_3 = (GameObject)Instantiate(obj_minigame, this.transform);
            break;
        }
    }
}
