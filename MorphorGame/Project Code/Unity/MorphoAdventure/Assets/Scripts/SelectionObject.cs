﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionObject : MonoBehaviour
{
    public Image img_selection;
    public GameObject panel_text;
    public GameObject prefab_text;
    public int hidden_index;

    public void setup(Selection selection, Sprite spriteToUse)
    {
        panel_text.SetActive(true);
        // UnityEngin.UnityEngine.Debug.Log("SelectionObject setup called");
        this.hidden_index = selection.hidden_index;

        // remove all childs
        foreach (Transform child in panel_text.transform) {
            child.GetChild(0).GetComponent<Text>().text = "";
            child.GetChild(1).gameObject.SetActive(false);
        }
        // add childs
        for (int i=0; i<selection.array_word.Length; i++)
        {
            Word w = selection.array_word[i];
            // char c = w.word_chi;
            GameObject newObj = panel_text.transform.GetChild(i).gameObject;//(GameObject)Instantiate(prefab_text, panel_text.transform);
            Text t = newObj.transform.GetChild(0).GetComponent<Text>();
            Outline t_o = newObj.transform.GetChild(0).GetComponent<Outline>();
            t.text = w.word_chi;
            t.color= new Color(0,0,0,1);
            
            if (i == hidden_index)
            {
                t.color= new Color(106, 0, 0, 1);
                newObj.transform.GetChild(1).gameObject.SetActive(true);
                t_o.effectColor = new Color(0,0,0,(128f/255f));
            }
            else
            {
                t_o.effectColor = new Color(255,255,255,(128f/255f));
            }
        }

        img_selection.sprite = spriteToUse;
    }
    public void showCoveredText()
    {
        panel_text.transform.GetChild(hidden_index).GetChild(1).gameObject.SetActive(false);
    }
    public void setSprite(Sprite sp)
    {
        Debug.Log("setSprite called");
        img_selection.sprite = sp;
    }
    public void showSprite(bool isShow)
    {
        // UnityEngin.UnityEngine.Debug.Log("showSprite called");
        img_selection.gameObject.SetActive(isShow);
    }
}

public class Word
{
    public string word_chi = "";
    public string word_eng = "";
    public Word()
    {
        word_chi = "";
        word_eng = "";
    }

    public static Word[] process_Words(string word_str)
    {
        // UnityEngin.UnityEngine.Debug.Log("word to process: " + word_str);
        string[] words = word_str.Split(' ');
        List<Word> list_word = new List<Word>();

        foreach (string str in words) 
        {
            int index_start = str.IndexOf("(");
            int index_end = str.IndexOf(")");
            // UnityEngin.UnityEngine.Debug.Log("index_start: " + index_start + " index_end: " + index_end);

            if (index_start != -1 && index_end != -1)
            {
                string str_eng = str.Substring(index_start+1, index_end - index_start - 1);
                string str_chi = str.Substring(index_start-1, index_start);

                // UnityEngin.UnityEngine.Debug.Log("str_eng: " + str_eng + " str_chi: " + str_chi);
                Word w = new Word();
                w.word_chi = str_chi;
                w.word_eng = str_eng;

                list_word.Add(w);
            }
        }
        return list_word.ToArray();
    }
}

public class Selection
{
    public int hidden_index;
    public Word[] array_word;
    public bool isCorrect;

    public Selection()
    {
        hidden_index = 0;
        // selection_content = new char[];
        isCorrect = false;
    }

    public Selection(string str_hiddenTarget, Word[] array_word, bool isCorrect)
    {
        this.array_word = array_word;
        this.isCorrect = isCorrect;

        // find hidden index
        for(int i=0; i<array_word.Length; i++)
        {
            Word w = array_word[i];
            if (w.word_eng == str_hiddenTarget)
            {
                hidden_index = i;
                break;
            }
        }
    }
}
public class Question_Tell
{
    public string id;
    public Selection[] selections;
    public bool isCorrect = false;
    public int difficulityLevel;
    public Question_Tell()
    {
        id = "";
        isCorrect = false;
    }
}
public class Question_Same
{
    public string id;
    public Selection[] selections;
    public Selection selection_ref;
    public int difficulityLevel;


    public Question_Same()
    {
        id = "";
    }
}
public class Question_Different
{
    public string id;
    public Selection[] selections;
    public int difficulityLevel;

    public Question_Different()
    {
        id = "";
    }
}
