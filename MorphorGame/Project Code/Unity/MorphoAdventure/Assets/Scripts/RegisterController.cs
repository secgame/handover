﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Models;
using Proyecto26;
public class RegisterController : MonoBehaviour
{
    public GameObject win_login;
    GameManager gm;
    private readonly string basePath = APISetting.basePath + "/api/user";
	private RequestHelper currentRequest;
    public InputField tf_username;
    public InputField tf_email;
    public InputField tf_pw;
    public InputField tf_pwConfirm;
    public InputField tf_age;
    string data_gender = "M";
    public Button btn_male;
    public Button btn_female;
    public Button btn_confirm;

    void Awake()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
        genderClicked(true);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void genderClicked(bool isMale)
    {
        gm.play_SE("se_confirm", 1f);
        if (isMale)
        {
            data_gender = "M";
            btn_male.interactable = false;
            btn_female.interactable = true;
        }
        else
        {
            data_gender = "F";
            btn_male.interactable = true;
            btn_female.interactable = false;
        }
    }
    bool checkInput()
    {
        if (tf_username.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "用戶名不能為空白", null, "確定", "取消", false);
            return false;
        }
        if (tf_age.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "年齡不能為空白", null, "確定", "取消", false);
            return false;
        }
        // if (!tf_age.text.All(char.IsDigit))
        // {
        //     AlertController alert =  gm.showAlert("錯誤", "年齡只能為數字", null, "確定", "取消", false);
        //     return false;
        // }
        if (tf_email.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "電郵不能為空白", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pw.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "密碼不能為空白", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pw.text.Length < 8)
        {
            AlertController alert =  gm.showAlert("錯誤", "密碼長度不能少於8", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pwConfirm.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "確認密碼不能為空白", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pwConfirm.text.Length < 8)
        {
            AlertController alert =  gm.showAlert("錯誤", "確認密碼長度不能少於8", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pw.text != tf_pwConfirm.text)
        {
            AlertController alert =  gm.showAlert("錯誤", "確認密碼與密碼不符", null, "確定", "取消", false);
            return false;
        }
        else
        {
            return true;
        }
    }

    public void closeClicked()
    {
        gm.play_SE("se_cancel", 1f);
        this.gameObject.SetActive(false);
        win_login.SetActive(true);
    }
    public void confirmClicked()
    {
        gm.play_SE("se_confirm", 1f);
        
        if (gm.check_networkStatus())
        {
            do_register();
        }
    }

    void do_register()
    {
        // disable buttons
        setButtonsInteractable(false);

        // show loading
        gm.show_loading(true);

        if (checkInput())
        {
            // call API
            var newUser = new User {
                    username = tf_username.text.Trim(),
                    email = tf_email.text.Trim(),
                    age = int.Parse(tf_age.text.Trim()),
                    gender = data_gender,
                    password = tf_pw.text.Trim()};
            // UnityEngin.UnityEngine.Debug.Log("newUser: " + newUser.ToString());
            currentRequest = new RequestHelper {
                Uri = basePath + "/register.php",
                Body = newUser
            };
            RestClient.Post<User>(currentRequest)
            .Then(res => {
                // EditorUtility.DisplayDialog ("Success", JsonUtility.ToJson(res, true), "Ok")
                Debug.Log("Register Success: " + JsonUtility.ToJson(res, true));
                // close window
                closeClicked();
   
                // enable buttons
                setButtonsInteractable(true);

                // hide loading
                gm.show_loading(false);
            })
            .Catch(err => 
            {
                Debug.Log("Register Fail: " + err.Message);
                // show login fail
                AlertController alert =  gm.showAlert("註冊失敗", "用戶名已被使用", null, "確定", "取消", false);

                // enable buttons
                setButtonsInteractable(true);

                // hide loading
                gm.show_loading(false);
            });
        }
        else
        {
            // enable buttons
            setButtonsInteractable(true);
        }
    }

    void setButtonsInteractable(bool isInteractable)
    {
        btn_confirm.interactable = isInteractable;
        btn_male.interactable = isInteractable;
        btn_female.interactable = isInteractable;

        tf_age.enabled = isInteractable;
        tf_email.enabled = isInteractable;
        tf_pw.enabled = isInteractable;
        tf_pwConfirm.enabled = isInteractable;
        tf_username.enabled = isInteractable;
    }
}
