﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Select_1 : MiniGameMaster
{
    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup grid view
        setup_gridView();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    void setup_gridView()
    {
        var gridLayoutGroup = panel_selections.GetComponent<GridLayoutGroup> ();
        var rect = panel_selections.GetComponent<RectTransform> ();
        var spacing = 20;
        var padding = 20;
        var numOfColum = 3;

        gridLayoutGroup.spacing = new Vector2(spacing, spacing);
        gridLayoutGroup.cellSize = new Vector2 ((rect.rect.width- 2 * padding - spacing*(numOfColum-1)) / numOfColum, (rect.rect.width- 2 * padding - spacing*(numOfColum - 1)) / numOfColum);
    }
}
