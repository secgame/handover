using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Models;
using System;
using Proyecto26;

public class GameSetting : MonoBehaviour
{
    public static float DEFAULT_VOLUME_SE = 0.5f;
    public static float DEFAULT_VOLUME_BGM = 0.1f;
    public static float DEFAULT_VOLUME_STORY = 0.8f;

    public static float minigame_getStarScore = 0.5f;
    public static int sticker_small_weekToShow = 8;
    public static int sticker_large_weekToShow = 2;
    public static int[] difficulity_RightchangeCount = new int[10] { 3, 4, 4, 5, 5, 5, 6, 7, 8, 9 };
    public static int[] difficulity_WrongchangeCount = new int[10] { 4, 3, 3, 3, 2, 2, 2, 1, 1, 1 };

    public static int week_Homophone = 4;
    public static int week_Homograph = 12;
    public static int week_Construction = 8;

    public static int week_tutorial_Homophone = 1;
    public static int week_tutorial_Homograph = 9;
    public static int week_tutorial_Construction = 5;
    public static int week_tutorial_Combination = 13;

    public static string path_tutorial_Homophone = "http://" + APISetting.basePath + "/" + APISetting.videoPath + "homophone.mp4";
    public static string path_tutorial_Homograph = "http://" + APISetting.basePath + "/" + APISetting.videoPath + "homograph.mp4";
    public static string path_tutorial_Construction = "http://" + APISetting.basePath + "/" + APISetting.videoPath + "structure.mp4";
    public static string path_tutorial_Combination = "http://" + APISetting.basePath + "/" + APISetting.videoPath + "combination.mp4";
}