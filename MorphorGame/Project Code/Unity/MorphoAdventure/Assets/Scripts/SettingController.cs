﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingController : MonoBehaviour
{
    public Slider slider_bgm;
    public Slider slider_story;
    public Slider slider_se;
    public GameManager gm;
    // Start is called before the first frame update
    void Awake()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
        
    }
    void Start()
    {
        setup_sliders();
    }

    void setup_sliders()
    {
        float f_bgm = PlayerPrefs.GetFloat("setting_bgm", 0.5f);
        float f_story = PlayerPrefs.GetFloat("setting_story", 0.5f);
        float f_se = PlayerPrefs.GetFloat("setting_se", 0.5f);

        slider_bgm.value = f_bgm;
        slider_story.value = f_story;
        slider_se.value = f_se;

        save_sliderValues();
    }

    void save_sliderValues()
    {
        PlayerPrefs.SetFloat("setting_bgm", slider_bgm.value);
        PlayerPrefs.SetFloat("setting_story", slider_story.value);
        PlayerPrefs.SetFloat("setting_se", slider_se.value);
    }
    // Update is called once per frame
    public void sliders_changed()
    {
        // save setting
        save_sliderValues();
        // edit audios volume
        gm.set_audios_volume();
    }
}
