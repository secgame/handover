﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Appear : MiniGameMaster
{
    
    public Image img_objToShow;
    public Image img_objToShow_frame;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup special UI
        setup_appearUI();

        // setup grid view
        setup_gridView();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }

    void FixedUpdate()
    {
        update_objToShow();
        check_playingAudio();
    }

    void setup_appearUI()
    {
        Sprite sprite_frame = Resources.Load<Sprite>(sourcePath + "showObject_frame");
        Sprite sprite_obj = Resources.Load<Sprite>(sourcePath + "showObject");

        img_objToShow_frame.sprite = sprite_frame;
        img_objToShow.sprite = sprite_obj;
    }

    void setup_gridView()
    {
        var gridLayoutGroup = panel_selections.GetComponent<GridLayoutGroup> ();
        var rect = panel_selections.GetComponent<RectTransform> ();
        var spacing = 20;
        var padding = 20;
        var numOfColum = 3;

        gridLayoutGroup.spacing = new Vector2(spacing, spacing);
        gridLayoutGroup.cellSize = new Vector2 ((rect.rect.width- 2 * padding - spacing*(numOfColum-1)) / numOfColum, (rect.rect.width- 2 * padding - spacing*(numOfColum - 1)) / numOfColum);
    }

    public void update_objToShow()
    {
        float fa = (float)obj_scoreBoard.current_score / (float)obj_progressBar.progress_max;
        img_objToShow.fillAmount = fa;
    }
}
