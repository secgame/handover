﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeLimitedController : MonoBehaviour
{
    public GameObject target_controller;

    void Start()
    {
        if (target_controller == null)
        {
            target_controller = this.transform.parent.gameObject;
        }
    }

    public void did_end_time()
    {
        target_controller.SendMessage("timesUp");
    }
}
