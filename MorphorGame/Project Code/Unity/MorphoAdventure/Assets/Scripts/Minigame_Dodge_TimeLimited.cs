﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minigame_Dodge_TimeLimited : MiniGameMaster
{
    

    int move_time=10;
    public GameObject obj_runner;
    public GameObject obj_catcher;
    public Image bg_1;
    public Image bg_2;

    void Start()
    {
        // setup basic UI
        setup_graphicUI();

        // setup Minigame_Doge_Normal
        setup_Minigame_Doge_Normal();

        // reset selected index
        current_selected = -1;

        // setup question type ui
        setup_questionTypeUI();

        play_audio();
    }
    void FixedUpdate()
    {
        check_playingAudio();
    }

    public void setup_Minigame_Doge_Normal()
    {
        // set objects
        obj_runner = GameObject.Find("obj_runner");
        obj_runner.GetComponent<Image>().sprite = Resources.Load<Sprite>(sourcePath + "runner");
        obj_catcher = GameObject.Find("obj_catcher");
        
        obj_catcher.GetComponent<Image>().sprite = Resources.Load<Sprite>(sourcePath + "catcher");

        // set object move time
        move_time = (limited_time != 0) ? limited_time : move_time;

        bg_1 = GameObject.Find("bg_1").GetComponent<Image>();
        bg_2 = GameObject.Find("bg_2").GetComponent<Image>();

        bg_1.sprite = img_bg.sprite;
        bg_2.sprite = img_bg.sprite;
    }
    void do_move_anim()
    {
        Animator anim = this.gameObject.GetComponent<Animator>();
        anim.speed = 60f/(float)move_time;
        anim.SetTrigger("right");
    }
    void do_pause_anim()
    {
        Animator anim = this.gameObject.GetComponent<Animator>();
        anim.speed = 0f;
    }

    // =====================================================================================================
    // override functions
    // =====================================================================================================
    public override void generateQuestion_and_setAnimation()
    {
        // call generate question
        generateQuestion();

        // call set movement
        do_move_anim();
    }
    public override void confirm_clicked()
    {
        // pause aniamtion
        do_pause_anim();

        // do check confirm
        do_confirmClicked(current_selected);
    }
    public override void judge_clicked(bool isCorrect)
    {
        // pause animation
        do_pause_anim();

        // do check judge
        do_judgeClicked(isCorrect);
    }
    // =====================================================================================================
}
