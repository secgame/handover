﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Models;
using System.Diagnostics;

public class MainController : MonoBehaviour
{
    // Start is called before the first frame update
    GameManager gm;
    public GameObject pannel_map;
    public GameObject pannel_loading;
    public GameObject pannel_story;
    public GameObject pannel_miniGame;
    public GameObject pannel_achievement;
    public GameObject pannel_setting;

    // ************************************************************************
    // For Testing lock account only
    // ************************************************************************
    public bool testing_needToLogout = false;
    // ************************************************************************
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // TUTORIAL
    
    public bool isTutorial = false;
    public int tutorial_step = 0;
    bool tutorialPlayed_Homophone = false;
    bool tutorialPlayed_Homograph = false;
    bool tutorialPlayed_Construction = false;
    bool tutorialPlayed_Combination = false;
    //
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // STORY
    bool isForceToNextWeek = false;
    public Sprite[] array_storysImage;
    public Image img_storySlide_pre;
    public Image img_storySlide_current;
    public Image img_storySlide_next;
    int current_storySlideIndex = 0;
    MainGamePage current_page;
    enum MainGamePage
    {
        MAP, STORY, MINIGAME, ACHIEVEMENT
    }
    public const float MAX_SWIPE_TIME = 0.5f; 
	public const float MIN_SWIPE_DISTANCE = 0.17f;
    public bool testWithArrowKeys = true;
    Vector2 startPos;
	float startTime;
    public GameObject btn_story_previous;
    public GameObject btn_story_next;
    public GameObject btn_story_quit;
    public GameObject btn_story_start;
    public GameObject btn_story_end;
    public GameObject panel_map_flag;
    public GameObject panel_map_days;
    public GameObject btn_playAudio;
    
    public int selected_week = 0;
    public int selected_days = 0;
    public AudioSource audio_story;
    public int[] story_part_count;

    string[,] data_items; 
    public TextAsset csv_story;
    public StoryData[] array_StoryData;
    public TextAsset csv_minigame;
    public MinigameData[] array_MiniGameData;
    public StoryData current_storyData;
    public MinigameData current_minigameData;
    public Button[] array_btn_dayInWeek;
    public Button[] array_btn_week;

    double start_story_time;
    double end_story_time;

    bool isplaying_Audio = false;
    float[][] socres_groupByDay;
    public Sprite sp_star_enable;
    public Sprite sp_star_disable;
    
    public bool isDisableAutoStart = false;
    public int numOfSmallSticker = 0;
    public int numOfLargeSticker = 0;
    public List<int> scoreOfEachWeek = new List<int>();

    //檢測程序運行時間（ivy）
    public Stopwatch stopwatch = new Stopwatch();
    public bool isendtime = false;
    public double minutes = 0f;

    //
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    void Awake()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
    }
    void Start()
    {
        // process progress data
        socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();

        do_load_minigameData();

        // get num of stickers
        numOfSmallSticker = get_smallSticker_count();
        numOfLargeSticker = get_largeSticker_count();
    }

    void didLoadMiniGameData()
    {
        do_load_storyData();
    }

    void didLoadStoryData()
    {
        switch_page(MainGamePage.MAP);
        setup_map_UI();

        // auto start story
        if (!isDisableAutoStart)
        {
            check_and_startStory();
        }
    }

    void check_and_startStory()
    {
        if (gm.isTestingMode)
        {
            int maxDay = gm.get_maxDayFromProgress();
            if (maxDay == 0)
            {
                // start tutorial
                isTutorial = true;
                selected_week = 1;
                daysClicked(0);
            }
            else
            {
                selected_week = (int)(System.Math.Ceiling((float)maxDay/(float)7));;

                int daysInWeekToPass = (maxDay % 7 == 0) ? 7 : maxDay % 7;
                daysClicked(daysInWeekToPass);
            }
        }
        else
        {
            // check start day
            if (gm.current_day >= 0)
            {
                // check start tutorial
                bool isStartTutorial = false;
                float[] scoresOfTutorial = gm.get_scoresOfDay(0);       // day 0 is tutorial
                isStartTutorial = (scoresOfTutorial.Length < 3);        // records < 3 is not finished
                if (isStartTutorial)
                {
                    // start tutorial
                    isTutorial = true;
                    selected_week = 1;
                    daysClicked(0);
                }
                // start new normal day
                else
                {
                    int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();
                    int dayToCheck_start = (current_week - 1) * 7 + 1;
                    int current_daysInweek = (gm.current_progress_day)%7;        // 1 = Mon, 2 = Tue, 3 = Wed, 4 = Thur, 5 = Fri, 6 = Sat, 0 = Sun
                    int dayToCheck_end = dayToCheck_start + (current_daysInweek == 0 ? 7 : current_daysInweek);
                    UnityEngine.Debug.Log("current_week: " + current_week);
                    UnityEngine.Debug.Log("dayToCheck_start: " + dayToCheck_start + " dayToCheck_end: " + dayToCheck_end);
                    // check scores of the week
                    for (int i=dayToCheck_start; i<dayToCheck_end; i++)
                    {
                        bool isGoingToStartGame = false;

                        // get scores
                        UnityEngine.Debug.Log("socres_groupByDay.Length: " + socres_groupByDay.Length);
                        float[] scoresOfDay = socres_groupByDay[i];

                        // check if played or not (check last progress record submit date)
                        if (scoresOfDay.Length < 3)
                        {
                            // not first day
                            if(i == dayToCheck_start)
                            {
                                // <-------------------------- Start/continue game
                                UnityEngine.Debug.Log("[Start first new game]");
                                isGoingToStartGame = true;
                            }
                            else
                            {
                                // get progress before the day(empty record)
                                GameProgress[] progressOfLastDay = gm.get_progressOfDay(i-1);
                                
                                // check if last record is today or not (check submit date)
                                long timestamp_lastRecordSubmitDate = progressOfLastDay[0].submit_date;
                                int diff_last = GameManager.compare_dayDiffToNow(timestamp_lastRecordSubmitDate);
                                // Today played
                                if (diff_last == 0)
                                {
                                    UnityEngine.Debug.Log("[Checking: " + i + "...today played]");
                                }
                                else
                                {
                                    if(scoresOfDay.Length == 0)
                                    {
                                        // <-------------------------- Start game
                                        UnityEngine.Debug.Log("[Start new game]");
                                        isGoingToStartGame = true;
                                    }
                                    else
                                    {
                                        GameProgress[] progressOfToday = gm.get_progressOfDay(i);
                                        long timestamp_todayRecordSubmitDate = progressOfToday[0].submit_date;
                                        int diff_today = GameManager.compare_dayDiffToNow(timestamp_todayRecordSubmitDate);
                                        UnityEngine.Debug.Log("diff_today: " + diff_today);
                                        // today's incomplete record
                                        if (diff_today == 0)
                                        {
                                            // <-------------------------- continue game
                                            UnityEngine.Debug.Log("[Continue game]");
                                            isGoingToStartGame = true;
                                        }
                                        else
                                        {
                                            UnityEngine.Debug.Log("[Checking: " + i + "...it's incomplete day before]");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.Log("[Checking: " + i + "...day finished]");
                        }

                        // start game
                        if (isGoingToStartGame)
                        {
                            UnityEngine.Debug.Log("[Checking: " + i + "...going to start game]");
                            int daysInWeekToPass = ((i - dayToCheck_start) % 7 + 1);
                            selected_week = current_week;
                            daysClicked(daysInWeekToPass);
                            break;
                        }
                    }
                }
            }
            else
            {
                UnityEngine.Debug.Log("[Checking...day before start day]");
            }
        }
    }

    void checkShowTutorialContent()
    {
        if (isTutorial)
        {
            switch (tutorial_step)
            {
                case 0:
                    gm.highlight_obj(btn_story_next.gameObject, "點擊進入下一頁", null);
                break;

                case 1:
                    gm.highlight_obj(btn_story_start.gameObject, "點擊進入小遊戲", null);
                break;

                default:
                break;
            }

            tutorial_step++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (current_page == MainGamePage.STORY)
        {
            if(Input.touches.Length > 0)
            {
                Touch t = Input.GetTouch(0);
                if(t.phase == TouchPhase.Began)
                {
                    startPos = new Vector2(t.position.x/(float)Screen.width, t.position.y/(float)Screen.width);
                    startTime = Time.time;
                }
                if(t.phase == TouchPhase.Ended)
                {
                    if (Time.time - startTime > MAX_SWIPE_TIME) // press too long
                        return;

                    Vector2 endPos = new Vector2(t.position.x/(float)Screen.width, t.position.y/(float)Screen.width);

                    Vector2 swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                    if (swipe.magnitude < MIN_SWIPE_DISTANCE) // Too short swipe
                        return;

                    if (Mathf.Abs (swipe.x) > Mathf.Abs (swipe.y)) { // Horizontal swipe
                        bool isIdle = pannel_story.GetComponent<Animator>().GetBool("idle");
                        if (isIdle)
                        {
                            int max_page_index = 0;
                            int min_page_index = 0;
                            
                            // get max page index
                            for (int i = 0; i <= gm.current_story_part; i++)
                            {
                                max_page_index += story_part_count[i];
                            }

                            // get min page index
                            min_page_index = (gm.current_story_part == 0) ? 0: (story_part_count[gm.current_story_part - 1]);

                            if (swipe.x < 0) 
                            {
                                if (current_storySlideIndex < max_page_index - 1)
                                {
                                    do_nextPage();//pannel_story.GetComponent<Animator>().SetTrigger("next");
                                }
                            }
                            else 
                            {
                                if (current_storySlideIndex > min_page_index)
                                {
                                    do_previousPage();//pannel_story.GetComponent<Animator>().SetTrigger("previous");
                                }
                            }
                        }
                        
                    }
                }
                
            }

            if (testWithArrowKeys) 
            {
                // swipedDown = swipedDown || Input.GetKeyDown (KeyCode.DownArrow);
                // swipedUp = swipedUp|| Input.GetKeyDown (KeyCode.UpArrow);
                bool isIdle = pannel_story.GetComponent<Animator>().GetBool("idle");

                int max_page_index = 0;
                int min_page_index = 0;
                
                // get max page index
                for (int i = 0; i <= gm.current_story_part; i++)
                {
                    max_page_index += story_part_count[i];
                }

                // get min page index
                min_page_index = (gm.current_story_part == 0) ? 0: (story_part_count[gm.current_story_part - 1]);

                if (Input.GetKeyDown (KeyCode.RightArrow) && isIdle)
                {
                    if (current_storySlideIndex < max_page_index - 1)
                    {
                        do_nextPage();
                    }
                    
                }
                if (Input.GetKeyDown (KeyCode.LeftArrow) && isIdle)
                {
                    if (current_storySlideIndex > min_page_index)
                    {
                        do_previousPage();
                    }
                }   
            }
        }
        if (isplaying_Audio)
        {
            if (!audio_story.isPlaying)
            {
                // set audio button animation
                btn_playAudio.GetComponent<Animator>().SetTrigger("idle");
                isplaying_Audio = false;
            }
        } 
    }
    public void do_nextPage()
    {
        pannel_story.GetComponent<Animator>().SetTrigger("next");
        pannel_story.GetComponent<Animator>().SetBool("idle", false);
    }
    public void do_previousPage()
    {
        pannel_story.GetComponent<Animator>().SetTrigger("previous");
        pannel_story.GetComponent<Animator>().SetBool("idle", false);
    }
    public void setup_map_UI()
    {
        int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();
        for (int i=0; i<current_week; i++)
        {
            array_btn_week[i].interactable = true;
            if (i+1 == current_week)
            {
                array_btn_week[i].transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                array_btn_week[i].transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
    public void playTutorialVideo(string videoPath)
    {
        // play video
        gm.play_video(videoPath);

        // set video end callback
        gm.videoPlayer.loopPointReached += DidEndTutorialVideo;
    }
    void DidEndTutorialVideo(UnityEngine.Video.VideoPlayer videoPlayer)
    {
        // continue show and play story
        startGameClicked(gm.current_day);
    }

    public void logout_clicked()
    {
        gm.play_SE("se_confirm", 1f);
        AlertController alert =  gm.showAlert("登出", "確定要登出嗎?", null, "登出", "取消", true);
        alert.btn_confirm.onClick.AddListener(() => do_logout());
    }
    public void do_logout()
    {
        PlayerPrefs.SetString("login_username", "");
        PlayerPrefs.SetString("login_password", "");
        gm.change_scene("0_LoginAndRegister");
    }

    public void setup_days_UI()
    {
        // reset starts
        foreach (Button btn in array_btn_dayInWeek)
        {
            foreach (Transform t_start in btn.transform.GetChild(1).transform)
            {
                t_start.gameObject.GetComponent<Image>().sprite = sp_star_disable;
            }
        }

        int numOfDaysToUse = (gm.isTestingMode) ? gm.get_maxDayFromProgress() : gm.current_progress_day;
        int dayToShow_start = (selected_week - 1) * 7 + 1;
        int dayToShow_end = dayToShow_start + 7;
        int current_daysInweek = numOfDaysToUse%7;        // 1 = Mon, 2 = Tue, 3 = Wed, 4 = Thur, 5 = Fri, 6 = Sat, 0 = Sun
        int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();

        socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();

        UnityEngine.Debug.Log("socres_groupByDay.Length: " + socres_groupByDay.Length);

        // start setting days button in week range
        for (int i=dayToShow_start; i<dayToShow_end; i++)
        {
            // get the day button
            Button b = array_btn_dayInWeek[i-dayToShow_start];

            // make sure have records before setting
            if (i < socres_groupByDay.Length)
            {
                UnityEngine.Debug.Log("setting i: " + i);
                // get score of the checking day
                float[] scoresOfDay = socres_groupByDay[i];

                // hide lock if on/before current progress day
                b.transform.GetChild(2).gameObject.SetActive(false);

                // set stars
                int numOfStar = 0;
                foreach(float score_float in scoresOfDay)
                {
                    if (score_float >= GameSetting.minigame_getStarScore)
                    {
                        numOfStar++;
                    }
                }
                for (int j=0; j<3; j++)
                {
                    array_btn_dayInWeek[i-dayToShow_start].transform.GetChild(1).GetChild(j).GetComponent<Image>().sprite = ((j<numOfStar) ? sp_star_enable : sp_star_disable);
                }

                // current week
                if (current_week == selected_week)
                {
                    // 6th or 7th day of the week
                    if (current_daysInweek == 6 || current_daysInweek == 0 || gm.isTestingMode)
                    {
                        UnityEngine.Debug.Log("checking weekend scoresOfDay.Length: " + scoresOfDay.Length);
                        if (scoresOfDay.Length < 3)
                        {
                            UnityEngine.Debug.Log("enable click of " + i);
                            b.interactable = true;
                        }
                        else
                        {
                            UnityEngine.Debug.Log("disable click of " + i);
                            // played disable click
                            b.interactable = false;
                        }
                    }
                    else
                    {
                        // check if played or not (check last progress record submit date)
                        if (scoresOfDay.Length < 3)
                        {
                            if (i == dayToShow_start)
                            {
                                b.interactable = true;
                            }
                            else
                            {
                                // get progress before the day(empty record)
                                GameProgress[] progressOfLastDay = gm.get_progressOfDay(i-1);
                                if (progressOfLastDay.Length == 0)
                                {
                                    // disable skip day
                                    b.interactable = false;
                                }
                                else
                                {
                                    // check if last record is today or not (check submit date)
                                    long timestamp_lastRecordSubmitDate = progressOfLastDay[0].submit_date;
                                    int diff_last = GameManager.compare_dayDiffToNow(timestamp_lastRecordSubmitDate);
                                    // Today played
                                    if (diff_last == 0)
                                    {
                                        // disable today played
                                        b.interactable = false;
                                    }
                                    else
                                    {
                                        if (scoresOfDay.Length == 0)
                                        {
                                            b.interactable = true;
                                        }
                                        else
                                        {
                                            GameProgress[] progressOfToday = gm.get_progressOfDay(i);
                                            long timestamp_todayRecordSubmitDate = progressOfToday[0].submit_date;
                                            int diff_today = GameManager.compare_dayDiffToNow(timestamp_todayRecordSubmitDate);
                                            UnityEngine.Debug.Log("diff_today: " + diff_today);
                                            // today's incomplete record
                                            if (diff_today == 0)
                                            {
                                                b.interactable = true;
                                            }
                                            else
                                            {
                                                b.interactable = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // played disable click
                            b.interactable = false;
                        }
                    }
                }
                // not current week
                else
                {
                    // all disable click
                    b.interactable = false;
                }
            }
            else
            {
                if (!gm.isTestingMode)
                {
                    // no record disable
                    b.interactable = false;
                }
                // else
                // {
                //     // enable new day for testing
                //     if (i == socres_groupByDay.Length)
                //     {
                //         // enable click
                //         b.interactable = true;
                //         // hide lock
                //         b.transform.GetChild(2).gameObject.SetActive(false);
                //     }
                // }
            }
        }
    }

    public void gotoSettingClicked()
    {
        gm.play_SE("se_confirm", 1f);
        pannel_setting.SetActive(true);
    }
    public void closeSettingClicked()
    {
        gm.play_SE("se_cancel", 1f);
        pannel_setting.SetActive(false);
    }
    public void gotoAchivementClicked()
    {
        gm.play_SE("se_confirm", 1f);
        switch_page(MainGamePage.ACHIEVEMENT);
    }
    public void startGameClicked(int numOfLevel)
    {
        gm.play_SE("se_confirm", 1f);

        // check if it's forece to next week
        isForceToNextWeek  =false;
        
        int current_daysInweek = (gm.current_progress_day)%7;        // 1 = Mon, 2 = Tue, 3 = Wed, 4 = Thur, 5 = Fri, 6 = Sat, 0 = Sun

        int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();
        if (current_week == 1 || current_week == 5 || current_week == 9 || current_week == 13)
        {
            if (current_daysInweek == 1)
            {
                gm.playerData.current_difficulity = 5;
                User objToUpdate = new User();
                objToUpdate.id = gm.playerData.id;
                objToUpdate.current_difficulity = gm.playerData.current_difficulity;
                gm.do_update_difficulity(objToUpdate);
            }
        }

        // checking isForceToNextWeek
        if (current_week > 1 && current_daysInweek == 1)
        {
            // check last week scores
            List<float[]> list_scoreOfTheWeek = new List<float[]>();
            int startDay = (current_week - 2) * 7 + 1;
            for (int i=startDay; i<startDay+7; i++)
            {
                list_scoreOfTheWeek.Add(socres_groupByDay[i]);
            }
            if (list_scoreOfTheWeek.Count != 7)
            {
                isForceToNextWeek = true;
            }
            else
            {
                foreach(float[] socresOfDay in list_scoreOfTheWeek)
                {
                    if (socresOfDay.Length == 0)
                    {
                        isForceToNextWeek = true;
                        break;
                    }
                }
            }
        }

        // UnityEngine.Debug.Log("isForceToNextWeek: " + isForceToNextWeek);

        // set current day
        // show loading
        show_loading(true);
        // switch to STORY
        switch_page(MainGamePage.STORY);
        
        // load story slides
        array_storysImage = Resources.LoadAll<Sprite>("StoryImages/Day_" + (numOfLevel));
        if (isForceToNextWeek)
        {
            List<Sprite> list_story = new List<Sprite>();
            Sprite sp_forceNextWeek = Resources.Load<Sprite>("StoryImages/forcetoback");
            list_story.Add(sp_forceNextWeek);
            foreach(Sprite s in array_storysImage)
            {
                list_story.Add(s);
            }
            array_storysImage = list_story.ToArray();
        }
        
        // load story part page data
        // UnityEngine.Debug.Log("array_StoryData: " + array_StoryData.Length);
        current_storyData= array_StoryData[numOfLevel];

        story_part_count = new int[]{current_storyData.num_story1 + (isForceToNextWeek ? 1:0), current_storyData.num_story2, current_storyData.num_story3, current_storyData.num_story4};
        UnityEngine.Debug.Log("story part: " + current_storyData.num_story1 + " " + current_storyData.num_story2 + " " + current_storyData.num_story3 + " " + current_storyData.num_story4);

        // set story slide images
        current_storySlideIndex = -1;

        // check num of progress of the day
        socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(numOfLevel) : gm.group_score_by_day();
        // UnityEngine.Debug.Log("setting [current_story_part] socres_groupByDay.Length: " + socres_groupByDay.Length);
        int numOfProgress = 0;
        if (socres_groupByDay.Length != 0)
        {
            if (gm.isTestingMode)
            {
                numOfProgress = (socres_groupByDay[numOfLevel]).Length;
            }
            else
            {
                numOfProgress = (socres_groupByDay[gm.current_day]).Length;
            }
        }
        gm.current_story_part = numOfProgress;

        // set current_storySlideIndex
        if (gm.current_story_part != 0)
        {
            for (int i=0; i<gm.current_story_part; i++)
            {
                current_storySlideIndex += story_part_count[i];
            }
        }
        UnityEngine.Debug.Log("gm.current_story_part: " + gm.current_story_part);
        UnityEngine.Debug.Log("current_storySlideIndex: " + current_storySlideIndex);
        
        // set default first slide
        if (array_storysImage.Length != 0)
        {
            if (numOfProgress == 0)
            {
                UnityEngine.Debug.Log("checkShowTutorialContent called 2");
                checkShowTutorialContent();
            }
            else
            {
                tutorial_step = 99;
            }
            next_storySlide(true);
        }
        else
        {
            switch_page(MainGamePage.MAP);
        }
        // hide loading
        show_loading(false);
    }
    public void daysClicked(int weekDays)
    {
        gm.play_SE("se_confirm", 1f);
        selected_days = weekDays;

        int days = (7 * (selected_week - 1)) + selected_days;
        gm.current_day = days;

        // check play tutorial video
        if (!check_playTutorialVideo())
        {
            // start show story
            startGameClicked(gm.current_day);
        }
    }

    bool check_playTutorialVideo()
    {
        // UnityEngine.Debug.Log("check_playTutorialVideo called");
        bool res = false;
        
        // only play when it's Monday
        // UnityEngine.Debug.Log("[check_playTutorialVideo] selected_days: " + selected_days);

        // check num of progress of the day
        socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.current_day) : gm.group_score_by_day();
        // UnityEngine.Debug.Log("setting [current_story_part] socres_groupByDay.Length: " + socres_groupByDay.Length);
        int numOfProgress = 0;
        if (socres_groupByDay.Length != 0)
        {
            numOfProgress = (socres_groupByDay[gm.current_day]).Length;
        }

        if (selected_days == 1 && numOfProgress == 0)
        {
            // get current week
            int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();
            // UnityEngine.Debug.Log("[check_playTutorialVideo] current_week: " + selected_days);

            if (current_week == GameSetting.week_tutorial_Homophone && !tutorialPlayed_Homophone)
            {
                playTutorialVideo(GameSetting.path_tutorial_Homophone);
                tutorialPlayed_Homophone = true;

                res = true;
            }
            else if (current_week == GameSetting.week_tutorial_Homograph && !tutorialPlayed_Homograph)
            {
                playTutorialVideo(GameSetting.path_tutorial_Homograph);
                tutorialPlayed_Homograph = true;

                res = true;
            }
            else if (current_week == GameSetting.week_tutorial_Construction && !tutorialPlayed_Construction)
            {
                playTutorialVideo(GameSetting.path_tutorial_Construction);
                tutorialPlayed_Construction = true;
                res = true;
            }
            else if (current_week == GameSetting.week_tutorial_Combination && !tutorialPlayed_Combination)
            {
                playTutorialVideo(GameSetting.path_tutorial_Combination);
                tutorialPlayed_Combination = true;
                res = true;
            }
        }

        return res;
    }
    public void showDaysClicked(int numOfWeek)
    {
        gm.play_SE("se_confirm", 1f);
        selected_week = numOfWeek;
        setup_days_UI();
        panel_map_days.SetActive(true);
    }
    public void hideDaysClicked()
    {
        gm.play_SE("se_cancel", 1f);
        // panel_map_flag.SetActive(true);
        panel_map_days.SetActive(false);
    }
    public void readStoryClicked()
    {
        float f_story = PlayerPrefs.GetFloat("setting_story", 0.5f);
        audio_story.volume = f_story;

        gm.play_SE("se_confirm", 1f);
        if (!isplaying_Audio)
        {
            int days = (7 * (selected_week - 1)) + selected_days;

            // filename
            string filename = "";
            if (isForceToNextWeek)
            {
                if (current_storySlideIndex == 0)
                {
                    filename = "story_force";
                }
                else
                {
                    filename = (days).ToString("000") + "_story_" + (current_storySlideIndex).ToString("00");
                }
            }
            else
            {
                filename = (days).ToString("000") + "_story_" + (current_storySlideIndex + 1).ToString("00");
            }

            // UnityEngine.Debug.Log("readStoryClicked filename: " + filename);
            // get audio source
            AudioClip storyClip = Resources.Load<AudioClip>("Sounds/story/" + filename);
            // set source
            audio_story.clip = storyClip;
            //play story
            audio_story.Play();

            // set audio button animation
            btn_playAudio.GetComponent<Animator>().SetTrigger("play");
            isplaying_Audio = true;
        }
    }
    public void next_storySlide(bool getNewStartTime)
    {
        if (getNewStartTime)
        {
            // get start story time
            start_story_time = (double)GameManager.GetTimeStamp(false) / (double)1000;
        }
        int max_page_index = 0;
        int min_page_index = 0;
        
        // get max page index
        for (int i = 0; i <= gm.current_story_part; i++)
        {
            max_page_index += story_part_count[i];
        }

        // get min page index
        if (gm.current_story_part == 0)
        {
            min_page_index = 0;
        }
        else
        {
            for (int i=0; i<gm.current_story_part; i++)
            {
                min_page_index += story_part_count[i];
            }
        }

        // set sprite
        if (current_storySlideIndex < max_page_index - 1)
        {
            current_storySlideIndex ++;
            img_storySlide_current.sprite = array_storysImage[current_storySlideIndex];

            if (current_storySlideIndex != 0)
            {
                img_storySlide_pre.sprite = array_storysImage[current_storySlideIndex - 1];
            }

            if (current_storySlideIndex != array_storysImage.Length - 1)
            {
                img_storySlide_next.sprite = array_storysImage[current_storySlideIndex + 1];
            }
            audio_story.Stop();
            isplaying_Audio = false;
        }
        
        // show hide buttons
        // start game button
        if (gm.current_story_part == story_part_count.Length - 1)
        {
            btn_story_start.SetActive(false);
        }
        else
        {
            bool isShowStartGameBtn = current_storySlideIndex == max_page_index - 1;
            btn_story_start.SetActive(isShowStartGameBtn);
            if (isShowStartGameBtn)
            {
                // UnityEngine.Debug.Log("checkShowTutorialContent called 1");
                checkShowTutorialContent();
            }
        }
        // end story button
        if ((gm.current_story_part == story_part_count.Length - 1) && (current_storySlideIndex == max_page_index - 1))
        {
            btn_story_end.SetActive(true);
        }
        else
        {
            btn_story_end.SetActive(false);
        }
        // next page button
        btn_story_next.SetActive(current_storySlideIndex != max_page_index - 1);
        // previous page button
        if (current_storySlideIndex == min_page_index)
        {
            btn_story_previous.SetActive(false);
            if (isTutorial)
            {
                btn_story_quit.SetActive(false);
            }
            else
            {
                btn_story_quit.SetActive(true);
            }
        }
        else
        {
            btn_story_previous.SetActive(true);
            btn_story_quit.SetActive(false);
        }
        
        // play story
        readStoryClicked();
    }
    public void previous_storySlide()
    {
        int max_page_index = 0;
        int min_page_index = 0;

        // get max page index
        for (int i = 0; i <= gm.current_story_part; i++)
        {
            max_page_index += story_part_count[i];
        }

       // get min page index
        if (gm.current_story_part == 0)
        {
            min_page_index = 0;
        }
        else
        {
            for (int i=0; i<gm.current_story_part; i++)
            {
                min_page_index += story_part_count[i];
            }
        }

        // set sprite
        if (current_storySlideIndex > min_page_index)
        {
            current_storySlideIndex --;
            img_storySlide_current.sprite = array_storysImage[current_storySlideIndex];

            if (current_storySlideIndex != 0)
            {
                img_storySlide_pre.sprite = array_storysImage[current_storySlideIndex - 1];
            }

            if (current_storySlideIndex != array_storysImage.Length - 1)
            {
                img_storySlide_next.sprite = array_storysImage[current_storySlideIndex + 1];
            }
            audio_story.Stop();
            isplaying_Audio = false;
        }

        // show hide buttons
        // start game button
        if (gm.current_story_part == story_part_count.Length - 1)
        {
            btn_story_start.SetActive(false);
        }
        else
        {
            btn_story_start.SetActive(current_storySlideIndex == max_page_index - 1);
        }
        // end story button
        if ((gm.current_story_part == story_part_count.Length - 1) && (current_storySlideIndex == max_page_index - 1))
        {
            btn_story_end.SetActive(true);
        }
        else
        {
            btn_story_end.SetActive(false);
        }
        // next page button
        btn_story_next.SetActive(current_storySlideIndex != max_page_index - 1);
        // previous page button
        if (current_storySlideIndex == min_page_index)
        {
            btn_story_previous.SetActive(false);
            if (isTutorial)
            {
                btn_story_quit.SetActive(false);
            }
            else
            {
                btn_story_quit.SetActive(true);
            }
        }
        else
        {
            btn_story_previous.SetActive(true);
            btn_story_quit.SetActive(false);
        }

        // play story
        readStoryClicked();
    }
    public void checkAndShowAchievement()
    {
       // get new count
        int new_smallStickerCount = get_smallSticker_count();
        // check new small sticker
        if (new_smallStickerCount > numOfSmallSticker)
        {
            Sprite sp_sticker = get_lastSmallStickerImage();
            AlertController alert = gm.showAlert("獲得新貼紙", "", sp_sticker, "確定", "取消", false);
            alert.btn_confirm.onClick.AddListener(check_new_largeSticker);

            numOfSmallSticker = new_smallStickerCount;
        }
        // check new large stiker
        else
        {
            check_new_largeSticker();
        }
    }
    public void check_new_largeSticker()
    {
        int new_largeStickerCount = get_largeSticker_count();

        if (new_largeStickerCount > numOfLargeSticker)
        {
            AlertController alert = gm.showAlert("新的進度", "獲得新的郵票碎片", null, "確定", "取消", false);
            numOfLargeSticker = new_largeStickerCount;
        }
    }
    public Sprite get_lastSmallStickerImage()
    {
        int current_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();
        Sprite[] sps = Resources.LoadAll<Sprite>("StickerImages/small/Week_" + current_week);
        int index_sticker = scoreOfEachWeek[current_week-1] - 1;
        return sps[index_sticker];
    }
    public int get_smallSticker_count()
    {
        scoreOfEachWeek = new List<int>();
        int numOfSmallSticker = 0;
        float[][] socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();
        // RULES: counting scores in week, >=2, >=4, >=5
        int numOfDays = socres_groupByDay.Length;
        int max_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();

        for (int checking_week=0; checking_week<max_week; checking_week++)
        {
            // counting num of scores in the week
            int numOfCompletedDay = 0;
            int numOfStickerOfWeek = 0;
            for (int day=1; day<=7; day++)
            {
                int dayToGet = day + checking_week*7;
                if (socres_groupByDay.Length > dayToGet)
                {
                    if (socres_groupByDay[dayToGet].Length >= 3)        // must have 3 score in 1 day to be calculated as finished day
                    {
                        numOfCompletedDay++;
                    }
                }
                else
                {
                    break;
                }
            }

            // calculate num of stickers
            if (numOfCompletedDay >=2)  // first sticker
            {
                numOfStickerOfWeek = 1;
            }
            if (numOfCompletedDay >=4)  // second sticker
            {
                numOfStickerOfWeek = 2;
            }
            if (numOfCompletedDay >=5)  // third sticker
            {
                numOfStickerOfWeek = 3;
            }

            numOfSmallSticker += numOfStickerOfWeek;

            scoreOfEachWeek.Add(numOfStickerOfWeek);
        }
        return numOfSmallSticker;
    }
    public int get_largeSticker_count()
    {
        int numOfLargeSticker = 0;
        float[][] socres_groupByDay = (gm.isTestingMode) ? gm.group_score_by_day(gm.get_maxDayFromProgress()) : gm.group_score_by_day();
        // RULES: counting number of score above % in week, every 5 count + 33.3% of fill mount
        int numOfDays = socres_groupByDay.Length;
        int max_week = (gm.isTestingMode) ? gm.get_current_week_FromProgress() : gm.get_current_week();

        for (int checking_week=0; checking_week<max_week; checking_week++)
        {
            // counting num of star in the week
            int numOfStarInWeek = 0;
            for (int day=1; day<=7; day++)
            {
                int dayToGet = day + checking_week*7;
                if (socres_groupByDay.Length > dayToGet)
                {
                    float[] day_scores = socres_groupByDay[dayToGet];
                    foreach(float score_float in day_scores)
                    {
                        if (score_float >= GameSetting.minigame_getStarScore)
                        {
                            numOfStarInWeek++;
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            // calculate num of star
            if (numOfStarInWeek >= 5)
            {
                numOfLargeSticker++;
            }
            else if (numOfStarInWeek >= 10)
            {
                numOfLargeSticker++;
            }
            else if (numOfStarInWeek >= 15)
            {
                numOfLargeSticker++;
            }
        }

        return numOfLargeSticker;
    }
    void switch_page(MainGamePage page)
    {
        // hide all
        pannel_map.SetActive(false);
        pannel_miniGame.SetActive(false);
        pannel_story.SetActive(false);
        pannel_achievement.SetActive(false);

        switch (page)
        {
            case MainGamePage.MAP:
                pannel_map.SetActive(true);
                stopwatch.Stop();
                break;
            case MainGamePage.STORY:
                pannel_story.SetActive(true);
                stopwatch.Stop();
                break;
            case MainGamePage.MINIGAME:
                pannel_miniGame.SetActive(true);
                stopwatch.Start();
            break;
            case MainGamePage.ACHIEVEMENT:
                pannel_achievement.SetActive(true);
                stopwatch.Stop();
                break;
        }
        current_page = page;
    }

    public void gotoMapClicked()
    {
        audio_story.Stop();
        setup_map_UI();
        
        switch_page(MainGamePage.MAP);
    }

    /*Changed by Ivy*/
    public void endStoryToMap()
    {
        gotoMapClicked();
        // ************************************************************************
        // For Testing lock account only
        // ************************************************************************
        if (testing_needToLogout)
        {
            AlertController alert = new AlertController();
            /*andded by ivy*/
            if (isendtime)
            {
                UnityEngine.Debug.Log("強制退出Training");
                alert = gm.showAlert("提示", "你今天的遊戲時間已經太多啦。請明天再來吧。", null, "確定", "取消", false);
                alert.btn_confirm.onClick.AddListener(() => do_logout());
                testing_needToLogout = false;

            }
            else
            {
                UnityEngine.Debug.Log("強制退出Map");
                alert = gm.showAlert("提示", "你已經完成了本週的遊戲，請下週再來哦。", null, "確定", "取消", false);
                alert.btn_confirm.onClick.AddListener(() => do_logout());
                testing_needToLogout = false;
            }
                
        }
        // ************************************************************************
    }

  /*Added by Ivy*/
    public void endOneDayTraining() {
        // ************************************************************************
        // For Testing lock account only
        // ************************************************************************
        gotoMapClicked();
        if (testing_needToLogout)
        {
            
        }
    }

    public void gotoStoryClicked()
    {
        gm.play_SE("se_confirm", 1f);
        audio_story.Stop();
        switch_page(MainGamePage.STORY);
    }
    public void upload_storyTime()
    {
        // get end story time
        end_story_time = (double)GameManager.GetTimeStamp(false) / (double)1000;

        // create game time object
        GameTime obj = new GameTime{
            user_id = gm.playerData.id,
            day = gm.current_day,
            part = gm.current_story_part,
            time_type = MiniGameController.MiniGameTimeType.Story.ToString(),
            start_time = start_story_time,
            end_time = end_story_time
        };

        // upload game time to server
        gm.do_upload_gameTime(obj);
    }    
    public void gotoMiniGameClicked()
    {
        gm.play_SE("se_confirm", 1f);
        audio_story.Stop();
        // load minigame data
        // temp_name	question_type	selections_pos	move_direction	limited_time	num_questions
        // get the minigame data
        current_minigameData = get_currentMinigameData((gm.current_day), gm.current_story_part);
        if (current_minigameData != null)
        {
            gm.curret_temp_name = current_minigameData.temp_name;
            gm.curret_question_type = current_minigameData.question_type;

            MiniGameController.MiniGameSelectionsPosition selections_pos = current_minigameData.selections_pos;
            MiniGameController.MiniGameMoveDirection move_direction = current_minigameData.move_direction;
            int limited_time = current_minigameData.limited_time;
            int num_questions = current_minigameData.num_questions;

            MiniGameController miniGameController = pannel_miniGame.GetComponent<MiniGameController>();
            UnityEngine.Debug.Log(gm.curret_temp_name + " " + selections_pos + " " + gm.curret_question_type + " " + move_direction + " " + limited_time + " " + num_questions);
            miniGameController.load_minigame(gm.curret_temp_name, selections_pos, gm.curret_question_type, move_direction, limited_time, num_questions);

            switch_page(MainGamePage.MINIGAME);
            gm.play_BGM("bgm_minigame");
        }
        else
        {
           UnityEngine.Debug.Log(">>>>>>>>>>>>>>>>>>>>>>>>>>>ERROR: No correct data found<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            // check end or coutinue
            if (gm.current_story_part < story_part_count.Length)
            {
                // change to next page
                next_storySlide(true);

                // show story view
                gotoStoryClicked();

                // play story
                readStoryClicked();
            }
            else
            {
                UnityEngine.Debug.Log("No next part");
                endStoryToMap();       
                   
            }
        }
    }

    MinigameData get_currentMinigameData(int numOfDay, int numOfPart)
    {
        // UnityEngine.Debug.Log("array_MiniGameData: " + array_MiniGameData.Length);
        foreach(MinigameData minigameData in array_MiniGameData)
        {
            // UnityEngine.Debug.Log("minigameData.day: " + minigameData.day);
            // UnityEngine.Debug.Log("minigameData.minigame_stage: " + minigameData.minigame_stage);
            if (minigameData.day == (numOfDay) && minigameData.minigame_stage == (numOfPart+1))
            {
                return minigameData;
            }
        }
        return null;
    }
    
    void show_loading(bool isShow)
    {
        pannel_loading.SetActive(isShow);
    }

    // ************************************************************************************************
    // STORY DATA LOADING
    // ************************************************************************************************
    public void do_load_storyData()
    {
        csv_story = Resources.Load<TextAsset>("csv/story_data");
        StartCoroutine(load_storyData());
    }
    IEnumerator load_storyData()
    {
        string[,] data_items = CSVReader.SplitCsvGrid(csv_story.text);
        ProcessStoryData(data_items);
        yield return null;
        
    }

    void ProcessStoryData(string[,] data_items)
    {
        // id	day	num_story1	num_story2	num_story3	num_story4
        const string key_id = "id";
        const string key_day = "day";
        const string key_story_1 = "num_story1";
        const string key_story_2 = "num_story2";
        const string key_story_3 = "num_story3";
        const string key_story_4 = "num_story4";

        int index_id=0;
        int index_day=0;
        int index_story_1=0;
        int index_story_2=0;
        int index_story_3=0;
        int index_story_4=0;

        array_StoryData = new StoryData[data_items.GetLength(1) - 2];

        for (int i = 0; i<data_items.GetLength(0); i++){
            string theKey = data_items[i,0];
            switch (theKey)
            {
                case key_id:
                    index_id = i;
                break;
                case key_day:
                    index_day = i;
                break;
                case key_story_1:
                    index_story_1 = i;
                break;
                case key_story_2:
                    index_story_2 = i;
                break;
                case key_story_3:
                    index_story_3 = i;
                break;
                case key_story_4:
                    index_story_4 = i;
                break;

                default:
                break;
            }
        }

        for(int j = 1; j < data_items.GetLength(1) - 2; j++) {
            StoryData item = new StoryData();
            item.id = data_items[index_id, j];
            item.day = int.Parse(data_items[index_day, j]);
            item.num_story1 = int.Parse(data_items[index_story_1, j]);
            item.num_story2 = int.Parse(data_items[index_story_2, j]);
            item.num_story3 = int.Parse(data_items[index_story_3, j]);
            item.num_story4 = int.Parse(data_items[index_story_4, j]);

            array_StoryData[j - 1] = item;
        }

        didLoadStoryData();
    }
    // ************************************************************************************************

    // ************************************************************************************************
    // MINIGAME DATA LOADING
    // ************************************************************************************************
    public void do_load_minigameData()
    {
        csv_minigame = Resources.Load<TextAsset>("csv/minigame_data");
        StartCoroutine(load_minigameData());
    }
    IEnumerator load_minigameData()
    {
        string[,] data_items = CSVReader.SplitCsvGrid(csv_minigame.text);
        ProcessMinigameData(data_items);
        yield return null;
    }

    void ProcessMinigameData(string[,] data_items)
    {
        // id	day	minigame_stage	temp_name	question_type	selections_pos	move_direction	limited_time	num_questions
        const string key_id = "id";
        const string key_day = "day";
        const string key_minigame_stage = "minigame_stage";
        const string key_temp_name = "temp_name";
        const string key_question_type = "question_type";
        const string key_selections_pos = "selections_pos";
        const string key_move_direction = "move_direction";
        const string key_limited_time = "limited_time";
        const string key_num_questions = "num_questions";

        int index_id=0;
        int index_day=0;
        int index_minigame_stage=0;
        int index_temp_name=0;
        int index_question_type=0;
        int index_selections_pos=0;
        int index_move_direction=0;
        int index_limited_time=0;
        int index_num_questions=0;

        array_MiniGameData = new MinigameData[data_items.GetLength(1) - 2];

        for (int i = 0; i<data_items.GetLength(0); i++){
            string theKey = data_items[i,0];
            switch (theKey)
            {
                case key_id:
                    index_id = i;
                break;
                case key_day:
                    index_day = i;
                break;
                case key_minigame_stage:
                    index_minigame_stage = i;
                break;
                case key_temp_name:
                    index_temp_name = i;
                break;
                case key_question_type:
                    index_question_type = i;
                break;
                case key_selections_pos:
                    index_selections_pos = i;
                break;
                case key_move_direction:
                    index_move_direction = i;
                break;
                case key_limited_time:
                    index_limited_time = i;
                break;
                case key_num_questions:
                    index_num_questions = i;
                break;

                default:
                break;
            }
        }

        for(int j = 1; j < data_items.GetLength(1) - 2; j++) {
            MinigameData item = new MinigameData();
            item.id = data_items[index_id, j];
            item.day = int.Parse(data_items[index_day, j]);
            item.minigame_stage = int.Parse(data_items[index_minigame_stage, j]);
            
            string temp_name = data_items[index_temp_name, j];
            foreach (MiniGameController.MiniGameType type in System.Enum.GetValues(typeof(MiniGameController.MiniGameType)))
            {
                if (type.ToString() == temp_name)
                {
                    item.temp_name = type;
                    break;
                }
            }
            string question_type = data_items[index_question_type, j];
            foreach (MiniGameController.MiniGameQuestionType type in System.Enum.GetValues(typeof(MiniGameController.MiniGameQuestionType)))
            {
                if (type.ToString() == question_type)
                {
                    item.question_type = type;
                    break;
                }
            }
            string selections_pos = data_items[index_selections_pos, j];
            foreach (MiniGameController.MiniGameSelectionsPosition type in System.Enum.GetValues(typeof(MiniGameController.MiniGameSelectionsPosition)))
            {
                if (type.ToString() == selections_pos)
                {
                    item.selections_pos = type;
                    break;
                }
            }
            string move_direction = data_items[index_move_direction, j];
            foreach (MiniGameController.MiniGameMoveDirection type in System.Enum.GetValues(typeof(MiniGameController.MiniGameMoveDirection)))
            {
                if (type.ToString() == move_direction)
                {
                    item.move_direction = type;
                    break;
                }
            }

            item.limited_time = int.Parse(data_items[index_limited_time, j]);
            item.num_questions = int.Parse(data_items[index_num_questions, j]);

            array_MiniGameData[j - 1] = item;
        }

        didLoadMiniGameData();
    }
    // ************************************************************************************************
}
