﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Models;
using Proyecto26;
using System;

public class LoginController : MonoBehaviour
{
    public Button btn_login;
    public Button btn_register;
    public GameObject win_register;
    public InputField tf_username;
    public InputField tf_pw;
    GameManager gm;
    private readonly string basePath = APISetting.basePath + "/api/user";
	private RequestHelper currentRequest;
    void Awake()
    {
        gm = GameObject.Find("GM").GetComponent<GameManager>();
    }
    void Start()
    {
        // enable buttons
        setButtonsInteractable(true);

        string last_username = PlayerPrefs.GetString("login_username", "");
        string last_password = PlayerPrefs.GetString("login_password", "");
        if (last_username != "" && last_password != "")
        {
            tf_username.text = last_username;
            tf_pw.text = last_password;
            do_login();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void loginClicked()
    {
        gm.play_SE("se_confirm", 1f);
        
        if (gm.check_networkStatus())
        {
            do_login();
        }
    }
    void do_login()
    {
        // disable buttons
        setButtonsInteractable(false);

        // show loading
        gm.show_loading(true);

        // check input
        if (checkInput())
        {
            // call API
            User newUser = new User {
                    username = tf_username.text.Trim(),
                    password = tf_pw.text.Trim()};
            // UnityEngine.Debug.Log("newUser: " + newUser.ToString());
            // UnityEngine.Debug.Log("obj: " + newUser.ToString());
            // UnityEngine.Debug.Log("url: " + basePath+"/login.php");
            currentRequest = new RequestHelper {
                Uri = basePath + "/login.php",
                Body = newUser
            };
            
            
            RestClient.Post<User>(currentRequest)
            .Then(res => {
                // EditorUtility.DisplayDialog ("Success", JsonUtility.ToJson(res, true), "Ok");
                UnityEngine.Debug.Log("Login Success: " + JsonUtility.ToJson(res, true));

                if (res.isLock)
                {
                    // show login fail->完成一周的情況
                    if (res.progress[res.progress.Length - 1].part == 3 && res.progress[res.progress.Length - 1].day % 7 == 0 && res.progress[res.progress.Length - 1].day !=0)
                    {
                        AlertController alert = gm.showAlert("帳號鎖定", "你已經完成了本週的遊戲，請下週再來哦。", null, "確定", "取消", false);
                    }
                    else {
                        //或許最後一條遊玩記錄，如果是昨天的，那麼就可以自動解鎖，後面再做-ivy
                        AlertController alert = gm.showAlert("帳號鎖定", "你今天的遊戲時間已經太多啦。請明天再來吧。", null, "確定", "取消", false);
                    }
                    
                }
                else
                {
                    // complete user data object
                    // ----------------------------------
                    // set player data
                    gm.playerData = res;

                    // calculate current progress day
                    int diff_days = GameManager.compare_dayDiffToNow(gm.playerData.start_time);

                    gm.current_progress_day = diff_days+ 1;
                    // ----------------------------------
                    // go to main
                    gm.change_scene("1_Main");

                    // save for next auto login
                    PlayerPrefs.SetString("login_username", newUser.username);
                    PlayerPrefs.SetString("login_password", newUser.password);
                }
                // hide loading
                gm.show_loading(false);
                // disable buttons
                setButtonsInteractable(true);
            })
            .Catch(err => 
            {
                UnityEngine.Debug.Log("Login Fail: " + err.Message);
                // show login fail
                AlertController alert =  gm.showAlert("登入失敗", "用戶名或密碼錯誤", null, "確定", "取消", false);

                // clean saved login info
                PlayerPrefs.SetString("login_username", "");
                PlayerPrefs.SetString("login_password", "");

                // enable buttons
                setButtonsInteractable(true);

                // hide loading
                gm.show_loading(false);
            });
        }
        else
        {
            // enable buttons
            setButtonsInteractable(true);

            // hide loading
            gm.show_loading(false);
        }
    }

    void setButtonsInteractable(bool isInteractable)
    {
        btn_login.interactable = isInteractable;
        btn_register.interactable = isInteractable;

        tf_username.enabled = isInteractable;
        tf_pw.enabled = isInteractable;
    }
    bool checkInput()
    {
        if (tf_username.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "用戶名不能為空白", null, "確定", "取消", false);
            return false;
        }
        else if (tf_pw.text == "")
        {
            AlertController alert =  gm.showAlert("錯誤", "密碼不能為空白", null, "確定", "取消", false);
            return false;
        }
        else
        {
            return true;
        }
    }
    public void registerClicked()
    {
        this.gameObject.SetActive(false);
        win_register.SetActive(true);
        gm.play_SE("se_confirm", 1f);
    }
}
