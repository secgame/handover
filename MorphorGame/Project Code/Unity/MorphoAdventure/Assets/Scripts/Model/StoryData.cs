﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryData
{
    // id   day	num_story1	num_story2	num_story3	num_story4
    public string id;
    public int day;
    public int num_story1;
    public int num_story2;
    public int num_story3;
    public int num_story4;

    public StoryData()
    {
        id = "";
        day = 0;
        num_story1 = 0;
        num_story2 = 0;
        num_story3 = 0;
        num_story4 = 0;
    }
}
