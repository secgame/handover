﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameData
{
    // id	day	minigame_stage	temp_name	question_type	selections_pos	move_direction	limited_time	num_questions
    public string id;
    public int day;
    public int minigame_stage;
    public MiniGameController.MiniGameType temp_name;
    public MiniGameController.MiniGameQuestionType question_type;
    public MiniGameController.MiniGameSelectionsPosition selections_pos;
    public MiniGameController.MiniGameMoveDirection move_direction;
    public int limited_time;
    public int num_questions;

    public MinigameData()
    {
        id = "";
        day = 0;
        minigame_stage = 0;
        temp_name = MiniGameController.MiniGameType.none;
        question_type = MiniGameController.MiniGameQuestionType.none;
        selections_pos = MiniGameController.MiniGameSelectionsPosition.none;
        move_direction = MiniGameController.MiniGameMoveDirection.none;
        limited_time = 0;
        num_questions = 0;
    }
}
