using System;

namespace Models
{
	[Serializable]
	public class GameAnswer
	{
		public int id;
		public int user_id;
		public int day;
		public int game_section;	// 1-3
		public string game_temp;	// name of game temp
        public string question_type;	// Find same, find different, judge
		public double answer_start_time;
		public double answer_end_time;
		public string question;
		public string answer;
		public double submit_date;


		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}
}

