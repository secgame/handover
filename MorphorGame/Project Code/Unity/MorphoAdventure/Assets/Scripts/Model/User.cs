﻿using System;

namespace Models
{
	[Serializable]
	public class User
	{
		public int id;
		public string username;
		public string email;
		public int age;
		public string gender;
        public string password;
		public long register_date;
		public long start_time;
		public int current_difficulity;
		public GameProgress[] progress;
		public bool isLock;


		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}
}

