using System;

namespace Models
{
	[Serializable]
	public class GameProgress
	{
		public int id;
		public int user_id;
		public int day;
		public int part;
		public int score;
		public int score_max;
		public double start_time;
		public double end_time;
		public long submit_date;
        public int islock;


		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}
}

