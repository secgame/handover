using System;

namespace Models
{
	[Serializable]
	public class GameTime
	{
		public int id;
		public int user_id;
		public int day;
		public int part;	// Story: 1-4, Game: 1-3
		public string time_type;
        public double start_time;
		public double end_time;
		public double submit_date;


		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}
}

