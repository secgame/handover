﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class EmailManager : MonoBehaviour {
    public string subject;
    public string body;

    private static EmailManager instance;
    
    public static EmailManager Instance {
        get;
        set;
    }

    void Update() {
        Instance = this;
    }

    /// <summary>
    /// 發送郵件
    /// </summary>
    /// <param name="toWhom"></param>
    public void SendEmail(string toWhom)
    {
        try
        {
            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            mail.From = new MailAddress("zhengkh0310@gmail.com");
            mail.To.Add("isihuang@eduhk.hk");
            mail.Subject = subject;
            mail.Body = body;
            //mail.To.Add("1193742365@qq.com");
            //mail.Subject = "Test Mail";
            //mail.Body = "This is for testing SMTP mail from GMAIL";

            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("zhengkh0310@gmail.com", "JohnnyHo1415") as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };

            smtpServer.Send(mail);
            Debug.Log("success");
        }
        catch (Exception err)
        {
            Debug.Log(err);
        }
    }
}
