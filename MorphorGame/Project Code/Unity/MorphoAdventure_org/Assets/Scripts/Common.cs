﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class Common {

    /// <summary>
    /// 判断邮件格式
    /// </summary>
    public static bool IsEmail(string inputEmail)
    {
        string strRegex = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$";
        Regex re = new Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return true;
        else
            return false;
    }

    /// <summary>
    /// 獲取字符串中的數字
    /// </summary>
    /// <param name="rawStr">待處理的字符串</param>
    /// <returns></returns>
    public static string GetNumber(string rawStr) {
        Regex r = new Regex("\\d+\\.?\\d*");
        bool ismatch = r.IsMatch(rawStr);
        MatchCollection mc = r.Matches(rawStr);

        string result = string.Empty;
        for (int i = 0; i < mc.Count; i++)
        {
            result += mc[i];//匹配结果是完整的数字，此处可以不做拼接的
        }

        return result;      
    }

    /// 获取时间戳
    /// </summary>
    /// <returns></returns>
    public static string GetTimeStamp(System.DateTime time)
    {
        long ts = ConvertDateTimeToInt(time);
        return ts.ToString();
    }

    /// <summary>  
    /// 将c# DateTime时间格式转换为Unix时间戳格式  
    /// </summary>  
    /// <param name="time">时间</param>  
    /// <returns>long</returns>  
    public static long ConvertDateTimeToInt(System.DateTime time)
    {
        System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
        long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
        return t;
    }

    /// <summary>
    /// 獲取指定關所包含的遊戲的id
    /// </summary>
    /// <param name="stage"></param>
    /// <returns></returns>
    public static ArrayList getGameIds(int stage)
    {
        //先找到三個遊戲的id
        ArrayList gameIdList = new ArrayList();
        ArrayList GameArrayList = UIAnimation._instance.GameArray;

        foreach (ArrayList gameInfo in GameArrayList)
        {
            gameIdList.Add(int.Parse(gameIdList[0].ToString()));
        }

        return gameIdList;

    }

}
