﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using UnityEngine;

public class QuestionManager : MonoBehaviour {

    int answerTime = 10*1000;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /*
     難度調整方針：
        player有“高興程度”
        * 初始值設定
            & 初始值為50
            & 根據前三天的遊玩情況進行初始值左右調整
        * 提高玩家的“高興程度”
            & 用實物題代替非實物
            & 詞語透明度高
            & 詞語相似度高
            & 干擾項少
        * 降低玩家的“高興程度”
            & 隨著答題時間遞減-> 10秒為答題時間，↑-1，↓+1
            & 隨著答題錯誤降低->第一次答對+2，第二次答對+0，第三次答對 -2，第四次答對-4...
        難度分級：
        
    */

    public void CreateQuestionCollection(string game_id, string user_id) {
        //產生一個play_id
        int play_id = DataManager.Instance.InsertPlayRecord(int.Parse( game_id), user_id);
        float happyVal = 60;
        float difficultyVal = 1;
        //判斷這遊戲是重玩還是第一次玩
        bool replay = false;
        DataTable tmpDT = GameManager.Instance.player.PlayRecord.Clone();
        foreach (DataRow row in GameManager.Instance.player.PlayRecord.Rows) {
            if (row["game_id"].ToString() == game_id && row["user_id"].ToString() == user_id) {
                tmpDT.ImportRow(row);
            }
        }
        if (tmpDT.Rows.Count > 0)
        {
            replay = true;
        }
        else {
            foreach (DataRow row in GameManager.Instance.player.PlayRecord.Rows) {
                tmpDT.Clear();
                //獲取上一個遊戲的遊玩結果
                if (int.Parse(game_id) > 1)
                {
                    if (row["game_id"].ToString() == (int.Parse(game_id) - 1).ToString() && row["user_id"].ToString() == user_id)
                    {
                        tmpDT.ImportRow(row);
                    }
                }
                
            }
        }

        //計算上次遊戲的高興值,遊戲的平均難度
        if (tmpDT.Rows.Count > 0)
        {
            happyVal = CalReacordVal(tmpDT)[0];
            difficultyVal = CalReacordVal(tmpDT)[1];
         }


        //80分代表可以晉級
        if (happyVal >= 80)
        {
            difficultyVal += 2;
        }//40分表示要降低難度
        else if (happyVal < 40) {
            difficultyVal -= 2;
        }
        //60分表示保持該狀態
        foreach (DataRow dt in GameManager.Instance.gameDT.Rows) {
            
        }
        //獲取遊戲的類型

        //按照類型生成題目(選出不多於20條作為題庫，實際題目只出現12道)
        //(1) 匹配
        //(2) 找不同
        //(3) 找相同
        //(4) 判斷

    }


    /// <summary>
    /// 獲取高興指數和難度指數
    /// </summary>
    public float[] CalReacordVal(DataTable dt) {

        float happyVal = 60;
        float difficultyVal = 1;

        foreach (DataRow row in dt.Rows)
        {
            //獲取高興值
            string fulltime = row["answer_time"].ToString();
            string Date = Regex.Split(fulltime, " ", RegexOptions.IgnoreCase)[0];
            string Time = Regex.Split(fulltime, " ", RegexOptions.IgnoreCase)[1];
            string Year = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[0];
            string Month = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[1];
            string Day = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[2];
            string Hour = Regex.Split(Time, ":", RegexOptions.IgnoreCase)[0];
            string Minute = Regex.Split(Date, ":", RegexOptions.IgnoreCase)[1];
            string Second = Regex.Split(Date, ":", RegexOptions.IgnoreCase)[2];

            DateTime dateStart = new DateTime(int.Parse(Year), int.Parse(Month), int.Parse(Day), int.Parse(Hour), int.Parse(Minute), int.Parse(Second));

            int timestamp_answer = GetTimeStamp(dateStart);

            fulltime = row["answer_time"].ToString();
            Date = Regex.Split(fulltime, " ", RegexOptions.IgnoreCase)[0];
            Time = Regex.Split(fulltime, " ", RegexOptions.IgnoreCase)[1];
            Year = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[0];
            Month = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[1];
            Day = Regex.Split(Date, "-", RegexOptions.IgnoreCase)[2];
            Hour = Regex.Split(Time, ":", RegexOptions.IgnoreCase)[0];
            Minute = Regex.Split(Date, ":", RegexOptions.IgnoreCase)[1];
            Second = Regex.Split(Date, ":", RegexOptions.IgnoreCase)[2];

            dateStart = new DateTime(int.Parse(Year), int.Parse(Month), int.Parse(Day), int.Parse(Hour), int.Parse(Minute), int.Parse(Second));

            int timestamp_show = GetTimeStamp(dateStart);

            int timeDelay = (timestamp_answer - timestamp_show - answerTime)/1000;

            int errorCount = Regex.Split(row["answer_info"].ToString(), ",", RegexOptions.IgnoreCase).Length-1;

            happyVal = 60 - timeDelay - errorCount * 2;


            //獲取難度係數
            difficultyVal += float.Parse(row["answer_time"].ToString());

        }
        difficultyVal = difficultyVal / dt.Rows.Count;


        return new float[2] { happyVal, difficultyVal};
    }


    /// <summary>
    /// 獲取時間戳
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    private int GetTimeStamp(DateTime dt)
    {
        DateTime dateStart = new DateTime(1970, 1, 1, 8, 0, 0);
        int timeStamp = Convert.ToInt32((dt - dateStart).TotalSeconds);
        return timeStamp;
    }  

    
}
