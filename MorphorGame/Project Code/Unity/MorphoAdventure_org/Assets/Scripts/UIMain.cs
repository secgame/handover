﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMain : UIBase
{
    public override string Name
    {
        get
        {
            return "UIMain";
        }
    }

    /// <summary>
    /// 重寫進入主界面事件
    /// 顯示主界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        gameObject.SetActive(true);

    }

    /// <summary>
    /// 重寫暫停主界面事件
    /// 隱藏主界面
    /// </summary>
    public override void DoOnPausing()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 重寫恢復主界面事件
    /// 顯示主界面嗎
    /// </summary>
    public override void DoOnResuming()
    {
        gameObject.SetActive(true);
        
    }

    /// <summary>
    /// 進入動畫界面
    /// </summary>
    public void GoToGame() {
        UIManager.Instance.PopUIPanel();
        UIManager.Instance.PushUIPanel("UIAnimation");
    }

    /// <summary>
    /// 進入成就榜
    /// </summary>
    public void GoToAchievement() {
       
        UIManager.Instance.PushUIPanel("UIAchievement");
    }
}
