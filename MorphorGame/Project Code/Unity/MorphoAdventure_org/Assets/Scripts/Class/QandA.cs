﻿using System.Collections;
using System.Collections.Generic;

public class QandA {

    private int difficulty; //題目的困難值
    private string aStr; //答案的字符串
    private string[] qArr; //題目的詞語數組

    public int Difficulty {
        get;
        set;
    }

    public string AStr {
        get;
        set;
    }

    public string[] QArr {
        get;
        set;
    }

}
