﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class User {
    private string name;
    private int age;
    private string gender;
    private string pwd;
    private int id;
    private DataTable playRecord;
    private float happyVal;
    private string email;

    public string Email {
        get;
        set;
    }

    public float HappyVel
    {
        get;
        set;
    }

    public string Name {
        get;
        set;
    }

    public int Age {
        get;
        set;
    }

    public string Gender {
        get;
        set;
    }

    public string Pwd {
        get;
        set;
    }

    public int Id {
        get;
        set;
    }

    public DataTable PlayRecord
    {
        get;
        set;
    }


}
