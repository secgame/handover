﻿using System;

public class PlayEachRecord
{

    private int play_id;
    private string question_info;
    private string choice_info;
    private int question_difficulty;
    private string answer_info;
    private string answer_time;
    private string show_time;



    public PlayEachRecord()
    {
    }

    public int Play_id
    {
        get;
        set;
    }

    public string Question_info
    {
        get;
        set;
    }

    public string Choice_info
    {
        get;
        set;
    }

    public int Question_difficulty
    {
        get;
        set;
    }

    public string Answer_info
    {
        get;
        set;
    }

    public string Answer_time
    {
        get;
        set;
    }

    public string Show_time
    {
        get;
        set;
    }
}

