﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using UnityEngine;

public class PlayDataSaveController : MonoBehaviour {

    DataTable willSubmit = new DataTable();
	// Use this for initialization
	void Start () {
        willSubmit.Columns.Add("play_id");
        willSubmit.Columns.Add("question_info");
        willSubmit.Columns.Add("choice_info");
        willSubmit.Columns.Add("question_difficulty");
        willSubmit.Columns.Add("answer_info");
        willSubmit.Columns.Add("answer_time");
        willSubmit.Columns.Add("show_time");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// 提出增加遊戲記錄的請求
    /// </summary>
    /// <returns></returns>
    public int AddPlayRecord(int game_id) {
        int play_id = -1;
        play_id =  DataManager.Instance.InsertPlayRecord(game_id, GameManager.Instance.player.Id.ToString());
        return play_id;
    }

    /// <summary>
    /// 暫存當前記錄到表格
    /// </summary>
    /// <param name="per"></param>
    public void AddRecord2Table(PlayEachRecord per) {
        willSubmit.Rows.Add(per.Play_id, per.Question_info, per.Choice_info, per.Question_difficulty, per.Answer_info, per.Answer_time, per.Show_time);
        long time = long.Parse(per.Answer_time) - long.Parse(per.Show_time);
        EmailManager.Instance.body += "題目:" + per.Question_info + " 選項:" + per.Choice_info + " 答題情況:" + per.Answer_info + " 答題時間:" + time.ToString() + "毫秒"+ "<br />";
    }

    /// <summary>
    /// 提出把暫存的數據表存到數據庫的申請
    /// </summary>
    public void AddRecord2DB() {
        foreach (DataRow dr in willSubmit.Rows) {
            //保存問題
            int question_id = -1;
            PlayEachRecord per = new PlayEachRecord();
            per.Play_id = int.Parse(dr["play_id"].ToString());
            per.Question_info = dr["question_info"].ToString();
            per.Choice_info = dr["choice_info"].ToString();
            per.Question_difficulty = int.Parse(dr["question_difficulty"].ToString());
            per.Answer_info = dr["answer_info"].ToString();
            per.Answer_time = dr["answer_time"].ToString();
            per.Show_time = dr["show_time"].ToString();
            question_id =  DataManager.Instance.Insert_PlayEachRecordQ(per);
            //保存答案
            while (question_id == -1) {
                Debug.Log("還未保存好問題");
            }

            DataManager.Instance.Insert_PlayEachRecordA(per, question_id);

        }

        ClearDT(willSubmit);
    }

    /// <summary>
    /// 清除表格
    /// </summary>
    /// <param name="dt"></param>
    public void ClearDT(DataTable dt) {
        dt.Clear();
    }

}

