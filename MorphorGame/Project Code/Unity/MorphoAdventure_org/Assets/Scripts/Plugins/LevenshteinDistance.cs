﻿using System;


public class LevenshteinDistance
{
    #region 私有變量
    /// <summary>
    /// 字符串1
    /// </summary>
    private char[] _ArrChar1;
    /// <summary>
    /// 字符串2
    /// </summary>
    private char[] _ArrChar2;
    /// <summary>
    /// 統計結果
    /// </summary>
    private Result _Result;
    /// <summary>
    /// 開始時間
    /// </summary>
    private DateTime _BeginTime;
    /// <summary>
    /// 結束時間
    /// </summary>
    private DateTime _EndTime;
    /// <summary>
    /// 計算次數
    /// </summary>
    private int _ComputeTimes;
    /// <summary>
    /// 算法矩陣
    /// </summary>
    private int[,] _Matrix;
    /// <summary>
    /// 矩陣列數
    /// </summary>
    private int _Column;
    /// <summary>
    /// 矩陣行數
    /// </summary>
    private int _Row;
    #endregion
    #region 屬性
    public Result ComputeResult
    {
        get { return _Result; }
    }
    #endregion
    #region 構造函數
    public LevenshteinDistance(string str1, string str2)
    {
        this.LevenshteinDistanceInit(str1, str2);
    }
    public LevenshteinDistance()
    {
    }
    #endregion
    #region 算法實現
    /// <summary>
    /// 計算兩個字符串的相似程度
    /// </summary>
    /// <param name="str1">字符串1</param>
    /// <param name="str2">字符串2</param>
    private void LevenshteinDistanceInit(string str1, string str2)
    {
        _ArrChar1 = str1.ToCharArray();
        _ArrChar2 = str2.ToCharArray();
        _Result = new Result();
        _ComputeTimes = 0;
        _Row = _ArrChar1.Length + 1;
        _Column = _ArrChar2.Length + 1;
        _Matrix = new int[_Row, _Column];
    }
    /// <summary>
    /// 計算相似度
    /// </summary>
    public void Compute()
    {
        //開始時間
        _BeginTime = DateTime.Now;
        //初始化矩陣的第一行和第一列
        this.InitMatrix();
        int intCost = 0;
        for (int i = 1; i < _Row; i++)
        {
            for (int j = 1; j < _Column; j++)
            {
                if (_ArrChar1[i - 1] == _ArrChar2[j - 1])
                {
                    intCost = 0;
                }
                else
                {
                    intCost = 1;
                }
                //關鍵步驟，計算當前位置值爲左邊+1、上面+1、左上角+intCost中的最小值 
                //循環遍歷到最後_Matrix[_Row - 1, _Column - 1]即爲兩個字符串的距離
                _Matrix[i, j] = this.Minimum(_Matrix[i - 1, j] + 1, _Matrix[i, j - 1] + 1, _Matrix[i - 1, j - 1] + intCost);
                _ComputeTimes++;
            }
        }
        //結束時間
        _EndTime = DateTime.Now;
        //相似率 移動次數小於最長的字符串長度的20%算同一題
        int intLength = _Row > _Column ? _Row : _Column;
        _Result.Rate = (1 - (double)_Matrix[_Row - 1, _Column - 1] / intLength).ToString();
        if (_Result.Rate.Length > 6)
        {
            _Result.Rate = _Result.Rate.Substring(0, 6);
        }
        _Result.UseTime = (_EndTime - _BeginTime).ToString();
        _Result.ComputeTimes = _ComputeTimes.ToString() + " 距離爲：" + _Matrix[_Row - 1, _Column - 1].ToString();
    }
    /// <summary>
    /// 計算相似度
    /// </summary>
    /// <param name="str1">字符串1</param>
    /// <param name="str2">字符串2</param>
    public void Compute(string str1, string str2)
    {
        this.LevenshteinDistanceInit(str1, str2);
        this.Compute();
    }
    /// <summary>
    /// 初始化矩陣的第一行和第一列
    /// </summary>
    private void InitMatrix()
    {
        for (int i = 0; i < _Column; i++)
        {
            _Matrix[0, i] = i;
        }
        for (int i = 0; i < _Row; i++)
        {
            _Matrix[i, 0] = i;
        }
    }
    /// <summary>
    /// 取三個數中的最小值
    /// </summary>
    /// <param name="First"></param>
    /// <param name="Second"></param>
    /// <param name="Third"></param>
    /// <returns></returns>
    private int Minimum(int First, int Second, int Third)
    {
        int intMin = First;
        if (Second < intMin)
        {
            intMin = Second;
        }
        if (Third < intMin)
        {
            intMin = Third;
        }
        return intMin;
    }
    #endregion
}
/// <summary>
/// 計算結果
/// </summary>
public struct Result
{
    /// <summary>
    /// 相似度
    /// </summary>
    public string Rate;
    /// <summary>
    /// 對比次數
    /// </summary>
    public string ComputeTimes;
    /// <summary>
    /// 使用時間
    /// </summary>
    public string UseTime;
}
