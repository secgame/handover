﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGame : UIBase {



    public override string Name
    {
        get
        {
            return "UIGame";
        }
    }

    //進入新的關卡要load新的數據
    void Awake() {

    }

    /// <summary>
    /// 重寫遊戲界面進入事件
    /// 顯示遊戲界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;

        //顯示
        gameObject.SetActive(true);
    }

    /// <summary>
    /// 重寫遊戲界面暫停事件
    /// 隱藏遊戲界面
    /// </summary>
    public override void DoOnPausing()
    {

        gameObject.SetActive(false);

    }

    /// <summary>
    /// 重寫遊戲界面回復事件
    /// 恢復遊戲界面
    /// </summary>
    public override void DoOnResuming()
    {
        gameObject.SetActive(true);
    }

    public override void DoOnExiting()
    {
        UIManager.Instance.PopUIPanel();
    }

    /// <summary>
    /// 進入動畫界面
    /// </summary>
    public void GoToGame()
    {
        UIManager.Instance.PushUIPanel("UIAnimation");
    }

}
