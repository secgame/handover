﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class DifficultyController : MonoBehaviour {

    float difficulty = 0; //題選對的難度，加權
    float distration = 3; //選項
    bool hasPicture = false; //選項是不是都是圖片

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// 開放接口做靜態訪問
    /// </summary>
    private static DifficultyController instance;
    public static DifficultyController Instance
    {
        get;
        set;
    }

    /// <summary>
    /// 根據需要調整的開心值調整難度
    /// </summary>
    /// <param name="adjustVal">開心值的調整</param>
    public void AdjustDifficultyByHappyVal(float adjustVal) {
        //5分為一個難度的提升
        float leveChange = adjustVal / 5;
        difficulty = difficulty - leveChange;
    }

    /// <summary>
    /// 修改問題的干擾項 (即修改問題選項)
    /// </summary>
    /// <param name="change"></param>
    public void changeDistraction(int change) {
        distration += change;
    }

    /// <summary>
    /// 修改是否選擇有圖片的詞語作為題目
    /// </summary>
    /// <param name="change"></param>
    public void changeHasPicture(bool change) {
        hasPicture = change;
    }

    /// <summary>
    /// 根據當前選定關卡計算上一次遊戲的難度
    /// </summary>
    /// <returns></returns>
    public float CalLastDifficulty()
    {
        ArrayList playList = new ArrayList();
        DataTable playRecord = GameManager.Instance.player.PlayRecord;
        ArrayList gameIds =Common.getGameIds(GameManager.Instance.stage);
        difficulty = 1;

        if (gameIds.Count == 0)
        {
            if (GameManager.Instance.stage > 1)
            {
                gameIds.Clear();
                gameIds =Common.getGameIds(GameManager.Instance.stage - 1);
            }
            else
            {
                return difficulty;
            }

        }

        foreach (int i in gameIds)
        {
            int minId = 9999;
            float thisDifficulty = -1;
            foreach (DataRow row in playRecord.Rows)
            {
                if (int.Parse(row["play_id"].ToString()) < minId)
                {
                    minId = int.Parse(row["play_id"].ToString());
                    thisDifficulty = float.Parse(row["play_difficulty"].ToString());
                }
            }
            playList.Add(new ArrayList() { minId, thisDifficulty });
        }


        //計算開心值
        float current = 0;
        foreach (ArrayList ar in playList)
        {
            current += float.Parse(ar[1].ToString());
        }


        return current / playList.Count;
    }



}
