﻿using UnityEngine;
using UnityEngine.UI;

public class UIRegister : UIBase
{
    GameObject txb_Name; //姓名框
    GameObject alert_Name; //姓名提示條
    GameObject chb_Gender;//性別
    GameObject txb_Age; //年齡框
    GameObject alert_Age; //年齡框提示條
    GameObject txb_Email; //郵件
    GameObject alert_Email; //郵件提示框
    GameObject txb_Pwd1; //密碼框
    GameObject alert_Pwd;  //密碼提示條
    GameObject txb_Pwd2; //密碼框
    GameObject btn_Login; //登錄按鈕
    GameObject btn_Close; //關閉按鈕

    GameObject toggle_Male; //單選框-男
    GameObject toggle_Female; // 單選框-女

    Text userName; //用戶名
    Text userPwd; //密碼
    Text userAge; //年齡
    public string gender;//性別
   
    public override string Name
    {
        get
        {
            return "UIRegister";
        }
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return)) {
            RegisterUser();
        }
    }

    void Awake() {
        txb_Name = GameObject.Find("txb_Name");
        alert_Name = GameObject.Find("alert_Name"); //默認隱藏
        alert_Name.SetActive(false);

        txb_Age = GameObject.Find("txb_Age");
        alert_Age = GameObject.Find("alert_Age"); //默認隱藏
        alert_Age.SetActive(false);

        txb_Email = GameObject.Find("txb_Email");
        alert_Email = GameObject.Find("alert_Email");
        alert_Email.SetActive(false);

        txb_Pwd1 = GameObject.Find("txb_Pwd1");
        txb_Pwd2 = GameObject.Find("txb_Pwd2");
        alert_Pwd = GameObject.Find("alert_Pwd"); //默認隱藏
        alert_Pwd.SetActive(false);

        chb_Gender = GameObject.Find("chb_Gender");
        btn_Login = GameObject.Find("btn_Login");
        btn_Close = GameObject.Find("btn_Close");

        toggle_Male = GameObject.Find("Male");
        toggle_Female = GameObject.Find("Female");


    }

    /// <summary>
    /// 重寫進入註冊頁面事件
    /// 顯示註冊界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        gameObject.SetActive(true);
        //默認只顯示男生的選項
        gender = "M";

    }

    /// <summary>
    /// 重寫暫停註冊頁面事件
    /// 暫停註冊頁面
    /// </summary>
    public override void DoOnPausing()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 重寫恢復註冊頁面事件
    /// 顯示註冊界面
    /// </summary>
    public override void DoOnResuming()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// 跳轉至登錄界面
    /// </summary>
    public void GoBackToLogin()
    {
        UIManager.Instance.PushUIPanel("UILogin");
    }

    /// <summary>
    /// 驗證并提交註冊新用戶申請
    /// </summary>
    public void RegisterUser()
    {

        Debug.Log("遞交註冊申請");
        //校對
        User newUser = new User();
        newUser.Name = txb_Name.GetComponent<InputField>().text;
        newUser.Age = txb_Age.GetComponent<InputField>().text!=""?int.Parse(txb_Age.GetComponent<InputField>().text):0;
        newUser.Email = txb_Email.GetComponent<InputField>().text;
        newUser.Pwd = txb_Pwd1.GetComponent<InputField>().text;
        if (toggle_Male.GetComponent<Toggle>().isOn) {
            newUser.Gender = "M";
        }
        else
        {
            newUser.Gender = "F";
        }

        //不能為空
        if (newUser.Name == "")
        {
            alert_Name.GetComponent<Text>().text = "用戶名不能為空！";
            alert_Name.SetActive(true);
            return;
        }else if (newUser.Age == 0)
        {
            alert_Age.GetComponent<Text>().text = "年齡不能為空！";
            alert_Age.SetActive(true);
            return;
        }
        else if (newUser.Email == "")
        {
            alert_Email.GetComponent<Text>().text = "郵件不能為空！";
            alert_Email.SetActive(true);
            return;
        }
        else if (newUser.Pwd == "") {
            alert_Pwd.GetComponent<Text>().text = "密碼不能為空！";
            alert_Pwd.SetActive(true);
            return;
        }
        
        //(1)校對用戶名是否存在
        if (DataManager.Instance.User_Exsisted(newUser.Name)) {
            Debug.Log("用戶名存在！");
            alert_Name.GetComponent<Text>().text = "用戶名存在！";
            alert_Name.SetActive(true);
            return;
        }

        //(2) 校對年齡是否符合規則
        if (int.Parse(txb_Age.GetComponent<InputField>().text) > 7) {
            Debug.Log("年齡不符合！");
            alert_Age.GetComponent<Text>().text = "年齡不符合";
            alert_Age.SetActive(true); 
            return;
        }

        //(3) 判斷密碼格式是否正確
        if (!Common.IsEmail(newUser.Email))
        {
            alert_Email.GetComponent<Text>().text = "格式不正確！";
            alert_Email.SetActive(true);
            return;
        }

        //(4) 先校對兩次密碼是否一致
        if (txb_Pwd1.transform.GetComponent<InputField>().text != txb_Pwd2.transform.GetComponent<InputField>().text) {
            Debug.Log("密碼不一致！");
            alert_Pwd.GetComponent<Text>().text = "密碼不一致！";
            alert_Pwd.SetActive(true);
            return;
        }

        //上面的校對沒問題，則添加用戶
        newUser.Id =  DataManager.Instance.User_Register(newUser);

        //設置遊玩記錄
        //newUser.PlayRecord = DataManager.Instance.getAllGameInfo(newUser);
        DataManager.Instance.getAllGameInfo(newUser);

        GameManager.Instance.player = newUser;

        //設置關卡
        GameManager.Instance.setStage();

        //跳轉到tutorial頁面
        UIManager.Instance.PushUIPanel("UIAnimation");

    }


}