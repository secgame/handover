﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class UILogin : UIBase {

    GameObject txb_Name; //姓名框
    GameObject lab_NameError; //姓名提示條
    GameObject txb_Pwd; //密碼框
    GameObject lab_PwdError;  //密碼提示條
    GameObject btn_Login; //登錄按鈕
    GameObject btn_Register; //註冊按鈕

    Text userName; //用戶名
    Text userPwd; //密碼

    public override string Name
    {
        get {
            return "UILogin";
        }
    }

    void Awake() {
        //用戶名和密碼框獲取并清空
        txb_Name = GameObject.Find("txb_Name");
        txb_Pwd = GameObject.Find("txb_Pwd");
        userName = txb_Name.transform.Find("Text").GetComponent<Text>();
        userName.text = "";
        userPwd = txb_Pwd.transform.Find("Text").GetComponent<Text>();
        userPwd.text = "";

        //提示字符獲取并隱藏
        lab_NameError = GameObject.Find("lab_NameError");
        lab_PwdError = GameObject.Find("lab_PwdError");
        lab_NameError.SetActive(false);
        lab_PwdError.SetActive(false);

        //獲取按鈕
        btn_Login = GameObject.Find("btn_Login");
        btn_Register = GameObject.Find("btn_Register");
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            GoToMain();
        }
    }

    /// <summary>
    /// 重寫進入登錄頁面事件
    /// 顯示登錄界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        //顯示
        gameObject.SetActive(true);
       
    }

    /// <summary>
    /// 重寫暫停登錄界面事件
    /// 隱藏登錄界面
    /// </summary>
    public override void DoOnPausing()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 重寫恢復登錄界面事件
    /// 恢復登錄界面
    /// </summary>
    public override void DoOnResuming()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// 進入主界面
    /// </summary>
    public void GoToMain()
    {
        UIManager.Instance.PushUIPanel("UIMain");
        // lab_NameError.SetActive(false);
        // lab_PwdError.SetActive(false);

        // User myUser = new User();
        // myUser.Name = userName.text;

        // //判斷是否為空
        // if(txb_Name.GetComponent<InputField>().text.Trim() == "")
        // {
        //     lab_NameError.SetActive(true);
        //     lab_NameError.transform.GetComponent<Text>().text = "用戶名不能為空！";
        // }else if(txb_Pwd.GetComponent<InputField>().text.Trim() == "")
        // {
        //     lab_PwdError.SetActive(true);
        //     lab_PwdError.transform.GetComponent<Text>().text = "密碼不能為空！";
        // }else  //判斷密碼和用戶名是否正確
        // {
        //     if (!DataManager.Instance.User_Exsisted(myUser.Name)) {
        //         lab_NameError.SetActive(true);
        //         lab_NameError.transform.GetComponent<Text>().text = "用戶名不存在！";
        //     }
        //     else {
        //         myUser = DataManager.Instance.GetUserByName(myUser.Name);
        //         if (myUser.Pwd == txb_Pwd.GetComponent<InputField>().text)
        //         {
        //             Debug.Log("登錄成功！");
        //             GameManager.Instance.login = true;
                    
        //             //確定全局的player的信息
        //             GameManager.Instance.player = myUser;
        //             //判斷一下玩家是否有通過教程的記錄，有則去主界面，沒有則跳回動畫界面 （教程的遊戲id為1,2,3）
        //             GameManager.Instance.setStage();
        //             Debug.Log("GameManager.Instance.stage:"+GameManager.Instance.stage);                

        //             if (GameManager.Instance.stage > 1)
        //             {
        //                 UIManager.Instance.PushUIPanel("UIMain");
        //             }else
        //             {
        //                 UIManager.Instance.PushUIPanel("UIAnimation");
        //             }

                    

        //         }
        //         else {
        //             lab_PwdError.SetActive(true);
        //             lab_PwdError.transform.GetComponent<Text>().text = "用戶名和密碼不符合！";
        //         }
        //     }

                     
        // }
                   
    }

    /// <summary>
    /// 進入註冊頁面
    /// </summary>
    public void GoToRegister()
    {
        lab_NameError.SetActive(false);
        lab_PwdError.SetActive(false);
        userName.text = "";
        userPwd.text = "";

        Debug.Log("去註冊！");
        UIManager.Instance.PushUIPanel("UIRegister");
    }

}
