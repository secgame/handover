﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using System;

public class QuestionCreater : MonoBehaviour {

    public int questionType; //題型 1-》匹配；2-》找不同；3-》找相同；4-》判斷
    public DataTable questionDB; //本關可選的題目
    int curIndex; //當前題庫裡面的指針
    int lastIndex; //上一道題的的指針

    LevenshteinDistance myLevenshteinDistance;//判斷距離

    public PlayEachRecord per;

     private static QuestionCreater instance;

    public static QuestionCreater Instance {
        get {
            return instance;
        }
        set {
            instance = value;
        }
    }

    // Use this for initialization
    void Start () {
        Instance = this;
        //初始化距離測試器
        myLevenshteinDistance = new LevenshteinDistance();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// 改變題型的數據源
    /// </summary>
    /// <param name="queType"></param>
    public void ChangeQuestionDB(int queType) {
        questionType = queType;
        switch (queType) {
            case 1:
                questionDB = GameManager.Instance.MatchDT;
                curIndex = PlayerPrefs.GetInt("MatchIndex",0);
                lastIndex = -1;
                break;
            case 2:
                questionDB = GameManager.Instance.DifferDT;
                curIndex = PlayerPrefs.GetInt("DifferIndex",0);
                lastIndex = -1;
                break;
            case 3:
                questionDB = GameManager.Instance.SameDT;
                curIndex = PlayerPrefs.GetInt("SameIndex", 0);
                lastIndex = -1;
                break;
            case 4:
                questionDB = GameManager.Instance.TellDT;
                curIndex = PlayerPrefs.GetInt("TellIndex", 0);
                lastIndex = -1;
                break;
        }

    }


    /// <summary>
    /// 顯示下一題
    /// </summary>
    /// <param name="play_id"></param>
    /// <returns></returns>
    public QandA NextQuestion(int play_id) { 
        per = new PlayEachRecord();
        if (lastIndex != -1) {
            do
            {
                if (curIndex >= questionDB.Rows.Count)
                {
                    Debug.Log("沒有題目可選了");
                    gameObject.GetComponent<UIGame>().GoToGame();
                }

                curIndex++;

                if (questionDB.Rows[lastIndex]["question"].ToString().Length != questionDB.Rows[curIndex]["question"].ToString().Length)
                {
                    break;
                }
                else
                {
                    myLevenshteinDistance.Compute(questionDB.Rows[lastIndex]["question"].ToString(), questionDB.Rows[curIndex]["question"].ToString());
                }

            } while (float.Parse(myLevenshteinDistance.ComputeResult.Rate) > 0.6);

            }

        lastIndex = curIndex;
        PlayerPrefs.SetInt("MatchIndex", curIndex);

        DataRow currentRow = questionDB.Rows[curIndex];
        string queStr = currentRow["question"].ToString();

        //找不同 //找相同

        QandA myQandA = new QandA();
        myQandA.Difficulty = int.Parse(currentRow["difficulty"].ToString());

        //不同題型，對數據進行不同的處理
        switch (questionType) {
            case 1: //匹配
                break;
            case 2: //找不同          
                string[] QsAndAs = queStr.Split(',');
                myQandA.AStr = QsAndAs[QsAndAs.Length - 1];
                myQandA.QArr = string.Join(",", QsAndAs,0, QsAndAs.Length-1).Split(',');
                break;
            case 3: //找相同
                myQandA.AStr = queStr;
                myQandA.QArr = currentRow["answer"].ToString().Split(',');

                break;
            case 4: //判斷
                myQandA.QArr = queStr.Split(',');
                char[] chars = myQandA.QArr[0].ToCharArray();
                foreach (char tmp in chars)
                {
                    myQandA.AStr = "1";
                    for (int i = 1; i < myQandA.QArr.Length; i++) {
                        if (myQandA.QArr[i].IndexOf(tmp) < 0) {
                            myQandA.AStr = "0";
                            break;
                        }
                    }
                    break;
                }
                break;
        }

        //保存數據
        per.Play_id = play_id;
        per.Question_info = string.Join(",", myQandA.QArr);
        per.Answer_info = "";
        per.Choice_info = myQandA.AStr;
        per.Question_difficulty = myQandA.Difficulty;
        per.Show_time = TimeClass.GetTimeStamp(DateTime.Now);

        return myQandA;

    }

    /// <summary>
    /// 暫存遊戲記錄
    /// </summary>
    /// <param name="name"></param>
    public void saveAns( string name) {
        //保存答案
        per.Answer_info = name;
        per.Answer_time = TimeClass.GetTimeStamp(DateTime.Now);

        transform.GetComponent<PlayDataSaveController>().AddRecord2Table(per);
    }
    
}
