﻿using System;
using System.Collections;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UnityEngine.EventSystems.EventTrigger))]
public class UIAnimation : UIBase {
    public int frame; //動畫幀的序號
    private string game; //遊戲的序號
    public GameObject btn_close;
    public GameObject btn_back;
    public GameObject btn_forward;
    public GameObject btn_game;
    public GameObject btn_finish;
    public ArrayList frameSpriteArray; //動畫的圖片集合
    public GameObject frameContainer; //存儲動畫的容器
    public SpriteRenderer spr; //渲染器
    public ArrayList GameArray; //遊戲信息數組
    public int beginframe;//表示在該套幀中的幀序號

    public static UIAnimation _instance;


    public override string Name
    {
        get
        {
            return "UIAnimation";
        }
    }


    //進入新的關卡要load新的數據
    void Awake() {

        //一開始並不現實後退按鈕
        btn_close = GameObject.Find("btn_close");
        btn_back = GameObject.Find("btn_back");
        btn_forward = GameObject.Find("btn_forward");
        btn_game = GameObject.Find("btn_game");
        btn_finish = GameObject.Find("btn_finish");
        frameContainer = GameObject.Find("frameContainer");
        frameSpriteArray = new ArrayList();
        GameArray = new ArrayList();
        frame = 0;
        game = "";
        beginframe = 0;

        _instance = this;
    }

    /// <summary>
    /// 根據關卡提取動畫幀
    /// </summary>
    /// <param name="stage"></param>
    public void setFrameGroup(int stage)
    {
        //先找到對應stage的start_frame_id
        int start_frame_id = 0;
        foreach (DataRow row in GameManager.Instance.stageDT.Rows)
        {
            if (int.Parse(row["stage_id"].ToString()) == stage)
            {
                start_frame_id = int.Parse(row["stage_begin_frame_id"].ToString());
            }
        }

        //清空數組
        frameSpriteArray.Clear();

        //找出所有的圖片并放入圖片集中
        beginframe = start_frame_id;
        frameSpriteArray = GetAllSublings(start_frame_id, GameManager.Instance.frameDT);

    }

    /// <summary>
    /// 獲取所有的動畫幀
    /// </summary>
    /// <param name="child">開始的幀</param>
    /// <param name="dt">幀集合</param>
    /// <returns></returns>
    public ArrayList GetAllSublings(int child, DataTable dt)
    {
        //判斷是不是頂點坐標
        int next = -1;
        foreach (DataRow row in dt.Rows)
        {
            if (int.Parse(row["frame_id"].ToString()) == child)
            {
                //找到父節點
                next = int.Parse(row["frame_next_id"].ToString());
                //當前節點放入集合中
                Texture2D texture2d = Resources.Load("Cartoon/"+row["frame_pic"].ToString()) as Texture2D;

                Sprite tmpspr = Sprite.Create(texture2d, new Rect(0.0f, 0.0f, texture2d.width, texture2d.height), new Vector2(0, 0));
                frameSpriteArray.Add(tmpspr);
                if (row["frame_game_id"].ToString()!="")
                {
                    foreach(DataRow dr in GameManager.Instance.gameDT.Rows)
                    {
                        if (int.Parse(dr["game_id"].ToString()) == int.Parse(row["frame_game_id"].ToString())) {
                            GameArray.Add(new ArrayList() { child, dr["game_name"].ToString()});
                        }
                    }
                    
                }

                break;
            }
        }

        if (next == 0)
        {
            return frameSpriteArray;
        }
        else
        {
            return GetAllSublings(next, dt);
        }
    }

    /// <summary>
    /// 根據當前動畫的幀數顯示按鈕控件
    /// </summary>
    void Update() {
        int currentFrame = frame + beginframe;

        if (frame == 0)
        {
            //只顯示前進按鈕
            btn_forward.SetActive(true);
            btn_finish.SetActive(false);
            btn_back.SetActive(false);
            btn_game.SetActive(false);
        }
        else if (frame == frameSpriteArray.Count-1)
        {
            //只顯示完成按鈕和退後按鈕
            btn_forward.SetActive(false);
            btn_game.SetActive(false);
            btn_back.SetActive(true);
            btn_finish.SetActive(true);
        }
        else
        {
            bool followGame = false;
            //判斷frame後面跟不跟遊戲
            foreach(ArrayList al in GameArray)
            {
                if (int.Parse(al[0].ToString()) == currentFrame) {
                    game = al[1].ToString();
                    followGame = true;
                }
            }

            if (followGame)
            {
                //只顯示前進和退後按鈕
                btn_forward.SetActive(false);
                btn_game.SetActive(true);
                btn_back.SetActive(true);
                btn_finish.SetActive(false);
            } else {
                //只顯示前進和退後按鈕
                btn_forward.SetActive(true);
                btn_game.SetActive(false);
                btn_back.SetActive(true);
                btn_finish.SetActive(false);
            }
        }

        //顯示數組[frame]的動畫
        frameContainer.transform.GetComponent<Image>().sprite = (Sprite)frameSpriteArray[frame];
    }

    /// <summary>
    /// 進跳轉到遊戲界面
    /// </summary>
    public void GoToGame()
    {
        frame++;
        UIManager.Instance.PushUIPanel(game);
    }

    /// <summary>
    /// 跳轉到主界面
    /// </summary>
    public void GoToMain()
    {
        UIManager.Instance.PushUIPanel("UIMain");
    }

    /// <summary>
    /// 顯示動畫的上一幀
    /// </summary>
    public void GoToLast()
    {
        frame--;
    }

    /// <summary>
    /// 顯示動畫的下一幀
    /// </summary>
    public void GoToNext()
    {
        frame++;
    }

    /// <summary>
    /// 發送結果郵件及跳轉到成就界面
    /// </summary>
    public void GoToResult() {
        frame = 0;
        //發郵件給家長
        EmailManager.Instance.body = "今日答題情況：<br />" + EmailManager.Instance.body;
        EmailManager.Instance.subject = GameManager.Instance.player.Name +"小朋友在毛福地球大冒險["+ DateTime.Now.ToShortDateString().ToString()+"]表現情況";
        Debug.Log("body:" + EmailManager.Instance.body);
        Debug.Log("subject:" + EmailManager.Instance.subject);
        Debug.Log("address:" + GameManager.Instance.player.Email);
        EmailManager.Instance.SendEmail(GameManager.Instance.player.Email);
        EmailManager.Instance.subject = "";
        EmailManager.Instance.body = "";
        UIManager.Instance.PushUIPanel("UIAchievement");
        
    }

    /// <summary>
    /// 重寫進入動畫界面事件
    /// 顯示動畫界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        setFrameGroup(GameManager.Instance.stage);
        //顯示
        gameObject.SetActive(true);
    }

    /// <summary>
    /// 重寫進入動畫界面事件
    /// 隱藏動畫界面
    /// </summary>
    public override void DoOnPausing()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 重寫重進入動畫界面事件
    /// 回到首個動畫幀
    /// </summary>
    public override void DoOnResuming()
    {
        Debug.Log("顯示");
        GameManager.Instance.setStage();
        setFrameGroup(GameManager.Instance.stage);
        frame = 0;
        //更新信息
    }

    /// <summary>
    /// 重寫退出動畫界面事件
    /// </summary>
    public override void DoOnExiting()
    {
        
    }
}
