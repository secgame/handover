﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAchievement : UIBase
{
    GameObject btn_close; //關閉框
    

    public override string Name
    {
        get
        {
            return "UIRegister";
        }
    }

    void Awake() {
        btn_close = GameObject.Find("btn_close");
        
    }

    /// <summary>
    /// 重寫進入成就界面
    /// 顯示成就界面
    /// </summary>
    public override void DoOnEntering()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        gameObject.SetActive(true);

    }

    /// <summary>
    /// 重寫暫停成就界面
    /// 隱藏成就界面
    /// </summary>
    public override void DoOnPausing()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 重寫回復成就界面
    /// 顯示成就界面
    /// </summary>
    public override void DoOnResuming()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    ///回到主界面
    /// </summary>
    public void GoBackToMain()
    {
        UIManager.Instance.PushUIPanel("UIMain");
    }

}