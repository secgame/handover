﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager instance;
    public static GameManager Instance {
        get;
        set;
    }

    public User player;
    public int stage;

    public DataTable frameDT;
    public DataTable stageDT;
    public DataTable gameDT;

    public float happyVal;

    public DataTable wordsDT;
    public DataTable wordsRelationsDT;
    public DataTable MatchDT;
    public DataTable DifferDT;
    public DataTable SameDT;
    public DataTable TellDT;

    Dictionary<string, Dictionary<string, DataTable>> myWordDic;

    public bool login;//是否登錄



    /// <summary>
    /// 獲取匹配題型的題庫
    /// </summary>
    /// <param name="myDic"></param>
    /// <returns></returns>
    public DataTable GetMatchDT(Dictionary<string, Dictionary<string, DataTable>> myDic)
    {
        DataTable tmpCom = new DataTable();
        //分完類以後，進行組合
        tmpCom.Columns.Add("answer", typeof(string));
        tmpCom.Columns.Add("question", typeof(string));
        tmpCom.Columns.Add("difficulty", typeof(float));
        tmpCom.Columns.Add("num", typeof(int));

        foreach (var item in myDic)
        {
            //按照音組合，并計算出難度
            Dictionary<string, DataTable> sameYin = item.Value;

           
            if (sameYin.Count >= 2)
            {
                foreach (var oneItem in sameYin)
                {
                    for (var i = 0; i < oneItem.Value.Rows.Count; i++)
                    {
                        for (var j = i + 1; j < oneItem.Value.Rows.Count; j++)
                        {
                            foreach (var twoItem in sameYin)
                            {
                                if (oneItem.Key != twoItem.Key)
                                {
                                    for (var k = 0; k < twoItem.Value.Rows.Count; k++)
                                    {
                                        for (var l = k + 1; l < twoItem.Value.Rows.Count; l++)
                                        {
                                            //2對
                                            string answer = oneItem.Value.Rows[i]["word_name"].ToString() + "," + twoItem.Value.Rows[k]["word_name"].ToString();
                                            string question = oneItem.Value.Rows[j]["word_name"].ToString() + "," + twoItem.Value.Rows[l]["word_name"].ToString();
                                            
                                           
                                            tmpCom.Rows.Add(answer, question, 0,2);
                                            foreach (var threeItem in sameYin)
                                            {
                                                if (threeItem.Key != oneItem.Key && threeItem.Key != twoItem.Key)
                                                {
                                                    for (var m = 0; m < threeItem.Value.Rows.Count; m++)
                                                    {
                                                        for (var n = m + 1; n < threeItem.Value.Rows.Count; n++)
                                                        {
                                                            //3對
                                                            answer = oneItem.Value.Rows[i]["word_name"].ToString() + "," + twoItem.Value.Rows[k]["word_name"].ToString() + "," + threeItem.Value.Rows[m]["word_name"].ToString();
                                                            question = oneItem.Value.Rows[j]["word_name"].ToString() + "," + twoItem.Value.Rows[l]["word_name"].ToString() + "," + threeItem.Value.Rows[n]["word_name"].ToString(); ;
                                                            tmpCom.Rows.Add(answer, question, 0,3);
                                                            foreach (var fourthItem in sameYin)
                                                            {
                                                                if (fourthItem.Key != oneItem.Key && fourthItem.Key != twoItem.Key && fourthItem.Key != threeItem.Key) {
                                                                    for (var o = 0; o < fourthItem.Value.Rows.Count; o++) {
                                                                        for (var p = o + 1; p < fourthItem.Value.Rows.Count; p++) {
                                                                            //4對
                                                                             answer = oneItem.Value.Rows[i]["word_name"].ToString() + "," + twoItem.Value.Rows[k]["word_name"].ToString() + "," + threeItem.Value.Rows[m]["word_name"].ToString() + "," + fourthItem.Value.Rows[o]["word_name"].ToString();
                                                                             question = oneItem.Value.Rows[j]["word_name"].ToString() + "," + twoItem.Value.Rows[l]["word_name"].ToString() + "," + threeItem.Value.Rows[n]["word_name"].ToString() + "," + fourthItem.Value.Rows[p]["word_name"].ToString();
                                                                            tmpCom.Rows.Add(answer, question, 0,3);


                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        DataView dv = tmpCom.DefaultView;
        dv.Sort = "num";
        tmpCom = dv.ToTable();

        return tmpCom;

    }

    /// <summary>
    /// 分割拼音與文字
    /// </summary>
    /// <returns></returns>
    public Dictionary<string, Dictionary<string, DataTable>> SpliceWords(){
        Dictionary<string, Dictionary<string, DataTable>> myDic = new Dictionary<string, Dictionary<string, DataTable>>();
        //先按音，后按字分類
        foreach (DataRow row in wordsDT.Rows)
        {
            //分割拼音
            string pattern = @"\(\w*d*/*\w*d*/*\w*d*/*\w*d*/*\w*d*\)";
            string[] pinyinArray = new string[3];
            MatchCollection matches = Regex.Matches(row["word_pinyin"].ToString(),pattern);
            int j = 0;
            foreach (Match m in matches)
            {
                pinyinArray[j] = m.Value.Replace("(", "").Replace(")", ""); 
                j++;
            }


            //去除拼音括號,并加入字典
            for (var i = 0; i < 3; i++)
            {
                if (pinyinArray[i] == null)
                    break;

                string tmpPY = pinyinArray[i];
                if (pinyinArray[i].Contains("/")){
                    tmpPY = Regex.Split(pinyinArray[i], "/", RegexOptions.IgnoreCase)[0];
                }
                
                //判斷是否有該音
                if (!myDic.ContainsKey(tmpPY))
                {
                    myDic.Add(tmpPY, new Dictionary<string, DataTable>());
                }

                Dictionary<string, DataTable> data = myDic[tmpPY];
               
                string wordKey = row["word_name"].ToString().ToCharArray()[i].ToString();

                //判斷一下有沒有該字
                if (!data.ContainsKey(wordKey))
                {
                    DataTable tmpTB = wordsDT.Clone();

                    data.Add(wordKey, tmpTB);
                }

                //有的話則添加字
                data[wordKey].ImportRow(row);
            }
        }

        return myDic;

    }

    /// <summary>
    /// 找到文字的透明
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    public int findTransparency(string word) {
        foreach (DataRow row in wordsDT.Rows) {
            if (word == row["word_name"].ToString()) {
                return int.Parse(row["word_transparency"].ToString());
            }
        }
        return -1;
    }

    /// <summary>
    /// 找到兩個詞彙之間的相關性
    /// </summary>
    /// <param name="word1"></param>
    /// <param name="word2"></param>
    /// <returns></returns>
    public int findRelation(string word1, string word2) {
        foreach (DataRow row in wordsRelationsDT.Rows) {
            if ((word1 == row["word_a_name"].ToString() && word2 == row["word_b_name"].ToString()) || (word2 == row["word_a_name"].ToString() && word1 == row["word_b_name"].ToString())) {
                return 10 - int.Parse(row["relation_level"].ToString());
            }
        }

        return -1;
    }



    /// <summary>
    /// 獲取找不同題型的題庫
    /// </summary>
    /// <returns></returns>
    public DataTable GetDifferDT(Dictionary<string, Dictionary<string, DataTable>> myDic) {
        DataTable tmpCom = new DataTable();   
        //分完類以後，進行組合      
        tmpCom.Columns.Add("question", typeof(string));
        tmpCom.Columns.Add("difficulty", typeof(float));
        tmpCom.Columns.Add("num", typeof(int));

        foreach (var item in myDic) {
            //按照音組合，并計算出難度
            Dictionary<string, DataTable> sameYin = item.Value;
            //獲取2/3/4的選項
            foreach (var subItem in sameYin) {
                
                if (subItem.Value.Rows.Count >= 2)
                {
                    for (var i = 0; i < subItem.Value.Rows.Count; i++)
                    {

                        for (var j = i+1; j < subItem.Value.Rows.Count; j++)
                        {
                            //獲取兩兩組合
                            foreach (var oneItem in sameYin) {
                                if (oneItem.Key != subItem.Key) {
                                    //如果字是不同的，就可以添加了
                                    foreach (DataRow answerRow in oneItem.Value.Rows) {
                                        string str = subItem.Value.Rows[i]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString() + "," + answerRow["word_name"].ToString();
                                        //計算改題的難度-》不同項的透明度
                                        //難度的計算
                                        int difficulty = 10 - findTransparency(answerRow["word_name"].ToString());
                                        tmpCom.Rows.Add(str, difficulty, 2);
                                    }
                                    
                                }
                            }
                            for (var k = j + 1; k < subItem.Value.Rows.Count; k++) {
                                //獲取三三組合
                                foreach (var oneItem in sameYin)
                                {
                                    if (oneItem.Key != subItem.Key)
                                    {
                                        //如果字是不同的，就可以添加了
                                        foreach (DataRow answerRow in oneItem.Value.Rows)
                                        {
                                            string str = subItem.Value.Rows[i]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString() + "," + subItem.Value.Rows[k]["word_name"].ToString() + "," + answerRow["word_name"].ToString();
                                            //難度的計算
                                            int difficulty = 10 - findTransparency(answerRow["word_name"].ToString());
                                            tmpCom.Rows.Add(str, difficulty,3);
                                        }

                                    }
                                }

                                for (var l = k + 1; l < subItem.Value.Rows.Count; l++) {
                                    //獲取四四組合
                                    foreach (var oneItem in sameYin)
                                    {
                                        if (oneItem.Key != subItem.Key)
                                        {
                                            //如果字是不同的，就可以添加了
                                            foreach (DataRow answerRow in oneItem.Value.Rows)
                                            {
                                                string str = subItem.Value.Rows[i]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString() + "," + subItem.Value.Rows[k]["word_name"].ToString() + "," +subItem.Value.Rows[l]["word_name"].ToString() + "," + answerRow["word_name"].ToString();
                                                //難度的計算
                                                int difficulty = 10 - findTransparency(answerRow["word_name"].ToString());
                                                tmpCom.Rows.Add(str, difficulty, 4);
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        DataView dv = tmpCom.DefaultView;
        dv.Sort = "num";
        tmpCom = dv.ToTable();

        return tmpCom;

    }

    /// <summary>
    /// 獲取找相同題型的題庫
    /// </summary>
    /// <returns></returns>
    public DataTable GetSameDT(Dictionary<string, Dictionary<string, DataTable>> myDic) {
        DataTable tmpCom = new DataTable();
        //分完類以後，進行組合      
        tmpCom.Columns.Add("question", typeof(string));
        tmpCom.Columns.Add("answer", typeof(string));
        tmpCom.Columns.Add("difficulty", typeof(float));
        tmpCom.Columns.Add("num", typeof(int));

        foreach (var item in myDic)
        {
            //按照相同音
            Dictionary<string, DataTable> sameYin = item.Value;
            foreach (var subItem in sameYin)
            {
                //先找出匹配的一對
                if (subItem.Value.Rows.Count >= 2)
                {
                    for (var i = 0; i < subItem.Value.Rows.Count; i++)
                    {
                        for (var j = i + 1; j < subItem.Value.Rows.Count; j++)
                        {
                            //匹配的一對為subItem.Value.Rows[i]["word_name"]和 subItem.Value.Rows[j]["word_name"]
                            //找出干擾項
                            //先把除自己以外的datatable集合在一起
                            DataTable allDT = wordsDT.Clone();
                            foreach (var oneItem in sameYin) {
                                if (oneItem.Key != subItem.Key)
                                {
                                    foreach (DataRow dr in oneItem.Value.Rows) {
                                        allDT.ImportRow(dr);
                                    }
                                    
                                }
                            }

                            //1項干擾項
                            if (allDT.Rows.Count >= 1) {
                                for (var k = 0; k < allDT.Rows.Count; k++) {
                                    int difficulty = findRelation(subItem.Value.Rows[i]["word_name"].ToString(), subItem.Value.Rows[j]["word_name"].ToString());
                                    tmpCom.Rows.Add(subItem.Value.Rows[i]["word_name"].ToString(), allDT.Rows[k]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString(), difficulty, 1);
                                    //2項干擾項
                                    for (var l = k + 1; l < allDT.Rows.Count; l++)
                                    {
                                        tmpCom.Rows.Add(subItem.Value.Rows[i]["word_name"].ToString(), allDT.Rows[k]["word_name"].ToString() + "," + allDT.Rows[l]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString(), difficulty,2);
                                        //3項干擾項
                                        for (var m = l + 1; m < allDT.Rows.Count; m++)
                                        {
                                            tmpCom.Rows.Add(subItem.Value.Rows[i]["word_name"].ToString(), allDT.Rows[k]["word_name"].ToString() + "," + allDT.Rows[l]["word_name"].ToString() + "," + allDT.Rows[m]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString(), difficulty,3);
                                            //4項干擾項
                                            for (var n = m + 1; n < allDT.Rows.Count; n++)
                                            {
                                                tmpCom.Rows.Add(subItem.Value.Rows[i]["word_name"].ToString(), allDT.Rows[k]["word_name"].ToString() + "," + allDT.Rows[l]["word_name"].ToString() + "," + allDT.Rows[m]["word_name"].ToString() + "," + allDT.Rows[n]["word_name"].ToString() + "," + subItem.Value.Rows[j]["word_name"].ToString(), difficulty,4);
                                            }
                                        }
                                    }
                                }
                            }
             

                        }
                    }
                }

            }
        }

        DataView dv = tmpCom.DefaultView;
        dv.Sort = "num";
        tmpCom = dv.ToTable();

        return tmpCom;
    }

    /// <summary>
    /// 獲取判斷題型的題型
    /// </summary>
    /// <returns></returns>
    public DataTable GetTellDT(Dictionary<string, Dictionary<string, DataTable>> myDic) {
        //判斷兩個是否相同，三個是否相同，四個是否相同
        DataTable tmpCom = new DataTable();
        //分完類以後，進行組合      
        tmpCom.Columns.Add("question", typeof(string));
        tmpCom.Columns.Add("difficulty", typeof(float));
        tmpCom.Columns.Add("num", typeof(int));

        foreach (var item in myDic)
        {
            //獲取相同音的組合
            Dictionary<string, DataTable> sameYin = item.Value;
            DataTable tmpDT = wordsDT.Clone();
            foreach (var subItem in sameYin)
            {
                foreach (DataRow dr in subItem.Value.Rows) {
                    tmpDT.ImportRow(dr);
                }
            }

            for (var i = 0; i < tmpDT.Rows.Count; i++)
            {
                for (var j = i + 1; j < tmpDT.Rows.Count; j++)
                {
                    //獲取兩個判斷
                    float difficulty =  (findTransparency(tmpDT.Rows[i]["word_name"].ToString())+findTransparency(tmpDT.Rows[j]["word_name"].ToString()))/2;
                    tmpCom.Rows.Add(tmpDT.Rows[i]["word_name"].ToString() + "," + tmpDT.Rows[j]["word_name"].ToString(), difficulty, 2);
                    for (var k = j + 1; k < tmpDT.Rows.Count; k++)
                    {
                        //獲取三個判斷
                        difficulty = (findTransparency(tmpDT.Rows[i]["word_name"].ToString()) + findTransparency(tmpDT.Rows[j]["word_name"].ToString()) + findTransparency( tmpDT.Rows[k]["word_name"].ToString())) / 3;
                        tmpCom.Rows.Add(tmpDT.Rows[i]["word_name"].ToString() + "," + tmpDT.Rows[j]["word_name"].ToString() + "," + tmpDT.Rows[k]["word_name"].ToString(), difficulty,3);
                        for (var l = k + 1; l < tmpDT.Rows.Count; l++) {
                            //獲取四個判斷
                            difficulty = (findTransparency(tmpDT.Rows[i]["word_name"].ToString()) + findTransparency(tmpDT.Rows[j]["word_name"].ToString()) + findTransparency(tmpDT.Rows[k]["word_name"].ToString()) + findTransparency(tmpDT.Rows[l]["word_name"].ToString())) / 4;
                            tmpCom.Rows.Add(tmpDT.Rows[i]["word_name"].ToString() + "," + tmpDT.Rows[j]["word_name"].ToString() + "," + tmpDT.Rows[k]["word_name"].ToString() + "," + tmpDT.Rows[l]["word_name"].ToString(), difficulty,4);
                        }

                    }

                }
            }

        }

        DataView dv = tmpCom.DefaultView;
        dv.Sort = "num";
        tmpCom = dv.ToTable();

        return tmpCom;

    }

    // Use this for initialization
    void Awake () {
        Instance = this;
        login = false;
        player = new User();
        frameDT = new DataTable();
        stageDT = new DataTable();
        myWordDic = new Dictionary<string, Dictionary<string, DataTable>>();
    }

    // Update is called once per frame
    void Start () {
        //賦值
        string table_name = "tb_frame_info";
        frameDT = DataManager.Instance.SelectAll(table_name);

        table_name = "tb_stage_info";
        stageDT = DataManager.Instance.SelectAll(table_name);

        table_name = "tb_game_info";
        gameDT = DataManager.Instance.SelectAll(table_name);

        table_name = "tb_words_info";
        wordsDT = DataManager.Instance.SelectAll(table_name);

        table_name = "tb_words_relation";
        wordsRelationsDT = DataManager.Instance.GetWordRelationDT();

        //題型分類
        myWordDic =  SpliceWords();

        //獲取匹配題型
        MatchDT = GetMatchDT(myWordDic);

        //獲取找不同題型；
        DifferDT = GetDifferDT(myWordDic);

        //獲取找相同題型
        SameDT = GetSameDT(myWordDic);

        //獲取判斷題型
        TellDT = GetTellDT(myWordDic);

    }

    /// <summary>
    /// 設置當前的遊戲關卡
    /// </summary>
    public void setStage() {
        player.PlayRecord = DataManager.Instance.getAllGameInfo(player);
        DataTable ds = player.PlayRecord;
        //找出最大記錄
        int tmpMax = 1;
        //判斷是不是新建的
        if (ds.Rows.Count == 0)
        {
            stage = tmpMax;
            
        }
        else {
            foreach (DataRow row in ds.Rows)
            {
                if (int.Parse(row["stage_id"].ToString()) > tmpMax)
                {
                    tmpMax = int.Parse(row["stage_id"].ToString());
                }
            }
            Debug.Log("tmpMax:"+tmpMax);

            bool hasNull = false;
            foreach (DataRow row in ds.Rows)
            {
                if (int.Parse(row["stage_id"].ToString()) == tmpMax)
                {
                    if (row["answer_id"].ToString() == "")
                    {
                        hasNull = true;
                        break;
                    }
                }

                //還要判斷時候具有三個遊戲的id
            }

            Debug.Log("hasNull:" + hasNull);

            if (hasNull)
            {
                stage = tmpMax;
            }
            else
            {
                stage = tmpMax + 1;
            }
        }

    

       
    }

}
