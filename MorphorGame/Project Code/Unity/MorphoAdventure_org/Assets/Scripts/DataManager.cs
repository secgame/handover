﻿using System.Collections;
using System.Data;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// 透過PHP處理數據
/// </summary>
public class DataManager : MonoBehaviour{

    private static DataManager instance;

    public static DataManager Instance{
        get{
            return instance;
        }

        set
        {
            instance = value;
        }   
    }

    void Start() {
        Instance = this;
    }

    /// <summary>
    /// 判斷用戶是否存在
    /// </summary>
    public bool User_Exsisted(string name) {
        //判斷是否有相同的用戶名
        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?table=tb_user_info&&method=getAll&&user_name=" + name);
        while (!www.isDone) { }

        JArray ja = (JArray)JsonConvert.DeserializeObject(www.text);
        if (ja.Count == 0)
        {
            return false;
        }
        else {
            return true;
        }

    }

    public DataTable GetWordRelationDT() {

        DataTable dt = new DataTable();

        dt.Columns.Add("words_relation_id");
        dt.Columns.Add("relation_level");
        dt.Columns.Add("word_a_name");
        dt.Columns.Add("word_b_name");
        WWW www = new WWW("http://localhost/MorphoSqlAccess/getWordRelation.php");
        while (!www.isDone) { }
        

        string[] rows = www.text.Split('&');

        for (int i = 0; i < rows.Length - 1; i++)
        {
            string[] cols = rows[i].Split(';');

            dt.Rows.Add(cols[0], cols[1], cols[2], cols[3]);

        }

        return dt;
    }


    public User GetUserByName(string name) {

        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?method=getAll&&table=tb_user_info&&user_name=" + name);

        while (!www.isDone) {}

        JArray ja = (JArray)JsonConvert.DeserializeObject(www.text);

        User myUser = new User();
        myUser.Name = ja[0]["user_name"].ToString();
        myUser.Pwd = ja[0]["user_pwd"].ToString();
        myUser.Id = int.Parse(ja[0]["id"].ToString());
        myUser.Age = int.Parse(ja[0]["user_age"].ToString());
        myUser.Gender = ja[0]["user_gender"].ToString();
        myUser.Email = ja[0]["user_email"].ToString();

        return myUser;

    }


    /// <summary>
    /// 用戶註冊
    /// </summary>
    /// <param name="NUser"></param>
    /// <returns>新註冊的用戶名</returns>
    public int User_Register(User NUser) {
        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?table=tb_user_info&&method=insert&&user_name=" + NUser.Name + "&&user_pwd=" + NUser.Pwd + "&&user_gender=" + NUser.Gender + "&&user_email=" + NUser.Email + "&&user_age=" + NUser.Age);

        while (!www.isDone) {}
        
        return int.Parse(Common.GetNumber(www.text));

    }

    /// <summary>
    /// 獲取該用戶所有的遊玩記錄
    /// </summary>
    /// <param name="myuser">用戶信息</param>
    /// <returns>用戶遊戲記錄</returns>
    public DataTable getAllGameInfo(User myuser)
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("play_id");
        dt.Columns.Add("user_id");
        dt.Columns.Add("game_id");
        dt.Columns.Add("play_happyVal");
        dt.Columns.Add("question_id");
        dt.Columns.Add("question_difficulty");
        dt.Columns.Add("question_info");
        dt.Columns.Add("choice_info");
        dt.Columns.Add("answer_id");
        dt.Columns.Add("answer_info");
        dt.Columns.Add("answer_time");
        dt.Columns.Add("show_time");
        dt.Columns.Add("frame_id");
        dt.Columns.Add("stage_id");

        WWW www = new WWW("http://localhost/MorphoSqlAccess/getAllGameInfo.php?user_id=" + myuser.Id);

        while (!www.isDone) {}

        string[] rows = www.text.Trim().Split('&');

        for (int i = 0; i < rows.Length - 1; i++)
        {

            string[] cols = rows[i].Split(';');
            foreach (var k in cols) {

            }
            int frameId = int.Parse(cols[12]);
            int fatherFrameId = GetRoot(frameId, GameManager.Instance.frameDT);
            int stageId = -1;
            foreach (DataRow row1 in GameManager.Instance.stageDT.Rows)
            {
                if (int.Parse(row1["stage_begin_frame_id"].ToString()) == fatherFrameId)
                {
                    stageId = int.Parse(row1["stage_id"].ToString());
                    cols[13] = stageId.ToString();
                }
            }

            dt.Rows.Add(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5], cols[6], cols[7], cols[8], cols[9], cols[10], cols[11], cols[12], cols[13]);

        }

        

        return dt;
    }

    /// <summary>
    /// 按表查詢所有數據
    /// </summary>
    /// <param name="tableName">表名</param>
    /// <returns></returns>
    public DataTable SelectAll(string tableName) {
        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?method=getAll&&table=" + tableName);
        while (!www.isDone) { }

        JArray ja = (JArray)JsonConvert.DeserializeObject(www.text);

        DataTable dt = new DataTable();
        dt = JsonConvert.DeserializeObject<DataTable>(ja.ToString());

        return dt;
    }

   /// <summary>
   /// 插入遊玩記錄
   /// </summary>
   /// <param name="gameId"></param>
   /// <param name="userId"></param>
   /// <returns>剛插入的id</returns>
    public int InsertPlayRecord(int gameId, string userId) {

        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?method=insert&&table=tb_play_record&&user_id=" + userId + "&&game_id=" + gameId);

        while (!www.isDone) { }

        return int.Parse(Common.GetNumber(www.text));
    }



    /// <summary>
    /// 添加問題信息
    /// </summary>
    /// <param name="per">該次遊玩的記錄</param>
    /// <returns>剛添加的問題id</returns>
    public int Insert_PlayEachRecordQ(PlayEachRecord per)
    {
        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?method=insert&&table=tb_question_info&&play_id=" + per.Play_id.ToString() + "&&question_info=" + per.Question_info+ "&&choice_info="+ per.Choice_info+ "&&question_difficulty="+ per.Question_difficulty.ToString());

        while (!www.isDone) { }

        return int.Parse(Common.GetNumber(www.text));
    }


    /// <summary>
    /// 添加答案信息
    /// </summary>
    /// <param name="per">該次遊玩的記錄</param>
    /// <param name="question_id">問題id</param>
    /// <returns>剛添加的答案id</returns>
    public int Insert_PlayEachRecordA(PlayEachRecord per, int question_id)
    {

        WWW www = new WWW("http://localhost/MorphoSqlAccess/getData.php?method=insert&&table=tb_answer_info&&question_id=" + question_id.ToString() + "&&answer_info=" + per.Answer_info + "&&answer_time=" + per.Answer_time + "&&show_time=" + per.Show_time);

        while (!www.isDone) { }

        return int.Parse(Common.GetNumber(www.text));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="child">幀節點</param>
    /// <param name="dt">動畫幀信息</param>
    /// <returns></returns>
    public int GetRoot(int child, DataTable dt) {
        //判斷是不是頂點坐標
        int parent = -1; 
        foreach (DataRow row in dt.Rows) {
            if (int.Parse(row["frame_id"].ToString()) == child) {
                parent = int.Parse(row["frame_last_id"].ToString());
            }
        }

        if (parent == 0) {
            return child;
        }else
        {
            return GetRoot(parent, dt);
        }
    }



}
