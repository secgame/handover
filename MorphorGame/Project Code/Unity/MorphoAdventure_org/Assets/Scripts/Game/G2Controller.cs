﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class G2Controller : MonoBehaviour {

    public GameObject gameBg; //遊戲背景

    public bool canRotate;//能否旋轉
    public int isTrue;//判斷對錯,-1是未選擇，0是錯，1是對
    public int ansTrue; //答案應該是正確或者錯誤
    public Vector3[] positions;
    public Vector3[] rotations;
    public int posRotIndex;

    public Text QuestionNo; //題目的UI

    public int queNum; //題目數量
    public int queNo; //題目序號
    public int correctNum;

    public GameObject Progress; // 進程
    public GameObject AddScore; //分數
    public GameObject Speakers; //喇叭


    public int play_id; //當前遊戲的標識

    public SpeakerController mySpeakerController;
    public ProgressController myProgressController;
    public AddScoreController myAddScoreController;

    // Use this for initialization
    void Start () {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(4);
        Debug.Log("G2Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        gameBg = GameObject.Find("gameContainer");
        isTrue = -1;
        posRotIndex = 0;
        canRotate = true;
        tellDisplay(false);

        Speakers = GameObject.Find("Speakers");

        positions = new Vector3[8] { new Vector3(9, 6, 0), new Vector3(12,0,0), new Vector3(9, -6, 0), new Vector3(0,-12,0), new Vector3(-9, -6, 0), new Vector3(-12,0,0),new Vector3(-9,6,0), new Vector3(0,12,0)};
        rotations = new Vector3[8] { new Vector3(0, 0, -65), new Vector3(0,0,-90), new Vector3(0, 0, -115), new Vector3(0,0,-180), new Vector3(0, 0, -245), new Vector3(0,0,90),new Vector3(0,0,65) ,new Vector3(0,0,0)};

        //定位體隱藏
        Speakers.transform.localPosition = positions[0];
        Speakers.transform.localRotation = Quaternion.Euler(rotations[0]);

        /**********************關於問題******************/
        //初始化題目數量
        queNo = 0;
        //初始化正確題目數量
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(2);

        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        changeQue();
    }
	
	// Update is called once per frame
	void Update () {
        if (canRotate) {
            gameBg.transform.Rotate(new Vector3(0f, 0f, 4f * Time.deltaTime));
        }

        if (isTrue > -1) {
            Progress.GetComponent<ProgressController>().FinishOneQue();

            //保存答案
            QuestionCreater.Instance.saveAns(isTrue.ToString());       

            //判斷是否正確
            if (isTrue == ansTrue) {
                correctNum++;
                myAddScoreController.play = true;
                Debug.Log("Score:" + correctNum);
            }
            //換題
            if (queNo == queNum)
            {//結束題目
             //把數據存到數據庫
                GameObject.Find("Main Camera").GetComponent<PlayDataSaveController>().AddRecord2DB();
                StartCoroutine("ToMain");
            }
            else {
                changeQue();
            }

            isTrue = -1;
            if (posRotIndex == 8) {
                posRotIndex = -1;
            }

            posRotIndex++;
            //定位體隱藏
            Speakers.transform.localPosition = positions[posRotIndex];
            Speakers.transform.localRotation = Quaternion.Euler(rotations[posRotIndex]);

            //修改下一個位置
            canRotate = true;

        }
	}

    IEnumerator ToMain()
    {
        yield return new WaitForSeconds(2);
        gameObject.GetComponent<UIGame>().GoToGame();
        StopCoroutine("ToMain");
    }


    public void changeCanRotate(bool val) {
        
        canRotate = val;
        
    }

    public void changeIsTrue(int val) {
        isTrue = val;
        tellDisplay(false);
    }

    public virtual void tellDisplay(bool val) {
        foreach (Transform tran in GameObject.Find("Player").transform) {
            tran.gameObject.SetActive(val);
        }
    }

    public void changeQue() {
        //數據清空
        mySpeakerController.ansIndex = -1;

        //先把喇叭全部隱藏
        mySpeakerController.resetSpeaker(0);

        //顯示序號
        queNo++;
        QuestionNo.text = queNo.ToString();

        QandA myQandA = QuestionCreater.Instance.NextQuestion(play_id);
        ansTrue = int.Parse(myQandA.AStr.ToString());

        //顯示題目
        mySpeakerController.showSpeaker(myQandA,3);

    }
}
