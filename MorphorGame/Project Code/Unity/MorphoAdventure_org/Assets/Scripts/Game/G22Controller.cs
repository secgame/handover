﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G22Controller : G1Controller
 {
    public Sprite[] BrickStatus; //0->石頭, //1->珊瑚

    public GameObject Bricks; //磚塊

    public BrickController myBrickController;



	// Use this for initialization
	void Start () {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(2);
        Debug.Log("G1Question:" + QuestionCreater.Instance.questionDB.Rows.Count);
        //初始化題目數量
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Bricks = GameObject.Find("Bricks");
        myBrickController = Bricks.GetComponent<BrickController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = false;
        changeQue();

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(22);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void SubmitAns()
    {
        if (mySpeakerController.chosenIndex == -1 || mySpeakerController.ansIndex == -1)
        {
            return;
        }

        int checkReturn = mySpeakerController.checkAns();
        if (checkReturn == 0)
        {
            return;
        }


        switch (checkReturn)
        {
            case 0:
                break;
            case 1:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[1];
                correctNum++;
                myAddScoreController.play = true;
                //(1) 修改選中的喇叭的tag
                int tmpIndex = mySpeakerController.speakerIndexArr[mySpeakerController.chosenIndex] + 1;
                GameObject.FindGameObjectWithTag("Speaker" + tmpIndex).tag = "Used";
                //(2) 修改對應磚塊的背景
                GameObject.Find("Brick" + tmpIndex).GetComponent<Image>().sprite = BrickStatus[1];
                break;
            case 2:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[0];
                break;
        }

        Progress.GetComponent<ProgressController>().FinishOneQue();
        Tell.SetActive(true);

        if (queNo == queNum)
        {//表示問題結束了
            Debug.Log("正確率:" + (double)correctNum / queNo);
            //把數據存到數據庫
            GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
            StartCoroutine("ToMain");
        }
        else
        {
            StartCoroutine("CreateAllChip");

        }
    }
}
