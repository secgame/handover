﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G8Controller : G5Controller {

    // Use this for initialization
    void Start()
    {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(4);
        Debug.Log("G5Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        gameBg = GameObject.Find("gameContainer");
        isTrue = -1;
        canRotate = true;
        tellDisplay(true);

        Speakers = GameObject.Find("Speakers");

        positions = new Vector3[1] { new Vector3(-8.6f, 0.1f, 0) };
        rotations = new Vector3[1] { new Vector3(0, 0, 0) };



        //定位體隱藏
        Speakers.transform.localPosition = positions[0];
        Speakers.transform.localRotation = Quaternion.Euler(rotations[0]);

        /**********************關於問題******************/
        //初始化題目數量
        queNo = 0;
        //初始化正確題目數量
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");


        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(8);

        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        changeQue();
    }

    // Update is called once per frame
    void Update()
    {
        if (canRotate)
        {
            gameBg.transform.Rotate(new Vector3(0f, 0f, 4f * Time.deltaTime));
            //喇叭要變旋轉邊移動
            Speakers.transform.Translate(1f * Time.deltaTime, 0f, 0f);
        }

        if (isTrue > -1)
        {
            Progress.GetComponent<ProgressController>().FinishOneQue();

            //保存答案
            QuestionCreater.Instance.saveAns(isTrue.ToString());

            //判斷是否正確
            if (isTrue == ansTrue)
            {
                correctNum++;
                AddScore.GetComponent<AddScoreController>().play = true;
                Debug.Log("Score:" + correctNum);
            }
            //換題
            if (queNo == queNum)
            {//結束題目
             //把數據存到數據庫
                GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
                StartCoroutine("ToMain");
            }
            else
            {
                changeQue();
            }

            isTrue = -1;

            //定位體隱藏
            Speakers.transform.localPosition = positions[0];
            Speakers.transform.localRotation = Quaternion.Euler(rotations[0]);

            //修改下一個位置
            canRotate = true;

        }
    }
}
