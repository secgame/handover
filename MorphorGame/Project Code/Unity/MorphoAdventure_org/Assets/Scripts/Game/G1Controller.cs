﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using UnityEngine.UI;
using System;

public class G1Controller : MonoBehaviour {
    public Sprite[] Tells; //判斷圖片集合
    /***遊戲組件***/
    public GameObject Progress; // 進程
    public GameObject AddScore; //分數
    public GameObject Speakers; //喇叭

    public Text QuestionNo; //題目的UI
    public GameObject Tell; //判斷正確與否的UI
    public bool next; //是否換下一道題

    public int correctNum; //記錄題型的正確數目
    public int queNum; //題目數量
    public int queNo; //題目序號

    public int play_id; //當前遊戲的標識

    public SpeakerController mySpeakerController;
    public ProgressController myProgressController;
    public AddScoreController myAddScoreController;

    // Use this for initialization
    void Start () {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(2);
        Debug.Log("G1Question:" + QuestionCreater.Instance.questionDB.Rows.Count);
        //初始化題目數量
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = false;
        changeQue();

        play_id =  GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(1);
    }
	
	// Update is called once per frame
	void Update () {

	}

    public virtual void SubmitAns() {
        if (mySpeakerController.chosenIndex == -1 || mySpeakerController.ansIndex == -1) {
            return;
        }

        int checkReturn = mySpeakerController.checkAns();
        if (checkReturn == 0)
        {
            return;
        }
       

        switch (checkReturn) {
            case 0:
                break;
            case 1:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[1];
                correctNum++;
                myAddScoreController.play = true;
                break;
            case 2:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[0];
                break;
        }

        Progress.GetComponent<ProgressController>().FinishOneQue();
        Tell.SetActive(true);

        if (queNo == queNum)
        {//表示問題結束了
            Debug.Log("正確率:" + (double)correctNum /queNo);
            //把數據存到數據庫
            GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
            StartCoroutine("ToMain");
        }
        else {
            StartCoroutine("CreateAllChip");
            
        }
        
    }

    IEnumerator CreateAllChip()
    {
       yield return new WaitForSeconds(2);
        changeQue();      
        StopCoroutine("CreateAllChip");


    }

    IEnumerator ToMain() {
        yield return new WaitForSeconds(2);
        gameObject.GetComponent<UIGame>().GoToGame();
        StopCoroutine("ToMain");
    }

    public virtual void changeQue() {
        //數據清空
        mySpeakerController.ansIndex = -1;
        mySpeakerController.chosenIndex = -1;
        Tell.SetActive(false);

        //先把喇叭全部隱藏
        mySpeakerController.resetSpeaker(0);

        //顯示序號
        queNo++;
        QuestionNo.text = queNo.ToString();

        //顯示題目
        QandA myQandA = QuestionCreater.Instance.NextQuestion(play_id);

        //隨機改變ansIndex的序號
        mySpeakerController.ansIndex = Mathf.FloorToInt(UnityEngine.Random.Range(0, myQandA.QArr.Length));

        //顯示題目
        mySpeakerController.showSpeaker(myQandA,1);

        //修改是否現實下一題
        next = false;
    }
}
