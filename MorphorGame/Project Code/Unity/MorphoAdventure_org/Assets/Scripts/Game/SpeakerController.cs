﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SpeakerController : MonoBehaviour {

    public Transform[] speakerArr; //喇叭集合
    public int[] speakerIndexArr;
    public int maxSpeaker; //最大的喇叭數量
    public int total;//最初的喇叭書

    public class SpeakerStatus {
        private Sprite active;
        private Sprite inactive;

        public Sprite Active {
            get;
            set;
        }

        public Sprite Inactive{
            get;
            set;
        }
    }

    public Sprite[] StatusArr; //狀態集合

    public float curScale; //物體當前的比例
    public float endScale; //物體目標的比例
    public bool changeScale; //是否修改比例
    public bool godown; //是否下移
    public bool goup; //是否上移
    public bool goleft;//是否左移
    public bool goright;//是否右移
    public bool canFly;
    public bool next;//已運轉完
    public float[] boundary; //邊界 4個，分別是上下左右
    public GameObject QuestionObj; //找相同題型中的題目
    public int chosenIndex; //選中的喇叭
    public int ansIndex; //正確的選項
    public bool G2; //能夠撞擊答案發生點事情? =>前面追趕
    public bool G5; //能夠撞擊答案發生點事情? =>後面追趕
    public bool G6; //能夠撞擊答案發生點事情? => 畫面從小到大
    public bool G7; //能夠撞擊答案發生點事情? =》破轉

    public bool beginChange;//開始換新的題

    int arryLength;//實際出現的喇叭數目


    // Use this for initialization
    void Awake () {
        if (total == 0)
        {
            total = maxSpeaker;
        }

        speakerArr = new Transform[total];


        chosenIndex = 0;

        if (QuestionCreater.Instance.questionType == 2) {
            QuestionObj = GameObject.Find("Question");
        }

        int tmpIndex = 0;
        foreach (Transform speaker in transform) {
            speakerArr[tmpIndex] = speaker;
            tmpIndex++;
        }

        


        beginChange = false;

        

        

    }

    /// <summary>
    /// 修改題目圖標的選中狀態
    /// </summary>
    /// <param name="mess"></param>
    public void changeState(string mess) {
        chosenIndex = int.Parse(mess) - 1;
        Debug.Log("chose:" + chosenIndex);
        //全部恢復未選中
        for (int i = 0; i < speakerArr.Length; i++){
            speakerArr[i].GetComponent<Image>().sprite = StatusArr[0];
        }

        //顯示選中
        speakerArr[chosenIndex].GetComponent<Image>().sprite = StatusArr[1];

        if (G7) {
            chosenIndex = Array.IndexOf(speakerIndexArr, chosenIndex);
        }

    }

    /// <summary>
    /// 重新給要顯示出來的speaker編序號
    /// </summary>
    public void GetContainerIndex(int length) {

        speakerIndexArr = new int[length];

        int random = -1;
        //先找出所有有tag的speaker
        for (int i = 0; i < length; i++) {
            int num = 0;
            /*判斷一下是否有tag，有則給其編號*/
            do
            {
                random = Mathf.FloorToInt(UnityEngine.Random.Range(0, total));
                num = Array.IndexOf(speakerIndexArr, random);
                speakerIndexArr[i] = random;
                Debug.Log("number:" + random);
                Debug.Log("position:" + Array.IndexOf(speakerIndexArr, random));
            }while (speakerArr[random].tag == "Used" || num > -1);
    
            
        }
    }


    //設置1-》可見 0-》不可見
    /// <summary>
    /// 設置題目圖標的可見狀態
    /// </summary>
    /// <param name="isVisible"></param>
    public void resetSpeaker(int isVisible) {
        for (int i = 0; i < speakerArr.Length; i++) {
            Debug.Log(StatusArr[0]);
            speakerArr[i].GetComponent<Image>().sprite = StatusArr[0];
            if (isVisible == 0) {
                ResetScale();
                speakerArr[i].gameObject.SetActive(false);
            }else if(isVisible == 1)
                speakerArr[i].gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// 把喇叭復位
    /// </summary>
    /// <param name="vec"></param>
    /// <param name="direction">1上2下3左4右</param>
    public void resetSpeaker(Vector3 vec,int direction) {

        switch (direction) {
            case 1:
                godown = true;
                break;
            case 2:
                goup = true;
                break;
            case 3:
                goleft = true;
                break;
            case 4:
                goright = true;
                break;
            
        }

        transform.localPosition = vec;
        //畫面停止下移
        gameObject.SendMessageUpwards("changeRun", true, SendMessageOptions.DontRequireReceiver);


    }

    /// <summary>
    /// 顯示題目->找不同
    /// </summary>
    /// <param name="myQandA"></param>
    /// /// <param name="type">1->找不同；2->找相同;3->判斷</param>
    public void showSpeaker(QandA myQandA,int type) {
        int tmpIndex = 0;
        arryLength = 0;
        if (type == 1)
        {
            arryLength = myQandA.QArr.Length + 1;
        }
        else if (type == 2)
        {
            arryLength = myQandA.QArr.Length;
            //把答案調換位置
            string tmp = myQandA.QArr[myQandA.QArr.Length - 1].ToString();
            myQandA.QArr[myQandA.QArr.Length - 1] = myQandA.QArr[ansIndex];
            myQandA.QArr[ansIndex] = tmp;
        }
        else if (type == 3) {
            arryLength = myQandA.QArr.Length;
        }
        if (G7)
        {
            GetContainerIndex(arryLength);
        }
        else {
            speakerIndexArr = new int[arryLength];
            for (int i = 0; i < arryLength; i++) {
                speakerIndexArr[i] = i;
            }
        }
        

        //開始顯示喇叭
        for (int j = 0; j < arryLength; j++) {
            int i = speakerIndexArr[j];

            speakerArr[i].gameObject.SetActive(true);

            char[] chars;
            if (ansIndex == j && type == 1)
            {
                chars = myQandA.AStr.ToCharArray();
                speakerArr[i].name = myQandA.AStr;
            }
            else {
                Debug.Log(tmpIndex );
                Debug.Log(":" + myQandA.QArr[tmpIndex]);
                chars = myQandA.QArr[tmpIndex].ToCharArray();
                speakerArr[i].name = myQandA.QArr[tmpIndex];
                tmpIndex++;
            }

            //獲取三個文字的位置
            int wordTmpIndex = 0;
            foreach (Transform word in speakerArr[i]) {
                //隱藏
                word.gameObject.SetActive(false);
                //放文字
                if (wordTmpIndex < chars.Length && chars[wordTmpIndex].ToString() != "") {
                    word.GetComponent<Text>().text = chars[wordTmpIndex].ToString();
                    word.gameObject.SetActive(true);
                    wordTmpIndex++;
                }
            }
        }
    }


    /// <summary>
    /// 保存答案
    /// </summary>
    /// <returns>
    /// 0-> 不好存
    /// 1-> 回答正確
    /// 2-> 回答錯誤
    /// </returns>
    public int checkAns() {
        if (chosenIndex == -1 || ansIndex == -1)
        {
            return 0;
        }
        else {
            QuestionCreater.Instance.saveAns(speakerArr[chosenIndex].name);
            if (chosenIndex == ansIndex)
            {
                return 1;
            }

            if (chosenIndex != ansIndex)
            {
                return 2;
            }
 
        }


        return 0;

    }

    /// <summary>
    /// 遊戲人物碰到玩家產生的觸碰事件
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other) {
        if (G2) {
            if (other.tag == "Player") {
                gameObject.SendMessageUpwards("changeCanRotate", false, SendMessageOptions.RequireReceiver);
                gameObject.SendMessageUpwards("tellDisplay", true, SendMessageOptions.RequireReceiver);
                gameObject.SendMessageUpwards("CanChase", true, SendMessageOptions.DontRequireReceiver);
            }
        }

        if (G5) {
            if (other.tag == "Player")
            {
                //碰到就輸啦
                gameObject.SendMessageUpwards("changeIsTrue", 2, SendMessageOptions.RequireReceiver);
                Debug.Log("超時！");

            }
        }

        if (G6) {
            if (other.tag == "Player") {
                //畫面停止下移
                gameObject.SendMessageUpwards("changeRun", false, SendMessageOptions.RequireReceiver);
                //喇叭本身停止下移
                godown = false;

            }
        }

        if (other.tag == "upBarrier") {
            //喇叭本身停止上移
            goup = false;
            Debug.Log("碰到了");
            //輸了，換一題
            gameObject.SendMessageUpwards("Submit", SendMessageOptions.RequireReceiver);
        }

        if (other.tag == "leftBarrier") {
            goleft = false;
            gameObject.SendMessageUpwards("Submit", SendMessageOptions.RequireReceiver);
        }

        if (other.tag == "rightBarrier")
        {
            goright = false;
            gameObject.SendMessageUpwards("Submit", SendMessageOptions.RequireReceiver);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (changeScale && curScale < endScale)
        {
            for (int i = 0; i < speakerArr.Length; i++)
            {
                speakerArr[i].localScale = new Vector3(curScale, curScale, 0);
                speakerArr[i].localRotation = Quaternion.Euler(new Vector3(0, 0, curScale * 180));
                curScale = curScale + 0.2f * Time.deltaTime;
            }
        }

        if (godown) {
            transform.Translate(new Vector3(0, -0.05f, 0));
        }

        if (goup)
        {
            transform.Translate(new Vector3(0, 0.01f, 0));
        }

        if (goleft) {
            transform.Translate(new Vector3(-0.01f,0, 0));
        }

        if (goright)
        {
            transform.Translate(new Vector3(0.01f, 0, 0));
        }

        if (canFly&&next) {
            next = false;
            Vector3 endpos =new Vector3(0,0,0);
            int direction = 0;
            bool again = false;

            //一個一個來
            for (int i = 0; i < arryLength; i++) {
                //找到合適距離
                float distance = UnityEngine.Random.Range(0.2f, 0.6f);
                again = false;

                //找到合適方向
                do
                {
                    direction = Mathf.FloorToInt(UnityEngine.Random.Range(1, 4.9f));

                    switch (direction)
                    {
                        case 1:
                            if (boundary[0] - speakerArr[i].transform.localPosition.y < 0.01)
                                again = true;
                            else
                                endpos = new Vector3(speakerArr[i].transform.localPosition.x, speakerArr[i].transform.localPosition.y + distance, 0);
                            break;
                        case 2:
                            if (speakerArr[i].transform.localPosition.y - boundary[1] < 0.01)
                                again = true;
                            else
                                endpos = new Vector3(speakerArr[i].transform.localPosition.x, speakerArr[i].transform.localPosition.y - distance, 0);
                            break;
                        case 3:
                            if (speakerArr[i].transform.localPosition.x - boundary[2] < 0.01)
                                again = true;
                            else
                                endpos = new Vector3(speakerArr[i].transform.localPosition.x-distance, speakerArr[i].transform.localPosition.y, 0);
                            break;
                        case 4:
                            if (boundary[3] - speakerArr[i].transform.localPosition.x < 0.01)
                                again = true;
                            else
                                endpos = new Vector3(speakerArr[i].transform.localPosition.x+distance, speakerArr[i].transform.localPosition.y, 0);
                            break;

                    }

                } while (again==false);

                Debug.Log("i:" + direction);
                Debug.Log(endpos);
                //移動
                speakerArr[i].transform.Translate(endpos);

            }

            next = true;
        }
    }

    public void ResetScale()
    {
        curScale = 0;
    }

}
