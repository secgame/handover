﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G16Controller : G3Controller {

    public Vector3 vec;
    public int moveDirection;

    // Use this for initialization
    void Start () {
        //獲取題目
        QuestionCreater.Instance.ChangeQuestionDB(3);

        /*****************初始化題目信息*********************/
        //初始化題目序號
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        //獲取問題載體
        Question = GameObject.Find("Question");

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = true;

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(16);
    }
	
	// Update is called once per frame
	void Update () {
        if (next)
        {
            changeQue();
            next = false;
        }
    }

    public override void SubmitAns()
    {
        base.SubmitAns();

        if (mySpeakerController.ansIndex == -1) {
            mySpeakerController.goup = false;
        }

        
    }

    public override void changeQue()
    {
        base.changeQue();

        //把喇叭復位
        mySpeakerController.resetSpeaker(vec, moveDirection);
    }



}
