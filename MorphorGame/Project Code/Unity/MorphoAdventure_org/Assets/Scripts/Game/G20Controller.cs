﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G20Controller : G6Controller{

	// Use this for initialization
	void Start () {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(2);
        Debug.Log("G1Question:" + QuestionCreater.Instance.questionDB.Rows.Count);
        //初始化題目數量
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = false;
        changeQue();

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(20);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void SubmitAns()
    {
        base.SubmitAns();

        if (mySpeakerController.ansIndex == -1)
        {
            mySpeakerController.goright = false;
        }


    }

    public override void changeQue()
    {
        base.changeQue();

        //把喇叭復位
        mySpeakerController.resetSpeaker(vec, moveDirection);
    }
}
