﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="startpos">生成的地方</param>
    /// <param name="endpos">移動到的地方</param>
    public void CreateBrick(Vector3 startpos, Vector3 endpos)
    {
        //初始化
        GameObject newBrick = Instantiate(Resources.Load("Element/OneBrick"), startpos, Quaternion.identity,transform) as GameObject;

        //移到終點
        newBrick.transform.localPosition = Vector3.Lerp(startpos, endpos, 1.0f);
        
    }
}
