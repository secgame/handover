﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G21Controller : G3Controller {
    GameObject rightBoundary;
    public Vector3 vec;

    // Use this for initialization
    void Start () {
        //獲取題目
        QuestionCreater.Instance.ChangeQuestionDB(3);
        Debug.Log("G3Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        /*****************初始化題目信息*********************/
        //初始化題目序號
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        //獲取問題載體
        Question = GameObject.Find("Question");

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        rightBoundary = GameObject.Find("rightBoundary");

        next = false;
        changeQue();
        

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(21);
    }
	
	// Update is called once per frame
	void Update () {
        if (qDirection == 4) {
            //移動
            Question.transform.Translate(new Vector3(0.01f, 0, 0));

            //判斷和RightBoundary的距離
            if (rightBoundary.transform.localPosition.x - Question.transform.localPosition.x < 1f) {
                //判定答錯
                Submit();
            }
        }
	}

    public void resetPos() {
        Question.transform.localPosition = vec;
    }

    public override void Submit()
    {
        base.Submit();
        resetPos();
    }

    public override void SubmitAns()
    {
        base.SubmitAns();
        resetPos();
    }
}
