﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G9Controller : G2Controller {
    public GameObject myTimer;
    public GameObject myPlayer;
    public Vector3 TimerPos;
    public bool canChase;

    void Start()
    {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(4);
        Debug.Log("G2Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        gameBg = GameObject.Find("gameContainer");
        isTrue = -1;
        posRotIndex = 0;
        canRotate = true;
        canChase = false;
        tellDisplay(false);

        Speakers = GameObject.Find("Speakers");

        positions = new Vector3[8] { new Vector3(9, 6, 0), new Vector3(12, 0, 0), new Vector3(9, -6, 0), new Vector3(0, -12, 0), new Vector3(-9, -6, 0), new Vector3(-12, 0, 0), new Vector3(-9, 6, 0), new Vector3(0, 12, 0) };
        rotations = new Vector3[8] { new Vector3(0, 0, -65), new Vector3(0, 0, -90), new Vector3(0, 0, -115), new Vector3(0, 0, -180), new Vector3(0, 0, -245), new Vector3(0, 0, 90), new Vector3(0, 0, 65), new Vector3(0, 0, 0) };

        //定位體隱藏
        Speakers.transform.localPosition = positions[0];
        Speakers.transform.localRotation = Quaternion.Euler(rotations[0]);

        /**********************關於問題******************/
        //初始化題目數量
        queNo = 0;
        //初始化正確題目數量
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        myTimer = GameObject.Find("Timer");
        myPlayer = GameObject.Find("Player");


        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(9);

        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        changeQue();
    }

    // Update is called once per frame
    void Update()
    {
        //關於追趕的人兒
        if (myPlayer.transform.localPosition.x - myTimer.transform.localPosition.x < 3.0f)
        {
            //輸了
            isTrue = 2;
            CanChase(false);

        }//觸碰到以後，人物就可以追逐了
        else if(canChase)
        {
            myTimer.transform.Translate(new Vector3(0.005f, 0f, 0f));
        }


        if (canRotate)
        {
            gameBg.transform.Rotate(new Vector3(0f, 0f, 4f * Time.deltaTime));
        }

        if (isTrue > -1)
        {
            Progress.GetComponent<ProgressController>().FinishOneQue();

            //保存答案
            QuestionCreater.Instance.saveAns(isTrue.ToString());

            //判斷是否正確
            if (isTrue == ansTrue)
            {
                correctNum++;
                myAddScoreController.play = true;
            }
            //換題
            if (queNo == queNum)
            {//結束題目
             //把數據存到數據庫
                GameObject.Find("Main Camera").GetComponent<PlayDataSaveController>().AddRecord2DB();
                StartCoroutine("ToMain");
            }
            else
            {
                changeQue();
            }

            isTrue = -1;
            if (posRotIndex == 8)
            {
                posRotIndex = -1;
            }

            posRotIndex++;
            //定位體隱藏
            Speakers.transform.localPosition = positions[posRotIndex];
            Speakers.transform.localRotation = Quaternion.Euler(rotations[posRotIndex]);

            //修改下一個位置
            canRotate = true;

            CanChase(false);

        }
    }

    public void CanChase(bool value) {
        myTimer.transform.localPosition = TimerPos;
        canChase = value;
    }



}
