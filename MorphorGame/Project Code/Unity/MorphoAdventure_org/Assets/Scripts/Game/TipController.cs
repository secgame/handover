﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipController : MonoBehaviour {
    public Vector3 beginPonit;//min
    public Vector3 endPoint; //max
    public Vector3 Dest;
    public bool running;
    public int tipType;

    // Use this for initialization
    void Start () {
        running = true;
        Dest = beginPonit;
        tipType = 1;
    }
	
	// Update is called once per frame
    /// <summary>
    /// 提示標籤根據類型參數顯示
    /// </summary>
	void Update () {
        if (tipType == 1) {
            if (transform.position == Dest)
            {
                if (Dest == beginPonit)
                    Dest = endPoint;
                else
                    Dest = beginPonit;
            }


            if (running)
            {
                transform.position = Vector3.Lerp(transform.position, Dest, 0.5f);
            }
        }
        
           
       
       

	}

    public void stopRunning() {
        running = false;
    }
}
