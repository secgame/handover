﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class AddScoreController : MonoBehaviour {

    public Vector3 beginPosition;
    public Vector3 endPosition;
    public bool play;
    float isBigEnough;
    bool reset;

    GameObject Hud;
    GameObject Ten;
    GameObject Sin;
    GameObject getScoreEffect;

    Texture2D texture2d;
    public Sprite sprite2D;
    public Sprite emptySpr;
    public Sprite[] numSpr;


    //public AudioClip AC;

    // Use this for initialization
    void Awake () {
        play = false;
        isBigEnough = 0;
        reset = false;

        Hud = GameObject.Find("Hud");
        Ten = GameObject.Find("Ten");
        Sin = GameObject.Find("Sin");

        getScoreEffect = GameObject.Find("getScore");
        getScoreEffect.GetComponent<ParticleSystem>().Stop();


    }
	

    /// <summary>
    /// 檢測加分鍵的出現、消失和變化
    /// </summary>
	// Update is called once per frame
	void Update () {
        if (play) {
            if (reset)
            {
                if (isBigEnough > 0.016)
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, endPosition, 0.2f);
              
                }
                else {
                    isBigEnough = isBigEnough+ 0.02f * Time.deltaTime;
                    transform.localScale = new Vector3(isBigEnough, isBigEnough, 0);
                }
               
                
            }
            else {
                Debug.Log("Add5初始化");              
                gameObject.GetComponent<Image>().sprite = sprite2D;
                transform.localPosition = beginPosition;
                transform.localScale = new Vector3(0,0,0);
                getScoreEffect.GetComponent<ParticleSystem>().Stop();
                reset = true;
           }
        }
	}

    /// <summary>
    /// 碰撞后顯示例子效果和分數組件改變
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other) {

        if (other.tag == "Score" && play == true) {
            Debug.Log("撞到了");
            //AudioSource.PlayClipAtPoint(AC, transform.localPosition);
            reset = false;
            play = false;
            isBigEnough = 0;
            //隱藏+5的符號
            transform.GetComponent<Image>().sprite = emptySpr;
            //啟動加分策略
            //先獲得現在的分數

            MatchCollection huns = Regex.Matches(Hud.GetComponent<Image>().sprite.name, @"(\d+)");
            MatchCollection tens = Regex.Matches(Ten.GetComponent<Image>().sprite.name, @"(\d+)");
            MatchCollection sins = Regex.Matches(Sin.GetComponent<Image>().sprite.name, @"(\d+)");

            int hun = int.Parse(huns[0].Value);
            int ten = int.Parse(tens[0].Value);
            int sin = int.Parse(sins[0].Value);

            //Debug.Log("hun:" + hun+ ",ten:" + ten+ ",sin:" + sin);

            int tmpScore = hun* 100 + ten* 10 + sin;
            tmpScore = tmpScore+ 5;
            //Debug.Log("tmpScore:" + tmpScore);

            char[] chars = tmpScore.ToString().ToCharArray();

            if (chars.Length == 1) {
                hun = 0;
                ten = 0;
                sin = int.Parse(chars[0].ToString());
            }

            if (chars.Length == 2) {
                hun = 0;
                ten = int.Parse(chars[0].ToString());
                sin = int.Parse(chars[1].ToString());
            }

            if (chars.Length == 3) {
                hun = int.Parse(chars[0].ToString()); ;
                ten = int.Parse(chars[1].ToString()); ;
                sin = int.Parse(chars[2].ToString()); ;
            }

            Hud.GetComponent<Image>().sprite = numSpr[hun];
            Ten.GetComponent<Image>().sprite = numSpr[ten];
            Sin.GetComponent<Image>().sprite = numSpr[sin];

            getScoreEffect.GetComponent<ParticleSystem>().Play();

        }
    }

    
}
