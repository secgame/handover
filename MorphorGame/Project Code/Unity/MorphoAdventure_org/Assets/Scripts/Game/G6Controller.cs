﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G6Controller : MonoBehaviour {
    public GameObject gameContainer;
    public bool run; //人物是否跑動
    public bool next; //是否更換題目

    public Sprite[] Tells; //判斷圖片集合
    /***遊戲組件***/
    public GameObject Progress; //進程
    public GameObject AddScore; //分數
    public GameObject Speakers; //喇叭

    public Text QuestionNo; //題目的UI
    public GameObject Tell;//判斷正確與否的UI
    public GameObject Next_btn; //選擇的按鈕

    public int queNum; //題目數量
    public int queNo; //題目序號
    public int correctNum; //記錄題型的正確數量

    public int play_id; //當前遊戲的標籤

    public SpeakerController mySpeakerController;
    public ProgressController myProgressController;
    public AddScoreController myAddScoreController;

    public Vector3 vec; //復位的位置
    public int moveDirection;

    // Use this for initialization
    void Start () {
        //獲取題目
        QuestionCreater.Instance.ChangeQuestionDB(2);


        /*****************初始化題目信息*******************/
        //初始化題目序號
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        //初始化插件
        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);


        gameContainer = GameObject.Find("gameContainer");
        next = true;
        run = true;
        Next_btn = GameObject.Find("Next");

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(6);

        changeQue();

    }
	
	// Update is called once per frame
	void Update () {
        //圖片一直變大然後向下移動
        if (run)
        {
            //變大
            gameContainer.transform.localScale = new Vector3(gameContainer.transform.localScale.x + 0.001f, gameContainer.transform.localScale.y + 0.001f, gameContainer.transform.localScale.z);
            //向下移動
            gameContainer.transform.Translate(new Vector3(0, -0.003f, 0));

            Next_btn.SetActive(false);

        }
        else {
            Next_btn.SetActive(true);
        }
	}


    /// <summary>
    /// 改變是否奔跑的狀態
    /// </summary>
    /// <param name="value"></param>
    public void changeRun(bool value) {
        run = value;
    }


    /// <summary>
    /// 提交答案
    /// </summary>
    public virtual void SubmitAns() {
        if (mySpeakerController.chosenIndex == -1 || mySpeakerController.ansIndex == -1) {
            return;
        }

        int checkReturn = mySpeakerController.checkAns();
        if (checkReturn == 0) {
            return;
        }

        switch (checkReturn) {
            case 0:
                break;
            case 1:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[1];
                correctNum++;
                myAddScoreController.play = true;
                break;
            case 2:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[0];
                break;
        }

        Progress.GetComponent<ProgressController>().FinishOneQue();
        Tell.SetActive(true);

        if (queNo == queNum)
        {
            //表示問題結束了
            Debug.Log("正確率：" + (double)correctNum / queNo);
            //把數據存到數據庫
            GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
            StartCoroutine("ToMain");
        }
        else {
            StartCoroutine("CreateAllChip");
        }
    }


    IEnumerator CreateAllChip() {
        yield return new WaitForSeconds(2);
        changeQue();
        StopCoroutine("CreateAllChip");
    }

    /// <summary>
    /// 返回主界面
    /// </summary>
    /// <returns></returns>
    IEnumerator ToMain() {
        yield return new WaitForSeconds(2);
        gameObject.GetComponent<UIGame>().GoToGame();
        StopCoroutine("ToMain");
    }


    //更換題目
    public virtual void changeQue()
    {
        //清空數據
        mySpeakerController.ansIndex = -1;
        mySpeakerController.chosenIndex = -1;
        Tell.SetActive(false);

        //先把喇叭全部隱藏
        mySpeakerController.resetSpeaker(0);

        //把喇叭復位
        mySpeakerController.resetSpeaker(vec, moveDirection);

        //修改序號
        queNo++;
        QuestionNo.text = queNo.ToString();

        //顯示題目
        QandA myQandA = QuestionCreater.Instance.NextQuestion(play_id);

        //隨機改變ansIndex的序號
        mySpeakerController.ansIndex = Mathf.FloorToInt(UnityEngine.Random.Range(0, myQandA.QArr.Length));

        //顯示題目
        mySpeakerController.showSpeaker(myQandA, 1);

        //修改是否換題
        next = false;



    }

    public void Submit()
    {
        QuestionCreater.Instance.saveAns("");
        Progress.GetComponent<ProgressController>().FinishOneQue();

        Tell.GetComponent<SpriteRenderer>().sprite = Tells[1];

        Tell.SetActive(true);

        if (queNo == queNum)
        {//表示問題結束了
            Debug.Log("正確率:" + (double)correctNum / queNo);
            //把數據存到數據庫
            GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
            StartCoroutine("ToMain");
        }
        else
        {
            StartCoroutine("CreateAllChip");

        }
    }

}
