﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G17Controller : G6Controller {

	// Use this for initialization
	void Start () {
        //獲取題目
        QuestionCreater.Instance.ChangeQuestionDB(2);


        /*****************初始化題目信息*******************/
        //初始化題目序號
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        //初始化插件
        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);


        gameContainer = GameObject.Find("gameContainer");
        next = true;
        run = true;
        Next_btn = GameObject.Find("Next");

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(17);

        changeQue();
    }
	
	// Update is called once per frame
	void Update () {
        //圖片一直變大然後向下移動
        if (run)
        {
            //變大
            gameContainer.transform.localScale = new Vector3(gameContainer.transform.localScale.x + 0.001f, gameContainer.transform.localScale.y + 0.001f, gameContainer.transform.localScale.z);
            //向下移動
            gameContainer.transform.Translate(new Vector3(0, -0.003f, 0));

            Next_btn.SetActive(false);

        }
        else
        {
            Next_btn.SetActive(true);
        }
    }
}
