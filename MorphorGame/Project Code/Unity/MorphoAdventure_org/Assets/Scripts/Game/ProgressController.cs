﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour {
    public int queNum;
    Transform[] bars;

	// Use this for initialization
	public void init (int num) {
        //初始化
        queNum = num;
        Debug.Log("queNum:"+queNum);
        bars = new Transform[20];
        int tmpindex = 0;
        foreach (Transform trans in transform) {
            bars[tmpindex] = trans;
            bars[tmpindex].gameObject.SetActive(false);
            if (tmpindex < queNum) {
                bars[tmpindex].GetComponent<Image>().color = Color.gray;
                bars[tmpindex].gameObject.SetActive(true);
            }
            
            tmpindex++;
        }
	}

    /// <summary>
    /// 完成一到選擇后進度條變化
    /// </summary>
    public void FinishOneQue() {
        for (int i = 0; i < queNum; i++) {
            if (bars[i].GetComponent<Image>().color == Color.gray) {
                bars[i].GetComponent<Image>().color = Color.green;
                return;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
