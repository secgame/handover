﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G10Controller : G3Controller {

    public GameObject Scale;

	// Use this for initialization
	 void Start () {
        //獲取題目
        QuestionCreater.Instance.ChangeQuestionDB(3);
        Debug.Log("G3Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        /*****************初始化題目信息*********************/
        //初始化題目序號
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        //獲取問題載體
        Question = GameObject.Find("Question");

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Scale = GameObject.Find("Scale");

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = true;

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(10);
    }
	
	// Update is called once per frame
	void Update () {
        if (next)
        {
            changeQue();
            next = false;
        }
    }

    public override void SubmitAns()
    {
        if (mySpeakerController.chosenIndex == -1 || mySpeakerController.ansIndex == -1)
        {
            return;
        }

        int checkReturn = mySpeakerController.checkAns();
        if (checkReturn == 0)
        {
            return;
        }

        switch (checkReturn)
        {
            case 0:
                break;
            case 1:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[0];
                correctNum++;
                myAddScoreController.play = true;

                //容量添加
                Scale.transform.localScale=new Vector3(0.26f, Scale.transform.localScale.y + 0.32f, 0f);
                //位置上移
                Scale.transform.Translate(new Vector3(0f, 0.16f, 0));

                break;
            case 2:
                Tell.GetComponent<SpriteRenderer>().sprite = Tells[1];
                break;
        }

        Progress.GetComponent<ProgressController>().FinishOneQue();
        Tell.SetActive(true);

        if (queNo == queNum)
        {//表示問題結束了
            Debug.Log("正確率:" + (double)correctNum / queNo);
            //把數據存到數據庫
            GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddRecord2DB();
            StartCoroutine("ToMain");
        }
        else
        {
            StartCoroutine("CreateAllChip");

        }
    }


}
