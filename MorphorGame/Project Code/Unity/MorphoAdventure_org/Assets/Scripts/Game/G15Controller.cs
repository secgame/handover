﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G15Controller : G2Controller {

    public GameObject[] Dusts;
    public Vector3[] DustsPos;
    public GameObject S2;

	// Use this for initialization
	void Start () {
        //獲取題目      
        QuestionCreater.Instance.ChangeQuestionDB(4);

        gameBg = GameObject.Find("gameContainer");
        isTrue = -1;
        tellDisplay(false);

        Speakers = GameObject.Find("Speakers");

        /**********************關於問題******************/
        //初始化題目數量
        queNo = 0;
        //初始化正確題目數量
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(15);

        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        changeQue();
    }
	
	// Update is called once per frame
	void Update () {

        //喇叭移位
        S2.transform.localPosition = DustsPos[queNo - 1];


        if (isTrue > -1)
        {
            Progress.GetComponent<ProgressController>().FinishOneQue();

            //保存答案
            QuestionCreater.Instance.saveAns(isTrue.ToString());

            //判斷是否正確
            if (isTrue == ansTrue)
            {
                correctNum++;
                myAddScoreController.play = true;
                Debug.Log("Score:" + correctNum);

                //如果正確，印漬消失
                GameObject.Find("Dust" + queNo).SetActive(false);

            }

            //換題
            if (queNo == queNum)
            {//結束題目
             //把數據存到數據庫
                GameObject.Find("Main Camera").GetComponent<PlayDataSaveController>().AddRecord2DB();
                StartCoroutine("ToMain");
            }
            else
            {
                changeQue();
            }

            isTrue = -1;

        }
    }

    public override void tellDisplay(bool val)
    {
        
    }
}
