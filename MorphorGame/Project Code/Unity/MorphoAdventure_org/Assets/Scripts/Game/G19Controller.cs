﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class G19Controller : G3Controller {

	// Use this for initialization
	void Start () {
        QuestionCreater.Instance.ChangeQuestionDB(3);
        Debug.Log("G7Question:" + QuestionCreater.Instance.questionDB.Rows.Count);

        //初始化題目數量
        queNo = 0;
        //初始化正確答題數
        correctNum = 0;

        //題目的序號的載體
        QuestionNo = GameObject.Find("QueNo").GetComponent<Text>();

        Question = GameObject.Find("Question");

        Progress = GameObject.Find("Progress");
        myProgressController = Progress.GetComponent<ProgressController>();
        myProgressController.init(queNum);

        AddScore = GameObject.Find("add");
        myAddScoreController = AddScore.GetComponent<AddScoreController>();

        Speakers = GameObject.Find("Speakers");
        mySpeakerController = Speakers.GetComponent<SpeakerController>();

        Tell = GameObject.Find("Tell");
        Tell.SetActive(false);

        next = false;
        changeQue();

        play_id = GameObject.Find("Main Camera").transform.GetComponent<PlayDataSaveController>().AddPlayRecord(19);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
