﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UIManager : MonoBehaviour {

    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }

    //存放現有的面板的棧
    private Stack<UIBase> UIStack = new Stack<UIBase>();

    //存放已入棧的面板的字典
    private Dictionary<string, UIBase> currentUIDict = new Dictionary<string, UIBase>();

    //存放所有的面板信息
    private Dictionary<string, GameObject> UIObjectDict = new Dictionary<string, GameObject>();

    public Transform UIParent;

    public string ResourceDir = "UI";

    void Awake() {
        _instance = this;
        LoadAllUIObject();
    }

    /// <summary>
    /// 入棧，把頁面顯示出來
    /// </summary>
    /// <param name="UIName"></param>
    public void PushUIPanel(string UIName) {
        if(UIStack.Count >0)
        {
            UIBase old_topUI = UIStack.Peek(); //獲取棧頂元素但是不移除
            old_topUI.DoOnPausing();
        }
        UIBase new_topUI = GetUIBase(UIName);
        new_topUI.DoOnEntering();
        UIStack.Push(new_topUI);
    }

    /// <summary>
    /// 根據界面文字獲取界面控件
    /// </summary>
    /// <param name="UIName"></param>
    /// <returns></returns>
    private UIBase GetUIBase(string UIName) {
        //如果有面板,取出來即可
        foreach(var name in currentUIDict.Keys)
        {
            if (name == UIName) {
                UIBase u = currentUIDict[UIName];
                return u;
            }
        }

        //如果沒有，就先得到面板的prefab
        GameObject UIPrefab = UIObjectDict[UIName];
        GameObject UIObject = GameObject.Instantiate<GameObject>(UIPrefab);
        UIObject.name = UIName;
        //創建面板
        UIObject.transform.SetParent(UIParent, false);
        UIBase uibase = UIObject.GetComponent<UIBase>();
        currentUIDict.Add(UIName, uibase);
        return uibase;
    }

    /// <summary>
    /// 出棧 把界面隱藏
    /// </summary>
    public void PopUIPanel()
    {
        if (UIStack.Count == 0)
            return;

        UIBase old_topUI = UIStack.Pop();
        old_topUI.DoOnExiting();

        if(UIStack.Count > 0)
        {
            UIBase new_topUI = UIStack.Peek();
            new_topUI.DoOnResuming();
        }

    }

    /// <summary>
    /// 加載所有的界面預製體
    /// </summary>
    private void LoadAllUIObject()
    {
        string path = Application.dataPath + "/Resources/" + ResourceDir;
        DirectoryInfo folder = new DirectoryInfo(path);
        foreach (FileInfo file in folder.GetFiles("*.prefab")) {
            int index = file.Name.LastIndexOf('.');
            string UIName = file.Name.Substring(0, index);
            string UIPath = ResourceDir + "/" + UIName;
            GameObject UIObject = Resources.Load<GameObject>(UIPath);
            UIObjectDict.Add(UIName, UIObject);
        }
    }

}
