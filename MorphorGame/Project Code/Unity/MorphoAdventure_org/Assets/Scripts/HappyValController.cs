﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class HappyValController : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    /// <summary>
    /// 檢測開心值變化，調整難度
    /// </summary>
    void Update() {
        //一直監測開心值的變化
        if (beginToObserve) {
            if (happyVal > 80)
            {
                //提升一個難度
                Call2AdjustDifficulty(-5);
            }
            else if(happyVal < 40){
                //降低一個難度
                Call2AdjustDifficulty(5);
            }
        }
        

    }

    /// <summary>
    /// 開放接口做靜態訪問
    /// </summary>
    private static HappyValController instance;
    public static HappyValController Instance
    {
        get;
        set;
    }

    float happyVal = 60;
    bool beginToObserve = false;

    /// <summary>
    /// 計算上一次遊戲的高興指數(如果是重玩，則計算重玩關卡的開心值)
    /// </summary>
    /// <returns></returns>
    public float CalLastHappyVal() {
        ArrayList playList = new ArrayList();
        DataTable playRecord = GameManager.Instance.player.PlayRecord;
        ArrayList gameIds =Common.getGameIds(GameManager.Instance.stage);
        happyVal = 60;

        if (gameIds.Count == 0)
        {
            if (GameManager.Instance.stage > 1)
            {
                gameIds.Clear();
                gameIds =Common.getGameIds(GameManager.Instance.stage - 1);
            }
            else
            {
                return happyVal;
            }

        }

        foreach (int i in gameIds)
        {
           int minId = 9999;
           float thisHappyVal = -1; 
           foreach (DataRow row in playRecord.Rows)
           {
               if (int.Parse(row["play_id"].ToString()) < minId)
               {
                   minId = int.Parse(row["play_id"].ToString());
                    thisHappyVal = float.Parse(row["play_happyVal"].ToString());
               }
           }
           playList.Add(new ArrayList() { minId, thisHappyVal });
          }


        //計算開心值
        float current = 0;
        foreach (ArrayList ar in playList) {
            current += float.Parse(ar[1].ToString());
        }

        beginToObserve = true;

        return current/playList.Count;
    }

    /// <summary>
    /// 調用難度調整
    /// </summary>
    public void Call2AdjustDifficulty(float adjustVal) {
        happyVal +=adjustVal;
        BroadcastMessage("AdjustDifficultyByHappyVal", adjustVal,SendMessageOptions.RequireReceiver);
    }

    /// <summary>
    /// 調整高興值
    /// </summary>
    /// <returns></returns>
    public float HappyValChange(int score) {
        return happyVal+score;
    }



}
